export myhome=$HOME
if [[ $HOME == /c/* ]]
then
        myhome="c:/"
        myhome+=${HOME:3}
fi

mvn versions:set -DremoveSnapshot -DgenerateBackupPoms=false
mvn clean deploy "-Drepo_url=file://$myhome/git/jmare-maven-repo/repository/"
if [[ $? -ne 0 ]] ; then
    exit 1
fi
mvn versions:set -DnextSnapshot -DgenerateBackupPoms=false
git add --all
git commit -m "development version set"
git push

cd $myhome/git/jmare-maven-repo/repository/
git add --all
git commit -m "new libraries"
git push
