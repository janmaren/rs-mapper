package cz.jmare.mappergen.type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import cz.jmare.mappergen.RsMapperGen;
import cz.jmare.mappergen.model.Column;
import cz.jmare.mappergen.model.Key;
import cz.jmare.mappergen.model.Table;
import cz.jmare.rsmapper.dbprofile.PostgresDBProfile;
import cz.jmare.rsmapper.rs.CollectionFieldMapper;
import cz.jmare.rsmapper.rs.CollectionMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.util.RSToMap;

public class PostgresqlDbType implements DbType {
    private static final String SELECT_TABLES_COLUMNS = "SELECT * FROM information_schema.columns WHERE table_schema = :schema order by table_name, ordinal_position";

    private static final String SELECT_PRIMARY_KEYS = "select KCU.table_schema, KCU.table_name, KCU.column_name, TC.constraint_type "
            + "from information_schema.table_constraints AS TC "
            + "inner join information_schema.key_column_usage AS KCU "
            + "on KCU.constraint_catalog = TC.constraint_catalog " + "and KCU.constraint_schema = TC.constraint_schema "
            + "and KCU.table_name = TC.table_name "
            + "and KCU.constraint_name = TC.constraint_name where kcu.table_schema=:schema order by kcu.table_name";

    @Override
    public String getDriverClassName() {
        return "org.postgresql.Driver";
    }

    @Override
    public List<Table> getTables(Connection connection, String catalog, String schema, boolean force) {
        if (schema == null) {
            RsMapperGen.exitWithMessage("Unable to determine schema for which to generate");
        }
//        SimpleHandle handle = new SimpleHandle(connection);
//        Relation<PGTableEntity> relation = new Relation<>(PGTableEntity.class);
//        List<PGTableEntity> pgTableEntities = relation.loadEntitiesByParameters(handle, Map.of("tableSchema", schema));
//
//        Set<List<PGTableEntity>> groups = Colutil.getGroups(pgTableEntities, new Comparator<PGTableEntity>() {
//            @Override
//            public int compare(PGTableEntity o1, PGTableEntity o2) {
//                String s1 = o1.tableCatalog + ":" + o1.tableSchema + o1.tableName;
//                String s2 = o2.tableCatalog + ":" + o2.tableSchema + o2.tableName;
//                return s1.compareTo(s2);
//            }
//        });
        
        List<Map<String, Object>> list;
        try {
            String sql = SELECT_TABLES_COLUMNS.replace(":schema", "'" + schema + "'");
            //sql = sql.replace(":catalog", "'" + (catalog != null ? catalog : "postgres") + "'");
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            ResultSet rs = prepareStatement.executeQuery();
            list = RSToMap.convertAll(rs, new PostgresDBProfile());
        } catch (SQLException e) {
            throw new RuntimeException("Unable to fetch tables", e);
        }
        BiConsumer<Map<String, Object>, Object> customMapper = createCustomMapper(force);
        CollectionMapper collectionMapper = new CollectionMapper("table_name", new SimpleRemapper(Table.class), new CollectionFieldMapper(
                "columns", "column_name", new SimpleRemapper(Column.class).withCustomMapper(customMapper)));
        List<Table> tables = collectionMapper.remapToList(list, Table.class);
        return tables;
    }

    @Override
    public List<Table> getKeys(Connection connection, String catalog, String schema) {
        if (schema == null) {
            RsMapperGen.exitWithMessage("Unable to determine schema for which to generate");
        }
        List<Map<String, Object>> list;
        try {
            String sql = SELECT_PRIMARY_KEYS.replace(":schema", "'" + schema + "'");
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            ResultSet rs = prepareStatement.executeQuery();
            list = RSToMap.convertAll(rs, new PostgresDBProfile());
        } catch (SQLException e) {
            throw new RuntimeException("Unable to fetch tables", e);
        }
        
        CollectionMapper collectionMapper = new CollectionMapper("table_name", new SimpleRemapper(Table.class), new CollectionFieldMapper(
                "keys", new Function<>() {
                    @Override
                    public Object apply(Map<String, Object> t) {
                        return t.get("constraint_type") + ":" + t.get("column_name");
                    }}, new SimpleRemapper(Key.class)));
        
        List<Table> tables = collectionMapper.remapToList(list, Table.class);
        return tables;
    }
    
    protected BiConsumer<Map<String, Object>, Object> createCustomMapper(boolean force) {
        BiConsumer<Map<String, Object>, Object> biConsumer = new BiConsumer<Map<String, Object>, Object>() {
            @Override
            public void accept(Map<String, Object> map, Object u) {
                Column column = (Column) u;
                String resultJavaType = null;
                String udtType = column.getUdtType();
                if ("int4".equals(udtType)) {
                    resultJavaType = "Integer";
                } else if ("int2".equals(udtType)) {
                    resultJavaType = "Short";
                } else if ("int8".equals(udtType)) {
                    resultJavaType = "Long";
                } else if ("varchar".equals(udtType) || "jsonb".equals(udtType) || "json".equals(udtType)) {
                    resultJavaType = "String";
                } else if ("bool".equals(udtType)) {
                    resultJavaType = "Boolean";
                } else if ("timestamp".equals(udtType)) {
                    resultJavaType = "LocalDateTime";
                } else if ("bytea".equals(udtType)) {
                    resultJavaType = "byte[]";
                } else if ("timestamptz".equals(udtType)) {
                    resultJavaType = "OffsetDateTime"; // OffsetDateTime but there is bug in 42.2.8
                } else if ("date".equals(udtType)) {
                    resultJavaType = "LocalDate";
                } else if ("interval".equals(udtType)) {
                    resultJavaType = "Long";
                } else if ("timetz".equals(udtType)) {
                    resultJavaType = "OffsetTime";  // OffsetTime but not supported in 42.2.8
                } else if ("time".equals(udtType)) {
                    resultJavaType = "LocalTime";
                } else if ("uuid".equals(udtType)) {
                    resultJavaType = "UUID";
                } else {
                    if (force) {
                        resultJavaType = udtType;
                    } else {
                        throw new RuntimeException("Unsupported db type '" + udtType + "' which is in table '" + map.get("table_name") + "' and column '" + map.get("column_name") + "'");
                    }
                }
                column.setResultJavaType(resultJavaType);
            }
        };
        return biConsumer;
    }

}
