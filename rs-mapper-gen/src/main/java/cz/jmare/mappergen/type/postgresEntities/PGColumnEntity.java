package cz.jmare.mappergen.type.postgresEntities;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.Table;

@Table(name = "information_schema.columns")
public class PGColumnEntity {
    @Id
    public String columnName;
    
    public String tableName;

    @Override
    public String toString() {
        return "PGColumnEntity [columnName=" + columnName + "]";
    }
}
