package cz.jmare.mappergen.type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import cz.jmare.mappergen.RsMapperGen;
import cz.jmare.mappergen.model.Column;
import cz.jmare.mappergen.model.Key;
import cz.jmare.mappergen.model.Table;
import cz.jmare.rsmapper.dbprofile.H2DBProfile;
import cz.jmare.rsmapper.rs.CollectionFieldMapper;
import cz.jmare.rsmapper.rs.CollectionMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.util.RSToMap;

public class H2DbType implements DbType {
    private static final String SELECT_TABLES_COLUMNS = "SELECT *, type_name as udt_name FROM information_schema.columns WHERE table_schema = :schema order by table_name, ordinal_position";

    private static final String SELECT_PRIMARY_KEYS = "select KCU.table_schema, KCU.table_name, KCU.column_name, TC.constraint_type "
            + "from information_schema.table_constraints AS TC "
            + "inner join information_schema.key_column_usage AS KCU "
            + "on KCU.constraint_catalog = TC.constraint_catalog " + "and KCU.constraint_schema = TC.constraint_schema "
            + "and KCU.table_name = TC.table_name "
            + "and KCU.constraint_name = TC.constraint_name where kcu.table_schema=:schema order by kcu.table_name";

    @Override
    public String getDriverClassName() {
        return "org.h2.Driver";
    }

    @Override
    public List<Table> getTables(Connection connection, String catalog, String schema, boolean force) {
        if (schema == null) {
            RsMapperGen.exitWithMessage("Unable to determine schema for which to generate");
        }
        List<Map<String, Object>> list;
        try {
            String sql = SELECT_TABLES_COLUMNS.replace(":schema", "'" + schema.toUpperCase() + "'");
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            ResultSet rs = prepareStatement.executeQuery();
            list = RSToMap.convertAll(rs, new H2DBProfile());
        } catch (SQLException e) {
            throw new RuntimeException("Unable to fetch tables", e);
        }
        BiConsumer<Map<String, Object>, Object> customMapper = createCustomMapper(force);
        CollectionMapper collectionMapper = new CollectionMapper("table_name", new SimpleRemapper(Table.class), new CollectionFieldMapper(
                "columns", "column_name", new SimpleRemapper(Column.class).withCustomMapper(customMapper)));
        List<Table> tables = collectionMapper.remapToList(list, Table.class);
        return tables;
    }

    @Override
    public List<Table> getKeys(Connection connection, String catalog, String schema) {
        if (schema == null) {
            RsMapperGen.exitWithMessage("Unable to determine schema for which to generate");
        }
        List<Map<String, Object>> list;
        try {
            String sql = SELECT_PRIMARY_KEYS.replace(":schema", "'" + schema.toUpperCase() + "'");
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            ResultSet rs = prepareStatement.executeQuery();
            list = RSToMap.convertAll(rs, new H2DBProfile());
        } catch (SQLException e) {
            throw new RuntimeException("Unable to fetch tables", e);
        }
        
        CollectionMapper collectionMapper = new CollectionMapper("table_name", new SimpleRemapper(Table.class), new CollectionFieldMapper(
                "keys", new Function<>() {
                    @Override
                    public Object apply(Map<String, Object> t) {
                        return t.get("constraint_type") + ":" + t.get("column_name");
                    }}, new SimpleRemapper(Key.class)));
        
        List<Table> tables = collectionMapper.remapToList(list, Table.class);
        return tables;
    }
    
    protected BiConsumer<Map<String, Object>, Object> createCustomMapper(boolean force) {
        BiConsumer<Map<String, Object>, Object> biConsumer = new BiConsumer<Map<String, Object>, Object>() {
            @Override
            public void accept(Map<String, Object> map, Object u) {
                Column column = (Column) u;
                String resultJavaType = null;
                String udtType = column.getUdtType();
                if ("DATE".equals(udtType)) {
                    resultJavaType = "LocalDate";
                } else
                if ("CHAR".equals(udtType)) {
                    resultJavaType = "String";
                } else
                if ("ENUM".equals(udtType)) {
                    resultJavaType = "Integer";
                } else
                if ("DECIMAL".equals(udtType)) {
                    resultJavaType = "BigDecimal";
                } else
                if ("UUID".equals(udtType)) {
                    resultJavaType = "UUID";
                } else                    
                if ("INTEGER".equals(udtType)) {
                    resultJavaType = "Integer";
                } else if ("INT2".equals(udtType)) {
                    resultJavaType = "Short";
                } else if ("INT8".equals(udtType)) {
                    resultJavaType = "Long";
                } else if ("VARCHAR".equals(udtType)) {
                    resultJavaType = "String";
                } else if ("BOOL".equals(udtType) || "BOOLEAN".equals(udtType) || "BIT".equals(udtType)) {
                    resultJavaType = "Boolean";
                } else if ("TIMESTAMP".equals(udtType)) {
                    resultJavaType = "LocalDateTime";
                } else if ("BLOB".equals(udtType)) {
                    resultJavaType = "byte[]";
                } else if ("TIME".equals(udtType)) {
                    resultJavaType = "LocalTime";
                } else {
                    if (force) {
                        resultJavaType = udtType;
                    } else {
                        throw new RuntimeException("Unsupported db type '" + udtType + "' which is in table '" + map.get("table_name") + "' and column '" + map.get("column_name") + "'");
                    }
                }
                column.setResultJavaType(resultJavaType);
            }
        };
        return biConsumer;
    }

}
