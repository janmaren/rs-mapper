package cz.jmare.mappergen.type.postgresEntities;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.Table;

@Table(name = "information_schema.columns")
public class PGTableEntity {
    @Id
    public String tableCatalog;
    
    @Id
    public String tableSchema;
    
    @Id
    public String tableName;

    @Id
    public String columnName;
    
    @cz.jmare.rsmapper.Column(name = "data_type")
    private String type;
    
    @cz.jmare.rsmapper.Column(name = "udt_name")
    private String udtType;
    
    @cz.jmare.rsmapper.Column(name = "numeric_precision")
    private Integer numbericPrecision;
    
    @cz.jmare.rsmapper.Column(name = "numeric_scale")
    private Integer numericScale;
    
    @cz.jmare.rsmapper.Column(name = "numeric_precision_radix")
    private Integer numericPrecisionRadix;
    
    private transient String resultJavaType;
    
    private String columnDefault;
    
    @Override
    public String toString() {
        return "PGTableEntity [tableName=" + tableName + ", columnName=" + columnName + "]";
    }
}
