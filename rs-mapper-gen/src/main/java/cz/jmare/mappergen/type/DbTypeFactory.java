package cz.jmare.mappergen.type;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbTypeFactory {
    private final static Pattern DB_TYPE_PATTERN = Pattern.compile("^jdbc:(?<type>[^:]+):");

    public static DbType createDbType(String url) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Matcher matcher = DB_TYPE_PATTERN.matcher(url);
        if (matcher.find()) {
            String type = matcher.group("type");
            String canonicalName = DbTypeFactory.class.getPackageName() + "." + type.substring(0, 1).toUpperCase() + type.substring(1) + "DbType";
            Class<?> c = Class.forName(canonicalName);
            try {
                return (DbType) c.getDeclaredConstructor().newInstance();
            } catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                    | SecurityException e) {
                throw new InstantiationException("Unable to create instance of " + c);
            }
        } else {
            throw new InstantiationException("Unable to resolve class name for " + url);
        }
    }
}
