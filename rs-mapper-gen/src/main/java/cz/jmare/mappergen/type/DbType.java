package cz.jmare.mappergen.type;

import java.sql.Connection;
import java.util.List;

import cz.jmare.mappergen.model.Table;

public interface DbType {
    String getDriverClassName();
    
    List<Table> getTables(Connection connection, String catalog, String schema, boolean force);
    
    List<Table> getKeys(Connection connection, String catalog, String schema);
}
