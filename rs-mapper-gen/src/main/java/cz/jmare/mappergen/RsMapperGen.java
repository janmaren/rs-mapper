package cz.jmare.mappergen;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.mappergen.model.Column;
import cz.jmare.mappergen.model.Key;
import cz.jmare.mappergen.model.Table;
import cz.jmare.mappergen.model.util.ReservedJavaWords;
import cz.jmare.mappergen.type.DbType;
import cz.jmare.mappergen.type.DbTypeFactory;


public class RsMapperGen {
    private final static Map<String, String> typeToCanonical = new HashMap<>();
    static {
        typeToCanonical.put("LocalDateTime", "java.time.LocalDateTime");
        typeToCanonical.put("LocalDate", "java.time.LocalDate");
        typeToCanonical.put("LocalTime", "java.time.LocalTime");
        typeToCanonical.put("Time", "java.sql.Time");
        typeToCanonical.put("Date", "java.util.Date");
        typeToCanonical.put("Timestamp", "java.sql.Timestamp");
        typeToCanonical.put("BigDecimal", "java.math.BigDecimal");
        typeToCanonical.put("UUID", "java.util.UUID");
        typeToCanonical.put("Enum<?>", "java.lang.Enum");
        typeToCanonical.put("OffsetTime", "java.time.OffsetTime");
        typeToCanonical.put("OffsetDateTime", "java.time.OffsetDateTime");
        typeToCanonical.put("UUID", "java.util.UUID");
    }
    
    private String buildImports(Set<String> importsSet) {
        StringBuilder sbImports = new StringBuilder();
        for (Entry<String, String> entry : typeToCanonical.entrySet()) {
            String key = entry.getKey();
            if (importsSet.contains(key)) {
                String value = entry.getValue();
                sbImports.append("import " + value + ";").append("\n");
            }
        }
        if (importsSet.contains("IDENTITY")) {
            sbImports.append("import static " + cz.jmare.rsmapper.IdType.class.getCanonicalName() + ".IDENTITY;").append("\n");
        }
        if (importsSet.contains("PASSED")) {
            sbImports.append("import static " + cz.jmare.rsmapper.IdType.class.getCanonicalName() + ".PASSED;").append("\n");
        }  
        if (importsSet.contains("Column")) {
            sbImports.append("import " + cz.jmare.rsmapper.Column.class.getCanonicalName() + ";").append("\n");
        }
        if (importsSet.contains("Id")) {
            sbImports.append("import " + cz.jmare.rsmapper.Id.class.getCanonicalName() + ";").append("\n");
        }
        if (importsSet.contains("IdType")) {
            sbImports.append("import " + cz.jmare.rsmapper.IdType.class.getCanonicalName() + ";").append("\n");
        }
        if (importsSet.contains("Getter")) {
            sbImports.append("import lombok.Getter;").append("\n");
        }
        if (importsSet.contains("Setter")) {
            sbImports.append("import lombok.Setter;").append("\n");
        }
        if (importsSet.contains("ToString")) {
            sbImports.append("import lombok.ToString;").append("\n");
        }

        if (sbImports.length() > 0) {
            sbImports.append("\n");
        }
        String imports = sbImports.toString();
        return imports;
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(RsMapperGen.class);

    private String url;

    private String username;
    
    private String password;

    private String schema;
    
    private String catalog;

    private String outputSrcDir;

    private String outputSuperPackage;

    private String outputSubPackage;
    
    private boolean genName;

    private boolean genLombok;

    private boolean clear;

    private boolean force;
    
    private Options getOptions() {
        Options options = new Options();

        options.addOption("url", true, "URL address to database. It usually starts with 'jdbc:'");
        options.addOption("password", true, "Password. Also environment variable DB_PASSWORD can be used instead");
        options.addOption("username", true, "Username");
        options.addOption("catalog", true, "Classes for tables in this catalog will be generated");
        options.addOption("schema", true, "Classes for tables in this schema will be generated");
        options.addOption("outputSrcDir", true, "Output base directory which contains root java packages");
        options.addOption("outputSuperPackage", true, "Output directory for output classes");
        options.addOption("outputSubPackage", true, "Optional. Output directory for output classes inherited from classes generated to outputSuperPackage");
        options.addOption("genName", false, "When used also the @Column annotation with name field will be generated");
        options.addOption("genLombok", false, "When used the getters and setters will not be generated but rather the Lombok annotations will be used");
        options.addOption("clear", false, "When used the super classes will be removed before generation");
        options.addOption("f", false, "Force to continue when unmappable type found. Java output will contain an invalid type which must be corrected manually");

        return options;
    }

    private void printHelp(Options options) {
        HelpFormatter hf = new HelpFormatter();
        hf.setWidth(160);
        String header = String.format("RsMapperGen Tool");
        String usage = String.format("<APP> [<OPTION>]%n");
        String footer = String.format("Example: java -jar rsmapper-gen-tool -url jdbc:postgresql://10.0.25.118:5432/postgres -username postgres -password secret -schema public -outputSrcDir /home/john/prj/src -outputSuperPackage com.pack.super -outputSubPackage cz.pack.sub");
        hf.printHelp(usage, header, options, footer);
    }

    public void init(String[] args) {
        Options options = getOptions();
        if (args.length == 0) {
            printHelp(options);
            System.exit(-1);
        }

        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            printHelp(options);
            System.exit(-1);
        }

        if (cmd.hasOption("url")) {
            url = cmd.getOptionValue("url");
        } else {
            exitWithMessage("url parameter must be populated");
        }

        if (cmd.hasOption("username")) {
            username = cmd.getOptionValue("username");
        }

        String envPassword = System.getenv("DB_PASSWORD");
        if (envPassword != null) {
            password = envPassword;
        }
        
        if (cmd.hasOption("password")) {
            password = cmd.getOptionValue("password");
        }

        if (cmd.hasOption("schema")) {
            schema = cmd.getOptionValue("schema");
        } 
        
        if (cmd.hasOption("catalog")) {
            catalog = cmd.getOptionValue("catalog");
        } 
        
        if (cmd.hasOption("outputSrcDir")) {
            outputSrcDir = cmd.getOptionValue("outputSrcDir");
        } else {
            exitWithMessage("outputSrcDir parameter must be populated");
        }
        
        if (cmd.hasOption("outputSuperPackage")) {
            outputSuperPackage = cmd.getOptionValue("outputSuperPackage");
        } else {
            exitWithMessage("outputSuperPackage parameter must be populated");
        }    
        
        if (cmd.hasOption("outputSubPackage")) {
            outputSubPackage = cmd.getOptionValue("outputSubPackage");
        } 
        
        if (cmd.hasOption("genName")) {
            genName = true;
        }  

        if (cmd.hasOption("genLombok")) {
            genLombok = true;
        }
        
        if (cmd.hasOption("clear")) {
            clear = true;
        }   
        
        if (cmd.hasOption("f")) {
            force = true;
        }   
    }

    public static void exitWithMessage(String message) {
        LOGGER.error(message);
        System.exit(-1);
    }

    public static void exitWithMessage(String message, Exception e) {
        LOGGER.error(message, e);
        System.exit(-1);
    }

    public void run() throws SQLException {
        DbType dbType = null;
        try {
            dbType = DbTypeFactory.createDbType(url);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e3) {
            exitWithMessage("Unable to find db type for " + url, e3);
        }

        String driverClassName = dbType.getDriverClassName();
        if (driverClassName == null) {
            DriverManager.getDriver(url);
        } else {
            try {
                Class.forName(driverClassName);
            } catch (ClassNotFoundException e2) {
                LOGGER.error("Unable to find driver " + driverClassName + " on classpath", e2);
            }
        }

        List<Table> tables = null;
        List<Table> tablesWithKeys = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            tables = dbType.getTables(connection, catalog, schema, force);
            tablesWithKeys = dbType.getKeys(connection, catalog, schema);
        } catch (SQLException e) {
            LOGGER.error("Unable to establish sql connection", e);
            System.exit(-1);
        }
        for (Table table : tables) {
            Optional<Table> findFirst = tablesWithKeys.stream().filter(t -> t.getName().equals(table.getName())).findFirst();
            if (findFirst.isPresent()) {
                table.setKeys(findFirst.get().getKeys());
            }
        }

        generate(tables);
    }

    private String getSuperClassSuffix() {
        if (outputSubPackage != null) {
            return "Base";
        }
        return "";
    }
    
    private void generate(List<Table> tables) {
        Path outputSrcDirPath = new File(outputSrcDir).toPath();
        if (!Files.isDirectory(outputSrcDirPath)) {
            LOGGER.error("Directory " + outputSrcDir + " doesn't exist");
            System.exit(-1);
        }
        
        Path superDirPath = null;
        try {
            superDirPath = outputSrcDirPath.resolve(packageToDir(outputSuperPackage));
            Files.createDirectories(superDirPath);
        } catch (IOException e) {
            LOGGER.error("Unable to create directory " + superDirPath);
            System.exit(-1);
        }
        
        if (clear) {
            try {
                Files.newDirectoryStream(superDirPath).forEach(file -> {
                    try {
                        Files.delete(file);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        Path subDirPath = null;
        if (outputSubPackage != null) {
            try {
                subDirPath = outputSrcDirPath.resolve(packageToDir(outputSubPackage));
                Files.createDirectories(subDirPath);
            } catch (IOException e) {
                LOGGER.error("Unable to create directory " + subDirPath);
                System.exit(-1);
            }
        }
        
        for (Table table : tables) {
            Path resultSuperPath = null;
            try {
                resultSuperPath = superDirPath.resolve(toCamelStartWithUpper(table.getName()) + getSuperClassSuffix() + ".java");
                Files.write(resultSuperPath, createSuperTextFile(table));
            } catch (IOException e) {
                LOGGER.error("Unable to create file " + resultSuperPath, e);
                System.exit(-1);
            }
        
            if (outputSubPackage != null) {
                Path resultSubPath = null;
                try {
                    resultSubPath = subDirPath.resolve(toCamelStartWithUpper(table.getName()) + ".java");
                    if (!Files.isRegularFile(resultSubPath)) {
                        Files.write(resultSubPath, createSubTextFile(table));
                    }
                } catch (IOException e) {
                    LOGGER.error("Unable to create file " + resultSubPath, e);
                    System.exit(-1);
                }
            }
        }
    }
    
    private byte[] createSuperTextFile(Table table) {
        String modifier = outputSubPackage != null ? "protected" : "private";
        Set<String> importsSet = new TreeSet<>();
        
        StringBuilder sb = new StringBuilder();
        if (genLombok) {
            importsSet.add("Getter");
            importsSet.add("Setter");
            importsSet.add("ToString");
            sb.append("@Getter @Setter @ToString\n");
        }
        sb.append("public class ").append(toCamelStartWithUpper(table.getName()) + getSuperClassSuffix()).append(" {\n");
        List<Column> columns = table.getColumns();
        List<Key> primKeys = table.getKeys().stream().filter(
                k -> k.getConstraintType()
                .equals("PRIMARY KEY"))
                .collect(Collectors.toList());
        Set<String> primKeysNames = primKeys.stream().map(k -> k.getName()).collect(Collectors.toSet());
        Collections.sort(columns, new Comparator<Column>() {
            @Override
            public int compare(Column o1, Column o2) {
                if (primKeysNames.contains(o1.getName())) {
                    return -1;
                }
                if (primKeysNames.contains(o2.getName())) {
                    return 1;
                }
                return o1.getName().compareTo(o2.getName());
            }
        });
        StringBuilder gettersSetters = new StringBuilder();
        
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            importsSet.add(column.getResultJavaType());
            if (i > 0) {
                sb.append("\n");
            }
            String genFieldName = ReservedJavaWords.toSafeJavaIdentifier(toCamel(column.getLowerName()));
            if (genName) {
                StringBuilder anno = new StringBuilder("    @Column(");
                if (genName) {
                    anno.append("name = \"").append(column.getLowerName()).append("\"");
                }
                anno.append(")\n");
                importsSet.add("Column");
                sb.append(anno);
            }
            if (primKeysNames.contains(column.getName())) {
                StringBuilder anno = new StringBuilder("    @Id(");
                boolean isIdentity = column.getColumnDefault() != null && (column.getColumnDefault().startsWith("nextval") || column.getColumnDefault().contains("NEXT VALUE"));
                if (isIdentity) {
                    importsSet.add("IDENTITY");
                } else {
                    importsSet.add("PASSED");
                }
                anno.append("type = ").append(isIdentity ? "IDENTITY" : "PASSED").append("");
                anno.append(")\n");
                importsSet.add("Id");
                sb.append(anno);
            }
            sb.append("    ").append(modifier).append(" ").append(column.getResultJavaType()).append(" ").append(genFieldName).append(";\n");
            
            if (!genLombok) {
                gettersSetters.append("    ").append("public void set").append(toCamelStartWithUpper(column.getName())).append("(").append(column.getResultJavaType()).append( " value) {\n");
                gettersSetters.append("        ").append("this.").append(genFieldName).append(" = value;\n");
                gettersSetters.append("    }\n\n");
                
                gettersSetters.append("    ").append("public ").append(column.getResultJavaType()).append(" get").append(toCamelStartWithUpper(column.getName())).append("() {\n");
                gettersSetters.append("        ").append("return this.").append(genFieldName).append(";\n");
                gettersSetters.append("    }\n\n");
            }
        }
        
        if (gettersSetters.length() > 0) {
            sb.append("\n").append(gettersSetters);
        }
        
        sb.append("}\n");
        
        String body = sb.toString();
        
        String imports = buildImports(importsSet);
        
        String resultString = "package " + outputSuperPackage + ";\n\n" + imports + body;
        
        return resultString.getBytes();
    }


    
    private byte[] createSubTextFile(Table table) {
        StringBuilder sb = new StringBuilder();
        String genClassName = toCamelStartWithUpper(table.getName());
        Set<String> importsSet = new TreeSet<>();
        if (genLombok) {
            importsSet.add("Getter");
            importsSet.add("Setter");
            importsSet.add("ToString");
            sb.append("@Getter @Setter @ToString(callSuper = true)\n");
        }        
        sb.append("public class ").append(genClassName).append(" extends ").append(genClassName).append(getSuperClassSuffix()).append(" {\n");
        
        sb.append("}\n");
        
        String body = sb.toString();
        
        String imports = "import " + outputSuperPackage + "." + genClassName + getSuperClassSuffix() + ";\n" + buildImports(importsSet) + (importsSet.isEmpty() ? "\n" : "");
        
        String resultString = "package " + outputSubPackage + ";\n\n" + imports + body;
        
        return resultString.getBytes();
    }
    

    private String packageToDir(String pakage) {
        return pakage.replace('.', File.separatorChar);
    }
    
    public static String toCamel(String str) {
        while (str.startsWith("_")) {
            str = str.substring(1);
        }        String[] split = str.split("_");
        StringBuilder sb = new StringBuilder(split[0]);
        for (int i = 1; i < split.length; i++) {
            String str2 = split[i];
            str2 = str2.substring(0, 1).toUpperCase() + str2.substring(1).toLowerCase();
            sb.append(str2);
        }
        return sb.toString();
    } 

    public static String toCamelStartWithUpper(String str) {
        while (str.startsWith("_")) {
            str = str.substring(1);
        }
        String[] split = str.split("_");
        if (split.length == 0) {
            throw new RuntimeException("Unable to split " + str);
        }
        if (split[0].length() == 0) {
            throw new RuntimeException("Unable to split " + str + " because empty string");
        }
        StringBuilder sb = new StringBuilder(split[0].substring(0, 1).toUpperCase() + split[0].substring(1).toLowerCase());
        for (int i = 1; i < split.length; i++) {
            String str2 = split[i];
            str2 = str2.substring(0, 1).toUpperCase() + str2.substring(1).toLowerCase();
            sb.append(str2);
        }
        return sb.toString();
    } 
    
    public static void main(String[] args) throws SQLException {
        RsMapperGen rsMapperGen = new RsMapperGen();
        rsMapperGen.init(args);
        rsMapperGen.run();
        System.out.println("OK");
    }
}
