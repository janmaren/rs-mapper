package cz.jmare.mappergen.model;


public class Column {
    @cz.jmare.rsmapper.Column(name = "column_name")
    private String name;
    
    @cz.jmare.rsmapper.Column(name = "data_type")
    private String type;
    
    @cz.jmare.rsmapper.Column(name = "udt_name")
    private String udtType;
    
    @cz.jmare.rsmapper.Column(name = "numeric_precision")
    private Integer numbericPrecision;
    
    @cz.jmare.rsmapper.Column(name = "numeric_scale")
    private Integer numericScale;
    
    @cz.jmare.rsmapper.Column(name = "numeric_precision_radix")
    private Integer numericPrecisionRadix;
    
    private transient String resultJavaType;
    
    private String columnDefault;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Column [name=" + name + ", type=" + type + "]";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNumbericPrecision() {
        return numbericPrecision;
    }

    public void setNumbericPrecision(Integer numbericPrecision) {
        this.numbericPrecision = numbericPrecision;
    }

    public Integer getNumericScale() {
        return numericScale;
    }

    public void setNumericScale(Integer numericScale) {
        this.numericScale = numericScale;
    }

    public Integer getNumericPrecisionRadix() {
        return numericPrecisionRadix;
    }

    public void setNumericPrecisionRadix(Integer numericPrecisionRadix) {
        this.numericPrecisionRadix = numericPrecisionRadix;
    }

    public String getResultJavaType() {
        return resultJavaType;
    }

    public void setResultJavaType(String resultJavaType) {
        this.resultJavaType = resultJavaType;
    }

    public String getUdtType() {
        return udtType;
    }

    public void setUdtType(String udtType) {
        this.udtType = udtType;
    }

    public String getColumnDefault() {
        return columnDefault;
    }

    public void setColumnDefault(String columnDefault) {
        this.columnDefault = columnDefault;
    }
    
    public String getLowerName() {
        return this.name.toLowerCase();
    }
}
