package cz.jmare.mappergen.model;

import cz.jmare.rsmapper.Column;

public class Key {
    @Column(name = "column_name")
    private String name;
    
    @Column(name = "constraint_type")
    private String constraintType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConstraintType() {
        return constraintType;
    }

    public void setConstraintType(String constraintType) {
        this.constraintType = constraintType;
    }
}
