package cz.jmare.mappergen.model.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ReservedJavaWords {
    public static final String[] RESERVED_WORDS = {"abstract", "assert", "boolean", "break", "byte", "case", "catch", "char", "class", "const", "default", "do", "double", "else", "enum", "extends", "false", "final", "finally", "float", "for", "goto", "if", "implements", "import", "instanceof", "int", "interface", "long", "native", "new", "null", "package", "private", "protected", "public", "return", "short", "static", "strictfp", "super", "switch", "synchronized", "this", "throw", "throws", "transient", "true", "try", "void", "volatile", "while", "continue"};
    
    public static final Set<String> RESERVED_WORDS_SET = new HashSet<>(Arrays.asList(RESERVED_WORDS));
    
    public static String toSafeJavaIdentifier(String str) {
        if (RESERVED_WORDS_SET.contains(str)) {
            return str + "$";
        }
        return str;
    }
}
