package cz.jmare.mappergen.model;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.rsmapper.Column;

public class Table {
    @Column(name = "table_name")
    private String name;
    
    private List<cz.jmare.mappergen.model.Column> columns;
    
    private List<Key> keys = new ArrayList<Key>();

    @Override
    public String toString() {
        return "Table [name=" + name + ", columns=" + columns + "]";
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public List<cz.jmare.mappergen.model.Column> getColumns() {
        return columns;
    }


    public void setColumns(List<cz.jmare.mappergen.model.Column> columns) {
        this.columns = columns;
    }


    public List<Key> getKeys() {
        return keys;
    }


    public void setKeys(List<Key> keys) {
        this.keys = keys;
    }
}
