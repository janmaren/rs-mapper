package cz.jmare.service;

import java.util.List;

import cz.jmare.PersonJson;

public interface PersonService {
    public List<PersonJson> getPersons();
    
    void addPerson();
}
