package cz.jmare.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.jmare.AccountJson;
import cz.jmare.model.sub.Account;
import cz.jmare.model.sub.Person;
import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.ToOnePrim;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    DataSource dataSource;

    @Autowired
    DSConf dsConf;

    private final Relation<Account> accountRelation = new Relation<>(Account.class)
            .withJoins(new ToOnePrim(Person.class, "person", "personId"));
    
    @Override
    public AccountJson createAccount(AccountJson accountJson) {
        Account account = new Account();
        accountJsonToPersistent(accountJson, account);
        accountRelation.save(dsConf, account);
        accountJson.setAccountId(account.getAccountId());
        return accountJson;
    }

    @Override
    public AccountJson getAccount(Integer accountId) {
        Account account = accountRelation.loadEntityById(dsConf, accountId);
        AccountJson accountJson = accountPersistentToJson(account);
        return accountJson;
    }

    @Override
    public void deleteAccount(Integer accountId) {
        accountRelation.deleteById(dsConf, accountId);
    }

    @Override
    public void updateAccount(AccountJson accountJson) {
        Account account = new Account();
        accountJsonToPersistent(accountJson, account);
        accountRelation.save(dsConf, account);
    }
    
    private static AccountJson accountPersistentToJson(Account account) {
        AccountJson accountJson = new AccountJson();
        accountJson.setAccountId(account.getAccountId());
        accountJson.setName(account.getPerson().getPersonName());
        accountJson.setEmail(account.getEmail());
        accountJson.setUsername(account.getUsername());
        return accountJson;
    }
    
    private static void accountJsonToPersistent(AccountJson accountJson, Account account) {
        account.setAccountId(accountJson.getAccountId());
        account.setUsername(accountJson.getUsername());
        account.setPassword(accountJson.getPassword());
        account.setEmail(accountJson.getEmail());
        
        Person person = new Person();
        person.setPersonName(accountJson.getName());
        account.setPerson(person);
    }

    @Override
    public List<AccountJson> getAccounts(String personName) {
        List<Account> accounts;
        if (personName != null) {
            accounts = accountRelation.loadByCondition(dsConf, "join person pt on _1.person_id=pt.person_id where pt.person_name=:personName", Map.of("personName", personName));
        } else {
            accounts = accountRelation.loadEntitiesByParameters(dsConf, Map.of());
        }
        ArrayList<AccountJson> result = new ArrayList<AccountJson>();
        for (Account account : accounts) {
            result.add(accountPersistentToJson(account));
        }
        return result;
    }
}
