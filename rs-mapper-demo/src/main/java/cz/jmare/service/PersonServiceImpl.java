package cz.jmare.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.jmare.PersonJson;
import cz.jmare.model.sub.Person;
import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.Relation;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {
    @Autowired
    DataSource dataSource;

    @Autowired
    DSConf dsConf;

    private Relation<Person> relation = new Relation<>(Person.class);

    @Override
    public List<PersonJson> getPersons() {
        //List<Person> persons = relation.loadAll(dsConf);
        //List<Person> persons = relation.loadByCondition(dsConf, "join account a on _1.person_id=a.person_id where a.username=:username", Map.of("username", "jack"));
        //List<Person> persons = relation.loadByCondition(dsConf, "join account a on _1.person_id=a.person_id where a.username=:username", Map.of("username", "jack"));
        //List<Person> persons = relation.selectMany(dsConf, "");
        //List<Person> persons = relation.selectMany(dsConf, "");
        //relation.selectOne(dsConf, "where _1.person_id=:person_id", Map.of("person_id", 1));
        List<Person> persons = relation.selectMany(dsConf, "join account a on _1.person_id=a.person_id where a.username=:username", Map.of("username", "jack"));
        
        List<PersonJson> result = new ArrayList<PersonJson>();
        for (Person person : persons) {
            PersonJson person1 = new PersonJson();
            person1.setPersonId(person.getPersonId());
            person1.setPersonName(person.getPersonName());
            result.add(person1);
        }

        return result;
    }

    @Override
    public void addPerson() {
        Person person = new Person();
        person.setPersonName("name" + System.currentTimeMillis());
        relation.save(dsConf, person);
    }
}