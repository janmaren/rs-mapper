package cz.jmare.service;

import java.util.List;

import cz.jmare.AccountJson;

public interface AccountService {

    AccountJson createAccount(AccountJson accountJson);

    AccountJson getAccount(Integer id);

    void deleteAccount(Integer id);

    void updateAccount(AccountJson accountJson);

    List<AccountJson> getAccounts(String personName);
}
