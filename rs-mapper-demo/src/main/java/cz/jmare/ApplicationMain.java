package cz.jmare;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cz.jmare.rsmapper.DSConf;

@SpringBootApplication
@Configuration
public class ApplicationMain {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationMain.class, args);
	}
	
	@Bean
	public DSConf getDSConf(DataSource dataSource) {
	    return new DSConf(dataSource);
	}

}
