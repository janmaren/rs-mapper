package cz.jmare.config;

import java.net.MalformedURLException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.util.dbpopulator.DBPopulator;

@Configuration
public class DBInitialize {
    @Autowired
    private DSConf dsConf;
    
    
    @PostConstruct
    public void init() throws MalformedURLException {
        try (Handle handle = dsConf.createHandle()) {
            try {
            DBPopulator.populate(handle.getConnection(), getClass().getClassLoader().getResource("database.sql"));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }
}
