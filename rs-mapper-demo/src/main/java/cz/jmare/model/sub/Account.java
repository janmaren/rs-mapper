package cz.jmare.model.sub;

import cz.jmare.model.base.AccountBase;

public class Account extends AccountBase {
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
