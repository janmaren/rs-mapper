package cz.jmare.model.base;

import java.time.LocalDateTime;
import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class AccountBase {
    @Column(name = "account_id")
    @Id(type = IDENTITY)
    protected Integer accountId;

    @Column(name = "created_on")
    protected LocalDateTime createdOn;

    @Column(name = "email")
    protected String email;

    @Column(name = "password")
    protected String password;

    @Column(name = "person_id")
    protected Integer personId;

    @Column(name = "username")
    protected String username;

    public void setAccountId(Integer value) {
        this.accountId = value;
    }

    public Integer getAccountId() {
        return this.accountId;
    }

    public void setCreatedOn(LocalDateTime value) {
        this.createdOn = value;
    }

    public LocalDateTime getCreatedOn() {
        return this.createdOn;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getEmail() {
        return this.email;
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPersonId(Integer value) {
        this.personId = value;
    }

    public Integer getPersonId() {
        return this.personId;
    }

    public void setUsername(String value) {
        this.username = value;
    }

    public String getUsername() {
        return this.username;
    }

}
