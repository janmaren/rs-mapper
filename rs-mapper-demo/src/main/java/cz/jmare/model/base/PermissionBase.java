package cz.jmare.model.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class PermissionBase {
    @Column(name = "permission_id")
    @Id(type = IDENTITY)
    protected Integer permissionId;

    @Column(name = "name")
    protected String name;

    public void setPermissionId(Integer value) {
        this.permissionId = value;
    }

    public Integer getPermissionId() {
        return this.permissionId;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return this.name;
    }

}
