package cz.jmare;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cz.jmare.service.AccountService;

@RestController
public class AccountController {
    @Autowired
    AccountService accountService;
    
    @PostMapping("/account")
    public ResponseEntity<AccountJson> createAccount(@RequestBody AccountJson accountJson) {
        AccountJson accountJsonResponse = accountService.createAccount(accountJson);
        return new ResponseEntity<>(accountJsonResponse, HttpStatus.CREATED);
    }

    @PutMapping("/account/{accountId}")
    public ResponseEntity<AccountJson> updateAccount(@RequestBody AccountJson accountJson, @PathVariable Integer accountId) {
        accountJson.setAccountId(accountId);
        accountService.updateAccount(accountJson);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @GetMapping("/account/{accountId}")
    public ResponseEntity<AccountJson> getAccount(@PathVariable Integer accountId) {
        return new ResponseEntity<>(accountService.getAccount(accountId), HttpStatus.OK);
    }
    
    @GetMapping("/account")
    public ResponseEntity<List<AccountJson>> getAccounts(@RequestParam(value = "name", required = false) String personName) {
        return new ResponseEntity<>(accountService.getAccounts(personName), HttpStatus.OK);
    }
    
    @DeleteMapping("/account/{accountId}")
    public ResponseEntity<AccountJson> deleteAccount(@PathVariable Integer accountId) {
        accountService.deleteAccount(accountId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
