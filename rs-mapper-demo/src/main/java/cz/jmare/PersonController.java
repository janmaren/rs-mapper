package cz.jmare;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.jmare.service.PersonService;

@RestController
public class PersonController {

    @Autowired
    PersonService personService;

    @GetMapping("/person")
    public List<PersonJson> getPersons() {
        return personService.getPersons();
    }
    
    @PostMapping("/person")
    public void addPerson() {
        personService.addPerson();
    }
}