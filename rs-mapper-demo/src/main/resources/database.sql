CREATE TABLE public.person (
   person_id serial PRIMARY KEY,
   person_name VARCHAR (50)
);

CREATE TABLE public.account (
   account_id serial PRIMARY KEY,
   username VARCHAR (50) NULL,
   password VARCHAR (50) NOT NULL,
   email VARCHAR (355) NOT NULL,
   created_on TIMESTAMP default now(),
   person_id INTEGER NOT NULL REFERENCES person(person_id)
);

CREATE TABLE public.permission (
   permission_id serial PRIMARY KEY,
   name VARCHAR(50) NULL
);

CREATE TABLE public.account_permission (
   account_id INTEGER NOT NULL REFERENCES account(account_id),
   permission_id INTEGER NOT NULL REFERENCES permission(permission_id),
   primary key(account_id, permission_id)
);

commit;
