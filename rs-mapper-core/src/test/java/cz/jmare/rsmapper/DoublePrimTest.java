package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.function.Executable;

import cz.jmare.rsmapper.astro.H2EngineerTestUtil;
import cz.jmare.rsmapper.doublefk.entity.sub.Doublefk;
import cz.jmare.rsmapper.doublefk.entity.sub.Doubleprim;
import cz.jmare.rsmapper.exception.NoRecordException;
import cz.jmare.rsmapper.handle.Handle;

@TestInstance(Lifecycle.PER_METHOD)
public class DoublePrimTest {
    private static Handle handle;
    
    @BeforeEach
    public void prepare() throws ClassNotFoundException, SQLException {
        handle = H2EngineerTestUtil.createConnection("/test-doublefk.sql");
    }

    @Test
    public void testDoubleFkToPrim() throws SQLException {
        Relation<Doubleprim> relation = new Relation<>(Doubleprim.class).withJoins(new ToOne(Doublefk.class));
        Doubleprim doubleprim = new Doubleprim();
        doubleprim.setId1(1);
        doubleprim.setId2(2);
        Doublefk doubleFk = new Doublefk();
        doubleprim.setDoublefk(doubleFk);
        relation.save(handle, doubleprim);
        
        Doubleprim doubleprimLoaded = relation.loadOneByIdFields(handle, Map.of("id1", 1, "id2", 2));
        assertNotNull(doubleprimLoaded.getDoublefk(), "prim is null");
        assertEquals(doubleprimLoaded.getId1(), doubleprimLoaded.getDoublefk().getPid1());
        assertEquals(doubleprimLoaded.getId2(), doubleprimLoaded.getDoublefk().getPid2());
        
        doubleprimLoaded.setDoublefk(null);
        relation.save(handle, doubleprimLoaded);
        
        doubleprimLoaded = relation.loadOneByIdFields(handle, Map.of("id1", 1, "id2", 2));
        
        assertNull(doubleprimLoaded.getDoublefk(), "double fk is not null");
        assertEquals(1, TestUtil.sqlCount(handle, "doublefk"));
    }
    
    @Test
    public void testDoubleFkToPrimOrphan() throws SQLException {
        Relation<Doubleprim> relation = new Relation<>(Doubleprim.class).withJoins(new ToOne(Doublefk.class).withDeleteOrphan());
        Doubleprim doubleprim = new Doubleprim();
        doubleprim.setId1(1);
        doubleprim.setId2(2);
        Doublefk doubleFk = new Doublefk();
        doubleprim.setDoublefk(doubleFk);
        relation.save(handle, doubleprim);
        
        Doubleprim doubleprimLoaded = relation.loadOneByIdFields(handle, Map.of("id1", 1, "id2", 2));
        assertNotNull(doubleprimLoaded.getDoublefk(), "prim is null");
        assertEquals(doubleprimLoaded.getId1(), doubleprimLoaded.getDoublefk().getPid1());
        assertEquals(doubleprimLoaded.getId2(), doubleprimLoaded.getDoublefk().getPid2());
        
        doubleprimLoaded.setDoublefk(null);
        relation.save(handle, doubleprimLoaded);
        
        doubleprimLoaded = relation.loadOneByIdFields(handle, Map.of("id1", 1, "id2", 2));
        
        assertNull(doubleprimLoaded.getDoublefk(), "double fk is not null");
        assertEquals(0, TestUtil.sqlCount(handle, "doublefk"));
    }
    
    @Test
    public void testDoubleFkToPrimDelete() throws SQLException {
        Relation<Doubleprim> relation = new Relation<>(Doubleprim.class).withJoins(new ToOne(Doublefk.class).withDeleteOrphan());
        Doubleprim doubleprim = new Doubleprim();
        doubleprim.setId1(1);
        doubleprim.setId2(2);
        Doublefk doubleFk = new Doublefk();
        doubleprim.setDoublefk(doubleFk);
        relation.save(handle, doubleprim);
        
        Doubleprim doubleprimLoaded = relation.loadOneByIdFields(handle, Map.of("id1", 1, "id2", 2));
        assertNotNull(doubleprimLoaded.getDoublefk(), "prim is null");
        assertEquals(doubleprimLoaded.getId1(), doubleprimLoaded.getDoublefk().getPid1());
        assertEquals(doubleprimLoaded.getId2(), doubleprimLoaded.getDoublefk().getPid2());
        
        relation.deleteOneByIdFieldsValues(handle, Map.of("id1", doubleprimLoaded.getId1(), "id2", doubleprimLoaded.getId2()));

        assertThrows(NoRecordException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                relation.loadOneByIdFields(handle, Map.of("id1", 1, "id2", 2));
            }});
        
        assertEquals(0, TestUtil.sqlCount(handle, "doublefk"));
        assertEquals(0, TestUtil.sqlCount(handle, "doubleprim"));
    }
    
    @AfterEach
    public void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
