package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;

public class OppositeOneToOneAnnoTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }
    
    @Test
    public void testToOneMasterAssoc() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        accountTest5.personTest = personTest5;
        
        Relation<AccountTest> relation = new Relation<>(AccountTest.class).withJoins(
                new ToOnePrim(PersonTest.class));
        List<AccountTest> accounts = relation.loadMany(handle, "", Map.of());
        for (AccountTest accountTest : accounts) {
            relation.delete(handle, accountTest);
        }
        
        relation.save(handle, accountTest5);
        relation.save(handle, accountTest5);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
