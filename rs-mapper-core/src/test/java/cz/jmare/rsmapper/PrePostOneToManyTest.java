package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.IdentityPrimaryKey;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

public class PrePostOneToManyTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        PersonTest personTest = new PersonTest();
        personTest.setPersonName("osoba1");

        AccountTest accountTest2 = createAccountTest();
        accountTest2.personTest = personTest;
        accountTest2.roleTests = new ArrayList<>();
        RoleTest rt1 = new RoleTest();
        rt1.setName("jedna");
        accountTest2.roleTests.add(rt1);
        RoleTest rt2 = new RoleTest();
        rt2.setName("dve");
        accountTest2.roleTests.add(rt2);
        RoleTest rt3 = new RoleTest();
        rt3.setName("tri");
        accountTest2.roleTests.add(rt3);

        Relation<AccountTest> relation = new Relation<>(AccountTest.class, new IdentityPrimaryKey("accountId")).withJoins(
                new ToOnePrim(PersonTest.class, "personTest", "personId", new IdentityPrimaryKey("personId")),
                new ToMany(RoleTest.class, "roleTests", "accountId", new IdentityPrimaryKey("roleId")).withDeleteOrphan());
        relation.save(handle, accountTest2);

        // load test
        AccountTest accountTestLoaded = relation.loadOneById(handle, accountTest2.getAccountId());
        assertEquals(accountTest2.getAccountId(), accountTestLoaded.getAccountId());
        assertEquals(accountTest2.personTest.getPersonId(), accountTestLoaded.personTest.getPersonId());
        assertEquals(accountTest2.roleTests.get(0).getRoleId(), accountTestLoaded.roleTests.get(0).getRoleId());
        assertEquals(accountTest2.roleTests.get(1).getRoleId(), accountTestLoaded.roleTests.get(1).getRoleId());
        assertEquals(accountTest2.roleTests.get(2).getRoleId(), accountTestLoaded.roleTests.get(2).getRoleId());

        // remove test
        accountTest2.roleTests.remove(2);
        relation.save(handle, accountTest2);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(2, TestUtil.sqlCount(handle, "role_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
