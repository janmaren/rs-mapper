package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PermissionTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

public class LoadCustomTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testLoadCustom() throws SQLException {
        AccountTest accountTest = createAccountTest();
        PersonTest personTest = new PersonTest();
        personTest.setPersonName("osoba5");
        personTest.accountTest = accountTest;
        
        PermissionTest permissionTest = new PermissionTest();
        permissionTest.setName("name1");
        
        accountTest.roleTests = new ArrayList<>();
        accountTest.roleTests.add(new RoleTest());
        RoleTest roleTest3 = new RoleTest();
        roleTest3.permissionTests = new ArrayList<>();
        PermissionTest permissionTest2 = new PermissionTest();
        permissionTest2.setName("perm1");
        roleTest3.permissionTests.add(permissionTest2);
        accountTest.roleTests.add(roleTest3);
        
        Relation<PersonTest> personRelation = new Relation<>(PersonTest.class).withAlias("per").withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId").withAlias("acc").withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId").withCondition("role_id!=9999").withJoins( 
                                new ToMany(PermissionTest.class, "permissionTests", "roleId").withCondition("name!='alex'"))));
        personRelation.save(handle, personTest);

        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        assertEquals(2, TestUtil.sqlCount(handle, "role_test"));
        
        List<PersonTest> personTests = personRelation.loadMany(handle, "join account_test at on per.person_id=at.person_id where at.username=:userName", Map.of("userName", "john"));
        assertEquals(2, personTests.get(0).accountTest.roleTests.size());
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
