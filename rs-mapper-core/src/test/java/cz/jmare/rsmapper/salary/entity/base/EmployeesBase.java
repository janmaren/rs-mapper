package cz.jmare.rsmapper.salary.entity.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import java.time.LocalDate;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class EmployeesBase {
    @Column(name = "emp_no")
    @Id(type = IDENTITY)
    protected Integer empNo;

    @Column(name = "birth_date")
    protected LocalDate birthDate;

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "gender")
    protected Integer gender;

    @Column(name = "hire_date")
    protected LocalDate hireDate;

    @Column(name = "last_name")
    protected String lastName;

    public void setEmpNo(Integer value) {
        this.empNo = value;
    }

    public Integer getEmpNo() {
        return this.empNo;
    }

    public void setBirthDate(LocalDate value) {
        this.birthDate = value;
    }

    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setGender(Integer value) {
        this.gender = value;
    }

    public Integer getGender() {
        return this.gender;
    }

    public void setHireDate(LocalDate value) {
        this.hireDate = value;
    }

    public LocalDate getHireDate() {
        return this.hireDate;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public String getLastName() {
        return this.lastName;
    }

}
