package cz.jmare.rsmapper.salary.entity.base;

import static cz.jmare.rsmapper.IdType.PASSED;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class DepartmentsBase {
    @Column(name = "dept_no")
    @Id(type = PASSED)
    protected String deptNo;

    @Column(name = "dept_name")
    protected String deptName;

    public void setDeptNo(String value) {
        this.deptNo = value;
    }

    public String getDeptNo() {
        return this.deptNo;
    }

    public void setDeptName(String value) {
        this.deptName = value;
    }

    public String getDeptName() {
        return this.deptName;
    }

}
