package cz.jmare.rsmapper.salary.entity.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import java.util.UUID;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class AstronautBase {
    @Column(name = "id")
    @Id(type = IDENTITY)
    protected Integer id;

    @Column(name = "uu_id")
    protected UUID uuId;

    public void setId(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return this.id;
    }

    public void setUuId(UUID value) {
        this.uuId = value;
    }

    public UUID getUuId() {
        return this.uuId;
    }

}
