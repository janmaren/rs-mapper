package cz.jmare.rsmapper.salary.entity.base;

import static cz.jmare.rsmapper.IdType.PASSED;
import java.time.LocalDate;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class SalariesBase {
    @Column(name = "emp_no")
    @Id(type = PASSED)
    protected Integer empNo;

    @Column(name = "from_date")
    @Id(type = PASSED)
    protected LocalDate fromDate;

    @Column(name = "salary")
    protected Integer salary;

    @Column(name = "to_date")
    protected LocalDate toDate;

    public void setEmpNo(Integer value) {
        this.empNo = value;
    }

    public Integer getEmpNo() {
        return this.empNo;
    }

    public void setFromDate(LocalDate value) {
        this.fromDate = value;
    }

    public LocalDate getFromDate() {
        return this.fromDate;
    }

    public void setSalary(Integer value) {
        this.salary = value;
    }

    public Integer getSalary() {
        return this.salary;
    }

    public void setToDate(LocalDate value) {
        this.toDate = value;
    }

    public LocalDate getToDate() {
        return this.toDate;
    }

}
