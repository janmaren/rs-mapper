package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PermissionTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

public class PostDeleteManyAssocAnnoTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }
    
    @Test
    public void testDeleteUseAssoc() throws SQLException {
        AccountTest accountTest = createAccountTest();
        PersonTest personTest = new PersonTest();
        personTest.setPersonName("osoba5");
        personTest.accountTest = accountTest;
        
        PermissionTest permissionTest = new PermissionTest();
        permissionTest.setName("name1");
        
        accountTest.roleTests = new ArrayList<>();
        accountTest.roleTests.add(new RoleTest());
        RoleTest roleTest3 = new RoleTest();
        roleTest3.permissionTests = new ArrayList<>();
        PermissionTest permissionTest2 = new PermissionTest();
        permissionTest2.setName("perm1");
        roleTest3.permissionTests.add(permissionTest2);
        accountTest.roleTests.add(roleTest3);
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withAlias("per").withJoins( 
                new ToOne(AccountTest.class).withAlias("acc").withJoins( 
                        new ToMany(RoleTest.class).withDeleteOrphan().withCondition("role_id!=9999").withJoins( 
                                new ToMany(PermissionTest.class, "permissionTests").withCondition("name!='alex'"))));
        relation.save(handle, personTest);
        accountTest.roleTests.clear();
        RoleTest roleTest11 = new RoleTest();
        roleTest11.setName("role11");
        roleTest11.permissionTests = new ArrayList<>();
        PermissionTest permissionTest3 = new PermissionTest();
        permissionTest3.setName("perm1");
        roleTest11.permissionTests.add(permissionTest3);
        accountTest.roleTests.add(roleTest11);
        
        relation.save(handle, personTest);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "role_test"));
        
        PersonTest personTest2 = relation.selectOne(handle, "join account_test acc on per.person_id = acc.person_id join role_test _3 on acc.account_id=_3.account_id join permission_test _4 on _3.role_id=_4.role_id where per.person_id=:personId", Map.of("personId", personTest.getPersonId()));
        RoleTest roleTest = personTest2.accountTest.roleTests.get(0);
        assertEquals("role11", roleTest.getName());
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
