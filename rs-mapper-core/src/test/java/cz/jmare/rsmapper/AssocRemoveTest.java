package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

@TestInstance(Lifecycle.PER_METHOD)
public class AssocRemoveTest {
    private static Handle handle;
    
    @BeforeEach
    public void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @AfterEach
    public void destroy() throws SQLException {
        handle.close();
    }
    
    @Test
    public void testToOnePrimDelete() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        //personTest5.accountTest = accountTest5;
        accountTest5.personTest = personTest5;
        
        
        Relation<AccountTest> relation = new Relation<>(AccountTest.class).withJoins( 
                new ToOnePrim(PersonTest.class, "personTest").withDeleteOrphan());
        relation.save(handle, accountTest5);
        
        AccountTest accountLoaded = relation.loadOneById(handle, accountTest5.getAccountId());
        assertNotNull(accountLoaded.personTest);
        
        accountLoaded.personTest = null;
        relation.save(handle, accountLoaded);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(0, TestUtil.sqlCount(handle, "person_test"));
    }
    
    @Test
    public void testToOnePrimDetach() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        //personTest5.accountTest = accountTest5;
        accountTest5.personTest = personTest5;
        
        
        Relation<AccountTest> relation = new Relation<>(AccountTest.class).withJoins( 
                new ToOnePrim(PersonTest.class, "personTest"));
        relation.save(handle, accountTest5);
        
        AccountTest accountLoaded = relation.loadOneById(handle, accountTest5.getAccountId());
        assertNotNull(accountLoaded.personTest);
        
        accountLoaded.personTest = null;
        relation.save(handle, accountLoaded);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        
        AccountTest accountLoaded2 = relation.loadOneById(handle, accountTest5.getAccountId());
        assertNull(accountLoaded2.personTest);
    }    
    
    @Test
    public void testToOneDelete() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId").withDeleteOrphan());
        relation.save(handle, personTest5);
        
        PersonTest personTestLoaded = relation.loadOneById(handle, personTest5.getPersonId());
        assertEquals(personTest5.getPersonId(), personTestLoaded.getPersonId());
        assertEquals(personTest5.accountTest.getAccountId(), personTestLoaded.accountTest.getAccountId());
        
        personTest5.accountTest = null;
        relation.save(handle, personTest5);
        
        assertEquals(0, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
    }

    @Test
    public void testToOneDetach() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId"));
        relation.save(handle, personTest5);
        
        PersonTest personTestLoaded = relation.loadOneById(handle, personTest5.getPersonId());
        assertEquals(personTest5.getPersonId(), personTestLoaded.getPersonId());
        assertEquals(personTest5.accountTest.getAccountId(), personTestLoaded.accountTest.getAccountId());
        
        personTest5.accountTest = null;
        relation.save(handle, personTest5);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        
        personTestLoaded = relation.loadOneById(handle, personTest5.getPersonId());
        assertEquals(null, personTestLoaded.accountTest);
    }
    
    @Test
    public void testToManyDelete() throws SQLException {
        Relation<AccountTest> relation = new Relation<>(AccountTest.class).withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId").withDeleteOrphan());
        AccountTest account = createAccountTest();
        account.roleTests = new ArrayList<RoleTest>();
        RoleTest rt1 = new RoleTest();
        account.roleTests.add(rt1);
        RoleTest rt2 = new RoleTest();
        account.roleTests.add(rt2);
        relation.save(handle, account);
        
        account.roleTests.remove(1);
        relation.save(handle, account);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "role_test"));
    }
    
    @Test
    public void testToManyDetach() throws SQLException {
        Relation<AccountTest> relation = new Relation<>(AccountTest.class).withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId"));
        AccountTest account = createAccountTest();
        account.roleTests = new ArrayList<RoleTest>();
        RoleTest rt1 = new RoleTest();
        account.roleTests.add(rt1);
        RoleTest rt2 = new RoleTest();
        account.roleTests.add(rt2);
        relation.save(handle, account);
        
        account.roleTests.remove(1);
        relation.save(handle, account);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(2, TestUtil.sqlCount(handle, "role_test"));
        
        List<AccountTest> loadAll = relation.loadManyAll(handle);
        AccountTest accountTest = loadAll.get(0);
        assertEquals(1, accountTest.roleTests.size());
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }    
}
