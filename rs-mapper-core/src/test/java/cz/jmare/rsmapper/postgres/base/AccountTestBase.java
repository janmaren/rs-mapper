package cz.jmare.rsmapper.postgres.base;

import java.time.LocalDateTime;
import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class AccountTestBase {
    @Column(name = "account_id")
    @Id(type = IDENTITY)
    protected Integer accountId;

    @Column(name = "created_on")
    protected LocalDateTime createdOn;

    @Column(name = "date1")
    protected LocalDateTime date1;

    @Column(name = "email")
    protected String email;

    @Column(name = "last_login")
    protected LocalDateTime lastLogin;

    @Column(name = "married")
    protected Boolean married;

    @Column(name = "password")
    protected String password;

    @Column(name = "person_id")
    protected Integer personId;

    @Column(name = "username")
    protected String username;

    @Column(name = "whole")
    protected Integer whole;

    public void setAccountId(Integer value) {
        this.accountId = value;
    }

    public Integer getAccountId() {
        return this.accountId;
    }

    public void setCreatedOn(LocalDateTime value) {
        this.createdOn = value;
    }

    public LocalDateTime getCreatedOn() {
        return this.createdOn;
    }

    public void setDate1(LocalDateTime value) {
        this.date1 = value;
    }

    public LocalDateTime getDate1() {
        return this.date1;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getEmail() {
        return this.email;
    }

    public void setLastLogin(LocalDateTime value) {
        this.lastLogin = value;
    }

    public LocalDateTime getLastLogin() {
        return this.lastLogin;
    }

    public void setMarried(Boolean value) {
        this.married = value;
    }

    public Boolean getMarried() {
        return this.married;
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPersonId(Integer value) {
        this.personId = value;
    }

    public Integer getPersonId() {
        return this.personId;
    }

    public void setUsername(String value) {
        this.username = value;
    }

    public String getUsername() {
        return this.username;
    }

    public void setWhole(Integer value) {
        this.whole = value;
    }

    public Integer getWhole() {
        return this.whole;
    }

}
