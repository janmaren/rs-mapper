package cz.jmare.rsmapper.postgres.base;

import java.time.LocalDateTime;
import java.time.LocalTime;
import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class VariousTypesTestBase {
    @Column(name = "id")
    @Id(type = IDENTITY)
    protected Integer id;

    @Column(name = "a_time")
    protected LocalTime aTime;

    @Column(name = "a_timestamp")
    protected LocalDateTime aTimestamp;

    public void setId(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return this.id;
    }

    public void setATime(LocalTime value) {
        this.aTime = value;
    }

    public LocalTime getATime() {
        return this.aTime;
    }

    public void setATimestamp(LocalDateTime value) {
        this.aTimestamp = value;
    }

    public LocalDateTime getATimestamp() {
        return this.aTimestamp;
    }

}
