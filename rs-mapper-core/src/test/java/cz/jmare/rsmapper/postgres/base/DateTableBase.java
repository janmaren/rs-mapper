package cz.jmare.rsmapper.postgres.base;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.LocalDate;
import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Id;

public class DateTableBase {
    @Id(type = IDENTITY)
    protected Integer id;

    protected LocalDate aDate;

    protected LocalTime aTimenotz;

    protected OffsetTime aTimetz;

    protected LocalDateTime tsNozone;

    protected OffsetDateTime tsWithzone;

    public void setId(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return this.id;
    }

    public void setADate(LocalDate value) {
        this.aDate = value;
    }

    public LocalDate getADate() {
        return this.aDate;
    }

    public void setATimenotz(LocalTime value) {
        this.aTimenotz = value;
    }

    public LocalTime getATimenotz() {
        return this.aTimenotz;
    }

    public void setATimetz(OffsetTime value) {
        this.aTimetz = value;
    }

    public OffsetTime getATimetz() {
        return this.aTimetz;
    }

    public void setTsNozone(LocalDateTime value) {
        this.tsNozone = value;
    }

    public LocalDateTime getTsNozone() {
        return this.tsNozone;
    }

    public void setTsWithzone(OffsetDateTime value) {
        this.tsWithzone = value;
    }

    public OffsetDateTime getTsWithzone() {
        return this.tsWithzone;
    }

}
