package cz.jmare.rsmapper.postgres.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class RoleTestBase {
    @Column(name = "role_id")
    @Id(type = IDENTITY)
    protected Integer roleId;

    @Column(name = "account_id")
    protected Integer accountId;

    @Column(name = "name")
    protected String name;

    public void setRoleId(Integer value) {
        this.roleId = value;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public void setAccountId(Integer value) {
        this.accountId = value;
    }

    public Integer getAccountId() {
        return this.accountId;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return this.name;
    }

}
