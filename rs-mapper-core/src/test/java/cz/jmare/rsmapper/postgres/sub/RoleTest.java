package cz.jmare.rsmapper.postgres.sub;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.rsmapper.Assoc;
import cz.jmare.rsmapper.postgres.base.RoleTestBase;

public class RoleTest extends RoleTestBase {
    @Assoc("roleId")
    public List<PermissionTest> permissionTests = new ArrayList<>();
}
