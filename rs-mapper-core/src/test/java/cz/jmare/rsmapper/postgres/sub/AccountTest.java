package cz.jmare.rsmapper.postgres.sub;

import java.util.List;

import cz.jmare.rsmapper.Assoc;
import cz.jmare.rsmapper.postgres.base.AccountTestBase;

public class AccountTest extends AccountTestBase {
    @Assoc(fkThere = "accountId =accountId ")
    public List<RoleTest> roleTests;
    
    @Assoc("personId")
    public PersonTest personTest;
}
