package cz.jmare.rsmapper.postgres.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class PersonTestBase {
    @Column(name = "person_id")
    @Id(type = IDENTITY)
    protected Integer personId;

    @Column(name = "person_name")
    protected String personName;

    public void setPersonId(Integer value) {
        this.personId = value;
    }

    public Integer getPersonId() {
        return this.personId;
    }

    public void setPersonName(String value) {
        this.personName = value;
    }

    public String getPersonName() {
        return this.personName;
    }

}
