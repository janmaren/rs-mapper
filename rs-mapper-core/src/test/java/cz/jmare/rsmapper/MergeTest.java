package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.util.MergeObjects;

public class MergeTest {
    @Test
    public void testMerge() {
        AccountTest destObject = new AccountTest();
        destObject.setAccountId(123);
        destObject.roleTests = new ArrayList<>();
        AccountTest mergingObject = new AccountTest();
        mergingObject.setPassword("pass1");
        MergeObjects.shallowMerge(destObject, mergingObject);
        assertNotNull(destObject.getPassword());
        assertEquals(123, destObject.getAccountId());
    }
}
