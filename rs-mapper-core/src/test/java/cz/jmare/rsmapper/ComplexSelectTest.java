package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PermissionTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

public class ComplexSelectTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        PermissionTest permissionTest = new PermissionTest();
        permissionTest.setName("name1");
        
        accountTest5.roleTests = new ArrayList<>();
        accountTest5.roleTests.add(new RoleTest());
        accountTest5.roleTests.add(new RoleTest());
        RoleTest roleTest3 = new RoleTest();
        roleTest3.permissionTests = new ArrayList<>();
        PermissionTest permissionTest2 = new PermissionTest();
        permissionTest2.setName("perm1");
        roleTest3.permissionTests.add(permissionTest2);
        accountTest5.roleTests.add(roleTest3);
        
//        Relation<PersonTest> relation = new Relation<>(PersonTest.class, 
//                new ToOne(AccountTest.class, "accountTest", "personId", 
//                        new ToMany(RoleTest.class, "roleTests", "accountId", 
//                                new ToMany(PermissionTest.class, "permissionTests", "roleId"))));
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withAlias("p").withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId").withAlias("a").withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId").withAlias("r").withJoins( 
                                new ToMany(PermissionTest.class, "permissionTests", "roleId").withAlias("pe"))));
        
        relation.save(handle, personTest5);

        List<PersonTest> select = relation.selectMany(handle, "join account_test a on p.person_id=a.person_id join role_test r on a.account_id=r.account_id join permission_test pe on r.role_id=pe.role_id");
        assertEquals(1, select.size());
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
