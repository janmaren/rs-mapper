package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.entity.ATimestamp;
import cz.jmare.rsmapper.entity.ATimestamp2;
import cz.jmare.rsmapper.entity.ATimestamp3;
import cz.jmare.rsmapper.entity.ATimestamp4;
import cz.jmare.rsmapper.handle.Handle;

public class VariousTypesMappingsTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testVariousTypes() throws SQLException {
        ATimestamp aTimestamp = new ATimestamp();
        aTimestamp.aTimestamp = new Date();
        
        Relation<ATimestamp> relation = new Relation<>(ATimestamp.class);
        relation.save(handle, aTimestamp);
        
        ATimestamp aTimestampResp = relation.selectOne(handle, "where _1.id=:id", ofMap("id", aTimestamp.id));
        assertEquals(aTimestamp.aTimestamp, aTimestampResp.aTimestamp);
        
        // ------
        
        ATimestamp2 aTimestamp2 = new ATimestamp2();
        aTimestamp2.aTimestamp = LocalDateTime.now();
        
        Relation<ATimestamp2> relation2 = new Relation<>(ATimestamp2.class);
        relation2.save(handle, aTimestamp2);
        
        ATimestamp2 aTimestamp2Resp = relation2.selectOne(handle, "where _1.id=:id", ofMap("id", aTimestamp2.id));
        assertEquals(aTimestamp2.aTimestamp.getMinute(), aTimestamp2Resp.aTimestamp.getMinute());
        assertEquals(aTimestamp2.aTimestamp.getSecond(), aTimestamp2Resp.aTimestamp.getSecond());
        
        // ------
        
        ATimestamp3 aTimestamp3 = new ATimestamp3();
        aTimestamp3.aTimestamp = LocalDate.now();
        
        Relation<ATimestamp3> relation3 = new Relation<>(ATimestamp3.class);
        relation3.save(handle, aTimestamp3);
        
        ATimestamp3 aTimestamp3Resp = relation3.selectOne(handle, "where _1.id=:id", ofMap("id", aTimestamp3.id));
        assertEquals(aTimestamp3.aTimestamp.getDayOfYear(), aTimestamp3Resp.aTimestamp.getDayOfYear());
        
        // ------
        
        ATimestamp4 aTimestamp4 = new ATimestamp4();
        aTimestamp4.aTime = Time.valueOf(LocalTime.now());
        
        Relation<ATimestamp4> relation4 = new Relation<>(ATimestamp4.class);
        relation4.save(handle, aTimestamp4);
        
        ATimestamp4 aTimestamp4Resp = relation4.selectOne(handle, "where _1.id=:id", ofMap("id", aTimestamp4.id));
        assertEquals(aTimestamp4.aTime.getTime(), aTimestamp4Resp.aTime.getTime());        
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
    
    public static Map<String, Object> ofMap(String key, Object value) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put(key, value);
        return result;
    }
}
