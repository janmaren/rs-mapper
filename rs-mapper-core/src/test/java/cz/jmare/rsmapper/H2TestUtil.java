package cz.jmare.rsmapper;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import cz.jmare.rsmapper.dbprofile.H2DBProfile;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.handle.SimpleHandle;
import cz.jmare.util.SimpleDataSource;
import cz.jmare.util.dbpopulator.DBPopulator;

public class H2TestUtil {
    public static Handle createConnection() {
        boolean inMemory = true; 
        Connection connection;
        try {
            String callerClassName = new Exception().getStackTrace()[1].getClassName();
            int lastIndexOf = callerClassName.lastIndexOf(".");
            if (lastIndexOf != -1) {
                callerClassName = callerClassName.substring(lastIndexOf + 1);
            }
            Class.forName("org.h2.Driver");
            String url;
            if (inMemory) {
                url = "jdbc:h2:mem:test";
            } else {
                url = "jdbc:h2:~/h2/" + callerClassName;
                File dbFile = new File(System.getProperty("user.home") + File.separator + "h2" + File.separator + callerClassName + ".mv.db");
                if (dbFile.isFile()) {
                    dbFile.delete();
                }
            }
            SimpleDataSource simpleDataSource = new SimpleDataSource(url);
            connection = simpleDataSource.getConnection();
            
            
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException("Unable to create connection", e);
        }

        DBPopulator.populate(connection, H2TestUtil.class.getResource("/test-rsmapper.sql"));

        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.TRACE);
        
        SimpleHandle simpleHandle = new SimpleHandle(connection, new H2DBProfile());
        return simpleHandle;
    }
    
    public static DSConf createRSDataSource() {
        boolean inMemory = false; 
        String callerClassName = new Exception().getStackTrace()[1].getClassName();
        int lastIndexOf = callerClassName.lastIndexOf(".");
        if (lastIndexOf != -1) {
            callerClassName = callerClassName.substring(lastIndexOf + 1);
        }
        String url;
        if (inMemory) {
            url = "jdbc:h2:mem:test";
        } else {
            url = "jdbc:h2:~/h2/" + callerClassName;
            File dbFile = new File(System.getProperty("user.home") + File.separator + "h2" + File.separator + callerClassName + ".mv.db");
            if (dbFile.isFile()) {
                dbFile.delete();
            }
        }
        SimpleDataSource simpleDataSource = new SimpleDataSource(url, "org.h2.Driver");
        
        try (Connection connection = simpleDataSource.getConnection()) {
            DBPopulator.populate(connection, H2TestUtil.class.getResource("/test-rsmapper.sql"));
        } catch (SQLException e) {
           throw new RuntimeException(e);
        }

        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.TRACE);
        return new DSConf(simpleDataSource);
    }
}
