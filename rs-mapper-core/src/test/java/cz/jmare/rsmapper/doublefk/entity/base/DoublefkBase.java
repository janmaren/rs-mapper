package cz.jmare.rsmapper.doublefk.entity.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class DoublefkBase {
    @Column(name = "id")
    @Id(type = IDENTITY)
    protected Integer id;

    @Column(name = "pid1")
    protected Integer pid1;

    @Column(name = "pid2")
    protected Integer pid2;

    @Column(name = "gain")
    protected Integer gain;
    
    public void setId(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return this.id;
    }

    public void setPid1(Integer value) {
        this.pid1 = value;
    }

    public Integer getPid1() {
        return this.pid1;
    }

    public void setPid2(Integer value) {
        this.pid2 = value;
    }

    public Integer getPid2() {
        return this.pid2;
    }

    public Integer getGain() {
        return gain;
    }

    public void setGain(Integer gain) {
        this.gain = gain;
    }
}
