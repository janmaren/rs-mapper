package cz.jmare.rsmapper.doublefk.entity.base;

import static cz.jmare.rsmapper.IdType.PASSED;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class DoubleprimBase {
    @Column(name = "id1")
    @Id(type = PASSED)
    protected Integer id1;

    @Column(name = "id2")
    @Id(type = PASSED)
    protected Integer id2;

    @Column(name = "name")
    protected String name;

    @Column(name = "gain")
    protected Integer gain;
    
    public void setId2(Integer value) {
        this.id2 = value;
    }

    public Integer getId2() {
        return this.id2;
    }

    public void setId1(Integer value) {
        this.id1 = value;
    }

    public Integer getId1() {
        return this.id1;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return this.name;
    }

    public Integer getGain() {
        return gain;
    }

    public void setGain(Integer gain) {
        this.gain = gain;
    }

}
