package cz.jmare.rsmapper.doublefk.entity.sub;

import cz.jmare.rsmapper.Assoc;
import cz.jmare.rsmapper.doublefk.entity.base.DoubleprimBase;

public class Doubleprim extends DoubleprimBase {
    @Assoc(fkThere = "pid2,pid1=id2,id1")
    private Doublefk doublefk;

    public Doublefk getDoublefk() {
        return doublefk;
    }

    public void setDoublefk(Doublefk doublefk) {
        this.doublefk = doublefk;
    }
}
