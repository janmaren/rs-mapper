package cz.jmare.rsmapper.doublefk.entity.sub;

import cz.jmare.rsmapper.Assoc;
import cz.jmare.rsmapper.doublefk.entity.base.DoublefkBase;

public class Doublefk extends DoublefkBase {
    @Assoc(fkHere = "pid2,pid1=id2,id1")
    private Doubleprim doubleprim;

    public Doubleprim getDoubleprim() {
        return doubleprim;
    }

    public void setDoubleprim(Doubleprim doubleprim) {
        this.doubleprim = doubleprim;
    }
}
