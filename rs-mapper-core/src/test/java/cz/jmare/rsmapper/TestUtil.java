package cz.jmare.rsmapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cz.jmare.rsmapper.handle.Handle;

public class TestUtil {
    public static int sqlCount(Handle handle, String table) {
        try {
            String sql = "select count(*) from " + table;
            PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException();
    }
}
