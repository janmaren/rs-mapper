package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PermissionTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

public class PostDeletesTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        PermissionTest permissionTest = new PermissionTest();
        permissionTest.setName("name1");
        
        accountTest5.roleTests = new ArrayList<>();
        accountTest5.roleTests.add(new RoleTest());
        accountTest5.roleTests.add(new RoleTest());
        RoleTest roleTest3 = new RoleTest();
        roleTest3.permissionTests = new ArrayList<>();
        PermissionTest permissionTest2 = new PermissionTest();
        permissionTest2.setName("perm1");
        roleTest3.permissionTests.add(permissionTest2);
        accountTest5.roleTests.add(roleTest3);
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId").withDeleteOrphan().withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId").withCondition("role_id<>9999").withJoins( 
                                new ToMany(PermissionTest.class, "permissionTests", "roleId").withCondition("name<>'alex'"))));
        relation.save(handle, personTest5);
        
        PersonTest personTestLoaded = relation.loadOneById(handle, personTest5.getPersonId());
        assertEquals(personTest5.getPersonId(), personTestLoaded.getPersonId());
        assertEquals(personTest5.accountTest.getAccountId(), personTestLoaded.accountTest.getAccountId());
        assertEquals(personTest5.accountTest.roleTests.size(), personTestLoaded.accountTest.roleTests.size());
        assertEquals(personTest5.accountTest.roleTests.get(0).permissionTests.size(), personTestLoaded.accountTest.roleTests.get(0).permissionTests.size());
        
        personTest5.accountTest = null;
        relation.save(handle, personTest5);
        
        assertEquals(0, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
