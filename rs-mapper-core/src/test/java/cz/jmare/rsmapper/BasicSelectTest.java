package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;

public class BasicSelectTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        RsCommand.execute(handle, "delete from account_test");
        int execute = RsCommand.execute(handle, "delete from account_test where account_id=:account_id", Map.of("account_id", 123));
        assertEquals(0, execute);
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins(
                new ToOne(AccountTest.class, "accountTest", "personId"));

        AccountTest createAccountTest1 = createAccountTest("john");
        relation.save(handle, createPerson("osoba1", createAccountTest1));
        relation.save(handle, createPerson("osoba2", createAccountTest("jack")));
        relation.save(handle, createPerson("osoba3", createAccountTest("mirek")));

        assertEquals(3, TestUtil.sqlCount(handle, "person_test"));
        assertEquals(3, TestUtil.sqlCount(handle, "account_test"));
        Long contPersons = RsCommand.selectOneScalar(handle, Long.class, "select count(*) from person_test");
        assertEquals(3, contPersons);
        Long contAccounts = RsCommand.selectOneScalar(handle, Long.class, "select count(*) from account_test where person_id=:person_id", Map.of("person_id", createAccountTest1.getAccountId()));
        assertEquals(1, contAccounts);
        List<Long> selectMultipleScalars = RsCommand.selectMultipleScalars(handle, Long.class, "select count(*) from person_test", Map.of());
        assertEquals(3, selectMultipleScalars.get(0));
        List<LinkedHashMap<String, Object>> selectMultipleVectors = RsCommand.selectMultipleVectors(handle, "select count(*) as co from person_test", Map.of());
        assertEquals(1, selectMultipleVectors.size());
        assertEquals(3L, selectMultipleVectors.get(0).get("co"));
        
        List<PersonTest> personTests = relation.selectMany(handle, "join account_test _2 on _1.person_id=_2.person_id", new HashMap<String, Object>());
        assertEquals(3, personTests.size());
        
        List<PersonTest> personTests2 = relation.selectMany(handle, "join account_test _2 on _1.person_id=_2.person_id where _1.person_name=:person_name order by _1.person_name limit :limit", Map.of("person_name", "osoba1", "limit", 2));
        assertEquals(1, personTests2.size());
    }

    private PersonTest createPerson(String personName, AccountTest accountTest5) {
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName(personName);
        personTest5.accountTest = accountTest5;
        return personTest5;
    }
    
    private AccountTest createAccountTest(String username) {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername(username);
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
