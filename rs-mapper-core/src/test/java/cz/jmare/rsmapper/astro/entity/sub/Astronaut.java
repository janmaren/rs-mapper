package cz.jmare.rsmapper.astro.entity.sub;

import java.util.List;

import cz.jmare.rsmapper.astro.entity.base.AstronautBase;

public class Astronaut extends AstronautBase {
    public List<AstronautRocket> astronautRockets;
}
