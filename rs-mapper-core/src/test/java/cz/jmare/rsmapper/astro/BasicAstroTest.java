package cz.jmare.rsmapper.astro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.SimpleQuery;
import cz.jmare.rsmapper.ToMany;
import cz.jmare.rsmapper.ToOnePrim;
import cz.jmare.rsmapper.astro.entity.sub.Astronaut;
import cz.jmare.rsmapper.astro.entity.sub.AstronautRocket;
import cz.jmare.rsmapper.astro.entity.sub.Rocket;
import cz.jmare.rsmapper.handle.Handle;

public class BasicAstroTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2EngineerTestUtil.createConnection();
    }

    @Test
    public void testEngineer() throws SQLException {
        Astronaut astronaut = new Astronaut();
        astronaut.setUuId(UUID.randomUUID());
        
        astronaut.astronautRockets = new ArrayList<>();
        
        AstronautRocket astronautRocket1 = new AstronautRocket();
        astronautRocket1.rocket = new Rocket();
        astronautRocket1.rocket.setWeight(new BigDecimal(1000L));

        AstronautRocket astronautRocket2 = new AstronautRocket();
        astronautRocket2.rocket = new Rocket();
        astronautRocket2.rocket.setWeight(new BigDecimal(2000L));

        astronaut.astronautRockets.add(astronautRocket1);
        astronaut.astronautRockets.add(astronautRocket2);
        
        Relation<Astronaut> relation = new Relation<>(Astronaut.class).withJoins(new ToMany(AstronautRocket.class,
                "astronautRockets", "astronautId=id").withDeleteOrphan().withJoins(new ToOnePrim(Rocket.class, "rocket", "rocketId=id")));
        relation.save(handle, astronaut);
        relation.save(handle, astronaut);
        
        Relation<AstronautRocket> relation2 = new Relation<>(AstronautRocket.class)
                .withJoins(new ToOnePrim(Rocket.class, "rocket", "rocketId"), new ToOnePrim(Astronaut.class, "astronaut", "astronautId"));        
        List<AstronautRocket> selectMany = relation2.selectMany(handle, "join rocket _2 on _1.rocket_id=_2.id join astronaut _3 on _1.astronaut_id=_3.id");
        assertEquals(2, selectMany.size());

        long selectCount = relation2.selectCount(handle, new SimpleQuery("join rocket _2 on _1.rocket_id=_2.id join astronaut _3 on _1.astronaut_id=_3.id"));
        assertEquals(2, selectCount);
        
        List<AstronautRocket> selectMany2 = relation2.selectMany(handle, "join rocket _2 on _1.rocket_id=_2.id join astronaut _3 on _1.astronaut_id=_3.id order by _2.weight limit :limit", Map.of("limit", 1));
        assertEquals(1, selectMany2.size());
        
        astronaut.astronautRockets.remove(1);
        
        relation.save(handle, astronaut);
        Astronaut astronaut2 = relation.loadOneById(handle, astronaut.getId());
        assertEquals(1, astronaut2.astronautRockets.size());
    }

    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
