package cz.jmare.rsmapper.astro;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import cz.jmare.rsmapper.dbprofile.H2DBProfile;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.handle.SimpleHandle;
import cz.jmare.util.SimpleDataSource;
import cz.jmare.util.dbpopulator.DBPopulator;

public class H2EngineerTestUtil {
    public static Handle createConnection() {
        return createConnection("/test-astro.sql");
    }
    
    public static Handle createConnection(String sqlScriptFile) {
        boolean inMemory = true; 
        Connection connection;
        try {
            String callerClassName = new Exception().getStackTrace()[1].getClassName();
            int lastIndexOf = callerClassName.lastIndexOf(".");
            if (lastIndexOf != -1) {
                callerClassName = callerClassName.substring(lastIndexOf + 1);
            }
            String url;
            if (inMemory) {
                url = "jdbc:h2:mem:test";
            } else {
                url = "jdbc:h2:~/h2/" + callerClassName;
                File dbFile = new File(System.getProperty("user.home") + File.separator + "h2" + File.separator + callerClassName + ".mv.db");
                if (dbFile.isFile()) {
                    dbFile.delete();
                }
            }
            connection = new SimpleDataSource(url).getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Unable to create connection", e);
        }

        DBPopulator.populate(connection, H2EngineerTestUtil.class.getResource(sqlScriptFile));

        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.TRACE);
        return new SimpleHandle(connection, new H2DBProfile());
    }
}
