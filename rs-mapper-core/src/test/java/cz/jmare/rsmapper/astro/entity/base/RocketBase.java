package cz.jmare.rsmapper.astro.entity.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import java.math.BigDecimal;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class RocketBase {
    @Column(name = "id")
    @Id(type = IDENTITY)
    protected Integer id;

    @Column(name = "weight")
    protected BigDecimal weight;

    public void setId(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return this.id;
    }

    public void setWeight(BigDecimal value) {
        this.weight = value;
    }

    public BigDecimal getWeight() {
        return this.weight;
    }

}
