package cz.jmare.rsmapper.astro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.astro.entity.sub.Engineer;
import cz.jmare.rsmapper.handle.Handle;

public class BasicEngeneerTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2EngineerTestUtil.createConnection();
    }

    @Test
    public void testEngineer() throws SQLException {
        Engineer engineer = new Engineer();
        engineer.setFirstName("John");
        engineer.setLastName("Rotor");
        engineer.setBirth(LocalDate.of(2003, 12, 13));
        
        Relation<Engineer> relation = new Relation<>(Engineer.class);
        relation.save(handle, engineer);
        Engineer engineer2 = relation.loadOneByIdFields(handle, Map.of("firstName", engineer.getFirstName(), "lastName", engineer.getLastName()));
        assertEquals(engineer.getBirth(), engineer2.getBirth());
        relation.save(handle, engineer);
        relation.delete(handle, engineer);
//        Employees employees2 = relation.loadById(connection, employees.getEmpNo()).get();
//        assertEquals(employees.getLastName(), employees2.getLastName());
    }

    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
