package cz.jmare.rsmapper.astro;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.SimpleQuery;
import cz.jmare.rsmapper.ToMany;
import cz.jmare.rsmapper.ToOnePrim;
import cz.jmare.rsmapper.astro.entity.sub.Astronaut;
import cz.jmare.rsmapper.astro.entity.sub.AstronautRocket;
import cz.jmare.rsmapper.astro.entity.sub.Rocket;
import cz.jmare.rsmapper.exception.NoRecordException;
import cz.jmare.rsmapper.handle.Handle;

public class LoadAstronautTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2EngineerTestUtil.createConnection();
    }

    @Test
    public void testAstronaut() throws SQLException {
        Astronaut astronaut = new Astronaut();
        UUID randomUUID = UUID.randomUUID();
        astronaut.setUuId(randomUUID);
        
        astronaut.astronautRockets = new ArrayList<>();
        
        AstronautRocket astronautRocket1 = new AstronautRocket();
        astronautRocket1.rocket = new Rocket();
        astronautRocket1.rocket.setWeight(new BigDecimal(1000L));

        AstronautRocket astronautRocket2 = new AstronautRocket();
        astronautRocket2.rocket = new Rocket();
        astronautRocket2.rocket.setWeight(new BigDecimal(2000L));

        astronaut.astronautRockets.add(astronautRocket1);
        astronaut.astronautRockets.add(astronautRocket2);
        
        Relation<Astronaut> astronautRelation = new Relation<>(Astronaut.class).withJoins(new ToMany(AstronautRocket.class,
                "astronautRockets", "astronautId = id").withJoins(new ToOnePrim(Rocket.class, "rocket", "rocketId")));
        astronautRelation.save(handle, astronaut);
        astronaut.setId(null);
        astronautRelation.save(handle, astronaut);
        
        List<Astronaut> loadByCondition = astronautRelation.loadMany(handle, " join astronaut_rocket ar on _1.id=ar.astronaut_id where _1.id<:lessThan", Map.of("lessThan", 200));
        assertEquals(2, loadByCondition.size());
        long selectCount = astronautRelation.selectCount(handle, new SimpleQuery(" join astronaut_rocket ar on _1.id=ar.astronaut_id where _1.id<:lessThan", Map.of("lessThan", 200)));
        assertEquals(2, selectCount);
        
        Relation<AstronautRocket> astronautRocketRelation = new Relation<>(AstronautRocket.class)
                .withJoins(
                        new ToOnePrim(Astronaut.class, "astronaut", "astronautId"),
                        new ToOnePrim(Rocket.class, "rocket", "rocketId"));
        astronautRocketRelation.loadOneByIdFields(handle, Map.of("astronautId", astronaut.getId(), "rocketId", astronaut.astronautRockets.get(0).rocket.getId()));
        
        astronautRocketRelation.deleteOneByIdFieldsValues(handle, Map.of("astronautId", astronaut.getId(), "rocketId", astronaut.astronautRockets.get(0).rocket.getId()));
        
        List<Astronaut> astros2 = astronautRelation.loadManyByFields(handle, Map.of("id", astronaut.getId()));
        Astronaut astro2 = astros2.get(0);
        assertEquals(1, astro2.astronautRockets.size());
        
        AstronautRocket astronautRocketLast = astro2.astronautRockets.get(0);
        astronautRocketRelation.delete(handle, astronautRocketLast);
        
        Astronaut astronaut2 = astronautRelation.loadOneById(handle, astronaut.getId());
        assertEquals(randomUUID, astronaut2.getUuId());
        
        astronautRelation.deleteOneById(handle, astronaut.getId());
        
        assertThrows(NoRecordException.class, () -> {
            astronautRelation.loadOneById(handle, astronaut.getId());
        });
    }

    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
