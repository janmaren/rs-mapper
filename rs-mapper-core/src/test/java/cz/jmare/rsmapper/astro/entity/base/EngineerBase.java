package cz.jmare.rsmapper.astro.entity.base;

import static cz.jmare.rsmapper.IdType.PASSED;
import java.time.LocalDate;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class EngineerBase {
    @Column(name = "first_name")
    @Id(type = PASSED)
    protected String firstName;

    @Column(name = "last_name")
    @Id(type = PASSED)
    protected String lastName;

    @Column(name = "birth")
    protected LocalDate birth;

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setBirth(LocalDate value) {
        this.birth = value;
    }

    public LocalDate getBirth() {
        return this.birth;
    }

}
