package cz.jmare.rsmapper.astro.entity.base;

import static cz.jmare.rsmapper.IdType.PASSED;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class AstronautRocketBase {
    @Column(name = "rocket_id")
    @Id(type = PASSED)
    protected Integer rocketId;

    @Column(name = "astronaut_id")
    @Id(type = PASSED)
    protected Integer astronautId;

    @Column(name = "relation_type")
    protected Integer relationType;

    public void setRocketId(Integer value) {
        this.rocketId = value;
    }

    public Integer getRocketId() {
        return this.rocketId;
    }

    public void setAstronautId(Integer value) {
        this.astronautId = value;
    }

    public Integer getAstronautId() {
        return this.astronautId;
    }

    public void setRelationType(Integer value) {
        this.relationType = value;
    }

    public Integer getRelationType() {
        return this.relationType;
    }

}
