package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import cz.jmare.rsmapper.astro.H2EngineerTestUtil;
import cz.jmare.rsmapper.doublefk.entity.sub.Doublefk;
import cz.jmare.rsmapper.doublefk.entity.sub.Doubleprim;
import cz.jmare.rsmapper.handle.Handle;

@TestInstance(Lifecycle.PER_METHOD)
public class DoubleFkTest {
    private static Handle handle;
    
    @BeforeEach
    public void prepare() throws ClassNotFoundException, SQLException {
        handle = H2EngineerTestUtil.createConnection("/test-doublefk.sql");
    }

    @Test
    public void testDoubleFkToPrim() throws SQLException {
        Relation<Doublefk> relation = new Relation<>(Doublefk.class).withJoins(new ToOnePrim(Doubleprim.class));
        Doublefk doubleFk = new Doublefk();
        Doubleprim doubleprim = new Doubleprim();
        doubleprim.setId1(1);
        doubleprim.setId2(2);
        doubleFk.setDoubleprim(doubleprim);
        relation.save(handle, doubleFk);
        
        Doublefk doubleFkLoaded = relation.loadOneById(handle, doubleFk.getId());
        assertNotNull(doubleFkLoaded.getDoubleprim(), "prim is null");
        assertEquals(doubleFkLoaded.getPid1(), doubleFkLoaded.getDoubleprim().getId1());
        assertEquals(doubleFkLoaded.getPid2(), doubleFkLoaded.getDoubleprim().getId2());
        
        doubleFkLoaded.setDoubleprim(null);
        relation.save(handle, doubleFkLoaded);
        
        assertEquals(1, TestUtil.sqlCount(handle, "doubleprim"));
    }

    @Test
    public void testDoubleFkToPrimOrphan() throws SQLException {
        Relation<Doublefk> relation = new Relation<>(Doublefk.class).withJoins(new ToOnePrim(Doubleprim.class).withDeleteOrphan());
        Doublefk doubleFk = new Doublefk();
        Doubleprim doubleprim = new Doubleprim();
        doubleprim.setId1(1);
        doubleprim.setId2(2);
        doubleFk.setDoubleprim(doubleprim);
        relation.save(handle, doubleFk);
        
        Doublefk doubleFkLoaded = relation.loadOneById(handle, doubleFk.getId());
        assertNotNull(doubleFkLoaded.getDoubleprim(), "prim is null");
        assertEquals(doubleFkLoaded.getPid1(), doubleFkLoaded.getDoubleprim().getId1());
        assertEquals(doubleFkLoaded.getPid2(), doubleFkLoaded.getDoubleprim().getId2());
        
        doubleFkLoaded.setDoubleprim(null);
        relation.save(handle, doubleFkLoaded);
        
        assertEquals(0, TestUtil.sqlCount(handle, "doubleprim"));
    }
    
    @Test
    public void testDoubleFkToPrimDelete() throws SQLException {
        Relation<Doublefk> relation = new Relation<>(Doublefk.class).withJoins(new ToOnePrim(Doubleprim.class).withDeleteOrphan());
        Doublefk doubleFk = new Doublefk();
        Doubleprim doubleprim = new Doubleprim();
        doubleprim.setId1(1);
        doubleprim.setId2(2);
        doubleFk.setDoubleprim(doubleprim);
        relation.save(handle, doubleFk);
        
        Doublefk doubleFkLoaded = relation.loadOneById(handle, doubleFk.getId());
        assertNotNull(doubleFkLoaded.getDoubleprim(), "prim is null");
        assertEquals(doubleFkLoaded.getPid1(), doubleFkLoaded.getDoubleprim().getId1());
        assertEquals(doubleFkLoaded.getPid2(), doubleFkLoaded.getDoubleprim().getId2());

        relation.deleteOneById(handle, doubleFk.getId());
        
        assertEquals(0, TestUtil.sqlCount(handle, "doubleprim"));
        assertEquals(0, TestUtil.sqlCount(handle, "doublefk"));
    }
    
    @AfterEach
    public void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
