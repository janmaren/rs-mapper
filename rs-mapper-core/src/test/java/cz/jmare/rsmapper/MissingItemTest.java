package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PermissionTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;
import cz.jmare.rsmapper.util.SqlCommandUtil;

public class MissingItemTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        accountTest5.roleTests = new ArrayList<>();
        accountTest5.roleTests.add(new RoleTest());
        accountTest5.roleTests.add(new RoleTest());
        RoleTest roleTest3 = new RoleTest();
        roleTest3.permissionTests = new ArrayList<>();
        PermissionTest permissionTest = new PermissionTest();
        permissionTest.setName("perm1");
        roleTest3.permissionTests.add(permissionTest);
        accountTest5.roleTests.add(roleTest3);
        
        Relation<PersonTest> saver5 = new Relation<>(PersonTest.class).withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId").withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId").withJoins( 
                                new ToMany(PermissionTest.class, "permissionTests", "roleId").withDeleteOrphan())));
        saver5.save(handle, personTest5);

        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        
        roleTest3.permissionTests = new ArrayList<>();
        saver5.save(handle, personTest5);
        assertEquals(0, TestUtil.sqlCount(handle, "permission_test"));
        
        SqlCommandUtil.deleteRecordsFromTable(handle, "role_test", Map.of("role_id", roleTest3.getRoleId()));
        saver5.save(handle, personTest5);
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
