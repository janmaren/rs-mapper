package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.PassedPrimaryKey;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;

public class BasicPassedKeyDeletingAssocTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        //connection = PostgresTestUtil.createConnection();
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testPassedKey() throws SQLException {
        AccountTest accountTest = createAccountTest();
        PersonTest personTest = new PersonTest();
        personTest.setPersonId(123);
        personTest.setPersonName("osoba5");
        personTest.accountTest = accountTest;
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class)
                .withPrimaryKey(new PassedPrimaryKey("personId")).withAlias("per")
                .withJoins(new ToOne(AccountTest.class, "accountTest", "personId").withAlias("acc").withDeleteOrphan());
        relation.save(handle, personTest);
        personTest.setPersonName("osoba6");
        accountTest.setAccountId(null);
        relation.save(handle, personTest);
        
        PersonTest personTest2 = relation.selectOne(handle, "join account_test acc on per.person_id = acc.person_id where per.person_id=:personId", Map.of("personId", personTest.getPersonId()));
        assertEquals("osoba6", personTest2.getPersonName());
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
