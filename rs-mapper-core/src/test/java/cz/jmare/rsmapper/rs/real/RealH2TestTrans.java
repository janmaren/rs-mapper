package cz.jmare.rsmapper.rs.real;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.H2TestUtil;
import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.active.basic.Deleter;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.util.TransWrapper;
import cz.jmare.util.dbpopulator.DBPopulator;

public class RealH2TestTrans {
    private static DSConf rsDataSource;

    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        rsDataSource = H2TestUtil.createRSDataSource();
        try (
            Connection connection = rsDataSource.getDataSource().getConnection()){
            DBPopulator.populate(connection,  RealTest1.class.getResource("/test-rsmapper.sql"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    @Disabled
    public void testSinglePostsaver() throws SQLException {
        PersonTest personTest = new PersonTest();
        personTest.setPersonName("osoba1");

        PersonTest personTest2 = new PersonTest();
        personTest2.setPersonName("osoba2");

        try (Handle handle = rsDataSource.createHandle()) {
            Deleter deleter = new Deleter("person_test");
            deleter.execute(handle, null);
        }

        Relation<PersonTest> relation = new Relation<>(PersonTest.class);
        
        
        new Thread(() -> {
        TransWrapper.doInTransaction(rsDataSource, Connection.TRANSACTION_READ_UNCOMMITTED,
                connection -> {
                    relation.save(connection, personTest);
                    sleep(2000);
                });
        }).start();
        sleep(500);
        try (Handle handle = rsDataSource.createHandle()) {
            relation.save(handle, personTest2);
        }

        Relation<ScalarLong> relation2 = new Relation<>(ScalarLong.class);
        TransWrapper.doInTransaction(rsDataSource, Connection.TRANSACTION_READ_UNCOMMITTED,
                connection -> {
                    List<PersonTest> loadByParameters = relation.loadManyByFields(connection, Map.of());
                    System.out.println(loadByParameters.size());
        });

        sleep(2200);

        try (Handle handle = rsDataSource.createHandle()) {
            ScalarLong scalarLong = relation2
                    .selectOne(handle, "select count(*) value__1 from person_test _1 ", Map.of());
            System.out.println(scalarLong.getValue());
        }
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        // connection.close();
    }
}
