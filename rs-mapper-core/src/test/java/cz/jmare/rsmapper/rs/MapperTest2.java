package cz.jmare.rsmapper.rs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.rs.CollectionFieldMapper;
import cz.jmare.rsmapper.rs.CollectionMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.rs.SingleFieldMapper;
import cz.jmare.rsmapper.rs.vo.Employee;
import cz.jmare.rsmapper.rs.vo.EmployeeGroup;
import cz.jmare.rsmapper.rs.vo.Group;

public class MapperTest2 {
    static List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

    @BeforeAll
    public static void prepare() {
        HashMap<String, Object> row = null;

        row = new HashMap<>();
        row.put("e_id", 1);
        row.put("e_firstname", "john");
        row.put("eg_id", 1);
        row.put("eg_employee_id", 1);
        row.put("eg_group_id", 1);
        row.put("g_id", 1);
        row.put("g_name", "gr 1");
        list.add(row);

        row = new HashMap<>();
        row.put("e_id", 1);
        row.put("e_firstname", "john");
        row.put("eg_id", 2);
        row.put("eg_employee_id", 1);
        row.put("eg_group_id", 2);
        row.put("g_id", 2);
        row.put("g_name", "gr 2");
        list.add(row);

        // left join employe-group, kde neexistuje zaznam
        row = new HashMap<>();
        row.put("e_id", 2);
        row.put("e_firstname", "jack");
        row.put("eg_id", null);
        row.put("eg_employee_id", null);
        row.put("eg_group_id", null);
        row.put("g_id", null);
        row.put("g_name", null);
        list.add(row);
    }

    @Test
    public void testRemapAuto() {
        CollectionMapper collectionMapper = new CollectionMapper("e_id", new SimpleRemapper(Employee.class), new CollectionFieldMapper("employeeGroupList",
                "eg_id", new SimpleRemapper(EmployeeGroup.class), new SingleFieldMapper("group", "g_id", new SimpleRemapper(Group.class))));

        List<Employee> employees = collectionMapper.remapToList(list, Employee.class);
        Assertions.assertEquals(2, employees.size());
        Assertions.assertEquals(2, employees.get(0).getEmployeeGroupList().size());
        Assertions.assertEquals(null, employees.get(1).getEmployeeGroupList());
    }
}
