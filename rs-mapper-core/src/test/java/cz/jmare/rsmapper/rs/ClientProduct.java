package cz.jmare.rsmapper.rs;

import java.util.Date;
import java.util.List;

public class ClientProduct {
    private long prodInstId;

    private String cluid;

    private Long prodId;
    private Date validFrom;
    private Date validTo;

    private Date sysInsertedDatetime;

    private List<ProductAttributeValue> productAttributeValues;

    public long getProdInstId() {
        return prodInstId;
    }

    public void setProdInstId(long prodInstId) {
        this.prodInstId = prodInstId;
    }

    public String getCluid() {
        return cluid;
    }

    public void setCluid(String cluid) {
        this.cluid = cluid;
    }

    public List<ProductAttributeValue> getProductAttributeValues() {
        return productAttributeValues;
    }

    public void setProductAttributeValues(List<ProductAttributeValue> productAttributeValues) {
        this.productAttributeValues = productAttributeValues;
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Date getSysInsertedDatetime() {
        return sysInsertedDatetime;
    }

    public void setSysInsertedDatetime(Date sysInsertedDatetime) {
        this.sysInsertedDatetime = sysInsertedDatetime;
    }

}
