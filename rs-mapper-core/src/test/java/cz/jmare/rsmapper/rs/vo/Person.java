package cz.jmare.rsmapper.rs.vo;

import cz.jmare.rsmapper.Column;

public class Person {
    @Column(name = "e_id")
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
