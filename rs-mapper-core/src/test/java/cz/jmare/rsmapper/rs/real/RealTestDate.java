package cz.jmare.rsmapper.rs.real;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.dbprofile.PostgresDBProfile;
import cz.jmare.rsmapper.handle.SimpleHandle;
import cz.jmare.rsmapper.postgres.sub.DateTable;

@Disabled
public class RealTestDate {
    private static Connection connection;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        connection = PostgresTestUtil.createRSDataSource().getConnection();
        //DBPopulator.populate(connection, RealTest1.class.getResource("/test-rsmapper.sql"));
    }

    @Test
    //@Disabled
    public void testSinglePostsaver() throws SQLException {
        SimpleHandle handle = new SimpleHandle(connection, new PostgresDBProfile());
        DateTable dateTable = new DateTable();
        dateTable.setADate(LocalDate.now());
        dateTable.setATimenotz(LocalTime.now());
        dateTable.setATimetz(OffsetTime.now());
        dateTable.setTsNozone(LocalDateTime.now());
        dateTable.setTsWithzone(OffsetDateTime.now());
        Relation<DateTable> relation = new Relation<>(DateTable.class);
        relation.save(handle, dateTable);
        
        DateTable dateTable2 = relation.loadOneById(handle, dateTable.getId());
        assertEquals(dateTable.getADate(), dateTable2.getADate());
        assertEquals(dateTable.getATimenotz().withNano(0), dateTable2.getATimenotz().withNano(0));
        assertEquals(dateTable.getATimetz().withNano(0), dateTable2.getATimetz().withNano(0));
        assertEquals(dateTable.getTsNozone().withNano(0), dateTable2.getTsNozone().withNano(0));
        assertEquals(dateTable.getTsWithzone().withNano(0).toInstant(), dateTable2.getTsWithzone().withNano(0).toInstant());
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        connection.close();
    }
}
