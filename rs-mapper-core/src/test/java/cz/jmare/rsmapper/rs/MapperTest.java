package cz.jmare.rsmapper.rs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.rs.CollectionFieldMapper;
import cz.jmare.rsmapper.rs.CollectionMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.rs.SingleFieldMapper;
import cz.jmare.rsmapper.rs.SingleMapper;
import cz.jmare.rsmapper.rs.vo.EmployedPerson;
import cz.jmare.rsmapper.rs.vo.Employee;
import cz.jmare.rsmapper.rs.vo.EmployeeGroup;
import cz.jmare.rsmapper.rs.vo.Group;

public class MapperTest {
    static List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

    @BeforeAll
    public static void prepare() {
        HashMap<String, Object> row = null;

        row = new HashMap<>();
        row.put("e_id", 1);
        row.put("e_firstname", "john");
        row.put("e_age", 53);
        row.put("eg_id", 1);
        row.put("eg_employee_id", 1);
        row.put("eg_group_id", 1);
        row.put("g_id", 1);
        row.put("g_name", "gr 1");
        list.add(row);

        row = new HashMap<>();
        row.put("e_id", 1);
        row.put("e_firstname", "john");
        row.put("e_age", 53);
        row.put("eg_id", 2);
        row.put("eg_employee_id", 1);
        row.put("eg_group_id", 2);
        row.put("g_id", 2);
        row.put("g_name", "gr 2");
        list.add(row);

        row = new HashMap<>();
        row.put("e_id", 2);
        row.put("e_firstname", "jack");
        row.put("e_age", 54);
        row.put("eg_id", 3);
        row.put("eg_employee_id", 2);
        row.put("eg_group_id", 3);
        row.put("g_id", 3);
        row.put("g_name", "gr 3");
        list.add(row);
    }

    @Test
    public void testRemapByHand() {
        CollectionMapper collectionMapper = new CollectionMapper("e_id", m -> {
            Employee employee = new Employee();
            return employee;
        }, new CollectionFieldMapper("employeeGroupList", "eg_id", new Function<Map<String, Object>, EmployeeGroup>() {
            @Override
            public EmployeeGroup apply(Map<String, Object> t) {
                EmployeeGroup employeeGroup = new EmployeeGroup();
                employeeGroup.setEgId((int) t.get("eg_id"));
                return employeeGroup;
            }
        }, new SingleFieldMapper("group", "g_id", new Function<Map<String, Object>, Group>() {
            @Override
            public Group apply(Map<String, Object> t) {
                Group group = new Group();
                group.setgId((int) t.get("g_id"));
                group.setgName((String) t.get("g_name"));
                return group;
            }
        })));

        List<Employee> employees = collectionMapper.remapToList(list, Employee.class);
        Assertions.assertEquals(2, employees.size());
        Assertions.assertEquals(2, employees.get(0).getEmployeeGroupList().size());
        Assertions.assertEquals(1, employees.get(1).getEmployeeGroupList().size());

        SingleMapper singleMapper = new SingleMapper("e_id", m -> {
            Employee employee = new Employee();
            employee.seteFirstname((String) m.get("e_firstname"));
            return employee;
        }, new CollectionFieldMapper("employeeGroupList", "eg_id", new Function<Map<String, Object>, EmployeeGroup>() {
            @Override
            public EmployeeGroup apply(Map<String, Object> t) {
                EmployeeGroup employeeGroup = new EmployeeGroup();
                employeeGroup.setEgId((int) t.get("eg_id"));
                return employeeGroup;
            }
        }, new SingleFieldMapper("group", "g_id", new Function<Map<String, Object>, Group>() {
            @Override
            public Group apply(Map<String, Object> t) {
                Group group = new Group();
                group.setgId((int) t.get("g_id"));
                group.setgName((String) t.get("g_name"));
                return group;
            }
        })));
        Employee employe = singleMapper.remap(list.subList(0, 2), Employee.class);
        Assertions.assertEquals("john", employe.geteFirstname());
    }

    @Test
    public void testRemapAuto() {
        CollectionMapper collectionMapper = new CollectionMapper("e_id", new SimpleRemapper(Employee.class), new CollectionFieldMapper("employeeGroupList",
                "eg_id", new SimpleRemapper(EmployeeGroup.class), new SingleFieldMapper("group", "g_id", new SimpleRemapper(Group.class))));

        List<Employee> employees = collectionMapper.remapToList(list, Employee.class);
        Assertions.assertEquals(2, employees.size());
        Assertions.assertEquals(2, employees.get(0).getEmployeeGroupList().size());
        Assertions.assertEquals(1, employees.get(1).getEmployeeGroupList().size());
        Assertions.assertEquals("gr 1", employees.get(0).getEmployeeGroupList().get(0).getGroup().getgName());

        SingleMapper singleMapper = new SingleMapper("e_id", new SimpleRemapper(Employee.class), new CollectionFieldMapper("employeeGroupList", "eg_id",
                new SimpleRemapper(EmployeeGroup.class), new SingleFieldMapper("group", "g_id", new SimpleRemapper(Group.class))));
        Employee employe = singleMapper.remap(list.subList(0, 2), Employee.class);
        Assertions.assertEquals("john", employe.geteFirstname());
    }
    
    @Test
    public void testRemapAutoInheritance() {
        CollectionMapper collectionMapper = new CollectionMapper("e_id", new SimpleRemapper(EmployedPerson.class), new CollectionFieldMapper("employeeGroupList",
                "eg_id", new SimpleRemapper(EmployeeGroup.class), new SingleFieldMapper("group", "g_id", new SimpleRemapper(Group.class))));

        List<EmployedPerson> employees = collectionMapper.remapToList(list, EmployedPerson.class);
        Assertions.assertEquals(2, employees.size());
        Assertions.assertEquals(2, employees.get(0).getEmployeeGroupList().size());
        Assertions.assertEquals(1, employees.get(1).getEmployeeGroupList().size());
        Assertions.assertEquals("gr 1", employees.get(0).getEmployeeGroupList().get(0).getGroup().getgName());

        SingleMapper singleMapper = new SingleMapper("e_id", new SimpleRemapper(EmployedPerson.class), new CollectionFieldMapper("employeeGroupList", "eg_id",
                new SimpleRemapper(EmployeeGroup.class), new SingleFieldMapper("group", "g_id", new SimpleRemapper(Group.class))));
        EmployedPerson employe = singleMapper.remap(list.subList(0, 2), EmployedPerson.class);
        Assertions.assertEquals("john", employe.geteFirstname());
        Assertions.assertEquals(1, employe.getId());
        Assertions.assertEquals(2, employe.getEmployeeGroupList().size());
    }    
}
