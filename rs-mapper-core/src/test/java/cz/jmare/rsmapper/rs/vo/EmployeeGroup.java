package cz.jmare.rsmapper.rs.vo;

public class EmployeeGroup {
    private int egId;

    private Group group;

    public int getEgId() {
        return egId;
    }

    public void setEgId(int egId) {
        this.egId = egId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
