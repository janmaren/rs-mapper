package cz.jmare.rsmapper.rs.real;

import cz.jmare.rsmapper.Id;

public class ScalarLong {
    @Id
    private Long value;

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
