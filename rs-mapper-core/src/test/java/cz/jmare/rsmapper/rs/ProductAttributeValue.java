package cz.jmare.rsmapper.rs;

import java.util.Date;

public class ProductAttributeValue {
    private long prodInstId;

    private String attrValue;

    private Date validTo;

    public long getProdInstId() {
        return prodInstId;
    }

    public void setProdInstId(long prodInstId) {
        this.prodInstId = prodInstId;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
}
