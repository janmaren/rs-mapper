package cz.jmare.rsmapper.rs.vo;

import java.util.List;

public class EmployedPerson extends Person {
    String eFirstname;
    List<EmployeeGroup> employeeGroupList;

    public String geteFirstname() {
        return eFirstname;
    }

    public void seteFirstname(String efirstname) {
        this.eFirstname = efirstname;
    }

    public List<EmployeeGroup> getEmployeeGroupList() {
        return employeeGroupList;
    }

    public void setEmployeeGroupList(List<EmployeeGroup> employeeGroupList) {
        this.employeeGroupList = employeeGroupList;
    }
}
