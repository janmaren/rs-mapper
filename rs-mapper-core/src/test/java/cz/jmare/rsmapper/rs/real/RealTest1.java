package cz.jmare.rsmapper.rs.real;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.TestUtil;
import cz.jmare.rsmapper.ToOne;
import cz.jmare.rsmapper.exception.SaveException;
import cz.jmare.rsmapper.handle.SimpleHandle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.util.SqlCommandUtil;

@Disabled
public class RealTest1 {
    private static Connection connection;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        connection = PostgresTestUtil.createRSDataSource().getConnection();
        //DBPopulator.populate(connection, RealTest1.class.getResource("/test-rsmapper.sql"));
    }

    @Test
    @Disabled
    public void testSinglePostsaver() throws SQLException {
        SimpleHandle handle = new SimpleHandle(connection);
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        Relation<PersonTest> saver5 = new Relation<>(PersonTest.class).withJoins(
                new ToOne(AccountTest.class, "accountTest", "personId"));
        saver5.save(handle, personTest5);
        saver5.save(handle, personTest5);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        
        Integer origId = accountTest5.getAccountId();
        assertThrows(SaveException.class, () -> {
            accountTest5.setAccountId(10000);
            saver5.save(handle, personTest5);
        });
        accountTest5.setAccountId(origId);
        
        SqlCommandUtil.deleteRecordsFromTable(handle, "account_test", Map.of("account_id", origId));
        assertThrows(SaveException.class, () -> {
            saver5.save(handle, personTest5);
        });
        
        accountTest5.setAccountId(null);
        saver5.save(handle, personTest5);
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        connection.close();
    }
}
