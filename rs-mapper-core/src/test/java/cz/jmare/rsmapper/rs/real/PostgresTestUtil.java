package cz.jmare.rsmapper.rs.real;

import javax.sql.DataSource;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import cz.jmare.util.SimpleDataSource;

public class PostgresTestUtil {
    public static DataSource createRSDataSource() {
        String callerClassName = new Exception().getStackTrace()[1].getClassName();
        int lastIndexOf = callerClassName.lastIndexOf(".");
        if (lastIndexOf != -1) {
            callerClassName = callerClassName.substring(lastIndexOf + 1);
        }
        SimpleDataSource simpleDataSource = new SimpleDataSource("jdbc:postgresql://localhost:5432/postgres", "postgres", "welcome1");
        //simpleDataSource.setSchema("jan");
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.TRACE);
        return simpleDataSource;
    }
}
