package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.entity.ALob;
import cz.jmare.rsmapper.handle.Handle;

public class LobTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
        
        //connection = PostgresTestUtil.createConnection();
        //DBPopulator.populate(connection, H2TestUtil.class.getResource("/test-rsmapper.sql"));
    }

    @Test
    public void testLob() throws SQLException {
        ALob createALob = createALob();
        
        Relation<ALob> relation = new Relation<>(ALob.class);
        relation.save(handle, createALob);
        
        ALob aLob = relation.selectOne(handle, "where _1.a_lob_id=:aLobId", Map.of("aLobId", createALob.aLobId));
        assertArrayEquals(createALob.aBlob, aLob.aBlob);
        assertEquals(createALob.aClob, aLob.aClob);
    }
    
    private ALob createALob() {
        ALob aLob = new ALob();
        aLob.aClob = "nejaky string";
        aLob.aBlob = new byte[]{'a', 'b', 'e'};
        return aLob;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
