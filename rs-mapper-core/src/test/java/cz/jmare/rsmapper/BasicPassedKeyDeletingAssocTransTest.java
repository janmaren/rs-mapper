package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;

import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.PassedPrimaryKey;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.util.TransWrapper;

public class BasicPassedKeyDeletingAssocTransTest {

    @Test
    public void testPassedKey() throws SQLException {
        AccountTest accountTest = createAccountTest();
        PersonTest personTest = new PersonTest();
        personTest.setPersonId(123);
        personTest.setPersonName("osoba5");
        personTest.accountTest = accountTest;
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class)
                .withPrimaryKey(new PassedPrimaryKey("personId")).withAlias("per")
                .withJoins(new ToOne(AccountTest.class, "accountTest", "personId").withAlias("acc").withDeleteOrphan());
        Handle handle = H2TestUtil.createConnection();
        TransWrapper.doInTransaction(handle, c -> {
            relation.save(c, personTest);
            personTest.setPersonName("osoba6");
            accountTest.setAccountId(null);
            relation.save(c, personTest);
            
            PersonTest personTest2 = relation.selectOne(c, "join account_test acc on per.person_id = acc.person_id where per.person_id=:personId", Map.of("personId", personTest.getPersonId()));
            assertEquals("osoba6", personTest2.getPersonName());
            
            assertEquals(1, TestUtil.sqlCount(c, "account_test"));
            assertEquals(1, TestUtil.sqlCount(c, "person_test"));
        });
        handle.close();
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
}
