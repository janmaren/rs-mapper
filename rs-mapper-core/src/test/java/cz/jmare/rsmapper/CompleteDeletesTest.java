package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PermissionTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.postgres.sub.RoleTest;

public class CompleteDeletesTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest5 = new PersonTest();
        personTest5.setPersonName("osoba5");
        personTest5.accountTest = accountTest5;
        
        PermissionTest permissionTest = new PermissionTest();
        permissionTest.setName("name1");
        
        accountTest5.roleTests = new ArrayList<>();
        accountTest5.roleTests.add(new RoleTest());
        accountTest5.roleTests.add(new RoleTest());
        RoleTest roleTest3 = new RoleTest();
        roleTest3.permissionTests = new ArrayList<>();
        PermissionTest pe = new PermissionTest();
        pe.setName("perm1");
        roleTest3.permissionTests.add(pe);
        accountTest5.roleTests.add(roleTest3);
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins( 
                new ToOne(AccountTest.class, "accountTest", "personId").withDeleteOrphan().withJoins( 
                        new ToMany(RoleTest.class, "roleTests", "accountId").withDeleteOrphan().withJoins( 
                                new ToMany(PermissionTest.class, "permissionTests", "roleId").withDeleteOrphan())));
        relation.save(handle, personTest5);
        
        assertEquals(1, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "person_test"));
        assertEquals(1, TestUtil.sqlCount(handle, "permission_test"));
        assertEquals(3, TestUtil.sqlCount(handle, "role_test"));
        
        relation.delete(handle, personTest5);
        
        assertEquals(0, TestUtil.sqlCount(handle, "account_test"));
        assertEquals(0, TestUtil.sqlCount(handle, "person_test"));
        assertEquals(0, TestUtil.sqlCount(handle, "permission_test"));
        assertEquals(0, TestUtil.sqlCount(handle, "role_test"));
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
