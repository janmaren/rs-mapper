package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.util.AssocKeysForPre;

public class AssocKeysForMasterTest {
    @Test
    public void test() {
        AccountTest accountTest = new AccountTest();
        accountTest.setPersonId(123);
        accountTest.personTest = new PersonTest();
        
        AssocKeysForPre assocKeysForMaster1 = new AssocKeysForPre(accountTest.getClass(), PersonTest.class, "personTest", "personId");        
        String leftForeignKeyFieldName = assocKeysForMaster1.getLeftForeignKeyFieldNames().get(0);
        assertEquals("personId", leftForeignKeyFieldName);
        
        AssocKeysForPre assocKeysForMaster2 = new AssocKeysForPre(accountTest.getClass(), PersonTest.class, "personTest");        
        String leftForeignKeyFieldName2 = assocKeysForMaster2.getLeftForeignKeyFieldNames().get(0);
        assertEquals("personId", leftForeignKeyFieldName2);

        AssocKeysForPre assocKeysForMaster3 = new AssocKeysForPre(accountTest.getClass(), PersonTest.class);        
        String leftForeignKeyFieldName3 = assocKeysForMaster3.getLeftForeignKeyFieldNames().get(0);
        assertEquals("personId", leftForeignKeyFieldName3);
    }
}
