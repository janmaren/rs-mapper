package cz.jmare.rsmapper;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.util.AssocFieldsUtil;
import cz.jmare.rsmapper.util.AssocFieldsUtil.FieldAssocAnno;

public class AssocTest {
    @Test
    public void assoc() {
        List<FieldAssocAnno> fieldAssocs = AssocFieldsUtil.getFieldAssocs(PersonTest.class);
        for (FieldAssocAnno fieldAssocAnno : fieldAssocs) {
            System.out.println(fieldAssocAnno);
        }
        
        List<FieldAssocAnno> fieldAssocs2 = AssocFieldsUtil.getFieldAssocs(AccountTest.class);
        System.out.println(fieldAssocs2);
        FieldAssocAnno fieldAssocAnno = fieldAssocs2.get(0);
        Field field = fieldAssocAnno.field;
        ParameterizedType genericType = (ParameterizedType) field.getGenericType();
        System.out.println(genericType.getActualTypeArguments()[0]);
    }
}
