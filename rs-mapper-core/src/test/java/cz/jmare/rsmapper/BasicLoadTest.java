package cz.jmare.rsmapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.postgres.sub.AccountTest;
import cz.jmare.rsmapper.postgres.sub.PersonTest;
import cz.jmare.rsmapper.util.AssocFkValues;

public class BasicLoadTest {
    private static Handle handle;
    
    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        handle = H2TestUtil.createConnection();
    }

    @Test
    public void testSinglePostsaver() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest = new PersonTest();
        personTest.setPersonName("osoba5");
        personTest.accountTest = accountTest5;
        
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins(new ToOne(AccountTest.class, "accountTest", "personId=personId"));
        relation.save(handle, personTest);
        
        PersonTest personTestLoaded = relation.loadOneById(handle, personTest.getPersonId());
        assertEquals(personTest.getPersonId(), personTestLoaded.getPersonId());
    }
    
    @Test
    public void testCustomPostsaver() throws SQLException {
        AccountTest accountTest5 = createAccountTest();
        PersonTest personTest = new PersonTest();
        personTest.setPersonName("osoba5");
        personTest.accountTest = accountTest5;
        
        Relation<AccountTest> relationAccountTest = new Relation<>(AccountTest.class);
        Relation<PersonTest> relation = new Relation<>(PersonTest.class).withJoins(
                new ToOne(AccountTest.class, "accountTest", "personId") {
                    @Override
                    protected Optional<Object> loadAssignedObject(Handle handle, AssocFkValues assocFKValues) {
                        return optionalByCollection(relationAccountTest.loadMany(handle, "where person_id=:personId", Map.of("personId", assocFKValues.leftPrimaryKeyValues.get(0))));
                    }});
        relation.save(handle, personTest);
        
        PersonTest personTestLoaded = relation.loadOneById(handle, personTest.getPersonId());
        assertEquals(personTest.getPersonId(), personTestLoaded.getPersonId());
    }
    
    public static Optional<Object> optionalByCollection(Collection<?> collection) {
        if (collection.size() == 0) {
            return Optional.empty();
        }
        Object object = collection.iterator().next();
        if (collection.size() > 1) {
            throw new IllegalArgumentException("Expected to return max 1 object " + object.getClass().getName() + "but found multiple");
        }
        return Optional.of(object);
    }
    
    private AccountTest createAccountTest() {
        AccountTest accountTest = new AccountTest();
        accountTest.setEmail("john@ss.cz");
        accountTest.setLastLogin(LocalDateTime.now());
        accountTest.setPassword("123");
        accountTest.setUsername("john");
        accountTest.setMarried(true);
        return accountTest;
    }
    
    @AfterAll
    public static void tidy() throws ClassNotFoundException, SQLException {
        handle.close();
    }
}
