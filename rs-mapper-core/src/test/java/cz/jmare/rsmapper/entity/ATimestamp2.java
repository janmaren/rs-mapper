package cz.jmare.rsmapper.entity;

import java.time.LocalDateTime;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.IdType;
import cz.jmare.rsmapper.Table;

@Table(name = "various_types_test")
public class ATimestamp2 {
    @Id(type = IdType.IDENTITY)
    public Long id;
    
    public LocalDateTime aTimestamp;
}
