package cz.jmare.rsmapper.entity;

import java.time.LocalDate;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.IdType;
import cz.jmare.rsmapper.Table;

@Table(name = "various_types_test")
public class ATimestamp3 {
    @Id(type = IdType.IDENTITY)
    public Long id;
    
    public LocalDate aTimestamp;
}
