package cz.jmare.rsmapper.entity;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.IdType;

public class ALob {
    @Id(type = IdType.IDENTITY)
    public Long aLobId;
    
    public String aClob;
    
    public byte[] aBlob;
}
