package cz.jmare.rsmapper.entity;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.IdType;

public class PermissionTest {
    @Id(type = IdType.IDENTITY)
    public Long permissionId;
    
    public String name;
    
    public Long roleId;

    public PermissionTest() {
        super();
    }

    public PermissionTest(String name) {
        this.name = name;
    }
}
