package cz.jmare.rsmapper.entity;

import java.util.Date;

import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.IdType;
import cz.jmare.rsmapper.Table;

@Table(name = "various_types_test")
public class ATimestamp {
    @Id(type = IdType.IDENTITY)
    public Long id;
    
    public Date aTimestamp;
}
