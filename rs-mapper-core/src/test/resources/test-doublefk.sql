CREATE TABLE doubleprim (
    id1          INT        NOT NULL,
    id2          INT        NOT NULL,
    name         VARCHAR(255),
    gain         INT default 5534,
    PRIMARY KEY(id1, id2)
);

CREATE TABLE doublefk (
    id          SERIAL          NOT NULL,
    pid1         INT             NULL,
    pid2         INT             NULL,
    gain         INT default 6622,
    PRIMARY KEY (id),
    FOREIGN KEY (pid1, pid2) REFERENCES doubleprim(id1, id2)
);

commit;
