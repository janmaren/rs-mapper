CREATE TABLE public.person_test (
   person_id serial PRIMARY KEY,
   person_name VARCHAR (50)
);

CREATE TABLE public.account_test (
   account_id serial PRIMARY KEY,
   username VARCHAR (50) NULL,
   password VARCHAR (50) NOT NULL,
   email VARCHAR (355) NOT NULL,
   created_on TIMESTAMP default now(),
   last_login TIMESTAMP,
   married BOOL,
   person_id INTEGER REFERENCES person_test(person_id),
   date1 TIMESTAMP default now(),
   whole INTEGER
);

CREATE TABLE public.role_test (
   role_id serial PRIMARY KEY,
   name VARCHAR(50) NULL,
   account_id INTEGER REFERENCES account_test(account_id)
);

CREATE TABLE public.permission_test (
   permission_id serial PRIMARY KEY,
   name VARCHAR(50) NULL,
   role_id INTEGER REFERENCES role_test(role_id)
);

CREATE TABLE public.various_types_test (
   id serial PRIMARY KEY,
   a_timestamp TIMESTAMP,
   a_time TIME
);

CREATE TABLE public.a_lob (
   a_lob_id serial PRIMARY KEY,
   a_blob BLOB,
   a_clob CLOB
);

commit;
