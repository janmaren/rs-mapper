CREATE TABLE astronaut (
    id          SERIAL          NOT NULL,
    uu_id  UUID            ,
    PRIMARY KEY (id)
);

CREATE TABLE rocket (
    id          SERIAL          NOT NULL,
    weight  decimal            ,
    PRIMARY KEY (id)
);

CREATE TABLE astronaut_rocket (
    astronaut_id  INT       NOT NULL,
    rocket_id  INT       NOT NULL,
    relation_type INT,
    FOREIGN KEY (astronaut_id) REFERENCES astronaut(id),
    FOREIGN KEY (rocket_id) REFERENCES rocket(id),
    PRIMARY KEY (astronaut_id, rocket_id)
);

CREATE TABLE engineer (
  first_name varchar(50) not null,
  last_name varchar(50) not null,
  birth date,
  PRIMARY KEY (first_name, last_name)
);

commit;
