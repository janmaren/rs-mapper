CREATE TABLE date_table (
    id          SERIAL          NOT NULL,
    ts_nozone  timestamp without time zone,
    ts_withzone  timestamp with time zone,
    a_date  date,
    a_timenotz time without time zone,
    a_timetz time with time zone,
    PRIMARY KEY (id)
);