package cz.jmare.rsmapper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.EntityAnnoFetcher.EntityData;
import cz.jmare.rsmapper.active.basic.AbstractRelation;
import cz.jmare.rsmapper.active.basic.Selecter;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.PrimaryKey;
import cz.jmare.rsmapper.rs.CollectionFieldMapper;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.FieldMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.util.AssocFkValues;
import cz.jmare.rsmapper.util.AssocKeysForPost;
import cz.jmare.rsmapper.util.AssocKeysForPostMany;
import cz.jmare.rsmapper.util.ColumnsGenerator;
import cz.jmare.rsmapper.util.FieldAccessUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.PlaceholdersQuery;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.SqlCommandUtil;
import cz.jmare.util.BuildKeyUtil;
import cz.jmare.util.KeyUtil;

/**
 * This table represents a related table which has a foreign key to join multiple (0..N) records 
 */
public class ToMany extends AbstractRelation implements PostJoin {
    private String foreignKeyFieldNameDef;

    private String leftAssocFieldNameDef;
    
    private Query condition;
    
    private AssocKeysForPost assocKeysForPost;

    private boolean deleteOrphan;
    
    public ToMany(Class<?> entityClass) {
        super(entityClass);
    }

    public ToMany(Class<?> entityClass, String leftAssocFieldName) {
        super(entityClass);
        this.leftAssocFieldNameDef = leftAssocFieldName;
    }
    
    public ToMany(Class<?> entityClass, String leftAssocFieldName, String foreignKeyFieldName) {
        super(entityClass);
        this.leftAssocFieldNameDef = leftAssocFieldName;
        this.foreignKeyFieldNameDef = foreignKeyFieldName;
    }
    
    public ToMany(Class<?> entityClass, String leftAssocFieldName, String foreignKeyFieldName, PrimaryKey primaryKey) {
        super(entityClass, primaryKey);
        this.leftAssocFieldNameDef = leftAssocFieldName;
        this.foreignKeyFieldNameDef = foreignKeyFieldName;
    }
    
    @Override
    public void saveByLeftPk(Handle handle, Object leftObject, Map<String, Object> leftPrimKeyNameValueMap) {
        AssocFkValues assocFKValues = assocKeysForPost.getAssocFKValues(leftPrimKeyNameValueMap);
        KeyUtil.checkPrimToFk(leftObject.getClass(), assocFKValues.leftPrimaryKeyValues, entityClass, assocFKValues.foreignKeyFieldNames);
        KeyUtil.checkTypesSame(assocKeysForPost.getForeignKeyFields().stream().map(f -> f.getType()).collect(Collectors.toList()), assocFKValues.leftPrimaryKeyValues.stream().map(o -> o.getClass()).collect(Collectors.toList()));

        Object leftAssocObject = FieldAccessUtil.getValue(leftObject, assocKeysForPost.getLeftAssocField());
        // TODO: when null create empty collection of type Set or List
        if (!(leftAssocObject instanceof Collection<?>)) {
            throw new RsWrongDefException("Not passed a collection but " + (leftAssocObject != null ? leftAssocObject.getClass() : "null") + " in field " + assocKeysForPost.getLeftAssocFieldName());
        }
        
        Collection<?> leftAssocCollection = (Collection<?>) leftAssocObject;
        
        saveAssignedObjects(handle, assocFKValues, leftAssocCollection);
    }

    /**
     * Save objects with foreign key defined as masterKey
     * @param handle
     * @param assocFKValues foreign keys and prim key values
     * @param leftAssocCollection desired collection to be persisted
     */
    protected void saveAssignedObjects(Handle handle, AssocFkValues assocFKValues, Collection<?> leftAssocCollection) {
        // ==== Deleting ====
        // keys from request
        List<List<Object>> passedKeyValues = new ArrayList<List<Object>>();
        for (Object assocItem : leftAssocCollection) {
            List<Object> extractIdValues = FieldsUtil.extractValuesList(assocItem, primaryKey.getFieldNames());
            passedKeyValues.add(extractIdValues);
        }
        
        // persisted keys
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
        String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);
        Collection<LinkedHashMap<String, Object>> compositeColumnValueIds = SqlCommandUtil.selectMultipleVectors(handle, classColumnFields, tablePrefix + tableName, wherePart, assocFKValues.leftPrimaryKeyValues, condition);
        List<List<Object>> persistedKeys = compositeColumnValueIds.stream().map(map -> {
            ArrayList<Object> arrayList = new ArrayList<Object>();
            Iterator<Entry<String, Object>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Entry<String, Object> next = iterator.next();
                arrayList.add(next.getValue());
            }
            return arrayList;
        }).collect(Collectors.toList());

        // diff = persisted - passed
        List<List<Object>> deleteKeys = persistedKeys.stream().filter(k -> {
            return !passedKeyValues.contains(k);
        }).collect(Collectors.toList());
        
        // cascaded deleting
        for (List<Object> deleteKey : deleteKeys) {
            if (deleteOrphan) {
                for (Join saverRelated : joins) {
                    if (saverRelated instanceof PostJoin) {
                        PostJoin postSaver = (PostJoin) saverRelated;
                        postSaver.deleteByLeftPk(handle, entityClass, KeyUtil.mergeKeysValues(primaryKey.getFieldNames(), deleteKey));
                    }
                }
                List<String> fieldNames = primaryKey.getFieldNames();
                LinkedHashMap<String, Object> idParams = new LinkedHashMap<String, Object>();
                for (int i = 0; i < fieldNames.size(); i++) {
                    String fieldName = fieldNames.get(i);
                    String columnName = FieldsUtil.columnNameByFieldName(entityClass, fieldName);
                    idParams.put(columnName, deleteKey.get(i));
                }
                    
                SqlCommandUtil.deleteRecordsFromTable(handle, tablePrefix + tableName, idParams);
            } else {
                ColumnsFields classColumnFieldsForeign = ProcessFieldsUtil.getClassColumnFields(entityClass, assocFKValues.foreignKeyFieldNames);
                SqlCommandUtil.updateWithParams(handle, tablePrefix + tableName, classColumnFieldsForeign.columnNames, KeyUtil.populateNulls(classColumnFieldsForeign.columnNames.size()), classColumnFieldsForeign.columnNames, assocFKValues.leftPrimaryKeyValues);
            }
        }
        
        // ===== Saving =====
        for (Object item : leftAssocCollection) {
            FieldAccessUtil.setValueWithConversion(item, assocFKValues.foreignKeyFields, assocFKValues.leftPrimaryKeyValues);
            save(handle, item);
        }
    }
    
    @Override
    public void deleteByLeftPk(Handle handle, Class<?> leftClass, Map<String, Object> leftPrimKeyNameValueMap) {
        AssocFkValues assocFKValues = assocKeysForPost.getAssocFKValues(leftPrimKeyNameValueMap);
        if (deleteOrphan) {
	        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
            String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);
            Collection<LinkedHashMap<String, Object>> compositeIds = SqlCommandUtil.selectMultipleVectors(handle, classColumnFields, tablePrefix + tableName, wherePart, assocFKValues.leftPrimaryKeyValues, condition);

	        for (LinkedHashMap<String, Object> compositeId : compositeIds) {
	            for (Join saverRelated : joins) {
	                if (saverRelated instanceof PostJoin) {
	                    PostJoin postSaver = (PostJoin) saverRelated;
	                    postSaver.deleteByLeftPk(handle, entityClass, KeyUtil.mergeKeysValues(primaryKey.getFieldNames(), KeyUtil.values(compositeId)));
	                }
	            }
	            SqlCommandUtil.deleteRecordsFromTable(handle, tablePrefix + tableName, compositeId);
	        }
        } else {
            ColumnsFields classColumnFieldsForeign = ProcessFieldsUtil.getClassColumnFields(entityClass, assocFKValues.foreignKeyFieldNames);
            SqlCommandUtil.updateWithParams(handle, tablePrefix + tableName, classColumnFieldsForeign.columnNames, KeyUtil.populateNulls(classColumnFieldsForeign.columnNames.size()), classColumnFieldsForeign.columnNames, assocFKValues.leftPrimaryKeyValues);
        }
    }

    @Override
    public FieldMapper toFieldMapper(Class<?> leftClass) {
        List<String> fieldNames = primaryKey.getFieldNames();
        Function<Map<String, Object>, Object> keyFunction = BuildKeyUtil.getKeyFunction(fieldNames, entityClass, alias);
        return new CollectionFieldMapper(assocKeysForPost.getLeftAssocFieldName(),
                keyFunction,
                new SimpleRemapper(entityClass, "_" + alias), AbstractRelation.toFieldMappers(joins, entityClass));
    }
    
    protected void initBasicByAnno() {
        EntityData entityData = EntityAnnoFetcher.initMeta(entityClass, primaryKey, tableName);
        this.primaryKey = entityData.primaryKey;
        this.tableName = entityData.tableName;
    }

    @Override
    public void loadByLeftPk(Handle handle, Object leftObject, Map<String, Object> leftPrimKeyNameValueMap) {
        AssocFkValues assocFKValues = assocKeysForPost.getAssocFKValues(leftPrimKeyNameValueMap);
        List<Object> list = loadAssignedObjects(handle, assocFKValues);
        Field leftAssocField = assocKeysForPost.getLeftAssocField();
        FieldAccessUtil.setValueWithConversion(leftObject, leftAssocField, list);
        
        for (Object object : list) {
            //LinkedHashMap<String, Object> key = FieldsUtil.extractColumnsValuesMap(object, primaryKey.getFieldNames());
            for (Join join : joins) {
                if (join instanceof PreJoin) {
                    PreJoin preJoin = (PreJoin) join;
                    preJoin.loadPrimByLeftFk(handle, object);        
                } else if (join instanceof PostJoin) {
                    PostJoin postJoin = (PostJoin) join;
                    Map<String, Object> pkFieldsValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
                    postJoin.loadByLeftPk(handle, object, pkFieldsValues);
                }
            }
        }
    }

    /**
     * Save objects with foreign key defined as masterKey
     * @param handle
     * @param assocFKValues pair(s) foreign key name - value
     * @param object
     */
    protected List<Object> loadAssignedObjects(Handle handle, AssocFkValues assocFKValues) {
        Selecter selecter = new Selecter(tablePrefix + tableName, primaryKey.getFieldNames());
        String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);

        List<Object> placeholderValues = new ArrayList<Object>(assocFKValues.leftPrimaryKeyValues);
        if (condition != null) {
            PlaceholdersQuery query = SqlCommandUtil.toPlaceholdersQuery(condition);
            wherePart += " AND " + query.getQueryString();
            placeholderValues.addAll(query.getValues());
        }
        
        List<Object> list = selecter.queryByPlaceholders(handle, entityClass, wherePart, placeholderValues);
        return list;
    }
    
    /**
     * Condition to be added into where part to filter out records to be deleted or updated
     * @param condition without table alias, for example: employee_type=123
     * @return
     */
    public ToMany withCondition(String condition) {
        this.condition = new SimpleQuery(condition);
        return this;
    }
    
    /**
     * Condition to be added into where part to filter out records to be deleted or updated
     * @param condition without table alias, for example: employee_type=123
     * @return
     */
    public ToMany withCondition(Query condition) {
        this.condition = condition;
        return this;
    }
    
    /**
     * Set primary key
     */
    public ToMany withPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }
    
    /**
     * Set human readable alias to replace default alias, like _1, _2,...
     * @param alias
     * @return
     */
    public ToMany withAlias(String alias) {
        this.customAlias = alias;
        return this;
    }

    /**
     * Set joins, ToOne, or ToOnePrim or ToMany. The appropriate association will be also loaded.
     * @param joins
     * @return
     */
    public ToMany withJoins(Join... joins) {
        this.joins = joins;
        return this;
    }
    
    /**
     * Set names of fields omitted in insert
     * @param nonInsertableFieldNames
     * @return
     */
    public ToMany withNonInsertableFields(String... nonInsertableFieldNames) {
        this.nonInsertableFieldNames = nonInsertableFieldNames;
        return this;
    }

    /**
     * Set names of fields omitted in update
     * @param nonUpdatableFieldNames
     * @return
     */
    public ToMany withNonUpdatableFields(String... nonUpdatableFieldNames) {
        this.nonUpdatableFieldNames = nonUpdatableFieldNames;
        return this;
    }
    
    /**
     * When deleting referencing record, delete also this
     * @return
     */
    public ToMany withDeleteOrphan() {
        this.deleteOrphan = true;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in insert.
     * @return
     */
    public ToMany withNonInsertableNulls() {
        this.nonInsertableNulls = true;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in update.
     * @return
     */
    public ToMany withNonUpdatableNulls() {
        this.nonUpdatableNulls = true;
        return this;
    }
    
    /**
     * A schema, catalog or database prefix before a table.
     * @param tablePrefix
     * @return
     */
    public ToMany withTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
        if (!tablePrefix.endsWith(".")) {
            this.tablePrefix += ".";
        }
        return this;
    }
    
    @Override
    public void init(ColumnsGenerator columnsGenerator, Class<?> leftClass) {
        alias = columnsGenerator.addClass(entityClass, customAlias);
        for (Join saverRelated : joins) {
            saverRelated.init(columnsGenerator, entityClass);
        }
        
        initBasicByAnno();
        
        if (primaryKey == null) {
            throw new RsWrongDefException("Class " + entityClass.getCanonicalName() + " has no id field defined although it is used in ToMany join.");
        }
        
        assocKeysForPost = new AssocKeysForPostMany(leftClass, entityClass,
                leftAssocFieldNameDef, foreignKeyFieldNameDef);
    }
}
