package cz.jmare.rsmapper.rs;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ColumnsFields {
    public List<String> columnNames;
    public List<Field> fields;
    
    public ColumnsFields(List<String> columnNames, List<Field> fields) {
        if (columnNames.size() != fields.size()) {
            throw new RuntimeException("Different number of fields " + fields + " vs column names " + columnNames);
        }
        this.columnNames = columnNames;
        this.fields = fields;
    }

    public ColumnsFields() {
        this.columnNames = new ArrayList<String>();
        this.fields = new ArrayList<Field>();
    }
}
