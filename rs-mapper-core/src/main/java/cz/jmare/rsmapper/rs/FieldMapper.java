package cz.jmare.rsmapper.rs;

import java.util.List;
import java.util.Map;

/**
 * Interface represents an inner mapper for associations which typically arises with each join in the select command.  
 */
public interface FieldMapper {
    /**
     * Remap objectRow and the result assign to appropriate field in destinationObject
     * @param objectRow all rows needed to create the associations unless there are next joins. In this case next grouping must be done and unique identity function must be present in implementation class
     * @param destinationObject object which has a field to be populated. The field name should be present in implementation class.
     */
    void remapField(List<Map<String, Object>> objectRow, Object destinationObject);

}
