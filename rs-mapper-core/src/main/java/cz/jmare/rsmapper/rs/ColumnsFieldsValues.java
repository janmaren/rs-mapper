package cz.jmare.rsmapper.rs;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ColumnsFieldsValues {
    public List<String> columnNames = new ArrayList<String>();
    public List<Object> values = new ArrayList<Object>();
    public List<Field> fields = new ArrayList<Field>();
    
    public ColumnsFieldsValues(List<String> columnNames, List<Field> fields, List<Object> values) {
        if (columnNames.size() != fields.size()) {
            throw new RuntimeException("Different number of fields " + fields + " vs column names " + columnNames);
        }
        if (columnNames.size() != values.size()) {
            throw new RuntimeException("Different number of values " + values + " vs column names " + columnNames);
        }
        this.columnNames = columnNames;
        this.fields = fields;
        this.values = values;
    }
    
    public ColumnsFieldsValues subtract(ColumnsFieldsValues subtrahent) {
        List<String> columnNames = new ArrayList<String>();
        List<Object> values = new ArrayList<Object>();
        List<Field> fields = new ArrayList<Field>();
        
        for (int i = 0; i < this.columnNames.size(); i++) {
            if (!subtrahent.columnNames.contains(this.columnNames.get(i))) {
                columnNames.add(this.columnNames.get(i));
                values.add(this.values.get(i));
                fields.add(this.fields.get(i));
            }
        }
        
        return new ColumnsFieldsValues(columnNames, fields, values);
    }
    
    public ColumnsFieldsValues add(ColumnsFieldsValues addent) {
        List<String> columnNames = new ArrayList<String>();
        List<Object> values = new ArrayList<Object>();
        List<Field> fields = new ArrayList<Field>();
        
        for (int i = 0; i < this.columnNames.size(); i++) {
            columnNames.add(this.columnNames.get(i));
            values.add(this.values.get(i));
            fields.add(this.fields.get(i));
        }
        
        for (int i = 0; i < addent.columnNames.size(); i++) {
            if (!this.columnNames.contains(addent.columnNames.get(i))) {
                columnNames.add(addent.columnNames.get(i));
                values.add(addent.values.get(i));
                fields.add(addent.fields.get(i));
            }
        }
        
        return new ColumnsFieldsValues(columnNames, fields, values);
    }
    
    public boolean isEmpty() {
        return columnNames.isEmpty();
    }
}
