package cz.jmare.rsmapper.rs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.util.Colutil;

/**
 * Remap to result (root) collection
 */
public class CollectionMapper {
    /**
     * Funkce, ktera na zaklade mapy reprezentujici zaznam vrati instanci objektu
     */
    private Function<Map<String, Object>, ?> remapObjectFunction;
    /**
     * Mapovani asociaci
     */
    private FieldMapper[] fieldMappers;
    /**
     * Shodne zaznamy, ktere vrati tato funkce maji patrit do stejne skupiny. Funkce nesmi vratit null
     */
    private Function<Map<String, Object>, Object> keyFunction;

    /**
     * Remap collection.
     * @param columnName column name by which is the resultset grouped to produce just one object of result collection
     * @param remapObjectFunction populate result object by row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */
    public CollectionMapper(String columnName, Function<Map<String, Object>, Object> remapObjectFunction, FieldMapper... fieldMappers) {
        this.keyFunction = new Function<Map<String, Object>, Object>(){
            @Override
            public Object apply(Map<String, Object> t) {
                Object object = t.get(columnName);
                if (object == null) {
                    if (!t.containsKey(columnName)) {
                        throw new RsWrongDefException("Column " + columnName + " not exists in resultset");
                    }
                }
                return object;
            }

            public String toString() {
                return columnName;
            }
        };
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    /**
     * Remap collection.
     * @param keyFunction identity object by which is the resultset grouped to produce just one object of result collection
     * @param remapObjectFunction
     * @param fieldMappers
     */
    public CollectionMapper(Function<Map<String, Object>, Object> keyFunction, Function<Map<String, Object>, Object> remapObjectFunction, FieldMapper... fieldMappers) {
        this.keyFunction = keyFunction;
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }
    
    /**
     * Remap resultset represented as map and return it as List
     * @param <T>
     * @param objectRow
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> remapToList(List<Map<String, Object>> objectRow, Class<T> clazz) {
        Collection<Object> resultCollection = new ArrayList<Object>();
        if (objectRow.size() > 0) {
            remap(objectRow, resultCollection);
        }
        return (List<T>) resultCollection;
    }

    /**
     * Remap resultset represented as map and return it as Set
     * @param <T>
     * @param objectRow
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> Set<T> remapToSet(List<Map<String, Object>> objectRow, Class<T> clazz) {
        Collection<Object> resultCollection = new HashSet<Object>();
        if (objectRow.size() > 0) {
            remap(objectRow, resultCollection);
        }
        return (Set<T>) resultCollection;
    }

    private <T> void remap(List<Map<String, Object>> objectRow, Collection<Object> resultCollection) {
        if (keyFunction.apply(objectRow.iterator().next()) == null) {
            throw new RsWrongDefException("There is column '" + keyFunction + "' with null. " + objectRow);
        }
        
        Map<Object, List<Map<String, Object>>> groupedRecords = Colutil.getGroups(objectRow, keyFunction);
        Collection<List<Map<String, Object>>> groupedRecordsWithoutKey = (Collection<List<Map<String, Object>>>) groupedRecords.values();

        for (List<Map<String, Object>> mapList : groupedRecordsWithoutKey) {
            Map<String, Object> next = mapList.iterator().next();
            Object item = remapObjectFunction.apply(next);
            for (FieldMapper fieldMapper : fieldMappers) {
                fieldMapper.remapField(mapList, item);
            }
            resultCollection.add(item);
        }
    }
    
    /**
     * Remap.<br>
     * Note: it doesn't return a collection but it's expected the data are attached to existing object, see {@link cz.jmare.rsmapper.rs.SimpleRemapper#withObjectProvider(Function)}
     * @param objectRow
     */
    public void remap(List<Map<String, Object>> objectRow) {
        if (objectRow.size() == 0) {
            return;
        }
        if (keyFunction.apply(objectRow.iterator().next()) == null) {
            throw new RsWrongDefException("There is column '" + keyFunction + "' with null. " + objectRow);
        }

        Map<Object, List<Map<String, Object>>> groupedRecords = objectRow.stream().collect(Collectors.groupingBy(keyFunction));
        Collection<List<Map<String, Object>>> groupedRecordsWithoutKey = (Collection<List<Map<String, Object>>>) groupedRecords.values();

        for (List<Map<String, Object>> mapList : groupedRecordsWithoutKey) {
            Map<String, Object> next = mapList.iterator().next();
            Object item = remapObjectFunction.apply(next);
            for (FieldMapper fieldMapper : fieldMappers) {
                fieldMapper.remapField(mapList, item);
            }
        }
    }    
}
