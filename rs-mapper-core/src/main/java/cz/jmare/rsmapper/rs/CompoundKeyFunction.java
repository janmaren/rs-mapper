package cz.jmare.rsmapper.rs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import cz.jmare.rsmapper.exception.RsWrongDefException;

/**
 * It creates a {@link CompoundKey}. Column names in constructor should make an unique identification of object.<br>
 */
public class CompoundKeyFunction implements Function<Map<String, Object>, Object> {
    private String[] columns;

    public CompoundKeyFunction(String... columns) {
        super();
        this.columns = columns;
    }
    
    public CompoundKeyFunction(List<String> columns) {
        super();
        this.columns = columns.toArray(new String[columns.size()]);
    }
    
    @Override
    public Object apply(Map<String, Object> map) {
        Map<String, Object> keyValueMap = new HashMap<String, Object>();
        boolean nonnullExists = false;
        for (String column : columns) {
            Object value = map.get(column);
            if (value == null) {
                if (!map.containsKey(column)) {
                    throw new RsWrongDefException("There is no column '" + column + "' in result set.");
                }
                return null;
            } else {
                nonnullExists = true;
            }
            keyValueMap.put(column, value);
        }
        if (!nonnullExists) {
            return null;
        }
        return new CompoundKey(keyValueMap);
    }
}
