package cz.jmare.rsmapper.rs;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.type.BasicTypeDecider;
import cz.jmare.rsmapper.util.CamelUtil;
import cz.jmare.rsmapper.util.ClassUtil;
import cz.jmare.rsmapper.util.FieldAccessUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.GlobalRSConfig;

/**
 * It remaps just one row of resultset to result object (without associations).<br>
 * By default it expects the column name same as field but field name is camel case and column name contains underscores.
 * E.g. field lastEmployeeName corresponds to column last_employee_name.
 * For different mapping names the annotation {@link Column#name()} must be used. For column name suffixes present in resultset it's possible
 * to use {@link #withColumnNameSuffix(String)}, in this case the right column in resultset is used and suffix is truncated to determine the right field name.<br>
 * <br>
 * By default it tries to populate all basic fields but when not found corresponding column in resultset the exception is thrown unless the annotation 
 * {@link Column#nonstrictRead()} is true. Basic fields are:<br>
 * Integer, Short, Long, Double, Float, Boolean, Byte, Character, String, java.util.Date, Calendar, java.sql.Date, Time, Timestamp, BigInteger, BigDecimal, LocalDate, LocalDateTime and all primitive types
 * unless the transient modifier used.
 * <br>
 * When needed to do conversion into other than basic type use the {@link #withCustomMapper(BiConsumer)}. E.g. varchar column uuid may be converted by line
 * <pre>((SomeClass) obj).setUuid(UUID.fromString(map.get("uuid")))</pre>Be careful because in case of custom mapping the column suffix must be used
 * regardless the {@link #withColumnNameSuffix(String)}
 * <br>
 * Associations (both single and collections) are not remapped here but rather later by result classes like {@link CollectionFieldMapper} or {@link SingleFieldMapper} or by a custom functional class.
 * <br>
 */
public class SimpleRemapper implements Function<Map<String, Object>, Object> {
    private Class<?> clazz;

    private List<FieldColumn> fieldColumns;

    private BiConsumer<Map<String, Object>, Object> customMapper;

    /**
     * Suffix added to all columns, it's in fact '_' + table alias. If the table alias is _1 the column name will be column__1
     */
    private String columnNameSuffix;
    
    private Function<Map<String, Object>, Object> objectProvider;

    public SimpleRemapper(Class<?> clazz) {
        super();
        this.clazz = clazz;
    }
    
    public SimpleRemapper(Class<?> clazz, String columnNameSuffix) {
        super();
        this.clazz = clazz;
        this.columnNameSuffix = columnNameSuffix;
    }

    public SimpleRemapper withColumnNameSuffix(String columnNameSuffix) {
        this.columnNameSuffix = columnNameSuffix;
        return this;
    }

    public SimpleRemapper withCustomMapper(BiConsumer<Map<String, Object>, Object> customMapper) {
        this.customMapper = customMapper;
        return this;
    }
    
    public SimpleRemapper withObjectProvider(Function<Map<String, Object>, Object> objectProvider) {
        this.objectProvider = objectProvider;
        return this;
    }
    
    protected void init() {
        BasicTypeDecider basicTypeDecider = GlobalRSConfig.getInstance().getBasicTypeDecider();
        Field[] allFields = ClassUtil.getAllFieldsSupportInheritance(clazz);
        List<Field> fieldsList = Arrays.asList(allFields);
        List<Field> fields = fieldsList.stream().filter(f -> {
            int modifiers = f.getModifiers();
            return !Modifier.isStatic(modifiers) && basicTypeDecider.isBasicType(f.getType()) && !Modifier.isTransient(modifiers);
        }).collect(Collectors.toList());
        fieldColumns = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            fieldColumns.add(new FieldColumn(field, CamelUtil.decamelString(field.getName()), FieldsUtil.findColumnAnnotation(field)));
        }
    }

    @Override
    public Object apply(Map<String, Object> map) {
        if (fieldColumns == null) {
            init();
        }
        
        Object object;
        
        if (objectProvider != null) {
            object = objectProvider.apply(map);
            if (object == null) {
                throw new RuntimeException("Not found object of type " + clazz.getCanonicalName() + " which corresponds to row " + map);
            }
        } else {
            try {
                Constructor<?> cons = clazz.getConstructor();
                object = cons.newInstance();
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                throw new RsWrongDefException("Unable to create instance of " + clazz, e);
            }
        }

        for (FieldColumn fieldColumn : fieldColumns) {
            Field field = fieldColumn.field;
            
            String columnName = fieldColumn.getName() + (columnNameSuffix != null ? columnNameSuffix : "");
            Object value = map.get(columnName);
            if (value == null) {
                if (!map.containsKey(columnName)) {
                    //if (!nonstrictRead && !(fieldColumn.remapAnnotation != null && fieldColumn.remapAnnotation.nonstrictRead())) {
                        throw new RuntimeException(
                                "Unable to remap to '" + clazz.getName() + "." + field.getName() + "' because resultset doesn't contain column '" + columnName + "'");
                    //}
                    //continue;
                }
            }
            FieldAccessUtil.setValueWithConversion(object, field, value);
        }

        if (customMapper != null) {
            customMapper.accept(map, object);
        }

        return object;
    }

    class FieldColumn {
        Field field;
        String columnName;
        Column remapAnnotation;
        public FieldColumn(Field field, String columnName, Column remapAnnotation) {
            super();
            this.field = field;
            this.columnName = columnName;
            this.remapAnnotation = remapAnnotation;
        }
        
        public String getName() {
            if (remapAnnotation != null && !"".equals(remapAnnotation.name())) {
                return remapAnnotation.name();
            }
            return columnName;
        }
    }
}
