package cz.jmare.rsmapper.rs;

import java.util.Map;

/**
 * Keys and values to be used to compare with other object. Typically all present columns make a compound key and are use in equals method.
 */
public class CompoundKey {
    private Map<String, Object> keyValueMap;

    public CompoundKey(Map<String, Object> keyValueMap) {
        super();
        this.keyValueMap = keyValueMap;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keyValueMap == null) ? 0 : keyValueMap.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CompoundKey other = (CompoundKey) obj;
        if (keyValueMap == null) {
            if (other.keyValueMap != null)
                return false;
        } else if (!keyValueMap.equals(other.keyValueMap))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CompoundKey [keyValueMap=" + keyValueMap + "]";
    }
}
