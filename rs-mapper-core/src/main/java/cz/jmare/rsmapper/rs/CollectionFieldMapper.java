package cz.jmare.rsmapper.rs;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.util.Colutil;

/**
 * Remap collection and assign to field
 */
public class CollectionFieldMapper implements FieldMapper {
    /**
     * Nazev fieldu v rodicovskem objektu, do ktereho se ma priradit vysledek tohoto objektu
     */
    private String fieldName;
    /**
     * Shodne zaznamy, ktere vrati tato funkce maji patrit do stejne skupiny. Muze taky vratit null - pak se pres ni vubec negroupuje a takovy zaznam se ignoruje
     */
    private Function<Map<String, Object>, Object> keyFunction;
    /**
     * Zahlasi se chyba, pokud je primarni klic null - napr. u LEFT joinu jsou vyplneny sloupce mastera, ale jiz ne tohoto slave. Asociace je pak prazdna a sloupce jsou null.
     * Pokud je true, tak se zahlasi chyba
     */
    private boolean strictKey;
    /**
     * Funkce, ktera na zaklade mapy reprezentujici zaznam vrati instanci objektu
     */
    private Function<Map<String, Object>, ?> remapObjectFunction;
    /**
     * Mapovani asociaci
     */
    private FieldMapper[] fieldMappers;

    /**
     * Remap collection and assign to field
     * @param fieldName collection field name to be populated
     * @param columnName grouping column name - all records with same value grouped by this column will produce just one object
     * @param remapObjectFunction function to return one populated object based on one row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */
    public CollectionFieldMapper(String fieldName, String columnName, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.fieldName = fieldName;
        this.keyFunction = new Function<Map<String, Object>, Object>(){
            @Override
            public Object apply(Map<String, Object> t) {
                Object object = t.get(columnName);
                if (object == null) {
                    if (!t.containsKey(columnName)) {
                        throw new RsWrongDefException("Column " + columnName + " not exists in resultset");
                    }
                }
                return object;
            }

            public String toString() {
                return columnName;
            }
        };
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    /**
     * Remap collection and assign to a field
     * @param fieldName collection field name to be populated
     * @param keyFunction create identity object to be used for grouping
     * @param remapObjectFunction function to return one populated object based on one row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */    
    public CollectionFieldMapper(String fieldName, Function<Map<String, Object>, Object> keyFunction, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.fieldName = fieldName;
        this.keyFunction = keyFunction;
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    /**
     * Remap collection and assign to a field.<br>
     * Note: missing a column or identity function for grouping - may be used e.g. when rest of resultset contains number of rows same as number of objects in result collection
     * @param fieldName collection field name to be populated
     * @param remapObjectFunction function to return one populated object based on one row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */       
    public CollectionFieldMapper(String fieldName, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.fieldName = fieldName;
        this.keyFunction = new AllMatchFunction(); // remapovavat se bude mapa, ktera jiz obsahuje radky jednoho objektu, proto se berou vsechny jako shodne a vznikne z nich jedna skupina
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    @Override
    public void remapField(List<Map<String, Object>> objectRow, Object destinationObject) {
        Field declaredField;
        try {
            declaredField = destinationObject.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException | SecurityException e1) {
            throw new RsWrongDefException("No field '" + fieldName + "' exists in '" + destinationObject.getClass().getName() + "'");
        }

        if (keyFunction.apply(objectRow.iterator().next()) == null) {
            if (strictKey) {
                throw new RsWrongDefException("Record doesn't exist because key column '" + keyFunction + "' returns null. " + objectRow.iterator().next());
            }
            return;
        }

        Collection<Object> resultCollection = null;
        if (List.class.isAssignableFrom(declaredField.getType())) {
            resultCollection = new ArrayList<Object>();
        } else if (Set.class.isAssignableFrom(declaredField.getType())) {
            resultCollection = new HashSet<>();
        } else {
            throw new RsWrongDefException("Not supported collection type '" + declaredField.getType().getName() + "' in '" + destinationObject.getClass() + "." + fieldName + "'");
        }

        populateResultCollectionByGrouping(objectRow, resultCollection);

        try {
            declaredField.setAccessible(true);
            declaredField.set(destinationObject, resultCollection);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RsWrongDefException("Unable to remap " + resultCollection.getClass().getName() + " to " + destinationObject.getClass() + "." + fieldName, e);
        }
    }

    private void populateResultCollectionByGrouping(List<Map<String, Object>> objectRow, Collection<Object> resultCollection) {
        Map<Object, List<Map<String, Object>>> groupedRecords = Colutil.getGroups(objectRow, keyFunction);
        Collection<List<Map<String, Object>>> groupedRecordsWithoutKey = (Collection<List<Map<String, Object>>>) groupedRecords.values();
        
        for (List<Map<String, Object>> mapList : groupedRecordsWithoutKey) {
            Map<String, Object> next = mapList.iterator().next();
            Object item = remapObjectFunction.apply(next);
            for (FieldMapper fieldMapper : fieldMappers) {
                fieldMapper.remapField(mapList, item);
            }
            resultCollection.add(item);
        }
    }

    public CollectionFieldMapper withStrictKey() {
        this.strictKey = true;
        return this;
    }
}
