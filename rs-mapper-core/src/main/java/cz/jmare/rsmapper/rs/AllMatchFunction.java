package cz.jmare.rsmapper.rs;

import java.util.Map;
import java.util.function.Function;

/**
 * Return object the matches any other object.
 */
public class AllMatchFunction implements Function<Map<String, Object>, Object> {
    @Override
    public Object apply(Map<String, Object> t) {
        return new AllMatch();
    }
}
