package cz.jmare.rsmapper.rs;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import cz.jmare.rsmapper.exception.ManyRecordsException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.util.Colutil;

/**
 * Remap to result (root) object to be assigned to a field. 
 * When multiple object would be returned the {@link ManyRecordsException} will be throwed.
 */
public class SingleFieldMapper implements FieldMapper {
    /**
     * Nazev fieldu v rodicovskem objektu, do ktereho se ma priradit vysledek tohoto objektu
     */
    private String fieldName;
    /**
     * Shodne zaznamy, ktere vrati tato funkce maji patrit do stejne skupiny
     */
    private Function<Map<String, Object>, Object> keyFunction;
    /**
     * Funkce, ktera na zaklade mapy reprezentujici zaznam vrati instanci objektu
     */
    private Function<Map<String, Object>, ?> remapObjectFunction;
    /**
     * Mapovani asociaci
     */
    private FieldMapper[] fieldMappers;

    /**
     * Remap object and assign to a field
     * @param fieldName field name to be populated
     * @param columnName grouping column name - all records with same value grouped by this column will produce just one object
     * @param remapObjectFunction function to return one populated object based on one row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */    
    public SingleFieldMapper(String fieldName, String columnName, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.fieldName = fieldName;
        this.keyFunction = new Function<Map<String, Object>, Object>() {
            @Override
            public Object apply(Map<String, Object> t) {
                Object object = t.get(columnName);
                if (object == null) {
                    if (!t.containsKey(columnName)) {
                        throw new RsWrongDefException("Column " + columnName + " not exists in resultset");
                    }
                }
                return object;
            }

            public String toString() {
                return columnName;
            }
        };
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    /**
     * Remap object and assign to a field
     * @param fieldName collection field name to be populated
     * @param keyFunction create identity object to be used for grouping
     * @param remapObjectFunction function to return one populated object based on one row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */        
    public SingleFieldMapper(String fieldName, Function<Map<String, Object>, Object> keyFunction, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.fieldName = fieldName;
        this.keyFunction = keyFunction;
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    /**
     * Remap collection and assign to a field.<br>
     * Note: missing a column or identity function for grouping - may be used e.g. when rest of resultset contains number of rows same as number of objects in result collection
     * @param fieldName collection field name to be populated
     * @param remapObjectFunction function to return one populated object based on one row of resultset
     * @param fieldMappers joined tables represented by {@link CollectionFieldMapper} or {@link SingleFieldMapper}
     */        
    public SingleFieldMapper(String fieldName, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.fieldName = fieldName;
        this.keyFunction = new AllMatchFunction(); // remapovavat se bude mapa, ktera jiz obsahuje radky jednoho objektu, proto se berou vsechny jako shodne a vznikne z nich jedna skupina
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    @Override
    public void remapField(List<Map<String, Object>> objectRow, Object destinationObject) {
        if (keyFunction.apply(objectRow.iterator().next()) == null) {
            return;
        }

        Object resultObject = getResultObjectByGrouping(objectRow, destinationObject);
        if (resultObject == null) {
            throw new RsWrongDefException("Object instance not created to set to '" + destinationObject.getClass().getName() + "." + fieldName + "'");
        }

        try {
            Field declaredField = destinationObject.getClass().getDeclaredField(fieldName);
            declaredField.setAccessible(true);
            declaredField.set(destinationObject, resultObject);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RsWrongDefException("Unable to remap " + resultObject.getClass().getName() +" to '" + destinationObject.getClass().getName() + "." + fieldName + "'", e);
        } catch (NoSuchFieldException e) {
            throw new RsWrongDefException("Unable to remap " + resultObject.getClass().getName() +" to '" + destinationObject.getClass().getName() + "." + fieldName + "' because such field doesn't exist", e);
        }
    }

    private Object getResultObjectByGrouping(List<Map<String, Object>> objectRow, Object destinationObject) {
        Map<Object, List<Map<String, Object>>> groupedRecords = Colutil.getGroups(objectRow, keyFunction);
        Collection<List<Map<String, Object>>> groupedRecordsWithoutKey = (Collection<List<Map<String, Object>>>) groupedRecords.values();
        
        if (groupedRecordsWithoutKey.size() > 1) {
            String message = createIdsListString(groupedRecords);
            throw new RsWrongDefException("Expected scalar (one object) but result data contain vector of '" + destinationObject.getClass().getName() + "'. Many objects cannot be assigned to " + fieldName + ". Id of records: " + message + ". Make sure the join clause connects the right keys. Also make sure database doesn't contain duplicit records.");
        }
        Object resultObject = null;
        if (groupedRecordsWithoutKey.size() == 1) {
            List<Map<String, Object>> records = groupedRecordsWithoutKey.iterator().next();
            Map<String, Object> record = records.iterator().next();
            resultObject = remapObjectFunction.apply(record);
            for (FieldMapper fieldMapper : fieldMappers) {
                fieldMapper.remapField(records, resultObject);
            }
        }
        return resultObject;
    }

    public static String createIdsListString(Map<Object, List<Map<String, Object>>> groupedRecords) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Object, List<Map<String, Object>>> entry : groupedRecords.entrySet()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(entry.getKey());
        }
        return sb.toString();
    }
}
