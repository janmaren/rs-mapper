package cz.jmare.rsmapper.type;

public interface BasicTypeDecider {
    boolean isBasicType(Class<?> clazz);
}
