package cz.jmare.rsmapper.type;

import java.util.Collection;

public class DefaultBasicTypeDecider implements BasicTypeDecider {
    
    public boolean isBasicType(Class<?> clazz) {
        if (Collection.class.isAssignableFrom(clazz)) {
            return false;
        }

        if (clazz.isArray() && clazz.getComponentType().isPrimitive()) {
            return true;
        }

        if (clazz.isPrimitive()) {
            return true;
        }

        String packageName = clazz.getPackageName();
    
        return packageName.equals("java.lang") || packageName.equals("java.sql") || packageName.equals("java.math") || packageName.equals("java.time") || packageName.equals("java.util");
    }
}
