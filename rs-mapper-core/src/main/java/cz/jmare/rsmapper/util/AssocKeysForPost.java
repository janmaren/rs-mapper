package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.util.AssocFieldsUtil.FieldAssocAnno;
import cz.jmare.rsmapper.util.AssocFieldsUtil.FieldAssocAnno.FkKeysPrimKeys;

public class AssocKeysForPost {
    protected Class<?> pointsToClass;
    
    /**
     * Never null
     */
    protected Class<?> leftClass;
    
    /**
     * Shouldn't be null unless able to detect
     */
    protected String leftAssocFieldName;
    
    protected Field leftAssocField;
    
    /**
     * It can be null but the foreignKeyFieldName must be able to determine
     */
    protected List<String> foreignKeyFieldNames;
    
    protected List<Field> foreignKeyFields;

    /**
     * It can be null when only one key used but for multiple key names the order must match with foreignKeyFieldName
     */
    protected List<String> primaryKeyFieldNames;
    
    public AssocKeysForPost(Class<?> leftClass, Class<?> pointsToClass, String leftAssocFieldName, String leftForeignKeyFieldNameDef) {
        super();
        this.leftClass = leftClass;
        this.pointsToClass = pointsToClass;
        this.leftAssocFieldName = leftAssocFieldName;
        initForeignKeyFieldNames(leftForeignKeyFieldNameDef);
    }
    
    public AssocKeysForPost(Class<?> leftClass, Class<?> pointsToClass, String leftAssocFieldName) {
        super();
        this.leftClass = leftClass;
        this.pointsToClass = pointsToClass;
        this.leftAssocFieldName = leftAssocFieldName;
        initForeignKeyFieldNames(null);
    }

    public AssocKeysForPost(Class<?> leftClass, Class<?> pointsToClass) {
        super();
        this.leftClass = leftClass;
        this.pointsToClass = pointsToClass;
        initForeignKeyFieldNames(null);
    }
    
    public List<Field> getForeignKeyFields() {
        if (foreignKeyFields == null) {
            List<Field> fields = new ArrayList<>();
            for (String foreignKeyFieldName : foreignKeyFieldNames) {
                Field foreignKeyField = FieldAccessUtil.getBasicFieldWithInheritance(pointsToClass, foreignKeyFieldName);
                fields.add(foreignKeyField);
            }
            this.foreignKeyFields = fields;
        }
        return foreignKeyFields;
    }
    
    public List<String> getForeignKeyFieldNames() {
        return foreignKeyFieldNames;
    }

    public void initForeignKeyFieldNames(String foreignKeyFieldNameDef) {
        List<String> primaryKeyNames = null;
        
        if (foreignKeyFieldNameDef == null) {
            FieldAssocAnno fieldAssocAnno = findFieldAssocAnno();
            if (fieldAssocAnno.getFkIsHere() != null) {
                if (fieldAssocAnno.getFkIsHere()) {
                    throw new RsWrongDefException("The fkThere @Assoc attribute must be used in " + leftClass + "." + fieldAssocAnno.field.getName() + " because foreign key is placed in " + pointsToClass);
                }
            }
            foreignKeyFieldNames = fieldAssocAnno.getForeignKeyNames();
            primaryKeyNames = fieldAssocAnno.getPrimaryKeyNames();
        } else {
            FkKeysPrimKeys parseKeys = AssocFieldsUtil.FieldAssocAnno.parseKeys(foreignKeyFieldNameDef);
            foreignKeyFieldNames = parseKeys.fkKeys;
            primaryKeyNames = parseKeys.pkKeys;
        }

        for (String foreignKeyName : foreignKeyFieldNames) {
            try {
                FieldsUtil.columnNameByFieldName(pointsToClass, foreignKeyName);
            } catch (Exception e) {
                throw new RsWrongDefException("Class " + pointsToClass.getCanonicalName() + " doesn't contain foreign key field " + foreignKeyName);
            }
        }

        if (foreignKeyFieldNames.size() > 1) {
            if (primaryKeyNames == null) {
                throw new RsWrongDefException("Multiple foreign key used, the @Assoc for " + leftClass.getCanonicalName() + "."  + leftAssocFieldName + " must contain pairs fk1=pk1,fk2=pk2,...");
            }
            if (foreignKeyFieldNames.size() != primaryKeyNames.size()) {
                throw new RsWrongDefException("Foreign keys doesn't match with primary keys in " + leftClass.getCanonicalName() + "."  + leftAssocFieldName);
            }
        }

        if (primaryKeyNames != null) {
            for (String primaryKeyName : primaryKeyNames) {
                try {
                    FieldsUtil.columnNameByFieldName(leftClass, primaryKeyName);
                } catch (Exception e) {
                    throw new RsWrongDefException("Class " + leftClass.getCanonicalName() + " doesn't contain primary key field " + primaryKeyName);
                }
            }
            this.primaryKeyFieldNames = primaryKeyNames;
        }
    }

    protected FieldAssocAnno findFieldAssocAnno() {
        FieldAssocAnno fieldAssocAnno = AssocFieldsUtil.findClassToSingleClass(leftClass, pointsToClass);
        return fieldAssocAnno;
    }
    
    public String getLeftAssocFieldName() {
        if (leftAssocFieldName == null) {
            FieldAssocAnno fieldAssocAnno = findFieldAssocAnno();
            leftAssocField = fieldAssocAnno.field;
            leftAssocFieldName = fieldAssocAnno.field.getName();
        }
        return leftAssocFieldName;
    }
    
    public Field getLeftAssocField() {
        if (leftAssocField == null) {
            if (leftAssocFieldName != null) {
                try {
                    leftAssocField = FieldAccessUtil.getFieldWithInheritance(leftClass, leftAssocFieldName);
                } catch (NoSuchFieldException e) {
                    throw new RsWrongDefException("Unable to find " + leftClass + "." + leftAssocFieldName, e);
                }
            } else {
                FieldAssocAnno fieldAssocAnno = findFieldAssocAnno();
                leftAssocField = fieldAssocAnno.field;
                leftAssocFieldName = leftAssocField.getName();
            }
        }
        return leftAssocField;
    }
    
    @Deprecated
    public List<Object> reorderToFKOrder(Map<String, Object> values) {
        if (primaryKeyFieldNames != null) {
            ArrayList<Object> result = new ArrayList<Object>();
            for (String name : primaryKeyFieldNames) {
                Object value = values.get(name);
                if (value == null) {
                    throw new RsWrongDefException("No primary key for " + name + ", passed prim. key " + values);
                }
                result.add(value);
            }
            return result;
        } else {
            if (values.size() > 1) {
                throw new RsWrongDefException("Primary keys for " + leftClass.getCanonicalName() + "."  + leftAssocFieldName + " must be present");
            }
            return new ArrayList<Object>(values.values());
        }
    }
    
    public AssocFkValues getAssocFKValues(Map<String, Object> primKeyNameValueMap) {
        ArrayList<Object> result = new ArrayList<Object>();
        if (primaryKeyFieldNames != null) {
            for (String name : primaryKeyFieldNames) {
                Object value = primKeyNameValueMap.get(name);
                if (value == null) {
                    throw new RsWrongDefException("No primary key for " + name + ", passed prim. key " + primKeyNameValueMap);
                }
                result.add(value);
            }
            return new AssocFkValues(foreignKeyFieldNames, getForeignKeyFields(), result);
        } else {
            if (primKeyNameValueMap.size() > 1) {
                throw new RsWrongDefException("Primary keys for " + leftClass.getCanonicalName() + "."  + leftAssocFieldName + " must be present");
            }
            result.add(primKeyNameValueMap.entrySet().iterator().next().getValue());
            return new AssocFkValues(foreignKeyFieldNames, getForeignKeyFields(), result);
        }
    }
}
