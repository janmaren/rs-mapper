package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;

public class FieldAccessUtil {
    public static void setBasicValueWithInheritance(Object object, String fieldName, Object value) throws RsWrongDefException {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, null, List.of(fieldName), null, false);
        if (objectColumnValues.fields.size() == 1) {
            Field field = objectColumnValues.fields.get(0);
            field.setAccessible(true);
            try {
                field.set(object, value);
                return;
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RsWrongDefException("Unable to set value " + value + (value != null ? " ("+ value.getClass().getCanonicalName() +")" : "") + " to " + object.getClass() + "." + fieldName);
            }
        }
        throw new IllegalArgumentException("Unable to set value " + value + (value != null ? " ("+ value.getClass() +")" : "") + " to " + object.getClass() + "." + fieldName + " because such field not found");
    }
    
    public static void setValueWithConversion(Object object, Field field, Object value) throws RsWrongDefException {
        try {
            field.setAccessible(true);
            value = ConvertUtil.convertValueToRequiredType(value, field.getType());
            field.set(object, value);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RsWrongDefException("Unable to set value " + value + (value != null ? " ("+ value.getClass() +")" : "") + " to " + object.getClass() + "." + field.getName(), e);
        }
    }

    public static void setValueWithConversion(Object object, List<Field> fields,
            List<Object> values) {
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            Object value = values.get(i);
            setValueWithConversion(object, field, value);
        }
    }
    
    public static void setValueWithConversion(Object object, Map<Field, Object> fieldsValues) {
        for (Entry<Field, Object> entry : fieldsValues.entrySet()) {
            Field field = entry.getKey();
            Object value = entry.getValue();
            setValueWithConversion(object, field, value);
        }
    }
    
    public static void setValue(Object object, Field field, Object value) throws RsWrongDefException {
        try {
            field.setAccessible(true);
            field.set(object, value);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RsWrongDefException("Unable to set value " + value + (value != null ? " ("+ value.getClass() +")" : "") + " to " + object.getClass() + "." + field.getName(), e);
        }
    }
   
    public static Object getValue(Object object, Field field) throws RsWrongDefException {
        try {
            field.setAccessible(true);
            return field.get(object);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RsWrongDefException("Unable to get value from " + object.getClass() + "." + field.getName(), e);
        }
    }
    
    public static List<Object> getValue(Object object, List<Field> fields) {
        List<Object> result = new ArrayList<>();
        for (Field field : fields) {
            result.add(getValue(object, field));
        }
        return result;
    }
    
    public static Object getBasicValue(Object object, String fieldName) throws RsWrongDefException {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, null, List.of(fieldName), null, false);
        if (objectColumnValues.fields.size() == 1) {
            return objectColumnValues.values.get(0);
        }
        throw new RsWrongDefException("Unable to get value from " + object.getClass() + "." + fieldName);
    }

    public static List<Object> getBasicValue(Object object, List<String> fieldNames) {
        List<Object> result = new ArrayList<Object>();
        for (String fieldName : fieldNames) {
            Object value = getBasicValue(object, fieldName);
            result.add(value);
        }
        return result;
    }
    
    public static Map<String, Object> getBasicMapOrNulls(Object object, List<String> fieldNames) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        if (object == null) {
            for (String fieldName : fieldNames) {
                result.put(fieldName, null);
            }
            return result;
        } 
        for (String fieldName : fieldNames) {
            Object value = getBasicValue(object, fieldName);
            result.put(fieldName, value);
        }
        return result;
    }
    
    static Field getBasicFieldWithInheritance(Class<?> clazz, String fieldName) throws RsWrongDefException {
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        List<Field> fields = classColumnFields.fields;
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        throw new RsWrongDefException("Unable to find field " + fieldName + " in " + clazz);
    }
    
    static Field getFieldWithInheritance(Class<?> clazz, String fieldName) throws NoSuchFieldException {
        try {
            Field declaredField = clazz.getDeclaredField(fieldName);
            return declaredField;
        } catch (NoSuchFieldException | SecurityException e) {
            Class<?> superClass = clazz.getSuperclass();
            if (superClass == null || superClass == Object.class) {
                throw new NoSuchFieldException("Unable to find field " + fieldName + " in " + clazz);
            }
            return getFieldWithInheritance(superClass, fieldName);
        }
    }

    public static List<Object> getObjectFieldKeyValues(Object object, List<String> keyFieldNames) throws RsWrongDefException {
        List<Object> values = new ArrayList<Object>();
        boolean isNonnull = false;
        boolean isNull = false;
        for (String fieldName : keyFieldNames) {
            Object value = getBasicValue(object, fieldName);
            values.add(value);
            if (value == null) {
                isNull |= true;
            } else {
                isNonnull |= true;
            }
        }
        if (isNonnull == isNull) {
            throw new RsWrongDefException("There is a combination of null key and nonnull key, keys:" + keyFieldNames);
        }
        
        return values;
    }
}
