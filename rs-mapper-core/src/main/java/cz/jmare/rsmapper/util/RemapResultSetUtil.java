package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;

public class RemapResultSetUtil {
    public static void populateGeneratedFields(Object object, ColumnsFieldsValues generatedColumnValues, ResultSet resultSet, DBProfile dbProfile, Logger logger)
            throws SQLException {
        StringBuilder sb = new StringBuilder();
        List<Field> fields = generatedColumnValues.fields;
        for (int i = 0; i < generatedColumnValues.columnNames.size(); i++) {
            Object value = null;
            try {
                value = resultSet.getObject(i + 1, fields.get(i).getType());
            } catch (Exception e1) {
                value = dbProfile.getObjectFromResultSet(resultSet, i + 1);
                value = ConvertUtil.convertValueToRequiredType(value, fields.get(i).getType());
            }
            String columnName = generatedColumnValues.columnNames.get(i);
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(columnName).append("=").append(value);
            Field field = generatedColumnValues.fields.get(i);
            try {
                FieldAccessUtil.setValueWithConversion(object, field, value);
            } catch (IllegalArgumentException e) {
                throw new RsWrongDefException("Unable to set value " + value + " (" + value.getClass().getCanonicalName() + ") of column '" + columnName + "' into " + object.getClass() + "." + field.getName() + " (" + field.getType().getName() + ")", e);
            }
        }
        logger.debug(">>>Returned: {}", sb);
    }
}
