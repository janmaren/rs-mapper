package cz.jmare.rsmapper.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;

public class ConvertUtil {
    private static final BigInteger LONG_MIN = BigInteger.valueOf(Long.MIN_VALUE);

    private static final BigInteger LONG_MAX = BigInteger.valueOf(Long.MAX_VALUE);
    
    public static final Set<Class<?>> PRIMITIVE_TYPES;

    static {
        Set<Class<?>> numberTypes = new HashSet<>(8);
        numberTypes.add(Byte.TYPE);
        numberTypes.add(Short.TYPE);
        numberTypes.add(Integer.TYPE);
        numberTypes.add(Long.TYPE);
        numberTypes.add(Float.TYPE);
        numberTypes.add(Double.TYPE);
        numberTypes.add(Boolean.TYPE);
        numberTypes.add(Character.TYPE);
        PRIMITIVE_TYPES = Collections.unmodifiableSet(numberTypes);
    }
    
    @SuppressWarnings("unchecked")
    public static <T extends Number> T convertNumberToRequiredType(Number number, Class<?> targetClass)
            throws RsWrongDefException {

        if (targetClass.isInstance(number)) {
            return (T) number;
        }
        else if (Byte.class == targetClass) {
            long value = checkedLongValue(number, targetClass);
            if (value < Byte.MIN_VALUE || value > Byte.MAX_VALUE) {
                raiseOverflowException(number, targetClass);
            }
            return (T) Byte.valueOf(number.byteValue());
        }
        else if (Short.class == targetClass) {
            long value = checkedLongValue(number, targetClass);
            if (value < Short.MIN_VALUE || value > Short.MAX_VALUE) {
                raiseOverflowException(number, targetClass);
            }
            return (T) Short.valueOf(number.shortValue());
        }
        else if (Integer.class == targetClass) {
            long value = checkedLongValue(number, targetClass);
            if (value < Integer.MIN_VALUE || value > Integer.MAX_VALUE) {
                raiseOverflowException(number, targetClass);
            }
            return (T) Integer.valueOf(number.intValue());
        }
        else if (Long.class == targetClass) {
            long value = checkedLongValue(number, targetClass);
            return (T) Long.valueOf(value);
        }
        else if (BigInteger.class == targetClass) {
            if (number instanceof BigDecimal) {
                // do not lose precision - use BigDecimal's own conversion
                return (T) ((BigDecimal) number).toBigInteger();
            }
            else {
                // original value is not a Big* number - use standard long conversion
                return (T) BigInteger.valueOf(number.longValue());
            }
        }
        else if (Float.class == targetClass) {
            return (T) Float.valueOf(number.floatValue());
        }
        else if (Double.class == targetClass) {
            return (T) Double.valueOf(number.doubleValue());
        }
        else if (BigDecimal.class == targetClass) {
            // always use BigDecimal(String) here to avoid unpredictability of BigDecimal(double)
            // (see BigDecimal javadoc for details)
            return (T) new BigDecimal(number.toString());
        }
        else {
            throw new RsWrongDefException("Could not convert number [" + number + "] of type [" +
                    number.getClass().getName() + "] to unsupported target class [" + targetClass.getName() + "]");
        }
    }
    
    private static long checkedLongValue(Number number, Class<?> targetClass) {
        BigInteger bigInt = null;
        if (number instanceof BigInteger) {
            bigInt = (BigInteger) number;
        }
        else if (number instanceof BigDecimal) {
            bigInt = ((BigDecimal) number).toBigInteger();
        }
        // Effectively analogous to JDK 8's BigInteger.longValueExact()
        if (bigInt != null && (bigInt.compareTo(LONG_MIN) < 0 || bigInt.compareTo(LONG_MAX) > 0)) {
            raiseOverflowException(number, targetClass);
        }
        return number.longValue();
    }
    
    private static void raiseOverflowException(Number number, Class<?> targetClass) {
        throw new RsWrongDefException("Could not convert number [" + number + "] of type [" +
                number.getClass().getName() + "] to target class [" + targetClass.getName() + "]: overflow");
    }
    
    public static Object parseNumber(String text, Class<?> targetClass) {
        if (text == null) {
            return null;
        }
        
        String trimmed = text.trim();
        if (trimmed.isEmpty()) {
            return null;
        }

        if (Byte.class == targetClass) {
            return Byte.valueOf(trimmed);
        }
        else if (Short.class == targetClass) {
            return Short.valueOf(trimmed);
        }
        else if (Integer.class == targetClass) {
            return Integer.valueOf(trimmed);
        }
        else if (Long.class == targetClass) {
            return Long.valueOf(trimmed);
        }
        else if (BigInteger.class == targetClass) {
            return new BigInteger(trimmed);
        }
        else if (Float.class == targetClass) {
            return Float.valueOf(trimmed);
        }
        else if (Double.class == targetClass) {
            return Double.valueOf(trimmed);
        }
        else if (BigDecimal.class == targetClass || Number.class == targetClass) {
            return new BigDecimal(trimmed);
        }
        else {
            throw new RsWrongDefException(
                    "Cannot convert String [" + text + "] to target class [" + targetClass.getName() + "]");
        }
    }

    public static Object convertValueToRequiredType(Object value, Class<?> requiredType) throws RsWrongDefException {
        if (value == null) {
            return null;
        }
        if (value.getClass() == requiredType) {
            return value;
        }
        if (Collection.class.isAssignableFrom(requiredType)) {
            @SuppressWarnings("unchecked")
            Collection<Object> col = (Collection<Object>) value;
            return convertCollectionToRequiredType(col, requiredType);
        } else if (String.class.equals(requiredType)) {
            return value.toString();
        } else if (Number.class.isAssignableFrom(requiredType)) {
            if (value instanceof Number) {
                // Convert original Number to target Number class.
                return convertNumberToRequiredType(((Number) value), requiredType);
            } else {
                // Convert stringified value to target Number class.
                return parseNumber(value.toString(), requiredType);
            }
        } else if (Boolean.class.isAssignableFrom(requiredType)) {
            return Boolean.valueOf(value.toString());
        } else if (java.time.LocalTime.class.isAssignableFrom(requiredType)) {
            if (value instanceof java.sql.Time) {
                java.sql.Time time = (java.sql.Time) value;
                return time.toLocalTime();
            }
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");

        } else if (OffsetTime.class.isAssignableFrom(requiredType)) {
            if (value.getClass() == java.sql.Time.class) {
                return ((java.sql.Time) value).toLocalTime().atOffset(OffsetTime.now().getOffset());
            }
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");
        } else if (LocalDateTime.class.isAssignableFrom(requiredType)) {
            if (value.getClass() == Timestamp.class) {
                return ((Timestamp) value).toLocalDateTime();
            }
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");
        } else if (LocalDate.class.isAssignableFrom(requiredType)) {
            if (value.getClass() == Timestamp.class) {
                return ((Timestamp) value).toLocalDateTime().toLocalDate();
            }
            
            if (value.getClass() == Date.class) {
                return ((Date) value).toLocalDate();
            }
            
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");
        } else if (java.util.Date.class.isAssignableFrom(requiredType)) {
            if (value.getClass() == Timestamp.class) {
                return new Date(((Timestamp) value).getTime());
            }
             
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");
        } else if (PRIMITIVE_TYPES.contains(requiredType)) {
            return convertToPrimitive(value, requiredType);
        } else {
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");
        }
    }


    private static Object convertCollectionToRequiredType(Collection<Object> collection, Class<?> requiredType) {
        if (collection instanceof Set) {
            if (requiredType.isAssignableFrom(List.class)) {
                return new ArrayList<>(collection);
            }
        }
        if (collection instanceof List) {
            if (requiredType.isAssignableFrom(Set.class)) {
                return new HashSet<>(collection);
            }
        }
        return collection;
    }

    private static Object convertToPrimitive(Object value, Class<?> requiredType) {
        if (Integer.class.isInstance(value)) {
            return (int) value;
        } else if (Byte.class.isInstance(value)) {
            return (byte) value;
        } else if (Short.class.isInstance(value)) {
            return (short) value;
        } else if (Long.class.isInstance(value)) {
            return (long) value;
        } else if (Float.class.isInstance(value)) {
            return (float) value;
        } else if (Double.class.isInstance(value)) {
            return (double) value;
        } else if (Boolean.class.isInstance(value)) {
            return (boolean) value;
        } else if (Character.class.isInstance(value)) {
            return (char) value;
        } else {
            throw new RsWrongDefException(
                    "Value [" + value + "] is of type [" + value.getClass().getName() +
                            "] and cannot be converted to required type [" + requiredType.getName() + "]");
        }
    }
    
    @SuppressWarnings("unchecked")
    public static Collection<Object> convertCollectionValuesToRequiredType(Collection<Object> objects, Class<?> requiredType) {
        Collection<Object> resultCollection;
        try {
            @SuppressWarnings("rawtypes")
            Constructor<? extends Collection> declaredConstructor = objects.getClass().getDeclaredConstructor();
            resultCollection = declaredConstructor.newInstance();
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException("Unable to create collection", e);
        }
        for (Object object : objects) {
            resultCollection.add(convertValueToRequiredType(object, requiredType));
        }
        return resultCollection;
    }

    public static void populateObject(ResultSet rs, DBProfile dbProfile, Object object, ColumnsFields columnsFields)
            throws SQLException {
        List<String> columnNames = columnsFields.columnNames;
        List<Field> fields = columnsFields.fields;
        for (int i = 0; i < columnNames.size(); i++) {
            Field field = fields.get(i);
            Object value;
            try {
                value = rs.getObject(i + 1, field.getType());
            } catch (SQLException e) {
                Object objectAnyType = dbProfile.getObjectFromResultSet(rs, i + 1);
                value = convertValueToRequiredType(objectAnyType, field.getType());
            }
            FieldAccessUtil.setValueWithConversion(object, field, value);
        }
    }
}
