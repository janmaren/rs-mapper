package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;

public class MergeObjects {
    public static <T> void shallowMerge(T destObject, T mergingObject) {
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(destObject.getClass());
        List<Field> fields = classColumnFields.fields;
        for (Field field : fields) {
            field.setAccessible(true);
            int modifiers = field.getModifiers();
            if (Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers)) {
                continue;
            }
            try {
                Object object1 = field.get(destObject);
                if (object1 == null) {
                    Object object2 = field.get(mergingObject);
                    field.set(destObject, object2);
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RsWrongDefException("Unable to access field " + field);
            }
        }
    }
}
