package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.Query;
import cz.jmare.rsmapper.RsCommand;
import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.ManyRecordsException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.util.NamedStatementPopulator.NamedStatementPopulatorResult;

public class SqlCommandUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlCommandUtil.class);

    public static <T> T selectOneScalar(Handle handle, String whatColumnName, Class<T> requiredClass, String tableName, String behindTablePart, Map<String, Object> parameters) {
        StringBuilder sb = new StringBuilder("select ");
        sb.append(whatColumnName);
        sb.append(" from ");
        sb.append(tableName);
        if (behindTablePart != null) {
            sb.append(" ");
            sb.append(behindTablePart);
        }
        
        String sql = sb.toString();
        
        return RsCommand.selectOneScalar(handle, requiredClass, sql, parameters);
    }

    /**
     * The delete from command. First 'delete from &lt;tableName&gt;' part is pregenerated and behindTableSqlPart is appended. It may contain
     * named parameters and the value may contain collection (multiple question placeholders expanded).<br>
     * Useful for batch delete
     * @param handle
     * @param tableName
     * @param behindTableSqlPart For example 'where id in (:ids)'
     * @param parametersValuesMap Named parameter-value pairs where value may be a collection
     * @return number of deleted rows
     */
    public static int deleteWithNamedParams(Handle handle, String tableName, String behindTableSqlPart, Map<String, Object> parametersValuesMap) {
        StringBuilder sb = new StringBuilder("delete from ");
        sb.append(tableName);
        sb.append(" ");
        if (behindTableSqlPart != null && !behindTableSqlPart.isBlank()) {
            sb.append(behindTableSqlPart);
        }
        
        String sql = sb.toString();
        return RsCommand.execute(handle, sql, parametersValuesMap);
    }    
    
    /**
     * The update command. First 'update &lt;tableName&gt; set' part is pregenerated and behindSetSqlPart is appended. It may contain
     * named parameters and the value may contain collection (multiple question placeholders expanded).<br>
     * Useful for batch update
     * @param handle
     * @param tableName
     * @param behindSetSqlPart For example 'where id in (:ids)'
     * @param parametersValuesMap Named parameter-value pairs where value may be a collection
     * @return number of deleted rows
     */
    public static int updateWithNamedParams(Handle handle, String tableName, String behindSetSqlPart, Map<String, Object> parametersValuesMap) {
        StringBuilder sb = new StringBuilder("update ");
        sb.append(tableName);
        sb.append(" set ");
        if (behindSetSqlPart != null && !behindSetSqlPart.isBlank()) {
            sb.append(behindSetSqlPart);
        }
        
        String sql = sb.toString();
        return RsCommand.execute(handle, sql, parametersValuesMap);
    } 
    
    public static int updateWithParams(Handle handle, String tableName, List<String> setColumnNames, List<Object> setValues, List<String> whereColumnNames, List<Object> whereValues) {
        StringBuilder sb = new StringBuilder("update ");
        sb.append(tableName);
        sb.append(" set ");
        StringBuilder setBuilder = new StringBuilder();
        for (String setColumnName : setColumnNames) {
            if (setBuilder.length() > 0) {
                setBuilder.append(", ");
            }
            setBuilder.append(setColumnName).append("=?");
        }
        sb.append(setBuilder);
        
        sb.append(" where ");

        StringBuilder whereBuilder = new StringBuilder();
        for (String whereColumnName : whereColumnNames) {
            if (whereBuilder.length() > 0) {
                whereBuilder.append(" and ");
            }
            whereBuilder.append(whereColumnName).append("=?");
        }
        sb.append(whereBuilder);
        
        String sql = sb.toString();
        
        List<Object> values = new ArrayList<Object>(setValues);
        values.addAll(whereValues);
        return RsCommand.execute(handle, sql, values);
    } 
    
    public static int deleteRecordsFromTable(Handle handle, String tableName, Map<String, Object> parametersValuesMap) {
        DBProfile dbProfile = handle.getDbProfile();
        StringBuilder sb = new StringBuilder("delete from ");
        sb.append(tableName);
        sb.append(" where ");
        String condition = parametersValuesMap.entrySet().stream().map(e -> {return e.getKey() + "=?";}).collect(Collectors.joining(" AND "));
        sb.append(condition);
        
        String sql = sb.toString();
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            Set<Entry<String, Object>> entrySet = parametersValuesMap.entrySet();
            int idx = 0;
            for (Entry<String, Object> e : entrySet) {
                dbProfile.setObjectToPreparedStatement(prepareStatement, ++idx, e.getValue());
            }
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", new ParamsToStringer(parametersValuesMap));
            prepareStatement.executeUpdate();
            
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            return updateCount;
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to delete record by: " + sql, e);
        }
    }    
    
    /**
     * Return true when record exists. Select is generated using 'select 1 from ' + added columns and values passed as parameters
     * @param handle
     * @param tableName table name where to check existence of record
     * @param parametersValuesMap column names and parameters
     * @return
     */
    public static boolean selectExists(Handle handle, String tableName, LinkedHashMap<String, Object> parametersValuesMap) {
        StringBuilder sb = new StringBuilder("select 1 from ");
        sb.append(tableName);
        sb.append(" where ");

        String condition = parametersValuesMap.entrySet().stream().map(e -> {return e.getKey() + "=?";}).collect(Collectors.joining(" AND "));
        sb.append(condition);
        DBProfile dbProfile = handle.getDbProfile();
        String sql = sb.toString();
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            Set<Entry<String, Object>> entrySet = parametersValuesMap.entrySet();
            int idx = 0;
            for (Entry<String, Object> e : entrySet) {
                dbProfile.setObjectToPreparedStatement(prepareStatement, ++idx, e.getValue());
            }
          
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parametersValuesMap);
            ResultSet resultSet = prepareStatement.executeQuery();

            if (resultSet.next()) {
                LOGGER.debug(">>>Returned: 1");
                return true;
            }
            LOGGER.debug(">>>Returned: 0");
            return false;
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by sql " + sql, e);
        }
    }
    
    public static Collection<LinkedHashMap<String, Object>> selectMultipleVectors(Handle handle, ColumnsFields classColumnFields, String tableName, String wherePart, List<Object> parameters, Query condition) {
        DBProfile dbProfile = handle.getDbProfile();
        StringBuilder sb = new StringBuilder("select ");
        sb.append(classColumnFields.columnNames.stream().collect(Collectors.joining(", ")));
        sb.append(" from ");
        sb.append(tableName);
        sb.append(" ");
        sb.append(wherePart);
        List<Object> useParameters = new ArrayList<Object>(parameters);
        if (condition != null && !condition.getQueryString().isBlank()) {
            PlaceholdersQuery placeholdersQuery = toPlaceholdersQuery(condition);
            sb.append(" AND ").append(placeholdersQuery.getQueryString());
            useParameters.addAll(placeholdersQuery.getValues());
        }
        
        String sql = sb.toString();
        List<LinkedHashMap<String, Object>> result = new ArrayList<>();
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            for (int i = 0; i < useParameters.size(); i++) {
                dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, useParameters.get(i));
            }
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", useParameters);
            ResultSet resultSet = prepareStatement.executeQuery();
            List<String> columnNames = classColumnFields.columnNames;
            List<Field> fields = classColumnFields.fields;
            while (resultSet.next()) {
                LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
                for (int i = 0; i < columnNames.size(); i++) {
                    row.put(columnNames.get(i), DBProfile.getObject(resultSet, i + 1, fields.get(i).getType(), dbProfile));
                }
                result.add(row);
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by sql " + sql, e);
        }
        LOGGER.debug(">>>Returned: {}", result);
        return result;
    }
    
    public static LinkedHashMap<String, Object> selectOneVector(Handle handle, ColumnsFields classColumnFields, String tableName, String wherePart, List<Object> parameters, Query condition) {
        Collection<LinkedHashMap<String, Object>> vectors = selectMultipleVectors(handle, classColumnFields, tableName, wherePart, parameters, condition);
        if (vectors.size() > 1) {
            throw new ManyRecordsException("Expected to select one record but found more");
        }
        if (vectors.size() == 0) {
            return null;
        }
        return vectors.iterator().next();
    }
    
    public static PlaceholdersQuery toPlaceholdersQuery(Query query) {
        NamedStatementPopulatorResult namedStatementPopulatorResult = NamedStatementPopulator.extractNamedParameters(query.getQueryString(), query.getQueryParameters());
        SimplePlaceholdersQuery simplePlaceholdersQuery = new SimplePlaceholdersQuery(namedStatementPopulatorResult.extractedSql, namedStatementPopulatorResult.extractedParameters);
        return simplePlaceholdersQuery;
    }
}
