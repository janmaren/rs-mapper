package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;

public class RSToMap {
    /**
     * @deprecated use {@link #convertAllToTypes(ResultSet, ColumnsFields, DBProfile)}
     * @param rs
     * @param dbProfile
     * @return
     * @throws SQLException
     */
    public static List<Map<String, Object>> convertAll(ResultSet rs, DBProfile dbProfile) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        while (rs.next()) {
            HashMap<String, Object> row = convertOne(rs, md, dbProfile);
            list.add(row);
        }

        return list;
    }

    /**
     * @deprecated use {@link #convertOneToTypes(ResultSet, ResultSetMetaData, ColumnsFields, DBProfile)}
     * @param rs
     * @param dbProfile
     * @return
     * @throws SQLException
     */
    public static HashMap<String, Object> convertOne(ResultSet rs, DBProfile dbProfile) throws SQLException {
        return convertOne(rs, dbProfile);
    }

    /**
     * @deprecated use {@link #convertOneToTypes(ResultSet, ResultSetMetaData, ColumnsFields, DBProfile)}
     * @param rs
     * @param md
     * @param dbProfile
     * @return
     * @throws SQLException
     */
    public static HashMap<String, Object> convertOne(ResultSet rs, ResultSetMetaData md, DBProfile dbProfile) throws SQLException {
        int columns = md.getColumnCount();
        HashMap<String, Object> row = new HashMap<String, Object>(columns);
        for (int i = 1; i <= columns; ++i) {
            String columnName = md.getColumnLabel(i).toLowerCase();
            Object value;
            if ("ENUM".equals(md.getColumnTypeName(i))) {
                value = rs.getInt(i);
            } else {
                value = dbProfile.getObjectFromResultSet(rs, i);
            }

            if (row.containsKey(columnName)) {
                throw new RsWrongDefException("Resultset contains duplicit column '" + columnName + "'");
            }
            row.put(columnName, value);
        }
        return row;
    }
    
    public static List<Map<String, Object>> convertAllToTypes(ResultSet rs, ColumnsFields columnsFields, DBProfile dbProfile) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        if (md.getColumnCount() != columnsFields.fields.size()) {
            throw new RsWrongDefException("Resultset data contain " + md.getColumnCount() + " but fields number is " + columnsFields.fields.size());
        }
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        while (rs.next()) {
            Map<String, Object> row = convertOneToTypes(rs, md, columnsFields, dbProfile);
            list.add(row);
        }

        return list;
    }
    
    public static Map<String, Object> convertOneToTypes(ResultSet rs, ResultSetMetaData md, ColumnsFields columnsFields, DBProfile dbProfile)
            throws SQLException {
        List<Field> fields = columnsFields.fields;
        LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            Object value;
            try {
                value = rs.getObject(i + 1, field.getType());
            } catch (Exception e) {
                value = dbProfile.getObjectFromResultSet(rs, i + 1);
                value = ConvertUtil.convertValueToRequiredType(value, field.getType());
            }
            
            String columnName = md.getColumnLabel(i + 1).toLowerCase();

            if (row.containsKey(columnName)) {
                throw new RsWrongDefException("Resultset contains duplicit column '" + columnName + "'");
            }
            row.put(columnName, value);
        }
        return row;
    }    
}
