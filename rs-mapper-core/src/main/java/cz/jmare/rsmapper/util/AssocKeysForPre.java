package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.util.AssocFieldsUtil.FieldAssocAnno;
import cz.jmare.rsmapper.util.AssocFieldsUtil.FieldAssocAnno.FkKeysPrimKeys;

public class AssocKeysForPre {
    private Class<?> pointsToClass;
    
    private Class<?> leftClass;
    
    /**
     * Shouldn't be null unless able to detect
     */
    private String leftAssocFieldName;
    
    private Field leftAssocField;
    
    /**
     * It can be null but the leftAssocFieldName must be able to determine
     */
    private List<String> leftForeignKeyFieldNames;
    
    private List<Field> leftForeignKeyFields;
    
    /**
     * It can be null when only one key used but for multiple key names the order must match with foreignKeyFieldName
     */
    protected List<String> primaryKeyFieldNames;
    
    public AssocKeysForPre(Class<?> leftClass, Class<?> pointsToClass, String leftAssocFieldName, String leftForeignKeyFieldNameDef) {
        super();
        this.leftClass = leftClass;
        this.pointsToClass = pointsToClass;
        this.leftAssocFieldName = leftAssocFieldName;
        initLeftForeignKeyFieldNames(leftForeignKeyFieldNameDef);
    }
    
    public AssocKeysForPre(Class<?> leftClass, Class<?> pointsToClass, String leftAssocFieldName) {
        super();
        this.leftClass = leftClass;
        this.pointsToClass = pointsToClass;
        this.leftAssocFieldName = leftAssocFieldName;
        initLeftForeignKeyFieldNames(null);
    }

    public AssocKeysForPre(Class<?> leftClass, Class<?> pointsToClass) {
        super();
        this.leftClass = leftClass;
        this.pointsToClass = pointsToClass;
        initLeftForeignKeyFieldNames(null);
    }
    
    public List<Field> getLeftForeignKeyFields() {
        if (leftForeignKeyFields == null) {
            List<String> leftForeignKeyFieldNames = getLeftForeignKeyFieldNames();
            List<Field> fields = new ArrayList<>();
            for (String leftForeignKeyFieldName : leftForeignKeyFieldNames) {
                try {
                    Field field = FieldAccessUtil.getBasicFieldWithInheritance(leftClass, leftForeignKeyFieldName);
                    fields.add(field);
                } catch (IllegalArgumentException e) {
                    throw new RsWrongDefException("Unable to find " + pointsToClass.getClass() + "." + getLeftForeignKeyFieldNames(), e);
                }
            }
            leftForeignKeyFields = fields;
        }
        return leftForeignKeyFields;
    }
    
    public void initLeftForeignKeyFieldNames(String foreignKeyFieldNameDef) {
        List<String> primaryKeyNames = null;
        
        if (foreignKeyFieldNameDef == null) {
            FieldAssocAnno fieldAssocAnno = findAssocAnno();
            if (fieldAssocAnno.getFkIsHere() != null) {
                if (!fieldAssocAnno.getFkIsHere()) {
                    throw new RsWrongDefException("The fkHere @Assoc attribute must be used in " + leftClass + "." + fieldAssocAnno.field.getName() + " because foreign key is placed in " + leftClass);
                }
            }
            leftForeignKeyFieldNames = fieldAssocAnno.getForeignKeyNames();
            primaryKeyNames = fieldAssocAnno.getPrimaryKeyNames();
            if (leftAssocFieldName == null) {
                leftAssocFieldName = fieldAssocAnno.field.getName();
            }
        } else {
            FkKeysPrimKeys parseKeys = AssocFieldsUtil.FieldAssocAnno.parseKeys(foreignKeyFieldNameDef);
            leftForeignKeyFieldNames = parseKeys.fkKeys;
            primaryKeyNames = parseKeys.pkKeys;
            if (leftAssocFieldName == null) {
                throw new RsWrongDefException("The association field musn't be null but it must be a field in " + leftClass + " which points to " + pointsToClass);
            }
        }
        
        for (String foreignKeyName : leftForeignKeyFieldNames) {
            try {
                FieldsUtil.columnNameByFieldName(leftClass, foreignKeyName);
            } catch (Exception e) {
                throw new RsWrongDefException("Class " + leftClass.getCanonicalName() + " doesn't contain foreign key field " + foreignKeyName);
            }
        }
        
        if (leftForeignKeyFieldNames.size() > 1) {
            if (primaryKeyNames == null) {
                throw new RsWrongDefException("Multiple foreign key used, the @Assoc for " + leftClass.getCanonicalName() + "."  + leftAssocFieldName + " must contain pairs fk1=pk1,fk2=pk2,...");
            }
            if (leftForeignKeyFieldNames.size() != primaryKeyNames.size()) {
                throw new RsWrongDefException("Foreign keys doesn't match with primary keys in " + leftClass.getCanonicalName() + "."  + leftAssocFieldName);
            }
        }
        
        if (primaryKeyNames != null) {
            for (String primaryKeyName : primaryKeyNames) {
                try {
                    FieldsUtil.columnNameByFieldName(pointsToClass, primaryKeyName);
                } catch (Exception e) {
                    throw new RsWrongDefException("Class " + pointsToClass.getCanonicalName() + " doesn't contain primary key field " + primaryKeyName);
                }
            }
            this.primaryKeyFieldNames = primaryKeyNames;
        }
    }
    
    public List<String> getLeftForeignKeyFieldNames() {
        return leftForeignKeyFieldNames;
    }

    private FieldAssocAnno findAssocAnno() {
        FieldAssocAnno assocAnno = AssocFieldsUtil.findClassToSingleClass(leftClass, pointsToClass);
        return assocAnno;
    }

    public String getLeftAssocFieldName() {
        return leftAssocFieldName;
    }
    
    public Field getLeftAssocField() {
        if (leftAssocField == null) {
            try {
                leftAssocField = FieldAccessUtil.getFieldWithInheritance(leftClass, leftAssocFieldName);
            } catch (NoSuchFieldException e) {
                throw new RsWrongDefException("Unable to find " + leftClass + "." + leftAssocFieldName, e);
            }
        }
        return leftAssocField;
    }
    
    public List<Object> fkValuesInPKOrder(Object object, List<String> pkFieldNames) {
        List<Object> values = new ArrayList<Object>();
        for (String pkFieldName : pkFieldNames) {
            String fkName = null;
            if (primaryKeyFieldNames == null) {
                if (pkFieldNames.size() > 1) {
                    throw new RsWrongDefException("Multiple primary keys in " + pointsToClass  + " which aren't present in " + leftClass + "." + leftAssocFieldName);
                }
                fkName = leftForeignKeyFieldNames.get(0);
            } else {
                int indexOf = primaryKeyFieldNames.indexOf(pkFieldName);
                if (indexOf == -1) {
                    throw new RsWrongDefException("Wrong association " +  leftClass + "." + leftAssocFieldName + " - no primary key " + pkFieldName + " exists");
                }
                fkName = leftForeignKeyFieldNames.get(indexOf);
            }
            Object fkValue = FieldAccessUtil.getBasicValue(object, fkName);
            values.add(fkValue);
        }
        return values;
    }

    public Map<Field, Object> getFkFieldsValues(Map<String, Object> pkNamesValues) {
        HashMap<Field, Object> result = new HashMap<>();
        List<Field> leftForeignKeyFields = getLeftForeignKeyFields();
        for (Entry<String, Object> entry : pkNamesValues.entrySet()) {
            String pkName = entry.getKey();
            Object value = entry.getValue();
            int index = -1;
            if (primaryKeyFieldNames == null) {
                if (pkNamesValues.size() > 1) {
                    throw new RsWrongDefException("Multiple primary keys in " + pointsToClass  + " which aren't present in " + leftClass + "." + leftAssocFieldName);
                }
                index = 0;
            } else {
                index = primaryKeyFieldNames.indexOf(pkName);
                if (index == -1) {
                    throw new RsWrongDefException("Wrong association " +  leftClass + "." + leftAssocFieldName + " - no primary key " + pkName + " exists");
                }
            }
            result.put(leftForeignKeyFields.get(index), value);
        }
        return result;
    }
}
