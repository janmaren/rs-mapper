package cz.jmare.rsmapper.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.Assoc;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;
import cz.jmare.rsmapper.type.BasicTypeDecider;

public class FieldsUtil {
    /**
     * Return all mapable basic fields which will be present in select or update or insert or delete (in where condition)
     * @param clazz
     * @return
     */
    static List<Field> getObjectFieldsToProcess(Class<?> clazz) {
        BasicTypeDecider basicTypeDecider = GlobalRSConfig.getInstance().getBasicTypeDecider();
        Field[] allFields = ClassUtil.getAllFieldsSupportInheritance(clazz);
        List<Field> fieldsList = Arrays.asList(allFields);
        List<Field> fields = fieldsList.stream().filter(f -> {
            int modifiers = f.getModifiers();
            return !Modifier.isStatic(modifiers) && basicTypeDecider.isBasicType(f.getType())
                    && !Modifier.isTransient(modifiers);
        }).collect(Collectors.toList());
        return fields;
    }
    
    public static String columnNameByFieldName(Class<?> clazz, String fieldName) {
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        List<Field> fields = classColumnFields.fields;
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            if (field.getName().equals(fieldName)) {
                return classColumnFields.columnNames.get(i);
            }
        }
        throw new RsWrongDefException("Not found field " + fieldName + " in " + clazz);
    }
    
    public static Map<String, Object> fieldsValuesParamsToColumnsValuesParams(Class<?> clazz, Map<String, Object> fieldsValuesParams) {
        ColumnsFields columnsFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        LinkedHashMap<String, Object> resultMap = new LinkedHashMap<>();
        List<Field> fields = columnsFields.fields;
        for (Entry<String, Object> entry : fieldsValuesParams.entrySet()) {
            String fieldName = entry.getKey();
            for (int i = 0; i < fields.size(); i++) {
                Field field = fields.get(i);
                if (fieldName.equals(field.getName())) {
                    String columnName = columnsFields.columnNames.get(i);
                    Object value = entry.getValue();
                    resultMap.put(columnName, value);
                }
            }
        }
        return resultMap;
    }
    
    public static Class<?> typeByFieldName(Class<?> clazz, String fieldName) {
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        List<Field> fields = classColumnFields.fields;
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            if (field.getName().equals(fieldName)) {
                return field.getType();
            }
        }
        throw new RsWrongDefException("Not found field " + fieldName + " in " + clazz);
    }
    
    public static boolean isInsertable(Field field) {
        Column findRemapAsAnno = FieldsUtil.findColumnAnnotation(field);
        return findRemapAsAnno == null || findRemapAsAnno.insertable();
    }
    
    public static boolean isUpdatable(Field field) {
        Column findRemapAsAnno = FieldsUtil.findColumnAnnotation(field);
        return findRemapAsAnno == null || findRemapAsAnno.updatable();
    }

    public static Column findColumnAnnotation(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof Column) {
                return (Column) annotation;
            }
        }
        return null;
    }
    
    public static Id findIdAnnotation(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof Id) {
                return (Id) annotation;
            }
        }
        return null;
    }
    
    public static Assoc findAssocAnnotation(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof Assoc) {
                return (Assoc) annotation;
            }
        }
        return null;
    }
    
    public static Collection<String> getNonInsertableFields(Object object, Collection<String> excludeFieldNames, boolean onlyNonNull, String... defaultNoninsertableFields) {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, excludeFieldNames,
                null, f -> !isInsertable(f), onlyNonNull);
        Set<String> set = objectColumnValues.fields.stream().map(f -> f.getName()).collect(Collectors.toSet());
        set.addAll(Arrays.asList(defaultNoninsertableFields));
        return set;
    }
    
    public static Collection<String> getNonUpdatableFields(Object object, Collection<String> excludeFieldNames, boolean onlyNonNull, String... defaultNoninsertableFields) {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, excludeFieldNames, null, f -> !isUpdatable(f), onlyNonNull);
        Set<String> set = objectColumnValues.fields.stream().map(f -> f.getName()).collect(Collectors.toSet());
        set.addAll(Arrays.asList(defaultNoninsertableFields));
        return set;
    }

    public static List<FieldAnno> findIdAnnos(Class<?> clazz) {
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        List<Field> fields = classColumnFields.fields;
        List<FieldAnno> result = new ArrayList<FieldsUtil.FieldAnno>();
        for (Field field : fields) {
            Id idAnnotation = findIdAnnotation(field);
            if (idAnnotation != null) {
                FieldAnno fieldAnno = new FieldAnno(field, idAnnotation);
                result.add(fieldAnno);
            }
        }

        return result;
    }
    
    public static class FieldAnno {
        public Field field;
        
        public Annotation annotation;

        public FieldAnno(Field field, Annotation annotation) {
            this.field = field;
            this.annotation = annotation;
        }
    }

    public static LinkedHashMap<String, Object> extractColumnsValuesMap(Object object, List<String> idFieldNames) {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, null, idFieldNames, null, false);
        List<String> columnNames = objectColumnValues.columnNames;
        List<Object> values = objectColumnValues.values;
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        for (int i = 0; i < columnNames.size(); i++) {
            String columnName = columnNames.get(i);
            Object value = values.get(i);
            parameters.put(columnName, value);
        }
        return parameters;
    }

    public static LinkedHashMap<String, Object> extractFieldsValuesMap(Object object, List<String> idFieldNames) {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, null, idFieldNames, null, false);
        List<String> fieldNames = objectColumnValues.fields.stream().map(f -> f.getName()).collect(Collectors.toList());
        List<Object> values = objectColumnValues.values;
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        for (int i = 0; i < fieldNames.size(); i++) {
            String columnName = fieldNames.get(i);
            Object value = values.get(i);
            parameters.put(columnName, value);
        }
        return parameters;
    }

    public static void checkColumnNamesAreIds(Class<?> clazz, Collection<String> columnNames, int primaryKeysSize) {
        if (columnNames.size() != primaryKeysSize) {
            throw new RsWrongDefException("Not the right number of parameters passed which must be same as number of identifiers. " + clazz + ", passed parameters: " + columnNames);
        }
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        List<String> columnNames2 = classColumnFields.columnNames;
        for (int i = 0; i < columnNames2.size(); i++) {
            String columnName = columnNames2.get(i);
            if (columnNames.contains(columnName)) {
                Field field = classColumnFields.fields.get(i);
                Id id = findIdAnnotation(field);
                if (id == null) {
                    throw new RsWrongDefException("Passed column name " + columnName + " is not an identifier");
                }
            }
        }
    }
    
    public static void checkAllIdsPresent(Class<?> clazz, Collection<String> fieldNames, int primaryKeysSize) {
        if (fieldNames.size() != primaryKeysSize) {
            throw new RsWrongDefException("Not the right number of parameters passed which must be same as number of identifiers. " + clazz + ", passed parameters: " + fieldNames);
        }
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(clazz);
        List<Field> fields = classColumnFields.fields;
        for (String fieldName : fieldNames) {
            Optional<Field> firstField = fields.stream().filter(f -> f.getName().equals(fieldName)).findFirst();
            if (firstField.isEmpty()) {
                throw new RsWrongDefException("Passed field name " + fieldName + " doesn't exist in " + clazz);
            }
            Field field = firstField.get();
            Id id = findIdAnnotation(field);
            if (id == null) {
                throw new RsWrongDefException("Passed field name " + fieldName + " is not an identifier in " + clazz);
            }
        }
    }
    
    public static List<Object> extractValuesList(Object object, List<String> idFieldNames) {
        ColumnsFieldsValues objectColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, null, idFieldNames, null, false);
        return objectColumnValues.values;
    }
    
    public static String generateWhereClause(Class<?> entityClass, List<String> fieldNames) {
        return generateWhereClause(entityClass, fieldNames, null);
    }
    
    public static String generateWhereClause(Class<?> entityClass, List<String> fieldNames, String alias) {
        if (fieldNames.size() == 0) {
            throw new RsWrongDefException("No columns to determine record for " + entityClass);
        }
        StringBuilder sb = new StringBuilder();
        for (String fieldName : fieldNames) {
            String columnName = FieldsUtil.columnNameByFieldName(entityClass, fieldName);   
            if (sb.length() > 0) {
                sb.append(" and ");
            }
            if (alias != null && !"".equals(alias)) {
                sb.append(alias).append(".");
            }
            sb.append(columnName).append("=?");
        }
        return "where " + sb.toString();
    }
}
