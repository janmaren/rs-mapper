package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;

public class ColumnsGenerator {
    private int index;
    
    private List<ClassAlias> classAliases = new ArrayList<ColumnsGenerator.ClassAlias>();
    
    public ColumnsGenerator() {
    }

    public ColumnsGenerator(Class<?> clazz) {
        this.addClass(clazz);
    }
    
    public ColumnsGenerator(Class<?> clazz, String alias) {
        this.addClass(clazz, alias);
    }
    
    public String addClass(Class<?> clazz) {
//        index++;
//        String alias = "_" + index;
//        classAliases.add(new ClassAlias(clazz, alias));
//        return alias;
        return addClass(clazz, null);
    }

    public String addClass(Class<?> clazz, String alias) {
        index++;
        if (alias == null) {
            alias = "_" + index;
        }
        for (ClassAlias classAlias : classAliases) {
            if (classAlias.alias.equals(alias)) {
                throw new RsWrongDefException("Alias " + alias + " used for more tables");
            }
        }
        classAliases.add(new ClassAlias(clazz, alias));
        return alias;
    }

    public ColumnsPartWithTypes getColumnsPartWithTypes() {
        if (classAliases.size() == 0) {
            throw new RsWrongDefException("Not added a class");
        }
        String str = "";
        List<String> columnNames = new ArrayList<String>();
        List<Field> fields = new ArrayList<Field>();
        ColumnsFields columnsFields = new ColumnsFields(columnNames, fields);
        for (ClassAlias classAlias : classAliases) {
            ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(classAlias.clazz);
            columnNames.addAll(classColumnFields.columnNames);
            fields.addAll(classColumnFields.fields);
            String part = createColumnsString(classColumnFields, classAlias.alias);
            if (str.length() > 0) {
                str += ",\n";
            }
            str += part;
        }
        if (str.length() == 0) {
            throw new RsWrongDefException("No class represents an entity with a column");
        }
        return new ColumnsPartWithTypes(str, columnsFields);
    }
    
    public static String createColumnsString(ColumnsFields classColumnFields, String alias) {
        List<Field> fields = classColumnFields.fields;
        List<String> columnNames = classColumnFields.columnNames;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fields.size(); i++) {
            String columnName = columnNames.get(i);
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(alias).append(".").append(columnName).append(" ").append(columnName).append("_").append(alias);
        }
        String part = sb.toString();
        return part;
    }  
    
    public static String createColumnsString(ColumnsFields classColumnFields) {
        List<String> columnNames = classColumnFields.columnNames;
        String columnsPart = columnNames.stream().collect(Collectors.joining(", "));
        return columnsPart;
    }
    
    public static class ColumnsPartWithTypes {
        public String columnsPart;
        
        public ColumnsFields columnFields;

        public ColumnsPartWithTypes(String columnsPart, ColumnsFields columnFields) {
            super();
            this.columnsPart = columnsPart;
            this.columnFields = columnFields;
        }
    }
    
    
    public static class ClassAlias {
        public Class<?> clazz;
        
        public String alias;

        public ClassAlias(Class<?> clazz, String alias) {
            this.clazz = clazz;
            this.alias = alias;
        }
    }
}

