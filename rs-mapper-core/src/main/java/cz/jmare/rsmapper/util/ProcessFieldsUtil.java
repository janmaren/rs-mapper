package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;

public class ProcessFieldsUtil {
    private static Map<Class<?>, ColumnsFields> classToColumnFields = new HashMap<Class<?>, ColumnsFields>();
    
    /**
     * Get filtered lists representing fiields or columns or values. Between excludeFieldNames, includFieldNames and byFieldDecider must
     * be logical AND unless is null
     * @param object
     * @param excludeFieldNames nullable but when populated then no excluded field will be returned
     * @param includFieldNames nullable but when populated then no other than include field will be returned
     * @param byFieldDecider nullable but when populated then only returning true fields will be returned
     * @return
     */
    public static ColumnsFieldsValues getObjectColumnValues(Object object, Collection<String> excludeFieldNames,
            Collection<String> includFieldNames, Function<Field, Boolean> byFieldDecider, boolean onlyNonNull) throws RsWrongDefException {
        Collection<String> includeFieldNames = includFieldNames != null ? new HashSet<>(includFieldNames) : null;
    
        ColumnsFields classColumnFields = getClassColumnFields(object.getClass());
        List<Field> fields = classColumnFields.fields;
        List<String> columnNames = classColumnFields.columnNames;
        List<String> resultColumnNames = new ArrayList<>();
        List<Object> resultValues = new ArrayList<>();
        List<Field> resultFields = new ArrayList<>();
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            if (byFieldDecider != null && !byFieldDecider.apply(field)) {
                continue;
            }
            String fieldName = field.getName();
            if (excludeFieldNames != null && excludeFieldNames.contains(fieldName)) {
                continue;
            }
            if (includeFieldNames != null) {
                if (!includeFieldNames.contains(fieldName)) {
                    continue;
                } else {
                    includeFieldNames.remove(fieldName);
                }
            }
            Object value;
            try {
                field.setAccessible(true);
                value = field.get(object);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RsWrongDefException("Unable to get value from " + object.getClass() + "." + fieldName,
                        e);
            }
            if (!onlyNonNull || value != null) {
                resultColumnNames.add(columnNames.get(i));
                resultValues.add(value);
                resultFields.add(field);
            }
        }
        if (includeFieldNames != null && !includeFieldNames.isEmpty()) {
            throw new RsWrongDefException(
                    "Unable to find fields " + includeFieldNames + " in " + object.getClass());
        }
        ColumnsFieldsValues columnsValues = new ColumnsFieldsValues(resultColumnNames, resultFields, resultValues);
        return columnsValues;
    }

    /**
     * Main cached entrypoint to get list of fields which correspond to concrete columns
     * @param clazz
     * @return
     */
    public static ColumnsFields getClassColumnFields(Class<?> clazz) {
        ColumnsFields columnsFields = classToColumnFields.get(clazz);
        if (columnsFields == null) {
            synchronized (ProcessFieldsUtil.class) {
                columnsFields = classToColumnFields.get(clazz);
                if (columnsFields == null) {
                    columnsFields = getClassColumnFieldsInternal(clazz);
                    classToColumnFields.put(clazz, columnsFields);
                }
            }
        }
        return columnsFields;
    }
    
    /**
     * Basic method to get processable fields including column names and annotations<br>
     * All fields will be used in selects but in inserts, updates and deletes only some fields (need to filter)
     * @param clazz
     * @return
     */
    private static ColumnsFields getClassColumnFieldsInternal(Class<?> clazz) {
        List<Field> fields = FieldsUtil.getObjectFieldsToProcess(clazz);
        List<String> colNames = new ArrayList<>();
        List<Field> cFields = new ArrayList<>();
        for (Field field : fields) {
            String fieldName = field.getName();
            String columnName = null;
            Column annotation = FieldsUtil.findColumnAnnotation(field);
            if (annotation != null && annotation.name() != null) {
                if (annotation.name() != null && !annotation.name().isEmpty()) {
                    columnName = annotation.name();
                }
            }
            if (columnName == null) {
                columnName = CamelUtil.decamelString(fieldName);
            }

            colNames.add(columnName);
            cFields.add(field);
        }
        ColumnsFields columnsValues = new ColumnsFields(colNames, cFields);
        return columnsValues;
    }
    
    public static ColumnsFields getClassColumnFields(Class<?> clazz, List<String> fieldNames) {
        ColumnsFields columnsFields = new ColumnsFields();
        ColumnsFields classColumnFields = getClassColumnFields(clazz);
        List<Field> fields = classColumnFields.fields;
        for (Iterator<String> iterator = fieldNames.iterator(); iterator.hasNext();) {
            String fieldName = iterator.next();
            for (int i = 0; i < fields.size(); i++) {
                Field field = fields.get(i);
                if (field.getName().equals(fieldName)) {
                    columnsFields.fields.add(field);
                    columnsFields.columnNames.add(classColumnFields.columnNames.get(i));
                }
            }
        }
        return columnsFields;
    }
}
