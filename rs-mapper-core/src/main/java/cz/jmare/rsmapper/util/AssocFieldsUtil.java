package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.Assoc;
import cz.jmare.rsmapper.exception.RsWrongDefException;

public class AssocFieldsUtil {
    private static Map<Class<?>, List<FieldAssocAnno>> map = new HashMap<>();
    
    public static void traverseAssocFieldsWithInheritance(Class<?> clazz, BiConsumer<Field, Assoc> biConsumer, Set<String> ignoreOverridenNames) {
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            String name = field.getName();
            if (ignoreOverridenNames.contains(name)) {
                continue;
            }
            field.setAccessible(true);
            Assoc assoc = FieldsUtil.findAssocAnnotation(field);
            if (assoc != null) {
                biConsumer.accept(field, assoc);
            }
            ignoreOverridenNames.add(name);
        }
        Class<?> superClass = clazz.getSuperclass();
        if (superClass != null) {
            traverseAssocFieldsWithInheritance(superClass, biConsumer, ignoreOverridenNames);
        }
    }
    
    public static List<FieldAssocAnno> getFieldAssocs(Class<?> clazz) {
        List<FieldAssocAnno> list = map.get(clazz);
        if (list == null) {
            synchronized (AssocFieldsUtil.class) {
                list = map.get(clazz);  
                if (list == null) {
                    list = getFieldAssocsInternal(clazz);
                    map.put(clazz, list);
                }
            }
        }
        return list;
    }
    
    public static List<FieldAssocAnno> getFieldAssocsInternal(Class<?> clazz) {
        List<FieldAssocAnno> faa = new ArrayList<>();
        BiConsumer<Field, Assoc> biConsumer = new BiConsumer<Field, Assoc>() {
            @Override
            public void accept(Field field, Assoc assoc) {
                FieldAssocAnno fieldAssocAnno = new FieldAssocAnno(field, assoc);
                faa.add(fieldAssocAnno);
            }
        };
        traverseAssocFieldsWithInheritance(clazz, biConsumer, new HashSet<>());
        return faa;
    }
    
    public static FieldAssocAnno findClassToSingleClass(Class<?> clazz, Class<?> assocType) {
        List<FieldAssocAnno> fieldAssocs = getFieldAssocs(clazz);
        List<FieldAssocAnno> matchingList = fieldAssocs.stream()
                .filter(fa -> fa.field.getType().isAssignableFrom(assocType)).collect(Collectors.toList());
        if (matchingList.size() == 0) {
            throw new RsWrongDefException("Class " + clazz.getCanonicalName() + " hasn't association with type " + assocType);
        }
        if (matchingList.size() > 1) {
            throw new RsWrongDefException("Ambiguous definition. Class " + clazz.getCanonicalName()
                    + " has multiple associations with type " + assocType.getCanonicalName() + ". These are: "
                    + matchingList.stream().map(f -> f.field.getName()).collect(Collectors.joining(", ")));
        }
        return matchingList.get(0);
    }
    
    public static FieldAssocAnno findClassToCollectionClass(Class<?> clazz, Class<?> assocGenericTypeArgument) {
        List<FieldAssocAnno> fieldAssocs = getFieldAssocs(clazz);
        List<FieldAssocAnno> matchingList = fieldAssocs.stream().filter(fa -> {
            if (!Collection.class.isAssignableFrom(fa.field.getType())) {
                return false;
            }
            ParameterizedType genericType = (ParameterizedType) fa.field.getGenericType();
            Type[] actualTypeArguments = genericType.getActualTypeArguments();
            if (actualTypeArguments.length != 1) {
                return false;
            }
            Type type = actualTypeArguments[0];
            if (type instanceof Class<?>) {
                Class<?> genType = (Class<?>) type;
                if (!genType.isAssignableFrom(assocGenericTypeArgument)) {
                    return false;
                }
            }
            return true;
        }).collect(Collectors.toList());
        if (matchingList.size() == 0) {
            throw new RsWrongDefException("Class " + clazz.getCanonicalName() + " hasn't association with type " + assocGenericTypeArgument);
        }
        if (matchingList.size() > 1) {
            throw new RsWrongDefException("Ambiguous definition. Class " + clazz.getCanonicalName()
                    + " has multiple associations with collection of type " + assocGenericTypeArgument.getCanonicalName() + ". These are: "
                    + matchingList.stream().map(f -> f.field.getName()).collect(Collectors.joining(", ")));
        }
        return matchingList.get(0);
    }
    
    public static class FieldAssocAnno {
        public Field field;
        
        private Assoc assoc;

        /**
         * Null means unknown here, true means foreign key is here, false there
         */
        private Boolean fkIsHere;
        
        private FkKeysPrimKeys fkKeysPrimKeys;
        
        public FieldAssocAnno(Field field, Assoc assoc) {
            super();
            this.field = field;
            this.assoc = assoc;
            String value = assoc.value();
            if (!"".equals(value)) {
                if (value.contains("=")) {
                    throw new RsWrongDefException("fk=pk definition in @Assoc annotation but such definition is allowed only for 'fkHere' and 'fkThere' attributes. Field " + humanFieldName(field));
                }
                List<String> foreignKeyNames = splitKeys(value);
                fkKeysPrimKeys = new FkKeysPrimKeys(foreignKeyNames, null);
            }
            if (!"".equals(assoc.fkHere())) {
                if (fkKeysPrimKeys != null) {
                    throw new RsWrongDefException("Ambigiuous fk definition in @Assoc annotation, only one of 'value' or 'fkHere' or 'fkThere' attribute may be populated. Field " + humanFieldName(field));
                }
                fkKeysPrimKeys = parseKeys(assoc.fkHere());
                fkIsHere = true;
            }
            if (!"".equals(assoc.fkThere())) {
                if (fkKeysPrimKeys != null) {
                    throw new RsWrongDefException("Ambigiuous fk definition in @Assoc annotation, only one of 'value' or 'fkHere' or 'fkThere' attribute may be populated. Field " + humanFieldName(field));
                }
                fkKeysPrimKeys = parseKeys(assoc.fkThere());
                fkIsHere = false;
            }
            if (fkKeysPrimKeys == null) {
                throw new RsWrongDefException("One of 'value' or 'fkHere' or 'fkThere' attribute must be populated. Field " + humanFieldName(field));
            }
        }

        private String humanFieldName(Field field) {
            return field.getDeclaringClass().getCanonicalName() + "." + field.getName();
        }
        
        public static FkKeysPrimKeys parseKeys(String str) {
            int indexOf = str.indexOf("=");
            List<String> fks = null;
            List<String> pks = null;
            if (indexOf > 0) {
                fks = splitKeys(str.substring(0, indexOf).trim());
                pks = splitKeys(str.substring(indexOf + 1).trim());
            } else {
                fks = splitKeys(str);
            }
            return new FkKeysPrimKeys(fks, pks);
        }
        
        public static List<String> splitKeys(String str) {
            List<String> keys = new ArrayList<String>();
            if (str.contains(",")) {
                String[] split = str.split(",");
                for (String string : split) {
                    keys.add(string.trim());
                }
            } else {
                keys.add(str.trim());
            }
            return keys;
        }
        
        public List<String> getForeignKeyNames() {
            return fkKeysPrimKeys.fkKeys;
        }
        
        public List<String> getPrimaryKeyNames() {
            return fkKeysPrimKeys.pkKeys;
        }
        
        @Override
        public String toString() {
            return "FieldAssocAnno [field=" + field + ", assoc=" + assoc + "]";
        }
        
        /**
         * Pkkeys is nullable but when populated both keys must have the same size 
         */
        public static class FkKeysPrimKeys  {
            public List<String> fkKeys;
            public List<String> pkKeys;
            public FkKeysPrimKeys(List<String> fkKeys, List<String> pkKeys) {
                super();
                this.fkKeys = fkKeys;
                this.pkKeys = pkKeys;
            }
         }

        public Boolean getFkIsHere() {
            return fkIsHere;
        }
    }
}
