package cz.jmare.rsmapper.util;

import java.util.List;

/**
 * PlaceholdersQuery represents part of a select command, where introducing part (format: select something from tableName alias) is pregenerated
 * and PlaceholdersQuery will be appended behind it.<br>
 * It can contain joins to tables, where condition (word 'where' must be entered) part, having part, order by part and placeholders starting with colon.
 * Example:
 * <pre>
 * join group g on _1.id=g.emp_id where _1.name=? or _1.name=?
 * </pre> 
 */
public interface PlaceholdersQuery {
    /**
     * Return part which is behind table name. The part 'select something from table' is generate automatically and this should add part behind it
     * It should usually start with 'where ...' unless next joins added. The named parameters can be used
     */
    String getQueryString();
    
    /**
     * Get values to be used instead of question characters
     */
    List<Object> getValues();
}
