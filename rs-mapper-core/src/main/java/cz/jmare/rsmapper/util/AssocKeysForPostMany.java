package cz.jmare.rsmapper.util;

import cz.jmare.rsmapper.util.AssocFieldsUtil.FieldAssocAnno;

public class AssocKeysForPostMany extends AssocKeysForPost {

    public AssocKeysForPostMany(Class<?> leftClass, Class<?> pointsToClass, String leftAssocFieldName,
            String leftForeignKeyFieldNameDef) {
        super(leftClass, pointsToClass, leftAssocFieldName, leftForeignKeyFieldNameDef);
    }

    public AssocKeysForPostMany(Class<?> leftClass, Class<?> pointsToClass, String leftAssocFieldName) {
        super(leftClass, pointsToClass, leftAssocFieldName);
    }

    public AssocKeysForPostMany(Class<?> leftClass, Class<?> pointsToClass) {
        super(leftClass, pointsToClass);
    }

    @Override
    protected FieldAssocAnno findFieldAssocAnno() {
        FieldAssocAnno fieldAssocAnno = AssocFieldsUtil.findClassToCollectionClass(leftClass, pointsToClass);
        return fieldAssocAnno;
    }

}
