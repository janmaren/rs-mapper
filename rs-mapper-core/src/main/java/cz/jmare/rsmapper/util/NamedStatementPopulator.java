package cz.jmare.rsmapper.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.regex.Matcher;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.util.ReplaceCallback;

public class NamedStatementPopulator {
    private final static String PARSE_PATTERN = "(?<lineComment>--)|(?<startComment>/\\*)|(?<endComment>\\*/)|(?<apos>\\')|(?<terminator>\\r?\\n)|(?<question>\\?)|(?<parameter>:[a-zA-Z$_0-9]+)";

    /**
     * Return PreparedStatement to which all parameters are set 
     * @param connection
     * @param sql sql with named paramaters, like ':parameter'
     * @param parametersMap key and values to be passed to parameters
     * @return PreparedStatement
     */
    public static PreparedStatement prepareStatement(Handle handle, String sql, Map<String, Object> parametersMap) {
        DBProfile dbProfile = handle.getDbProfile();
        
        try {
            NamedStatementPopulatorResult extractNamedParameters = extractNamedParameters(sql, parametersMap);
            PreparedStatement preparedStatement = handle.getConnection().prepareStatement(extractNamedParameters.extractedSql);
            List<Object> extractedParameters = extractNamedParameters.extractedParameters;
            int index = 1;
            for (Object object : extractedParameters) {
                dbProfile.setObjectToPreparedStatement(preparedStatement, index++, object);
            }
            return preparedStatement;
        } catch (SQLException e) {
            throw new RuntimeException("Unable to prepare: " + sql, e);
        }
    }

    /**
     * Parses sql and convert it to contain question chars instead of named parameters and also return object values.
     * Note, the named parameter present in a comment or between quotes is not considered to be a parameter and it's ignored
     * @param sql
     * @param parametersMap
     * @return
     */
    public static NamedStatementPopulatorResult extractNamedParameters(String sql, Map<String, Object> parametersMap) {
        Set<String> keySet = new HashSet<>(parametersMap.keySet());
        List<Object> expandedParameters = new ArrayList<>();
        
        String extractedSql = ReplaceCallback.replace(PARSE_PATTERN, sql, new ReplaceCallback.Callback() {
            boolean inLineComment = false;
            boolean inComment = false;
            boolean inApos = false;
            
            @Override
            public String matchFound(Matcher matcher) {
                boolean lineComment = matcher.group("lineComment") != null;
                boolean startComment = matcher.group("startComment") != null;
                boolean endComment = matcher.group("endComment") != null;
                boolean terminator = matcher.group("terminator") != null;
                boolean apos = matcher.group("apos") != null;
                
                if (lineComment || startComment || endComment || terminator || apos) {
                    if (lineComment) {
                        inLineComment = true;
                    } else if (terminator) {
                        inLineComment = false;
                    } else if (startComment) {
                        inComment = true;
                    } else if (endComment) {
                        inComment = false;
                    } else if (apos) {
                        inApos = !inApos;
                    }
                } else {
                    if (inLineComment || inComment || inApos) {
                        return matcher.group();
                    }
                    if (matcher.group("question") != null) {
                        throw new IllegalArgumentException("Found parameter defined by '?' in " + sql + " which is not allowed. Only named parameters may be used");
                    }
                    String parameter = matcher.group("parameter");
                    if (parameter == null) {
                        throw new RuntimeException("Parsed as parameter but it is null");
                    }
                    String name = parameter.substring(1);
                    keySet.remove(name);
                    Object object = parametersMap.get(name);
                    if (object == null) {
                        if (!parametersMap.containsKey(name)) {
                            throw new IllegalArgumentException("Found parameter '" + name + "' in sql but not such parameter exists in map");
                        }
                    }
                    
                    if (object instanceof Collection) {
                        Collection<?> collection = (Collection<?>) object;
                        if (collection.size() == 0) {
                            throw new IllegalArgumentException("There is a collection value for parameter " + name + " but the collection is empty (unable to create '?' placeholders)");
                        }
                        expandedParameters.addAll(collection);
                        int number = collection.size();
                        return fillQuestions(number);
                    } else {
                        expandedParameters.add(object);
                        return "?";
                    }
                }
                
                return matcher.group();
            }});
        
        if (!keySet.isEmpty()) {
            throw new IllegalArgumentException("Passed parameters " + keySet + " not found in: " + sql);
        }
        NamedStatementPopulatorResult namedStatementPopulatorResult = new NamedStatementPopulatorResult();
        namedStatementPopulatorResult.extractedParameters = expandedParameters;
        namedStatementPopulatorResult.extractedSql = extractedSql;
        return namedStatementPopulatorResult;
    }
    
    private static String fillQuestions(int number) {
        StringJoiner sj = new StringJoiner(",");
        for (int i = 0; i < number; i++) {
            sj.add("?");
        }
        String par = sj.toString();
        return par;
    }

    public static class NamedStatementPopulatorResult {
        public List<Object> extractedParameters;
        
        public String extractedSql;
    }
    
    public static void main(String[] args) {
        List<String> vals = new ArrayList<>();
        vals.add("val1");
        vals.add("val2");
        Map<String, Object> map = new HashMap<>();
        map.put("par0", "single");
        map.put("par1", vals);
        NamedStatementPopulatorResult expandNamedParameters = extractNamedParameters("select * from table where b = :par0 and a in (:par1)", map);
        System.out.println(expandNamedParameters.extractedSql);
        System.out.println(expandNamedParameters.extractedParameters);
    }
}
