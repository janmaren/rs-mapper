package cz.jmare.rsmapper.util;

public class ArrayUtil {
    public static String[] concatArrays(String[] array1, String[] array2) {
        int aLen = array1.length;
        int bLen = array2.length;
        String[] result = new String[aLen + bLen];
        System.arraycopy(array1, 0, result, 0, aLen);
        System.arraycopy(array2, 0, result, aLen, bLen);
        return result;
    }
    
    public static String[] concatToArray(String[] array1, String string) {
        int aLen = array1.length;
        String[] array2 = {string};
        int bLen = array2.length;
        String[] result = new String[aLen + bLen];
        System.arraycopy(array1, 0, result, 0, aLen);
        System.arraycopy(array2, 0, result, aLen, bLen);
        return result;
    }
}
