package cz.jmare.rsmapper.util;

import java.sql.SQLException;
import java.util.function.Consumer;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.handle.Handle;

public class TransWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransWrapper.class);
    
    public static void doInTransaction(DSConf dataSource, int isolationLevel, Consumer<Handle> consumer) {
        try (Handle handle = dataSource.createHandle()) {
            doInTransaction(handle, isolationLevel, consumer);
        }
    }
    
    public static void doInTransaction(DSConf dataSource, Consumer<Handle> consumer) {
        doInTransaction(dataSource, -1, consumer);
    }
    
    public static <T> T getInTransaction(DSConf dataSource, int isolationLevel, boolean readOnly, Function<Handle, T> function) {
        try (Handle handle = dataSource.createHandle()) {
            return getInTransaction(handle, isolationLevel, readOnly, function);
        }
    }
    
    public static <T> T getInTransaction(DSConf dataSource, Function<Handle, T> function) {
        return getInTransaction(dataSource, -1, true, function);
    }
    
    public static void doInTransaction(Handle handle, int isolationLevel, Consumer<Handle> consumer) {
        int origTransactionIsolation;
        try {
            origTransactionIsolation = handle.getConnection().getTransactionIsolation();
        } catch (SQLException e2) {
            throw new IllegalStateException("Unable to get transaction isolation", e2);
        }
        boolean origAutoCommit;
        try {
            origAutoCommit = handle.getConnection().getAutoCommit();
        } catch (SQLException e2) {
            throw new IllegalStateException("Unable to get autocommit state", e2);
        }
        try {
            handle.getConnection().setAutoCommit(false);
            if (isolationLevel >= 0) {
                handle.getConnection().setTransactionIsolation(isolationLevel);
            }
            
            consumer.accept(handle);
                        
            handle.getConnection().commit();
        } catch (Exception e) {
            try {
                LOGGER.info("doing rollback");
                handle.getConnection().rollback();
            } catch (SQLException e1) {
                throw new IllegalStateException("Unable to rollback transaction after failure", e1);
            }
            throw new IllegalStateException(e);
        } finally {
            if (isolationLevel >= 0) {
                try {
                    handle.getConnection().setTransactionIsolation(origTransactionIsolation);
                } catch (SQLException e) {
                    throw new IllegalStateException("Unable to return back transaction isolation level", e);
                }
            }
            try {
                handle.getConnection().setAutoCommit(origAutoCommit);
            } catch (SQLException e) {
                throw new IllegalStateException("Unable to return autocommit back to riginal state");
            }
        }
    }
    
    public static <T> T getInTransaction(Handle handle, Function<Handle, T> function) {
        return getInTransaction(handle, -1, true, function);
    }
    
    public static <T> T getInTransaction(Handle handle, int isolationLevel, boolean readOnly, Function<Handle, T> function) {
        int origTransactionIsolation;
        try {
            origTransactionIsolation = handle.getConnection().getTransactionIsolation();
        } catch (SQLException e2) {
            throw new IllegalStateException("Unable to get transaction isolation", e2);
        }
        boolean origAutoCommit;
        try {
            origAutoCommit = handle.getConnection().getAutoCommit();
        } catch (SQLException e2) {
            throw new IllegalStateException("Unable to get autocommit state", e2);
        }
        try {
            handle.getConnection().setAutoCommit(false);
            if (isolationLevel >= 0) {
                handle.getConnection().setTransactionIsolation(isolationLevel);
            }
            
            T apply = function.apply(handle);
                        
            handle.getConnection().commit();
            
            return apply;
        } catch (Exception e) {
            try {
                LOGGER.info("doing rollback");
                handle.getConnection().rollback();
            } catch (SQLException e1) {
                throw new IllegalStateException("Unable to rollback transaction after failure", e1);
            }
            throw new IllegalStateException(e);
        } finally {
            if (isolationLevel >= 0) {
                try {
                    handle.getConnection().setTransactionIsolation(origTransactionIsolation);
                } catch (SQLException e) {
                    throw new IllegalStateException("Unable to return back transaction isolation level", e);
                }
            }
            try {
                handle.getConnection().setAutoCommit(origAutoCommit);
            } catch (SQLException e) {
                throw new IllegalStateException("Unable to return autocommit back to original state");
            }
        }
    }
    
    public static void doInTransaction(Handle handle, Consumer<Handle> consumer) {
        doInTransaction(handle, -1, consumer);
    }    
}
