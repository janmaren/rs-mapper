package cz.jmare.rsmapper.util;

import java.util.List;

public class ParamsToStringer {
    private Object[] params;

    public ParamsToStringer(Object... params) {
        this.params = params;
    }
    
    public ParamsToStringer(List<Object> params) {
        this.params = params.toArray(new Object[params.size()]);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Object object : params) {
            String addParam = null;
            if (object instanceof byte[]) {
                addParam = "<byte[] length=" + ((byte[]) object).length + ">";
            } if (object instanceof String) {
                addParam = "\"" + object.toString() + "\"";
            } else {
                addParam = object != null ? object.toString() : "null";
            }
            if (addParam != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(addParam.toString());
            }
        }
        
        return sb.toString();
    }
}