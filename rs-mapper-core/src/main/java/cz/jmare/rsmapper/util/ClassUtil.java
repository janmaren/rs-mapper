package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClassUtil {
    public static Field[] getAllFieldsSupportInheritance(final Class<?> cls) {
        final List<Field> allFieldsList = getAllFieldsListSubclassOverrides(cls);
        return allFieldsList.toArray(new Field[allFieldsList.size()]);
    }

    /**
     * Gets all fields of the given class and its parents (if any).
     */
    public static List<Field> getAllFieldsList(final Class<?> cls) {
        final List<Field> allFields = new ArrayList<>();
        Class<?> currentClass = cls;
        while (currentClass != null) {
            final Field[] declaredFields = currentClass.getDeclaredFields();
            Collections.addAll(allFields, declaredFields);
            currentClass = currentClass.getSuperclass();
        }
        return allFields;
    }
    
    /**
     * Gets all fields of the given class and its parents (if any). Fields from superclasses overidden in subclass is ommited.
     */
    public static List<Field> getAllFieldsListSubclassOverrides(final Class<?> cls) {
        final List<Field> allFields = new ArrayList<>();
        Class<?> currentClass = cls;
        while (currentClass != null) {
            final Field[] declaredFields = currentClass.getDeclaredFields();
            for (Field field : declaredFields) {
                String name = field.getName();
                if (!allFields.stream().filter(f -> f.getName().equals(name)).findAny().isPresent()) {
                    allFields.add(field);
                }
            }
            currentClass = currentClass.getSuperclass();
        }
        return allFields;
    }    
}
