package cz.jmare.rsmapper.util;

import java.lang.reflect.Field;
import java.util.List;

public class AssocFkValues {
    public List<String> foreignKeyFieldNames;
    public List<Field> foreignKeyFields;
    public List<Object> leftPrimaryKeyValues;
    public AssocFkValues(List<String> foreignKeyFieldName, List<Field> foreignKeyFields, List<Object> leftPrimaryKeyValues) {
        if (foreignKeyFieldName.size() != leftPrimaryKeyValues.size()) {
            throw new IllegalArgumentException("Number of foreign key field names is " + foreignKeyFieldName.size() + " but primary keys values size is " + leftPrimaryKeyValues.size());
        }
        this.foreignKeyFieldNames = foreignKeyFieldName;
        this.foreignKeyFields = foreignKeyFields;
        this.leftPrimaryKeyValues = leftPrimaryKeyValues;
    }
}
