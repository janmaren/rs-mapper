package cz.jmare.rsmapper.util;

import java.util.ArrayList;
import java.util.List;

public class SimplePlaceholdersQuery implements PlaceholdersQuery {
    private String queryString;
    
    private List<Object> values = new ArrayList<>();
    
    public SimplePlaceholdersQuery(String queryString, List<Object> values) {
        super();
        this.queryString = queryString;
        this.values = values;
    }

    public SimplePlaceholdersQuery(String queryString) {
        super();
        this.queryString = queryString;
    }

    @Override
    public String getQueryString() {
        return queryString;
    }

    @Override
    public List<Object> getValues() {
        return values;
    }
}
