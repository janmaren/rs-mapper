package cz.jmare.rsmapper.util;

import cz.jmare.rsmapper.type.BasicTypeDecider;
import cz.jmare.rsmapper.type.DefaultBasicTypeDecider;

public class GlobalRSConfig {
    private static GlobalRSConfig rsConfig;
    
    private BasicTypeDecider basicTypeDecider = new DefaultBasicTypeDecider();

    public static GlobalRSConfig getInstance() {
        if (rsConfig == null) {
            synchronized (GlobalRSConfig.class) {
                if (rsConfig == null) {
                    rsConfig = new GlobalRSConfig();
                }
            }
        }
        return rsConfig;
    }

    public void setBasicTypeDecider(BasicTypeDecider basicTypeDecider) {
        this.basicTypeDecider = basicTypeDecider;
    }

    public BasicTypeDecider getBasicTypeDecider() {
        return basicTypeDecider;
    }
}
