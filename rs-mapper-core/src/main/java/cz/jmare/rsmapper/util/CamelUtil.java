package cz.jmare.rsmapper.util;

public class CamelUtil {

    public static String decamelString(String str) {
        StringBuilder sb = new StringBuilder();
        for (String token : str.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            if (sb.length() > 0) {
                sb.append("_");
            }
            sb.append(token.toLowerCase());
        }
        return sb.toString();
    }

    public static String toCamel(String str) {
        String[] split = str.split("_");
        StringBuilder sb = new StringBuilder(split[0]);
        for (int i = 1; i < split.length; i++) {
            String str2 = split[i];
            str2 = str2.substring(0, 1).toUpperCase() + str2.substring(1);
            sb.append(str2);
        }
        return sb.toString();
    }

}
