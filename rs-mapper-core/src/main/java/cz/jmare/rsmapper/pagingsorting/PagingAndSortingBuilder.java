package cz.jmare.rsmapper.pagingsorting;

import cz.jmare.rsmapper.Query;

public abstract class PagingAndSortingBuilder {
    protected String orderColumns;
    protected Integer pageSize;
    protected Integer pageNumber;
    protected boolean oneMoreEntity;
    protected Query query;
    
    /**
     * Comma separated part which is behind 'order by'
     * @param orderColumns
     */
    public void setOrderColumns(String orderColumns) {
        this.orderColumns = orderColumns;
    }
    
    /**
     * Page size
     * @param pageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    
    /**
     * Page number starting with 0
     * @param pageNumber
     */
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }
    
    /**
     * Query represents part starting with 'where' clause followed by conditions over columns with placeholders and values to be populated<br>
     * The condition is used to get filtered paged list. It's usually reused somewhere else to also select count. 
     * @param conditionAndParams
     */
    public void setQuery(Query query) {
        this.query = query;
    }
    
    /**
     * When true the limit of returned entities will be one bigger.<br>
     * Useful to detect whether there is a next slice 
     * @param oneMoreEntity
     */
    public void setOneMoreEntity(boolean oneMoreEntity) {
        this.oneMoreEntity = oneMoreEntity;
    }
    
    public abstract Query build();
}
