package cz.jmare.rsmapper.pagingsorting;

import java.util.HashMap;
import java.util.Map;

import cz.jmare.rsmapper.Query;
import cz.jmare.rsmapper.SimpleQuery;
import cz.jmare.rsmapper.exception.RsWrongDefException;

public class H2PagingAndSortingBuilder extends PagingAndSortingBuilder {
    @Override
    public Query build() {
        StringBuilder sb = new StringBuilder();
        Map<String, Object> map = new HashMap<String, Object>();
        if (query != null) {
            sb.append(query.getQueryString());
            map.putAll(query.getQueryParameters());
        }
        if (orderColumns != null) {
            sb.append(" ").append("order by ").append(orderColumns);
        }
        if (pageSize != null) {
            sb.append(" ").append("limit ").append(":pagingLimit");
            int limit = pageSize;
            if (oneMoreEntity) {
                limit++;
            }
            map.put("pagingLimit", limit);
        }
        if (pageNumber != null) {
            if (pageSize == null) {
                throw new RsWrongDefException("Page number set but without pageSize, please pass also pageSize");
            }
            sb.append(" ").append("offset ").append(":pagingOffset");
            map.put("pagingOffset", pageNumber * pageSize);
        }
        return new SimpleQuery(sb.toString(), map);
    }
}
