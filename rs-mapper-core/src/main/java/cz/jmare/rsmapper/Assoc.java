package cz.jmare.rsmapper;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({FIELD}) 
@Retention(RUNTIME)
public @interface Assoc {
    /**
     * Name of foreign key field(s). When multiple fields present (compound foreign key)
     * they must be separated by comma.
     */
    String value() default "";
    
    /**
     * Name of <b>foreign</b> key field(s) which are present in <b>this</b> class. When multiple fields present (compound foreign key)
     * they must be separated by comma. Format: <br> <pre>&lt;fk1&gt;[,&lt;fk2&gt;[...]]</pre><br>
     * Example: <pre>employeeId</pre> or <pre>employeeId,classId</pre>
     * Optionally <b>primary</b> key field(s) (present in <b>other</b> class) can be present together with foreign key fields. In such
     * case the foreign and primary key are separated by '=' and the sizes of keys must be the same. Format:<br>
     * <pre>&lt;fk1&gt;[,&lt;fk2&gt;[...]]=&lt;pk1&gt;[,&lt;pk2&gt;[...]]</pre>
     * Example: <pre>employeeId=id</pre>
     */
    String fkHere() default "";
    
    /**
     * Name of <b>foreign</b> key field(s) which are present in the <b>other</b> class. When multiple fields present (compound foreign key)
     * they must be separated by comma. Format: <br> <pre>&lt;fk1&gt;[,&lt;fk2&gt;[...]]</pre><br>
     * Example: <pre>employeeId</pre> or <pre>employeeId,classId</pre>
     * Optionally <b>primary</b> key field(s) (present in <b>this</b> class) can be present together with foreign key fields. In such
     * case the foreign and primary key are separated by '=' and the sizes of keys must be the same. Format:<br>
     * <pre>&lt;fk1&gt;[,&lt;fk2&gt;[...]]=&lt;pk1&gt;[,&lt;pk2&gt;[...]]</pre>
     * Example: <pre>employeeId=id</pre>
     */
    String fkThere() default "";
}
