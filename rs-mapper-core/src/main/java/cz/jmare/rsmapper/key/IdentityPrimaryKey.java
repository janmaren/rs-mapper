package cz.jmare.rsmapper.key;

import java.util.ArrayList;
import java.util.List;

/**
 * Identity (auto increment) primary key 
 */
public class IdentityPrimaryKey implements PrimaryKey {
    private String fieldName;

    public IdentityPrimaryKey(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public List<String> getFieldNames() {
        ArrayList<String> fieldNameToReturn = new ArrayList<>();
        fieldNameToReturn.add(fieldName);
        return fieldNameToReturn;
    }

    @Override
    public int size() {
        return 1;
    }
}
