package cz.jmare.rsmapper.key;

import java.util.List;

import cz.jmare.rsmapper.exception.RsWrongDefException;

public interface PrimaryKey {
    public List<String> getFieldNames();
    
    static String getOneFieldName(List<String> fieldNames, Class<?> clazz) {
        if (fieldNames.size() > 1) {
            throw new RsWrongDefException("Required one field name for " + clazz + " but there is more");
        }
        if (fieldNames.size() == 0) {
            throw new RsWrongDefException("Required one field name for " + clazz + " but there is no present");
        }
        return fieldNames.get(0);
    }
    
    static String getOneFieldName(PrimaryKey primaryKey, Class<?> clazz) {
        return getOneFieldName(primaryKey.getFieldNames(), clazz);
    }

    public int size();
}
