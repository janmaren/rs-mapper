package cz.jmare.rsmapper.key;

import java.util.ArrayList;
import java.util.List;

public class SequencePrimaryKey implements PrimaryKey {
    private String fieldName;
    
    private String sequenceName;

    public SequencePrimaryKey(String fieldName, String sequenceName) {
        this.fieldName = fieldName;
        this.sequenceName = sequenceName;
    }

    public SequencePrimaryKey(String fieldName) {
        this.fieldName = fieldName;
    }
    
    @Override
    public List<String> getFieldNames() {
        ArrayList<String> fieldNameToReturn = new ArrayList<>();
        fieldNameToReturn.add(fieldName);
        return fieldNameToReturn;
    }

    public String getSequenceName(String tableName, String columnName, String sequencePattern) {
        if (sequenceName == null) {
            sequencePattern = sequencePattern.replace("<TABLE>", tableName);
            sequenceName = sequencePattern.replace("<IDCOLUMN>", columnName);
        }
        return sequenceName;
    }

    @Override
    public int size() {
        return 1;
    }
}
