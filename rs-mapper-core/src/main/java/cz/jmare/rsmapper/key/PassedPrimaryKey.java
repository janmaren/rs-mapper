package cz.jmare.rsmapper.key;

import java.util.ArrayList;
import java.util.List;

/**
 * Primary key(s) passed in field and there is no need to somehow fetch it from database 
 */
public class PassedPrimaryKey implements PrimaryKey {
    private List<String> fieldNames;

    public PassedPrimaryKey(List<String> fieldNames) {
        super();
        this.fieldNames = fieldNames;
    }

    public PassedPrimaryKey(String fieldName) {
        this.fieldNames = new ArrayList<>();
        this.fieldNames.add(fieldName);
    }

    @Override
    public List<String> getFieldNames() {
        return fieldNames;
    }

    @Override
    public int size() {
        return fieldNames.size();
    }
}
