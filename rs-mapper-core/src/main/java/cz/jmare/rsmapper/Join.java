package cz.jmare.rsmapper;

import cz.jmare.rsmapper.rs.FieldMapper;
import cz.jmare.rsmapper.util.ColumnsGenerator;

/**
 * Interface for a join regardless it has primary key or foreign key to join by  
 */
public interface Join {
    //void addToColumnsGenerator(ColumnsGenerator columnsGenerator);
    
    FieldMapper toFieldMapper(Class<?> leftClass);
    
    void init(ColumnsGenerator columnsGenerator, Class<?> leftClass);
}
