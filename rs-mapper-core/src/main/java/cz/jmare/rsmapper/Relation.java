package cz.jmare.rsmapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cz.jmare.rsmapper.active.basic.AbstractRelation;
import cz.jmare.rsmapper.dbprofile.CountType;
import cz.jmare.rsmapper.exception.ManyRecordsException;
import cz.jmare.rsmapper.exception.NoRecordException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.PrimaryKey;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.util.ColumnsGenerator;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.SqlCommandUtil;

public class Relation<T> extends AbstractRelation {

    public Relation(Class<T> entityClass) {
        super(entityClass);
        init();
    }

    public Relation(Class<T> entityClass, PrimaryKey primaryKey) {
        super(entityClass, primaryKey);
        init();
    }
    
    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and the sql is appended behind it.
     * The appendSql musn't contain named parameters or placeholders.
     * @param dsConf
     * @param appendSql sql to be appended, example: <pre>join tableB _2 on _1.id=_2.fkid</pre>
     * @return
     */
    public List<T> selectMany(DSConf dsConf, String appendSql) {
        try (Handle handle = dsConf.createHandle()) {
            return (List<T>) selectMany(handle, appendSql);
        }
    }
    
    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and the sql is appended behind it.
     * The appendSql musn't contain named parameters or placeholders.
     * @param handle
     * @param appendSql sql to be appended, example: <pre>join tableB _2 on _1.id=_2.fkid</pre>
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<T> selectMany(Handle handle, String appendSql) {
        return (List<T>) super.select(handle, appendSql, new HashMap<String, Object>());
    }

    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and the sql is appended behind it.
     * @param dsConf
     * @param appendSql sql to be appended, example: <pre>join tableB _2 on _1.id=_2.fkid</pre>
     * @param namedParametersMap parameters to replace placeholders with values
     * @return
     */
    public List<T> selectMany(DSConf dsConf, String appendSql, Map<String, Object> namedParametersMap) {
        try (Handle handle = dsConf.createHandle()) {
            return (List<T>) selectMany(handle, appendSql, namedParametersMap);
        }
    }
    
    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and the sql is appended behind it.
     * @param handle
     * @param appendSql sql to be appended, for example: <pre>join tableB _2 on _1.id=_2.fkid where _1.name=:name and _2.type=:type</pre>
     * @param namedParametersMap parameters to replace placeholders with values
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<T> selectMany(Handle handle, String appendSql, Map<String, Object> namedParametersMap) {
        return (List<T>) super.select(handle, appendSql, namedParametersMap);
    }

    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and by query the next part can be appended, like
     * <pre>join tableB _2 on _1.id=_2.fkid where _1.name=:name and _2.type=:type</pre><br>
     * @param dsConf
     * @param query part to be appended to basic select
     * @return
     */
    public List<T> selectMany(DSConf dsConf, Query query) {
        try (Handle handle = dsConf.createHandle()) {
            return (List<T>) selectMany(handle, query);
        }
    }

    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and by query the next part can be appended, like
     * <pre>join tableB _2 on _1.id=_2.fkid where _1.name=:name and _2.type=:type</pre><br>
     * @param handle
     * @param query part to be appended to basic select
     * @return
     */
    
    public List<T> selectMany(Handle handle, Query query) {
		return selectMany(handle, query.getQueryString(), query.getQueryParameters());
    }

    /**
     * Select one entity. The part 'select columns from table alias' is pregenerated and by query the next part can be appended, like
     * <pre>join tableB _2 on _1.id=_2.fkid where _1.name=:name and _2.type=:type</pre><br>
     * @param dsConf
     * @param appendSql sql part behind basic table
     * @param namedParametersMap pairs namedParameter-value
     * @return
     * @throws ManyRecordsException when resultset after grouping contains multiple entities
     * @throws NoRecordException when no record found corresponding with given parametersMap
     */
    public T selectOne(DSConf dsConf, String appendSql, Map<String, Object> namedParametersMap) throws NoRecordException, ManyRecordsException {
        try (Handle handle = dsConf.createHandle()) {
            return selectOne(handle, appendSql, namedParametersMap);
        }
    }
    
    /**
     * Select many entities. The part 'select columns from table alias' is pregenerated and by query the next part can be appended, like
     * <pre>join tableB _2 on _1.id=_2.fkid where _1.name=:name and _2.type=:type</pre><br>
     * @param handle
     * @param appendSql sql part behind basic table
     * @param namedParametersMap pairs namedParameter-value
     * @return
     * @throws ManyRecordsException when resultset after grouping contains multiple entities
     * @throws NoRecordException when no record found corresponding with given parametersMap
     */
    @SuppressWarnings("unchecked")
    public T selectOne(Handle handle, String appendSql, Map<String, Object> namedParametersMap) throws NoRecordException, ManyRecordsException {
        List<?> select = super.select(handle, appendSql, namedParametersMap);
        if (select.size() > 1) {
            throw new ManyRecordsException("Select returned more than 1 record");
        }
        if (select.size() == 0) {
            throw new NoRecordException(tablePrefix + tableName, namedParametersMap);
        }
        return (T) select.get(0);        
    }
    
    /**
     * Load entity by single primary key.
     * Load also referencing and referenced entities.
     * @param dsConf
     * @param idValue primary key value
     * @return
     * @throws NoRecordException when no record found corresponding with given parametersMap
     * @throws ManyRecordsException when multiple records found
     */
    public T loadOneById(DSConf dsConf, Object idValue) throws NoRecordException, ManyRecordsException {
        try (Handle handle = dsConf.createHandle()) {
            return loadOneById(handle, idValue);
        }
    }
    
    /**
     * Load entity by single primary key.
     * Load also referencing and referenced entities.
     * @param handle
     * @param idValue primary key value
     * @return
     * @throws NoRecordException when no record found corresponding with given parametersMap
     * @throws LoadException when multiple records found
     */
    @SuppressWarnings("unchecked")
    public T loadOneById(Handle handle, Object idValue) throws NoRecordException, ManyRecordsException {
        String primaryKeyFieldName = PrimaryKey.getOneFieldName(primaryKey, entityClass);
        return (T) super.loadOneByIdFieldsValues(handle, Map.of(primaryKeyFieldName, idValue));
    }
    
    /**
     * Load entity by composite id.
     * Load also referencing and referenced entities.
     * @param dsConf
     * @param id id parameters pairs fieldName-value
     * @return
     * @throws NoRecordException when no record found corresponding with given parametersMap 
     * @throws ManyRecordsException when multiple records found
     */
    public T loadOneByIdFields(DSConf dsConf, Map<String, Object> idFieldNamesValues) throws NoRecordException, ManyRecordsException {
        try (Handle handle = dsConf.createHandle()) {
            return loadOneByIdFields(handle, idFieldNamesValues);
        }
    }
    
    /**
     * Load entity by composite id.
     * Load also referencing and referenced entities.
     * @param handle
     * @param id id parameters pairs fieldName-value
     * @return
     * @throws NoRecordException when no record found corresponding with given parametersMap
     * @throws ManyRecordsException when multiple records found
     */
    @SuppressWarnings("unchecked")
    public T loadOneByIdFields(Handle handle, Map<String, Object> idFieldNamesValues) throws NoRecordException, ManyRecordsException {
        FieldsUtil.checkAllIdsPresent(entityClass, idFieldNamesValues.keySet(), primaryKey.getFieldNames().size());
        return (T) super.loadOneByIdFieldsValues(handle, idFieldNamesValues);
    }
    
    /**
     * Load entities. Filter them by parameters. 
     * Load also referencing and referenced entities.
     * @param dsConf
     * @param fieldNamesValues filtering parameters pairs fieldName-value
     * @return
     */
    public List<T> loadManyByFields(DSConf dsConf, Map<String, Object> fieldNamesValues) {
        try (Handle handle = dsConf.createHandle()) {
            return loadManyByFields(handle, fieldNamesValues);
        }
    }
    
    /**
     * Load entities. Filter them by parameters. 
     * Load also referencing and referenced entities.
     * @param handle
     * @param fieldNamesValues filtering parameters pairs fieldName-value
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<T> loadManyByFields(Handle handle, Map<String, Object> fieldNamesValues) {
        List<Object> list = super.loadManyByFieldsValues(handle, fieldNamesValues);
        return (List<T>) list;
    }
    
    /**
     * Load entities. The basic part is pregenerated as 'select columns from table alias' but
     * it is possible to append extending part of sql, like joins and where condition.
     * Load also referencing and referenced entities.
     * @param dsConf
     * @param appendSql sql part to be appended to basic select, like: join employee e on _1.id=e.employee_id where male=:male
     * @param namedParametersMap parameters pairs namedPar-value
     * @return
     */
    public List<T> loadMany(DSConf dsConf, String appendSql, Map<String, Object> namedParametersMap) {
        try (Handle handle = dsConf.createHandle()) {
            return loadMany(handle, appendSql, namedParametersMap);
        }
    }
    
    /**
     * Load entities. The basic part is pregenerated as 'select columns from table alias' but
     * it is possible to append extending part of sql, like joins and where condition.
     * Load also referencing and referenced entities.
     * Suitable for paging, sorting and filtering.
     * @param dsConf
     * @param query part to be appended to basic select
     * @return
     */
    public List<T> loadMany(DSConf dsConf, Query query) {
        return loadMany(dsConf, query.getQueryString(), query.getQueryParameters());
    }

    /**
     * Load all entities.
     * Not suitable for paging, sorting and filtering.
     * @param dsConf
     * @return
     */
    public List<T> loadManyAll(DSConf dsConf) {
        try (Handle handle = dsConf.createHandle()) {
            return loadManyAll(handle);
        }
    }
    
    /**
     * Load all entities.
     * Not suitable for paging, sorting and filtering.
     * @param dsConf
     * @return
     */
    public List<T> loadManyAll(Handle handle) {
        return loadMany(handle, new SimpleQuery());
    }
    
    /**
     * Load entities. The basic part is pregenerated as 'select columns from table alias' but
     * it is possible to append extending part of sql, like joins and where condition.
     * Load also referencing and referenced entities.
     * @param handle
     * @param appendSql sql part to be appended to basic select, like: join employee e on _1.id=e.employee_id where male=:male
     * @param namedParametersMap parameters pairs namedPar-value
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<T> loadMany(Handle handle, String appendSql, Map<String, Object> namedParametersMap) {
        List<Object> list = super.loadByQuery(handle, appendSql, namedParametersMap);
        return (List<T>) list;
    }
    
    /**
     * Load entities. The basic part is pregenerated as 'select columns from table alias' but
     * it is possible to append extending part of sql, like joins and where condition.
     * Load also referencing and referenced entities.
     * Suitable for paging, sorting and filtering.
     * @param handle
     * @param query part to be appended to basic select
     * @return
     */
    public List<T> loadMany(Handle handle, Query query) {
        return loadMany(handle, query.getQueryString(), query.getQueryParameters());
    }
    
    /**
     * Delete entity using id passed as idName-value. Suitable
     * for compound keys
     * Delete also referencing entities
     * @param dsConf
     * @param idFieldNamesValues pairs fieldName-value
     */
    public void deleteOneByIdFields(DSConf dsConf, Map<String, Object> idFieldNamesValues) {
        try (Handle handle = dsConf.createHandle()) {
            deleteOneByIdFieldsValues(handle, idFieldNamesValues);
        }
    }
    
    /**
     * Delete entity using id passed as idName-value. Suitable
     * for compound keys
     * Delete also referencing entities
     * @param handle
     * @param idFieldNamesValues pairs fieldName-value
     */
    public void deleteOneByIdFieldsValues(Handle handle, Map<String, Object> idFieldNamesValues) {
        FieldsUtil.checkAllIdsPresent(entityClass, idFieldNamesValues.keySet(), primaryKey.getFieldNames().size());
        super.deleteOneByIdFieldsValues(handle, idFieldNamesValues);
    }
    
    /**
     * Delete entity using id value.
     * Delete also referencing entities.
     * Only single id may be present.
     * @param dsConf
     * @param idValue value of id column
     */
    public void deleteOneById(DSConf dsConf, Object idValue) {
        try (Handle handle = dsConf.createHandle()) {
            deleteOneById(handle, idValue);
        }
    }
    
    /**
     * Delete entity using id value
     * Delete also referencing entities
     * Only single id may be present.
     * @param handle
     * @param idValue value of id column
     */
    public void deleteOneById(Handle handle, Object idValue) {
        super.deleteOneByIdFieldsValues(handle, Map.of(PrimaryKey.getOneFieldName(primaryKey, entityClass), idValue));
    }

    /**
     * Delete multiple records by condition defined by parameters. Parameters may contain any field of current entity.
     * @param dsConf
     * @param fieldNamesValues pairs fieldName-value
     */
    public void deleteManyByFields(DSConf dsConf, Map<String, Object> fieldNamesValues) {
        try (Handle handle = dsConf.createHandle()) {
            deleteManyByFields(handle, fieldNamesValues);
        }
    }

    /**
     * Delete multiple records by condition defined by parameters. Parameters may contain any field of current entity
     * @param handle
     * @param fieldNamesValues pairs fieldName-value
     */
    public void deleteManyByFields(Handle handle, Map<String, Object> fieldNamesValues) {
        super.deleteManyByFields(handle, fieldNamesValues);
    }
    
    /**
     * Delete entity. The id(s) used to delete record.
     * Delete also referencing entities.
     * @param dsConf
     * @param entity entity with id value(s)
     */
    public void delete(DSConf dsConf, Object entity) {
        try (Handle handle = dsConf.createHandle()) {
            delete(handle, entity);
        }
    }
    
    /**
     * Delete entity. The id(s) used to delete record.
     * Delete also referencing entities.
     * @param handle
     * @param entity entity with id value(s)
     */
    public void delete(Handle handle, Object entity) {
        super.delete(handle, entity);
    }
    
    /**
     * Update or insert entity.
     * Save also referencing and referenced entities.
     * @param dsConf
     * @param entity entity with id value(s)
     */
    public void save(DSConf dsConf, Object entity) {
        try (Handle handle = dsConf.createHandle()) {
            save(handle, entity);
        }
    }
    
    /**
     * Update or insert entity.
     * Save also referencing and referenced entities.
     * @param handle
     * @param entity entity with id value(s)
     */
    public void save(Handle handle, Object entity) {
        super.save(handle, entity);
    }

    /**
     * Select count. The select command will be built to comprise current table plus query.
     * @param dsConf
     * @param query
     * @return distinct number of primary keys
     */
    public long selectCount(DSConf dsConf, Query query) {
        try (Handle handle = dsConf.createHandle()) {
            return selectCount(handle, query);
        }
    }
    
    /**
     * Select count. The select command will be built to comprise current table plus query.
     * @param handle
     * @param query query to be appended or use empty instance of {@link SimpleQuery}
     * @return distinct number of primary keys
     */
    public long selectCount(Handle handle, Query query) {
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
        String primKeyColumns = classColumnFields.columnNames.stream().map(colName -> alias + "." + colName).collect(Collectors.joining(", "));
        if (handle.getDbProfile().getCountType() == CountType.COUNT_DISTINCT) {
            return SqlCommandUtil.selectOneScalar(handle, "count(distinct " + primKeyColumns + ")", Long.class,
                    tablePrefix + tableName + " " + alias, query.getQueryString(), query.getQueryParameters());
        } else if (handle.getDbProfile().getCountType() == CountType.COUNT_WRAPPED_SELECT) {
            return SqlCommandUtil.selectOneScalar(handle, "count(*)", Long.class,
                    "(select distinct " + primKeyColumns + " from " + tablePrefix + tableName + " " + alias +  " " + query.getQueryString() + ")", "", query.getQueryParameters());
        } else {
            throw new IllegalStateException("Not supported count type");
        }
    }
    
    /**
     * Select count of all records in current table.
     * @param dsConf
     * @return distinct number of primary keys
     */
    public long selectCountAll(DSConf dsConf) {
        try (Handle handle = dsConf.createHandle()) {
            return selectCountAll(handle);
        }
    }
    
    /**
     * Select count of all records in current table.
     * @param handle
     * @param query query to be appended or use empty instance of {@link SimpleQuery}
     * @return distinct number of primary keys
     */
    public long selectCountAll(Handle handle) {
        return selectCount(handle, new SimpleQuery());
    }
    
    /**
     * Set primary key
     */
    public Relation<T> withPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }
    
    /**
     * Set human readable alias to replace default alias, like _1, _2,...
     * @param alias
     * @return
     */
    public Relation<T> withAlias(String alias) {
        this.customAlias = alias;
        init();
        return this;
    }
    
    /**
     * Set joins, ToOne, or ToOnePrim or ToMany. The appropriate association will be also loaded.
     * @param joins
     * @return
     */
    public Relation<T> withJoins(Join... joins) {
        this.joins = joins;
        init();
        return this;
    }
    
    void setJoinsInternal(Join... joins) {
        this.joins = joins;
    }
    
    /**
     * Set names of fields omitted in insert
     * @param nonInsertableFieldNames
     * @return
     */
    public Relation<T> withNonInsertableFields(String... nonInsertableFieldNames) {
        this.nonInsertableFieldNames = nonInsertableFieldNames;
        return this;
    }
    
    /**
     * Set names of fields omitted in update
     * @param nonUpdatableFieldNames
     * @return
     */
    public Relation<T> withNonUpdatableFields(String... nonUpdatableFieldNames) {
        this.nonUpdatableFieldNames = nonUpdatableFieldNames;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in insert.
     * @return
     */
    public Relation<T> withNonInsertableNulls() {
        this.nonInsertableNulls = true;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in update.
     * @return
     */
    public Relation<T> withNonUpdatableNulls() {
        this.nonUpdatableNulls = true;
        return this;
    }
    
    /**
     * A schema, catalog or database prefix before a table.
     * @param tablePrefix
     * @return
     */
    public Relation<T> withTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
        if (!tablePrefix.endsWith(".")) {
            this.tablePrefix += ".";
        }
        return this;
    }
    
    public void init() {
        columnsGenerator = new ColumnsGenerator();
        addToColumnsGenerator(columnsGenerator);
        
        initBasicByAnno();
    }
}
