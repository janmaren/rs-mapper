package cz.jmare.rsmapper;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.dbprofile.DefaultDBProfile;
import cz.jmare.rsmapper.dbprofile.H2DBProfile;
import cz.jmare.rsmapper.dbprofile.PostgresDBProfile;
import cz.jmare.rsmapper.exception.RsConnectionException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.handle.SimpleHandle;
import cz.jmare.util.ClassUtil;

public class DSConf {
    private static final Logger LOGGER = LoggerFactory.getLogger(DSConf.class);
    
    private DataSource dataSource;
    
    private DBProfile dbProfile;
    
    private Boolean supportSpringTransactions;
    
    private static Method doGetConnectionMethod; 
    
    private String sequencePattern = "<TABLE>_<IDCOLUMN>_SEQ";
            
    static {
        try {
            Class<?> clazz = ClassUtil.getClassByName("org.springframework.jdbc.datasource.DataSourceUtils");
            doGetConnectionMethod = clazz.getMethod("doGetConnection", DataSource.class);
        } catch (NoSuchMethodException | SecurityException | RsWrongDefException e) {
            // not supported Spring
        }
    }
    
    public DSConf(DataSource dataSource) {
        super();
        this.dataSource = dataSource;
        detectProfile();
        detectSpring();
    }

    public DSConf(DataSource dataSource, DBProfile dbProfile) {
        super();
        this.dataSource = dataSource;
        this.dbProfile = dbProfile;
        detectSpring();
    }

    public DBProfile getDbProfile() {
        return dbProfile;
    }

    public Handle createHandle() {
        try {
            if (supportSpringTransactions) {
                Connection connection = getConnectionBySpring();
                SimpleHandle simpleHandle = new SimpleHandle(connection, dbProfile);
                simpleHandle.enableSupportSpringTransactions(dataSource);
                simpleHandle.setSequencePattern(sequencePattern);
                return simpleHandle;
            } else {
                Connection connection = this.dataSource.getConnection();
                SimpleHandle simpleHandle = new SimpleHandle(connection, dbProfile);
                simpleHandle.setSequencePattern(sequencePattern);
                return simpleHandle;
            }
        } catch (SQLException e) {
            throw new RsConnectionException("Unable to obtain connection from datasource", e);
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }
    
    private void detectProfile() {
        Connection connection;
        try {
            connection = this.dataSource.getConnection();
        } catch (SQLException e) {
            LOGGER.warn("Unable to detect concrete dbProfile because unable to connect database. Using DefaultDBProfile");
            this.dbProfile = new DefaultDBProfile();
            return;
        }
        String driverNameLowerCase = null;
        try {
            DatabaseMetaData metaData = connection.getMetaData();
            driverNameLowerCase = metaData.getDriverName().toLowerCase();
            if (driverNameLowerCase.contains("h2")) {
                this.dbProfile = new H2DBProfile();
            } else if (driverNameLowerCase.contains("postgres")) {
                this.dbProfile = new PostgresDBProfile();
            } else {
                this.dbProfile = new DefaultDBProfile();
            }
        } catch (SQLException e) {
            LOGGER.warn("For this kind of DBMS [" + driverNameLowerCase + "] is no db profile, using DefaultDBProfile");
            this.dbProfile = new DefaultDBProfile();
            return;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }
    
    private void detectSpring() {
        supportSpringTransactions = doGetConnectionMethod != null;
    }
    
    private Connection getConnectionBySpring() {
        try {
            Connection c = (Connection) doGetConnectionMethod.invoke(null, dataSource);
            return c;
        } catch (Exception e) {
            if (e instanceof SQLException) {
                SQLException sqlException = (SQLException) e;
                throw new RsConnectionException("Unable to obtain connection by Spring", sqlException);
            } else {
                throw new RsWrongDefException("Unable to obtain connection by Spring", e);
            }
        }
    }

    public String getSequencePattern() {
        return sequencePattern;
    }

    public void setSequencePattern(String sequencePattern) {
        this.sequencePattern = sequencePattern;
    }
}
