package cz.jmare.rsmapper;

import java.util.Map;

import cz.jmare.rsmapper.handle.Handle;

/**
 * Join to table which has a foreign key
 * This table record is created after primary key record
 */
public interface PostJoin extends Join {
    /**
     * Save joined entity(ies) after primary key entity
     * @param handle
     * @param leftObject
     * @param leftPrimaryKey map field-value with primary key of left object 
     */
    public void saveByLeftPk(Handle handle, Object leftObject, Map<String, Object> leftPrimaryKey);
    
    /**
     * Delete record(s) before primary key record
     * @param handle
     * @param leftClass note the instance of left object needn't be loaded but Class should be sufficient
     * @param leftPrimaryKey map field-value with primary key of left object 
     */
    public void deleteByLeftPk(Handle handle, Class<?> leftClass, Map<String, Object> leftPrimaryKey);

    /**
     * Load entity(ies) attached to primary key entity
     * @param handle
     * @param leftObject
     * @param leftPrimaryKey map field-value with primary key of left object 
     */
    public void loadByLeftPk(Handle handle, Object leftObject, Map<String, Object> leftPrimaryKey);
}
