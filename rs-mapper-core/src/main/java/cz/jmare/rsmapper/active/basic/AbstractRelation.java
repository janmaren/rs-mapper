package cz.jmare.rsmapper.active.basic;

import static cz.jmare.rsmapper.util.SqlCommandUtil.deleteRecordsFromTable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.EntityAnnoFetcher;
import cz.jmare.rsmapper.EntityAnnoFetcher.EntityData;
import cz.jmare.rsmapper.Join;
import cz.jmare.rsmapper.PostJoin;
import cz.jmare.rsmapper.PreJoin;
import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.NoRecordException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.exception.SaveException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.IdentityPrimaryKey;
import cz.jmare.rsmapper.key.PassedPrimaryKey;
import cz.jmare.rsmapper.key.PrimaryKey;
import cz.jmare.rsmapper.key.SequencePrimaryKey;
import cz.jmare.rsmapper.rs.CollectionMapper;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.CompoundKeyFunction;
import cz.jmare.rsmapper.rs.FieldMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.util.ArrayUtil;
import cz.jmare.rsmapper.util.ColumnsGenerator;
import cz.jmare.rsmapper.util.ColumnsGenerator.ColumnsPartWithTypes;
import cz.jmare.rsmapper.util.ConvertUtil;
import cz.jmare.rsmapper.util.FieldAccessUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.NamedStatementPopulator;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.RSToMap;
import cz.jmare.rsmapper.util.SqlCommandUtil;
import cz.jmare.util.KeyUtil;

public abstract class AbstractRelation {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRelation.class);
    
    protected String tableName;
    
    protected String tablePrefix = "";
    
    protected PrimaryKey primaryKey;

    protected Join[] joins = new Join[0];

    protected Class<?> entityClass;
    
    protected String customAlias;
    
    protected String alias;
    
    protected String[] nonInsertableFieldNames = new String[0];
    
    protected String[] nonUpdatableFieldNames = new String[0];
    
    protected ColumnsGenerator columnsGenerator;
    
    protected boolean nonInsertableNulls;
    
    protected boolean nonUpdatableNulls;
    
    public AbstractRelation(Class<?> entityClass, PrimaryKey primaryKey) {
        this.entityClass = entityClass;
        this.primaryKey = primaryKey;
    }
    
    public AbstractRelation(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    public void save(Handle handle, Object object) {
        if (object == null) {
            throw new RsWrongDefException("Expected instance of " + entityClass + " but passed null");
        }
        if (object.getClass() != entityClass) {
            throw new RsWrongDefException("Saving object which is " + object.getClass() + " but expected " + entityClass);
        }
        
        for (Join join : joins) {
            if (join instanceof PreJoin) {
                PreJoin preJoin = (PreJoin) join;
                preJoin.savePrimByLeftAssoc(handle, object);
            }
        }
 
        if (primaryKey instanceof PassedPrimaryKey) {
            List<String> idFieldNames = primaryKey.getFieldNames();
            for (String idFieldName : idFieldNames) {
                Object primaryKeyValue = FieldAccessUtil.getBasicValue(object, idFieldName);
                if (primaryKeyValue == null) {
                    throw new RsWrongDefException("Object " + object.getClass().getCanonicalName() + " hasn't set field " + idFieldName);
                }
            }

            Collection<String> nonInsertableFields = FieldsUtil.getNonInsertableFields(object, new ArrayList<>(), false, nonInsertableFieldNames);
            Inserter inserter = new Inserter(tablePrefix + tableName, nonInsertableFields);
            inserter.setNonInsertableNulls(nonInsertableNulls);
            LinkedHashMap<String, Object> parameters = FieldsUtil.extractColumnsValuesMap(object, idFieldNames);
            boolean exists = SqlCommandUtil.selectExists(handle, tablePrefix + tableName, parameters);
            if (!exists) {
                inserter.execute(handle, object);
            } else {
                LOGGER.debug("Record with given id already exists, doing update");
                Updater updater = new Updater(tablePrefix + tableName, idFieldNames);
                updater.withNonUpdatableFields(nonUpdatableFieldNames);
                updater.setNonUpdatableNulls(nonUpdatableNulls);
                int updateCount = updater.execute(handle, object);
                if (updateCount != 1) {
                    throw new RsWrongDefException("Passed primary key " + parameters + " but no such record exists in " + tablePrefix + tableName);
                }
            }
        } else if (primaryKey instanceof IdentityPrimaryKey) {
            String primaryKeyFieldName = PrimaryKey.getOneFieldName(primaryKey.getFieldNames(), entityClass);
            Object primaryKeyValue = FieldAccessUtil.getBasicValue(object, primaryKeyFieldName);
            if (primaryKeyValue == null) {
                Collection<String> nonInsertableFields = FieldsUtil.getNonInsertableFields(object, new ArrayList<>(), false, ArrayUtil.concatToArray(nonInsertableFieldNames, primaryKeyFieldName));
                Inserter inserter = new Inserter(tablePrefix + tableName, nonInsertableFields);
                inserter.setNonInsertableNulls(nonInsertableNulls);
                inserter.execute(handle, object);
            } else {
                Updater updater = new Updater(tablePrefix + tableName, primaryKeyFieldName);
                updater.withNonUpdatableFields(nonUpdatableFieldNames);
                updater.setNonUpdatableNulls(nonUpdatableNulls);
                int updateCount = updater.execute(handle, object);
                if (updateCount == 0) {
                    Collection<String> nonInsertableFields = FieldsUtil.getNonInsertableFields(object, new ArrayList<>(), false, nonInsertableFieldNames);
                    Inserter inserter = new Inserter(tablePrefix + tableName, nonInsertableFields);
                    inserter.setNonInsertableNulls(nonInsertableNulls);
                    inserter.execute(handle, object);
                }
            }
        } else if (primaryKey instanceof SequencePrimaryKey) {
            String primaryKeyFieldName = PrimaryKey.getOneFieldName(primaryKey.getFieldNames(), entityClass);
            Object primaryKeyValue = FieldAccessUtil.getBasicValue(object, primaryKeyFieldName);
            if (primaryKeyValue == null) {
                populatePrimaryKeyBySequence(handle, object, primaryKeyFieldName);
                
                Collection<String> nonInsertableFields = FieldsUtil.getNonInsertableFields(object, new ArrayList<>(), false, nonInsertableFieldNames);
                Inserter inserter = new Inserter(tablePrefix + tableName, nonInsertableFields);
                inserter.setNonInsertableNulls(nonInsertableNulls);
                inserter.execute(handle, object);
            } else {
                Updater updater = new Updater(tablePrefix + tableName, primaryKeyFieldName);
                updater.withNonUpdatableFields(nonUpdatableFieldNames);
                updater.setNonUpdatableNulls(nonUpdatableNulls);
                int updateCount = updater.execute(handle, object);
                if (updateCount == 0) {
                    Collection<String> nonInsertableFields = FieldsUtil.getNonInsertableFields(object, new ArrayList<>(), false, nonInsertableFieldNames);
                    Inserter inserter = new Inserter(tablePrefix + tableName, nonInsertableFields);
                    inserter.setNonInsertableNulls(nonInsertableNulls);
                    inserter.execute(handle, object);
                }
            }
        } else {
            throw new RsWrongDefException("No Id field defined for " + entityClass);
        }
        
        for (Join join : joins) {
            if (join instanceof PostJoin) {
                //List<Object> objectKey = FieldAccessUtil.getBasicValue(object, primaryKey.getFieldNames());
                PostJoin postSaver = (PostJoin) join;
                Map<String, Object> pkFieldsValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
                postSaver.saveByLeftPk(handle, object, pkFieldsValues);
            }
            if (join instanceof PreJoin) {
                PreJoin preJoin = (PreJoin) join;
                preJoin.deleteOrphan(handle, object);
            }
        }        
    }

    private void populatePrimaryKeyBySequence(Handle handle, Object object, String primaryKeyFieldName) {
        DBProfile dbProfile = handle.getDbProfile();
        SequencePrimaryKey sequencePrimaryKey = (SequencePrimaryKey) primaryKey;
        String sequenceName = sequencePrimaryKey.getSequenceName(tablePrefix + tableName, FieldsUtil.columnNameByFieldName(entityClass, primaryKeyFieldName), handle.getSequencePattern());
        String sqlForSequence = dbProfile.getSqlForSequence(sequenceName);
        try (Statement pst = handle.getConnection().createStatement()) {
            ResultSet rs = pst.executeQuery(sqlForSequence);
            if (rs.next()) {
                Class<?> requiredType = FieldsUtil.typeByFieldName(object.getClass(), primaryKeyFieldName);
                Object value;
                try {
                    value = rs.getObject(1, requiredType);
                } catch (Exception e) {
                    value = dbProfile.getObjectFromResultSet(rs, 1);
                    value = ConvertUtil.convertValueToRequiredType(value, requiredType);
                }
                FieldAccessUtil.setBasicValueWithInheritance(object, primaryKeyFieldName, value);
            } else {
                throw new RsWrongDefException("Sequencer " + sequenceName + " didn't return a value");
            }
        } catch (SQLException e) {
            throw new SaveException("Unable to get sequence by " + sqlForSequence, e);
        }
    }

    protected List<Object> loadByQuery(Handle handle, String behindTablePart, Map<String, Object> params) {
        Selecter selecter = new Selecter(tablePrefix + tableName, primaryKey.getFieldNames()).withAlias(alias);
        List<Object> list = selecter.queryWithNamedParams(handle, entityClass, behindTablePart, params);
        for (Object object : list) {
            doLoadJoins(handle, object);
        }
        return list;        
    }

    protected void doLoadJoins(Handle handle, Object object) {
        for (Join join : joins) {
            if (join instanceof PreJoin) {
                PreJoin preJoin = (PreJoin) join;
                preJoin.loadPrimByLeftFk(handle, object);        
            } else if (join instanceof PostJoin) {
                PostJoin postJoin = (PostJoin) join;
                //List<Object> primKeyValue = FieldAccessUtil.getBasicValue(object, this.primaryKey.getFieldNames());
                Map<String, Object> pkFieldsValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
                postJoin.loadByLeftPk(handle, object, pkFieldsValues);
            }
        }
    }
    
    protected List<Object> loadManyByFieldsValues(Handle handle, Map<String, Object> fieldsValues) {
        List<Object> list = loadManyByFieldsValuesNoAssoc(handle, fieldsValues);
        
        for (Object object : list) {
            doLoadJoins(handle, object);
        }
        
        return list;
    }

    protected List<Object> loadManyByFieldsValuesNoAssoc(Handle handle, Map<String, Object> fieldsValues) {
        Selecter selecter = new Selecter(tablePrefix + tableName, primaryKey.getFieldNames());
        List<Object> values = new ArrayList<Object>();
        String behindTablePart = null;
        if (fieldsValues.size() > 0) {
            behindTablePart = "where " + fieldsValues.entrySet().stream().map(e -> {
                values.add(e.getValue());
                return FieldsUtil.columnNameByFieldName(entityClass, e.getKey()) + "=?";
            }).collect(Collectors.joining(" AND "));
        }
        
        List<Object> list = selecter.queryByPlaceholders(handle, entityClass, behindTablePart, values);
        return list;
    }
    
    /**
     * 
     * @param handle
     * @param fieldsValues
     * @return
     * @throws NoRecordException when no record found corresponding with given parametersMap
     */
    protected Object loadOneByIdFieldsValues(Handle handle, Map<String, Object> fieldsValues) {
        Object object = loadOneByIdFieldsValuesNoAssoc(handle, fieldsValues);
        
        doLoadJoins(handle, object);
        
        return object;
    }

    protected Object loadOneByIdFieldsValuesNoAssoc(Handle handle, Map<String, Object> idValues) {
        if (idValues.size() != primaryKey.getFieldNames().size()) {
            throw new RsWrongDefException("Passed id parameter(s) " + idValues + " but number of primary key(s) is " + primaryKey.size());
        }
        Selecter selecter = new Selecter(tablePrefix + tableName, primaryKey.getFieldNames());
        selecter.withAlias(alias);
        
        String behindTablePart = FieldsUtil.generateWhereClause(entityClass, primaryKey.getFieldNames());
        
        Object object = selecter.queryOneByPlaceholders(handle, entityClass, behindTablePart, KeyUtil.values(idValues, primaryKey.getFieldNames()));
        return object;
    }  
    
    protected void delete(Handle handle, Object object) {
        for (Join join : joins) {
            if (join instanceof PostJoin) {
                PostJoin postJoin = (PostJoin) join;
                Map<String, Object> pkFieldsValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
                postJoin.deleteByLeftPk(handle, entityClass, pkFieldsValues);
            }
        }
        LinkedHashMap<String, Object> idColumnValues = FieldsUtil.extractColumnsValuesMap(object, primaryKey.getFieldNames());
        deleteRecordsFromTable(handle, tablePrefix + tableName, idColumnValues);
    }
    
    /**
     * Delete record by primary key which can be compound key
     * @param handle
     * @param idValues single or compound key
     */
    protected void deleteOneByIdFieldsValues(Handle handle, Map<String, Object> idValues) {
        for (Join join : joins) {
            if (join instanceof PostJoin) {
                PostJoin postJoin = (PostJoin) join;
                postJoin.deleteByLeftPk(handle, entityClass, idValues);
            }
        }
        
        Optional<Join> isPrejoinOpt = Arrays.asList(joins).stream().filter(j -> j instanceof PreJoin && ((PreJoin) j).isDeleteOrphan()).findFirst();
        Object deletingObject = null;
        if (isPrejoinOpt.isPresent()) {
            deletingObject = loadOneByIdFieldsValuesNoAssoc(handle, idValues);
        } 
        
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
        LinkedHashMap<String, Object> columnsValuesMap = KeyUtil.mergeKeysValues(classColumnFields.columnNames, KeyUtil.values(idValues, primaryKey.getFieldNames()));
        deleteRecordsFromTable(handle, tablePrefix + tableName, columnsValuesMap);
        
        if (deletingObject != null) {
            for (Join join : joins) {
                if (join instanceof PreJoin) {
                    PreJoin preJoin = (PreJoin) join;
                    preJoin.deleteByLeftFk(handle, deletingObject);
                }
            }
        }        
    }

    protected void deleteManyByFields(Handle handle, Map<String, Object> fieldsValuesParams) {
        List<Object> objects = loadManyByFieldsValuesNoAssoc(handle, fieldsValuesParams);
        for (Object object : objects) {
            LinkedHashMap<String, Object> idFieldsValuesParams = FieldsUtil.extractFieldsValuesMap(object, primaryKey.getFieldNames());
            deleteOneByIdFieldsValues(handle, idFieldsValuesParams);
        }
    }
    
    protected void addToColumnsGenerator(ColumnsGenerator columnsGenerator) {
        alias = columnsGenerator.addClass(entityClass, customAlias);
        for (Join saverRelated : joins) {
            saverRelated.init(columnsGenerator, entityClass);
        }
    }
    
    /**
     * Do select. This method pregenerates 'select columns from table alias'
     * @param handle
     * @param sql sql part to be appended
     * @param parametersMap
     * @return
     */
    protected List<?> select(Handle handle, String sql, Map<String, Object> parametersMap) {
        ColumnsPartWithTypes columnsPartWithTypes = columnsGenerator.getColumnsPartWithTypes();

        String selectPart = columnsPartWithTypes.columnsPart;
        String sqlWhole = "select " + selectPart + " " + "from " + tablePrefix + tableName + " " + alias + " " + sql;
        
        List<Map<String, Object>> convertAll;
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sqlWhole, parametersMap)) {
            LOGGER.debug(">>>SQL: {}", sqlWhole);
            LOGGER.debug(">>>Params: {}", parametersMap);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                convertAll = RSToMap.convertAllToTypes(resultSet, columnsPartWithTypes.columnFields, handle.getDbProfile());
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select " + sqlWhole, e);
        }
        
        FieldMapper[] fieldMappers = toFieldMappers(joins, entityClass);
        
        CollectionMapper collectionMapper;
        
        List<String> fieldNames = primaryKey.getFieldNames();
        String columnSuffix = "_" + alias;
        if (fieldNames.size() > 1) {
            // Prepare by compound grouping key
            List<String> columns = new ArrayList<>();
            for (String keyField : fieldNames) {
                String columnNameByFieldName = FieldsUtil.columnNameByFieldName(entityClass, keyField) + "_" + alias;
                columns.add(columnNameByFieldName);
            }
            collectionMapper = new CollectionMapper(new CompoundKeyFunction(columns.toArray(new String[columns.size()])), new SimpleRemapper(entityClass, columnSuffix), fieldMappers);
        } else {
            // Prepare by single column grouping key
            String groupColumPrimName = FieldsUtil.columnNameByFieldName(entityClass, PrimaryKey.getOneFieldName(primaryKey.getFieldNames(), entityClass)) + columnSuffix;
            collectionMapper = new CollectionMapper(groupColumPrimName, new SimpleRemapper(entityClass, columnSuffix), fieldMappers);
        }
        
        List<?> list = collectionMapper.remapToList(convertAll, entityClass);
        LOGGER.debug(">>>Returned: {} entities", list.size());
        return list;
    }

    protected void initBasicByAnno() {
        EntityData entityData = EntityAnnoFetcher.initMeta(entityClass, primaryKey, tableName);
        this.primaryKey = entityData.primaryKey;
        this.tableName = entityData.tableName;
    }

    public static FieldMapper[] toFieldMappers(Join[] saverRelateds, Class<?> entityClass) {
        List<FieldMapper> fieldMappersList = new ArrayList<FieldMapper>();
        for (Join saverRelated : saverRelateds) {
            FieldMapper fieldMapper = saverRelated.toFieldMapper(entityClass);
            fieldMappersList.add(fieldMapper);
        }
        FieldMapper[] fieldMappers = fieldMappersList.toArray(new FieldMapper[fieldMappersList.size()]);
        return fieldMappers;
    }
}
