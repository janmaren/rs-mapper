package cz.jmare.rsmapper.active.basic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.ManyRecordsException;
import cz.jmare.rsmapper.exception.NoRecordException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.rs.CollectionMapper;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.CompoundKeyFunction;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.util.ColumnsGenerator;
import cz.jmare.rsmapper.util.ConvertUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.NamedStatementPopulator;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.RSToMap;

public class Selecter {
    private static final Logger LOGGER = LoggerFactory.getLogger(Selecter.class);

    private String tableName;
    
    private List<String> keyFieldName;

    private String alias;
    
    public Selecter(String tableName, String keyFieldName) {
        this.tableName = tableName;
        this.keyFieldName = new ArrayList<>();
        this.keyFieldName.add(keyFieldName);
    }
    
    public Selecter(String tableName, List<String> keyFieldName) {
        super();
        this.tableName = tableName;
        if (keyFieldName.size() == 0) {
            throw new RsWrongDefException("At least one primary key column must be present");
        }
        this.keyFieldName = keyFieldName;
    }

    /**
     * Select entity by where condition with named parameters like column=:param
     * @param <T> type to return
     * @param handle
     * @param entityClass
     * @param behindTablePart part to be attached behind "select columns from table alias" like "where column=:param1"
     * @param parametersMap key value pairs like param1,value
     * @return concrete entity
     */
    @SuppressWarnings("unchecked")
    public <T> T queryOneWithNamedParams(Handle handle, Class<?> entityClass, String behindTablePart, Map<String, Object> parametersMap) {
        ColumnsFields columnsFields = ProcessFieldsUtil.getClassColumnFields(entityClass);
        String columnsPart;
        if (alias != null) {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields, alias);
        } else {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields);
        }
        String sql = "select " + columnsPart + " from " + tableName;
        if (alias != null) {
            sql += " " + alias;
        }
        if (behindTablePart != null) {
            sql += " " + behindTablePart;
        }
        DBProfile dbProfile = handle.getDbProfile();
        Constructor<?> cons;
        try {
            cons = entityClass.getConstructor();
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
            throw new RsWrongDefException("Unable to find nonparametric constructor " + entityClass.getCanonicalName() + "()", e);
        }
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sql, parametersMap)) {
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parametersMap);
            List<T> list = new ArrayList<>();
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                T object;
                if (resultSet.next()) {
                    try {
                        object = (T) cons.newInstance();
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                            | InvocationTargetException e) {
                        throw new RsWrongDefException("Unable to create instance of " + entityClass, e);
                    }
                    ConvertUtil.populateObject(resultSet, dbProfile, object, columnsFields);
                    list.add(object);
                } else {
                    throw new NoRecordException(tableName, parametersMap);
                }
                if (resultSet.next()) {
                    throw new ManyRecordsException("Expected to find one entity " + entityClass + " but found more by parameters " + parametersMap);
                }
                LOGGER.debug(">>>Returned: 1 entity");
                return object;
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by " + sql, e);
        }
    }
    
    /**
     * Select with question '?' placeholders in where condition. Number of placeholders must be same as number of parameters
     * @param <T>
     * @param connection
     * @param entityClass
     * @param behindTablePart part to be attached behind "select columns from table alias" like "where column=:param1"
     * @param placeholdersValues
     * @return
     * @throws NoRecordException when no record found corresponding with given placeholdersValues
     * @throws LoadException when multiple records found
     */
    @SuppressWarnings("unchecked")
    public <T> T queryOneByPlaceholders(Handle handle, Class<?> entityClass, String behindTablePart, List<Object> placeholdersValues) {
        ColumnsFields columnsFields = ProcessFieldsUtil.getClassColumnFields(entityClass);
        String columnsPart;
        if (alias != null) {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields, alias);
        } else {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields);
        }
        
        String sql = "select " + columnsPart + " from " + tableName;
        if (alias != null) {
            sql += " " + alias;
        }
        if (behindTablePart != null) {
            sql += " " + behindTablePart;
        }
        DBProfile dbProfile = handle.getDbProfile();
        Constructor<?> cons;
        try {
            cons = entityClass.getConstructor();
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
            throw new RsWrongDefException("Unable to find nonparametric constructor " + entityClass.getCanonicalName() + "()", e);
        }
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            for (int i = 0; i < placeholdersValues.size(); i++) {
                Object value = placeholdersValues.get(i);
                dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
            }
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", placeholdersValues);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                T object;
                if (resultSet.next()) {
                    try {
                        object = (T) cons.newInstance();
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                            | InvocationTargetException e) {
                        throw new RsWrongDefException("Unable to create instance of " + entityClass, e);
                    }
                    ConvertUtil.populateObject(resultSet, dbProfile, object, columnsFields);
                } else {
                    throw new NoRecordException(tableName, placeholdersValues);
                }
                if (resultSet.next()) {
                    throw new ManyRecordsException("Expected to find one entity " + entityClass + " but found more by parameters " + placeholdersValues);
                }
                LOGGER.debug(">>>Returned: 1 entity");
                return object;
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by "+ sql, e);
        }
    }
    
    /**
     * Select entities by where condition with named parameters like column=:param
     * @param <T> type to return
     * @param connection
     * @param entityClass
     * @param wherePart condition behund where word with named parameters like column=:param1
     * @param parametersMap key value pairs like param1,value
     * @return list of entities
     */
    public <T> List<T> queryWithNamedParams(Handle handle, Class<?> entityClass, String behindTablePart, Map<String, Object> parametersMap) {
        ColumnsFields columnsFields = ProcessFieldsUtil.getClassColumnFields(entityClass);
        String columnsPart;
        if (alias != null) {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields, alias);
        } else {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields);
        }
        
        String sql = "select " + columnsPart + " from " + tableName;
        String prefixedAlias = "";
        if (alias != null) {
            sql += " " + alias;
            prefixedAlias = "_" + alias;
        }
        if (behindTablePart != null) {
            sql += " " + behindTablePart;
        }
        DBProfile dbProfile = handle.getDbProfile();
        List<Map<String, Object>> convertAll;
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sql, parametersMap)) {
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parametersMap);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                convertAll = RSToMap.convertAllToTypes(resultSet, columnsFields, dbProfile);
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select "+ sql, e);
        }
        
        CollectionMapper collectionMapper = null;
        if (keyFieldName.size() > 1) {
            List<String> columns = new ArrayList<>();
            for (String keyField : keyFieldName) {
                String columnNameByFieldName = FieldsUtil.columnNameByFieldName(entityClass, keyField) + prefixedAlias;
                columns.add(columnNameByFieldName);
            }
            collectionMapper = new CollectionMapper(
                    new CompoundKeyFunction(columns.toArray(new String[columns.size()])),
                    new SimpleRemapper(entityClass, (alias != null ? "_" + alias : "")));
        } else {
            collectionMapper = new CollectionMapper(FieldsUtil.columnNameByFieldName(entityClass, keyFieldName.get(0)) + prefixedAlias, new SimpleRemapper(entityClass, prefixedAlias));
        }
        @SuppressWarnings("unchecked")
        List<T> list = (List<T>) collectionMapper.remapToList(convertAll, entityClass);
        LOGGER.debug(">>>Returned: {} entities", list.size());
        return list;
    }
    
    /**
     * Select with question '?' placeholders in where condition. Number of placeholders must be same as number of parameters
     * @param <T>
     * @param connection
     * @param entityClass
     * @param wherePart
     * @param placeholdersValues
     * @return
     */
    public <T> List<T> queryByPlaceholders(Handle handle, Class<?> entityClass, String behindTablePart, List<Object> placeholdersValues) {
        ColumnsFields columnsFields = ProcessFieldsUtil.getClassColumnFields(entityClass);
        String columnsPart;
        if (alias != null) {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields, alias);
        } else {
            columnsPart = ColumnsGenerator.createColumnsString(columnsFields);
        }
        
        String sql = "select " + columnsPart + " from " + tableName;
        String prefixedAlias = "";
        if (alias != null) {
            sql += " " + alias;
            prefixedAlias = "_" + alias;
        }
        if (behindTablePart != null) {
            sql += " " + behindTablePart;
        }
        DBProfile dbProfile = handle.getDbProfile();
        List<Map<String, Object>> convertAll;
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            for (int i = 0; i < placeholdersValues.size(); i++) {
                Object value = placeholdersValues.get(i);
                dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
            }
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", placeholdersValues);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                convertAll = RSToMap.convertAllToTypes(resultSet, columnsFields, dbProfile);
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select "+ sql, e);
        }
        
        CollectionMapper collectionMapper = null;
        if (keyFieldName.size() > 1) {
            List<String> columns = new ArrayList<>();
            for (String keyField : keyFieldName) {
                String columnNameByFieldName = FieldsUtil.columnNameByFieldName(entityClass, keyField) + prefixedAlias;
                columns.add(columnNameByFieldName);
            }
            collectionMapper = new CollectionMapper(
                    new CompoundKeyFunction(columns.toArray(new String[columns.size()])),
                    new SimpleRemapper(entityClass, prefixedAlias));
        } else {
            collectionMapper = new CollectionMapper(FieldsUtil.columnNameByFieldName(entityClass, keyFieldName.get(0)) + prefixedAlias, new SimpleRemapper(entityClass, prefixedAlias));
        }
        @SuppressWarnings("unchecked")
        List<T> list = (List<T>) collectionMapper.remapToList(convertAll, entityClass);
        LOGGER.debug(">>>Returned: {} entities", list.size());
        return list;
    }
    
    public Selecter withAlias(String alias) {
        this.alias = alias;
        return this;
    }
}
