package cz.jmare.rsmapper.active.basic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.exception.SaveException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.ParamsToStringer;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.RemapResultSetUtil;

public class Updater {
    private static final Logger LOGGER = LoggerFactory.getLogger(Updater.class); 
    
    private String tableName;
    
    private Collection<String> keyFieldNames;

    private Collection<String> nonUpdatableFields;

    private boolean nonUpdatableNulls;
    
    public Updater(String tableName) {
        this.tableName = tableName;
    }

    public Updater(String tableName, Collection<String> keyFieldNames) {
        this.tableName = tableName;
        this.keyFieldNames = keyFieldNames;
    }
    
    public Updater(String tableName, String keyFieldName) {
        this.tableName = tableName;
        this.keyFieldNames = new HashSet<>();
        this.keyFieldNames.add(keyFieldName);
    }
    
    public int execute(Handle handle, Object object) {
        DBProfile dbProfile = handle.getDbProfile();
        
        Collection<String> excludeFieldNames = new HashSet<>();
        if (keyFieldNames != null) {
            excludeFieldNames.addAll(keyFieldNames);
        }
        if (nonUpdatableFields != null) {
            excludeFieldNames.addAll(nonUpdatableFields);
        }
        ColumnsFieldsValues savingColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, excludeFieldNames, null, f -> FieldsUtil.isUpdatable(f), nonUpdatableNulls);
        
        if (savingColumnValues.columnNames.size() == 0) {
            // no fields to update => we must at least return the right number of existing record(s)
            StringBuilder sb = new StringBuilder("select count(*) from ");
            sb.append(tableName);
            List<Object> values = new ArrayList<Object>();
            if (keyFieldNames != null && !keyFieldNames.isEmpty()) {
                ColumnsFieldsValues savingWhereColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, new HashSet<>(), keyFieldNames, rmpAs -> true, false);
                sb.append(" where ");
                sb.append(savingWhereColumnValues.columnNames.stream().map(cn -> cn + "=?").collect(Collectors.joining(" and ")));
                values = savingWhereColumnValues.values;
            }
            try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sb.toString())) {
                for (int i = 0; i < values.size(); i++) {
                    Object value = values.get(i);
                    try {
                        dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
                    } catch (SQLException e) {
                        throw new SaveException("Unable to set type " + value.getClass() + " into column " + tableName + "." + savingColumnValues.columnNames.get(i) + ". Do you use the right DBProfile?", e);
                    }                
                }
                ResultSet executeQuery = prepareStatement.executeQuery();
                executeQuery.next();
                return executeQuery.getInt(1);
            } catch (SQLException e1) {
                throw new SaveException("Unable to detect count of record in " + tableName, e1);
            }
        }
        
        StringBuilder sb = new StringBuilder("update ");
        sb.append(tableName);
        sb.append(" set ");
        sb.append(savingColumnValues.columnNames.stream().map(cn -> cn + "=?").collect(Collectors.joining(", ")));
        
        if (keyFieldNames != null && !keyFieldNames.isEmpty()) {
            ColumnsFieldsValues savingWhereColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, new HashSet<>(), keyFieldNames, rmpAs -> true, false);
            savingColumnValues.columnNames.addAll(savingWhereColumnValues.columnNames);
            savingColumnValues.values.addAll(savingWhereColumnValues.values);
            sb.append(" where ");
            sb.append(savingWhereColumnValues.columnNames.stream().map(cn -> cn + "=?").collect(Collectors.joining(" and ")));
        }
        
        String sql = sb.toString();
        PreparedStatement prepareStatement = null;
        try {
            ColumnsFieldsValues allColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, new HashSet<>(), null, rmpAs -> true, false);
            ColumnsFieldsValues generatedColumnValues = allColumnValues.subtract(savingColumnValues);
            if (!generatedColumnValues.isEmpty()) {
                prepareStatement = handle.getConnection().prepareStatement(sql, generatedColumnValues.columnNames.toArray(new String[generatedColumnValues.columnNames.size()]));
            } else {
                prepareStatement = handle.getConnection().prepareStatement(sql);
            }
            
            List<Object> values = savingColumnValues.values;
            for (int i = 0; i < values.size(); i++) {
                Object value = values.get(i);
                try {
                    dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
                } catch (SQLException e) {
                    throw new SaveException("Unable to set type " + value.getClass() + " into column " + tableName + "." + savingColumnValues.columnNames.get(i) + ". Do you use the right DBProfile?", e);
                }                
            }
            
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", new ParamsToStringer(values));
            prepareStatement.executeUpdate();
            
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            if (!generatedColumnValues.isEmpty()) {
                ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    RemapResultSetUtil.populateGeneratedFields(object, generatedColumnValues, generatedKeys, dbProfile, LOGGER);
                } else {
                    throw new RsWrongDefException("Resultset didn't return generated columns " + generatedColumnValues.columnNames);
                }
            }            
            
            return updateCount;
        } catch (SQLException e) {
            throw new SaveException("Unable to update record by " + sql, e);
        } finally {
            if (prepareStatement != null) {
                try {
                    prepareStatement.close();
                } catch (SQLException e) {
                    LOGGER.warn("Unable to close prepared statement");
                }
            }
        }
    }
    
    public Updater withNonUpdatableFields(String... nonUpdatableFields) {
        this.nonUpdatableFields = Arrays.asList(nonUpdatableFields);
        return this;
    }

    public void setNonUpdatableNulls(boolean nonUpdatableNulls) {
        this.nonUpdatableNulls = nonUpdatableNulls;
    }
}
