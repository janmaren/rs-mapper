package cz.jmare.rsmapper.active.basic;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.SaveException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;
import cz.jmare.rsmapper.util.ParamsToStringer;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;

public class Deleter {
    private static final Logger LOGGER = LoggerFactory.getLogger(Deleter.class);

    private String tableName;
    
    private Collection<String> keyFieldNames;

    public Deleter(String tableName) {
        this.tableName = tableName;
    }

    public Deleter(String tableName, Collection<String> keyFieldNames) {
        this.tableName = tableName;
        this.keyFieldNames = keyFieldNames;
    }
    
    public Deleter(String tableName, String keyFieldName) {
        this.tableName = tableName;
        this.keyFieldNames = new HashSet<>();
        this.keyFieldNames.add(keyFieldName);
    }

    public void execute(Handle handle, Object object) {
        DBProfile dbProfile = handle.getDbProfile();
        
        Set<String> excludeFieldNames = new HashSet<>();
        ColumnsFieldsValues savingColumnValues = new ColumnsFieldsValues(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        
        StringBuilder sb = new StringBuilder("delete from ");
        sb.append(tableName);
        
        if (keyFieldNames != null && !keyFieldNames.isEmpty()) {
            ColumnsFieldsValues savingWhereColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, excludeFieldNames, keyFieldNames, rmpAs -> true, false);
            savingColumnValues.columnNames.addAll(savingWhereColumnValues.columnNames);
            savingColumnValues.values.addAll(savingWhereColumnValues.values);
            sb.append(" where ");
            sb.append(savingWhereColumnValues.columnNames.stream().map(cn -> cn + "=?").collect(Collectors.joining(" and ")));
        }
        
        String sql = sb.toString();

        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            List<Object> values = savingColumnValues.values;
            for (int i = 0; i < values.size(); i++) {
                Object value = values.get(i);
                dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
            }
            
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", new ParamsToStringer(values));
            prepareStatement.executeUpdate();
            
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            if (updateCount == 0) {
                LOGGER.warn("No record deleted by " + sql);
            }
        } catch (SQLException e) {
            throw new SaveException("Unable to delete by " + sql, e);
        }
    }
}
