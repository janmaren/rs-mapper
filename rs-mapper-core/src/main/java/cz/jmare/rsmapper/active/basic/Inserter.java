package cz.jmare.rsmapper.active.basic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.exception.SaveException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.rs.ColumnsFieldsValues;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.ParamsToStringer;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.RemapResultSetUtil;

public class Inserter {
    private static final Logger LOGGER = LoggerFactory.getLogger(Inserter.class);
    
    private String tableName;
    
    private Collection<String> nonInsertableFields;
    
    private boolean nonInsertableNulls;
    
    public Inserter(String tableName) {
        this.tableName = tableName;
    }
    
    public Inserter(String tableName, Collection<String> nonInsertableFieldNames) {
        this.tableName = tableName;
        this.nonInsertableFields = nonInsertableFieldNames;
    }
    
    public Inserter(String tableName, String nonInsertableFieldName) {
        this.tableName = tableName;
        this.nonInsertableFields = new HashSet<>();
        this.nonInsertableFields.add(nonInsertableFieldName);
    }
    
    public void execute(Handle handle, Object object)  {
        DBProfile dbProfile = handle.getDbProfile();
        
        Set<String> nonInsertableFieldNamesSet = new HashSet<>();
        if (nonInsertableFields != null && !nonInsertableFields.isEmpty()) {
            nonInsertableFieldNamesSet.addAll(nonInsertableFields);
        }
        ColumnsFieldsValues savingColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, nonInsertableFieldNamesSet, null, f -> FieldsUtil.isInsertable(f), nonInsertableNulls);
        
        StringBuilder sb = new StringBuilder("insert into ");
        sb.append(tableName);
        if (savingColumnValues.columnNames.size() == 0) {
            sb.append(" ").append("DEFAULT VALUES");
        } else {
            sb.append(" (");
            sb.append(savingColumnValues.columnNames.stream().collect(Collectors.joining(", ")));
            sb.append(") values (");
            sb.append(savingColumnValues.columnNames.stream().map(v -> "?").collect(Collectors.joining(", ")));
            sb.append(")");
        }
        
        String sql = sb.toString();

        PreparedStatement prepareStatement = null;
        try {
            ColumnsFieldsValues allColumnValues = ProcessFieldsUtil.getObjectColumnValues(object, new HashSet<>(), null, rmpAs -> true, false);
            ColumnsFieldsValues generatedColumnValues = allColumnValues.subtract(savingColumnValues);
            if (!generatedColumnValues.isEmpty()) {
                String[] generatedColumnValuesArr = generatedColumnValues.columnNames.toArray(new String[generatedColumnValues.columnNames.size()]);
                prepareStatement = handle.getConnection().prepareStatement(sql, generatedColumnValuesArr);
            } else {
                prepareStatement = handle.getConnection().prepareStatement(sql);
            }
            
            List<Object> values = savingColumnValues.values;
            for (int i = 0; i < values.size(); i++) {
                Object value = values.get(i);
                try {
                    dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
                } catch (SQLException e) {
                    throw new SaveException("Unable to set type " + value.getClass() + " into column " + tableName + "." + savingColumnValues.columnNames.get(i) + ". Do you use the right DBProfile?", e);
                }
            }

            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", new ParamsToStringer(values));
            prepareStatement.executeUpdate();
            
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            if (updateCount != 1) {
                throw new RsWrongDefException("Record not inserted by " + sql);
            }
            
            if (!generatedColumnValues.isEmpty()) {
                ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    RemapResultSetUtil.populateGeneratedFields(object, generatedColumnValues, generatedKeys, dbProfile, LOGGER);
                } else {
                    throw new RsWrongDefException("Resultset didn't return generated columns " + generatedColumnValues.columnNames);
                }
            }
        } catch (SQLException e) {
            throw new SaveException("Unable to insert record by " + sql, e);
        } finally {
            if (prepareStatement != null) {
                try {
                    prepareStatement.close();
                } catch (SQLException e) {
                    LOGGER.warn("Unable to close prepared statement");
                }
            }
        }
    }

    public void setNonInsertableNulls(boolean nonInsertableNulls) {
        this.nonInsertableNulls = nonInsertableNulls;
    }
}
