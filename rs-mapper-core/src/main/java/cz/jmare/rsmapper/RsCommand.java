package cz.jmare.rsmapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.exception.ManyRecordsException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.exception.SaveException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.util.NamedStatementPopulator;

public abstract class RsCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(RsCommand.class);
    
    public static <T> T selectOneScalar(DSConf dsConf, Class<T> resultClass, String sql) {
        try (Handle handle = dsConf.createHandle();) {
            return selectOneScalar(handle, resultClass, sql);
        }
    }
    
    public static <T> T selectOneScalar(Handle handle, Class<T> resultClass, String sql) {
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            LOGGER.debug(">>>SQL: {}", sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            T result = null;
            if (resultSet.next()) {
                result = resultSet.getObject(1, resultClass);
            }
            if (resultSet.next()) {
                throw new ManyRecordsException("Expected to select one value but found more");
            }
            LOGGER.debug(">>>Returned: " + result);
            return result;
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by command: " + sql, e);
        }
    }
    
    public static <T> T selectOneScalar(DSConf dsConf, Class<T> resultClass, String sql, Map<String, Object> parameters) {
        try (Handle handle = dsConf.createHandle()) {
            return selectOneScalar(handle, resultClass, sql, parameters);
        }
    }
    
    /**
     * Return one value from one row. Use such sql which can return only one value from one record,
     * if more columns used then the others will be ignored. When more records returned the NotScalarException
     * will be thrown
     * @param handle
     * @param requiredClass
     * @param sql select command to be launched
     * @param parameters named parameters map to replace placeholders
     * @return
     */
    public static <T> T selectOneScalar(Handle handle, Class<T> resultClass, String sql, Map<String, Object> parameters) {
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sql, parameters)) {
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parameters);
            ResultSet resultSet = prepareStatement.executeQuery();
            T result = null;
            if (resultSet.next()) {
                result = resultSet.getObject(1, resultClass);
            }
            if (resultSet.next()) {
                throw new ManyRecordsException("Expected to select one value but found more");
            }
            LOGGER.debug(">>>Returned: " + result);
            return result;
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by command " + sql, e);
        }
    }
    
    public static <T> List<T> selectMultipleScalars(DSConf dsConf, Class<T> resultClass, String sql, Map<String, Object> parameters) {
        try (Handle handle = dsConf.createHandle()) {
            return selectMultipleScalars(handle, resultClass, sql, parameters);
        }
    }
    
    public static <T> List<T> selectMultipleScalars(Handle handle, Class<T> resultClass, String sql, Map<String, Object> parameters) {
        List<T> result = new ArrayList<>();
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sql, parameters)) {
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parameters);
            ResultSet resultSet = prepareStatement.executeQuery();
            while (resultSet.next()) {
                T object = resultSet.getObject(1, resultClass);
                result.add(object);
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by command: " + sql, e);
        }
        LOGGER.debug(">>>Returned: {}", result);
        return result;
    }
    
    public static int execute(DSConf dsConf, String sql) {
        try (Handle handle = dsConf.createHandle()) {
            return execute(handle, sql);
        }
    }
    
    public static int execute(Handle handle, String sql) {
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            LOGGER.debug(">>>SQL: {}", sql);
            prepareStatement.executeUpdate();
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            return updateCount;
        } catch (SQLException e) {
            throw new SaveException("Unable to execute: " + sql, e);
        }
    }
    
    /**
     * Do custom update, delete or insert operation. Use valid sql command. Named parameters can be present
     * in sql and a value from map will be assigned to them.
     * @param dsConf
     * @param sql sql with parameters like update table set col=:val1.
     * @param parametersValuesMap map where key is placeholder to be replaced and value is value to be set
     * @return
     */
    public static int execute(DSConf dsConf, String sql, Map<String, Object> parameters) {
        try (Handle handle = dsConf.createHandle()) {
            return execute(handle, sql, parameters);
        }
    }
    
    /**
     * Do custom update, delete or insert operation. Use valid sql command. Named parameters can be present
     * in sql and a value from map will be assigned to them.
     * @param handle
     * @param sql sql with parameters like update table set col=:val1.
     * @param parametersValuesMap map where key is placeholder to be replaced and value is value to be set
     * @return
     */
    public static int execute(Handle handle, String sql, Map<String, Object> parameters) {
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sql, parameters)) {
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parameters);
            prepareStatement.executeUpdate();
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            return updateCount;
        } catch (SQLException e) {
            throw new SaveException("Unable to execute: " + sql, e);
        }
    }
    
    /**
     * Do custom update, delete or insert operation. Use valid sql command. Named parameters can be present
     * in sql and a value from map will be assigned to them.
     * @param handle
     * @param sql sql with parameters like update table set col=:val1.
     * @param parametersValuesMap map where key is placeholder to be replaced and value is value to be set
     * @return
     */
    public static int execute(Handle handle, String sql, List<Object> placeholderValues) {
        DBProfile dbProfile = handle.getDbProfile();
        try (PreparedStatement prepareStatement = handle.getConnection().prepareStatement(sql)) {
            for (int i = 0; i < placeholderValues.size(); i++) {
                Object value = placeholderValues.get(i);
                try {
                    dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, value);
                } catch (SQLException e) {
                    throw new SaveException("Unable to set type " + (value == null ? "null" : value.getClass()) + " into column " + i, e);
                }                
            }
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", placeholderValues);
            prepareStatement.executeUpdate();
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Update count: {}", updateCount);
            return updateCount;
        } catch (SQLException e) {
            throw new SaveException("Unable to execute: " + sql, e);
        }
    }
    
    public static List<LinkedHashMap<String, Object>> selectMultipleVectors(DSConf dsConf, String sql, Map<String, Object> parameters) {
        try (Handle handle = dsConf.createHandle()) {
            return selectMultipleVectors(handle, sql, parameters);
        }
    }
    
    public static List<LinkedHashMap<String, Object>> selectMultipleVectors(Handle handle, String sql, Map<String, Object> parameters) {
        DBProfile dbProfile = handle.getDbProfile();
        List<LinkedHashMap<String, Object>> result = new ArrayList<>();
        try (PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(handle, sql, parameters)) {
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", parameters);
            ResultSet resultSet = prepareStatement.executeQuery();
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            while (resultSet.next()) {
                LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i).toLowerCase(), dbProfile.getObjectFromResultSet(resultSet, i));
                }
                result.add(row);
            }
        } catch (SQLException e) {
            throw new RsWrongDefException("Unable to select by command: " + sql, e);
        }
        LOGGER.debug(">>>Returned count: {}", result.size());
        return result;
    }

    /**
     * Insert record and return default values or generated ids or any column.
     * The part 'insert into &lt;tableName&gt; is pregenerated and the columns and values part is generated by this method. 
     * @param dsConf
     * @param tableName name of table
     * @param columnNameValueMap pairs columnName-value to be inserted
     * @param defaultColumns columns for which to return values
     * @return
     */
    public static Map<String, Object> insertAndGetDefaults(DSConf dsConf, String tableName, Map<String, Object> columnNameValueMap, List<String> defaultColumns) {
        try (Handle handle = dsConf.createHandle()) {
            return insertAndGetDefaults(handle, tableName, columnNameValueMap, defaultColumns);
        }
    }
    
    /**
     * Insert record and return default values or generated ids or any column.
     * The part 'insert into &lt;tableName&gt; is pregenerated and the columns and values part is generated by this method. 
     * @param handle
     * @param tableName name of table
     * @param columnNameValueMap pairs columnName-value to be inserted
     * @param defaultColumns columns for which to return values
     * @return
     */
    public static Map<String, Object> insertAndGetDefaults(Handle handle, String tableName, Map<String, Object> columnNameValueMap, List<String> defaultColumns) {
        DBProfile dbProfile = handle.getDbProfile();
        StringBuilder sb = new StringBuilder("insert into ");
        sb.append(tableName);
        sb.append(" ");
        List<Object> valuesObjects = new ArrayList<>();
        if (columnNameValueMap.isEmpty()) {
            sb.append("DEFAULT VALUES");
        } else {
            StringBuilder columns = new StringBuilder();
            StringBuilder valuesPlaceholders = new StringBuilder();
            for (Entry<String, Object> entry : columnNameValueMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (columns.length() > 0) {
                    columns.append(", ");
                    valuesPlaceholders.append(", ");
                }
                columns.append(key);
                valuesPlaceholders.append("?");
                valuesObjects.add(value);
            }
            
            sb.append("(").append(columns).append(")");
            sb.append(" values(").append(valuesPlaceholders).append(")");
        }
        
        String[] defaultColumnsArr = defaultColumns.toArray(new String[defaultColumns.size()]);
        String sql = sb.toString();
        HashMap<String, Object> result = new HashMap<String, Object>();
        try (PreparedStatement prepareStatement = defaultColumnsArr.length > 0 ? handle.getConnection().prepareStatement(sql, defaultColumnsArr) : handle.getConnection().prepareStatement(sql)) {
            for (int i = 0; i < valuesObjects.size(); i++) {
                Object object = valuesObjects.get(i);
                dbProfile.setObjectToPreparedStatement(prepareStatement, i + 1, object);
            }
            
            LOGGER.debug(">>>SQL: {}", sql);
            LOGGER.debug(">>>Params: {}", columnNameValueMap);

            prepareStatement.executeUpdate();

            if (defaultColumnsArr.length > 0) {
                ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    for (int i = 0; i < defaultColumns.size(); i++) {
                        String defaultColumn = defaultColumns.get(i);
                        Object object = dbProfile.getObjectFromResultSet(generatedKeys, i + 1);
                        result.put(defaultColumn.toLowerCase(), object);
                    }
                } else {
                    throw new RsWrongDefException("Resultset didn't return generated columns " + defaultColumns);
                }
            }
            
            int updateCount = prepareStatement.getUpdateCount();
            LOGGER.debug(">>>Returned: {}", result);
            LOGGER.debug(">>>Update count: {}", updateCount);
            return result;
        } catch (SQLException e) {
            throw new SaveException("Unable to insert record by: " + sql, e);
        }
    }
    
    public static long selectNextSequence(Handle handle, String sequenceName) {
        return selectOneScalar(handle, Long.class, handle.getDbProfile().getSqlForSequence(sequenceName));
    }
    
    public static long selectNextSequence(DSConf dsConf, String sequenceName) {
        try (Handle handle = dsConf.createHandle()) {
            return selectNextSequence(handle, sequenceName);
        }
    }    
}
