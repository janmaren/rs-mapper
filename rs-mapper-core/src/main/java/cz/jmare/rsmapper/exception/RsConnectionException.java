package cz.jmare.rsmapper.exception;

import java.sql.SQLException;

/**
 * Problems to get connection from datasource
 */
public class RsConnectionException extends RsDbException {
    private static final long serialVersionUID = -4248766749106765627L;

    private SQLException sqlException;

    public RsConnectionException(String message, SQLException cause) {
        super(message, cause);
        this.sqlException = cause;
    }

    public SQLException getSqlException() {
        return sqlException;
    }
}
