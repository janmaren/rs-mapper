package cz.jmare.rsmapper.exception;

/**
 * RsWrongDefException means an unrecoverable error caused by some misconfiguration.
 * This exception is usually propagated to the caller. Some misconfigration, like field name or
 * column name or SQL must be corrected. 
 */
public class RsWrongDefException extends RuntimeException {
    private static final long serialVersionUID = -3459674692266383109L;

    public RsWrongDefException(String s) {
        super(s);
    }

    public RsWrongDefException(String message, Throwable cause) {
        super(message, cause);
    }
}
