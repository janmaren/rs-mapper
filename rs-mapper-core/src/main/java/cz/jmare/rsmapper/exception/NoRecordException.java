package cz.jmare.rsmapper.exception;

/**
 * Used when just one record expected to be made but resultset doesn't contain any data 
 */
public class NoRecordException extends RsDbException {
    private static final long serialVersionUID = 4091223691696677553L;
    private String tableColumnIdentification;
    private Object value;

    public NoRecordException(String tableColumnIdentification, Object value) {
        super("Not found record with value " + value + " in " + tableColumnIdentification);
        this.tableColumnIdentification = tableColumnIdentification;
        this.value = value;
    }

    public String getTableColumnIdentification() {
        return tableColumnIdentification;
    }

    public Object getValue() {
        return value;
    }
}
