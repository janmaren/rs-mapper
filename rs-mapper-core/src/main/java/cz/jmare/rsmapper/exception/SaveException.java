package cz.jmare.rsmapper.exception;

import java.sql.SQLException;

/**
 * Exception for Insert, Update and Delete operation. Caller should examine the embedded SQLException
 * to determine what kind of response to return to client
 */
public class SaveException extends RsDbException {
    private static final long serialVersionUID = -1328630682792939500L;
    
    private SQLException sqlException;

    public SaveException(String message, SQLException cause) {
        super(message, cause);
        this.sqlException = cause;
    }

    public SQLException getSqlException() {
        return sqlException;
    }
}
