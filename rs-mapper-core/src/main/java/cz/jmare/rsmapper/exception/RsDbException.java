package cz.jmare.rsmapper.exception;

/**
 * Baseclass for DB related exceptions 
 */
public class RsDbException extends RuntimeException {
    private static final long serialVersionUID = -8065319781341796845L;

    public RsDbException(String message) {
        super(message);
    }
    
    public RsDbException(String message, Throwable cause) {
        super(message, cause);
    }
}
