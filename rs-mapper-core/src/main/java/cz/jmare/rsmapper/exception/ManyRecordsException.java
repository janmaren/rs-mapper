package cz.jmare.rsmapper.exception;

/**
 * Used when expected one result object but a vector would be returned 
 */
public class ManyRecordsException extends RsDbException {
    private static final long serialVersionUID = 4091223691696677553L;

    public ManyRecordsException(String message) {
        super(message);
    }
}
