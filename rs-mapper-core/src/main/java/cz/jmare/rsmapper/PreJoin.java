package cz.jmare.rsmapper;

import cz.jmare.rsmapper.handle.Handle;

/**
 * Join to table which has a primary key
 * This table record is a primary key record which must exist before joining record 
 */
public interface PreJoin extends Join {
    /**
     * Use association stored in slaveObject and save it.
     * Note that implementation like ToOneMaster must know the name of
     * association field to be able to get its value to obtain object to be
     * saved as primary key object  
     * @param handle
     * @param leftObject
     */
    void savePrimByLeftAssoc(Handle handle, Object leftObject);

    /**
     * Use foreign key which points to slaveObject to load primary key entity.
     * Note that implementation like ToOneMaster must know the name of
     * foreign key field to be able to get its value to be used as primary key.
     * @param handle
     * @param leftObject
     */
    void loadPrimByLeftFk(Handle handle, Object leftObject);

    /**
     * Delete prepared orphan
     * @param handle
     * @param leftObject - maybe unused
     */
    void deleteOrphan(Handle handle, Object leftObject);

    /**
     * Return true when enabled deleting orphans - optimalisation
     * @return
     */
    boolean isDeleteOrphan();

    /**
     * Delete orphan or prepare deleting orphan when unable to delete it in current time
     * @param handle
     * @param leftObject
     */
	void deleteByLeftFk(Handle handle, Object leftObject);
}
