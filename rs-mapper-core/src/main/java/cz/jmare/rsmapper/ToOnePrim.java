package cz.jmare.rsmapper;

import static cz.jmare.util.KeyUtil.isNull;
import static cz.jmare.util.KeyUtil.keysSame;
import static cz.jmare.util.KeyUtil.mergeKeysValues;
import static cz.jmare.util.KeyUtil.populateNulls;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.active.basic.AbstractRelation;
import cz.jmare.rsmapper.active.basic.Selecter;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.PrimaryKey;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.FieldMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.rs.SingleFieldMapper;
import cz.jmare.rsmapper.util.AssocKeysForPre;
import cz.jmare.rsmapper.util.ColumnsGenerator;
import cz.jmare.rsmapper.util.FieldAccessUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.SqlCommandUtil;
import cz.jmare.util.BuildKeyUtil;
import cz.jmare.util.KeyUtil;

/**
 * This table represents a table which has a primary key. Foreign key has the other table, not this.
 */
public class ToOnePrim extends AbstractRelation implements PreJoin {
    private static final Logger LOGGER = LoggerFactory.getLogger(ToOnePrim.class);
    
    private String foreignKeyFieldNameDef;

    private String leftAssocFieldNameDef;

    private AssocKeysForPre assocKeysForPre;

    private boolean deleteOrphan;
    
    private List<Object> orphanPrimKeysToDelete;
    
    public ToOnePrim(Class<?> entityClass) {
        super(entityClass);
    }
    
    public ToOnePrim(Class<?> entityClass, String leftAssocFieldName) {
        super(entityClass);
        this.leftAssocFieldNameDef = leftAssocFieldName;
    }
    
    public ToOnePrim(Class<?> entityClass, String leftAssocFieldName, String leftForeignKeyFieldName) {
        super(entityClass);
        this.leftAssocFieldNameDef = leftAssocFieldName;
        this.foreignKeyFieldNameDef = leftForeignKeyFieldName;
    }
    
    public ToOnePrim(Class<?> entityClass, String leftAssocFieldName, String leftForeignKeyFieldName, PrimaryKey primaryKey) {
        super(entityClass, primaryKey);
        this.leftAssocFieldNameDef = leftAssocFieldName;
        this.foreignKeyFieldNameDef = leftForeignKeyFieldName;
    }
    
    @Override
    public void savePrimByLeftAssoc(Handle handle, Object leftObject) {
        Object object = FieldAccessUtil.getValue(leftObject, assocKeysForPre.getLeftAssocField());
        
        //List<Object> leftForeignKeyValues = FieldAccessUtil.getObjectFieldKeyValues(leftObject, assocKeysForPre.getLeftForeignKeyFieldNames());
        //KeyUtil.checkFkToPrim(leftForeignKeyValues, entityClass, primaryKey.getFieldNames());

        List<Object> leftForeignKeyValues = assocKeysForPre.fkValuesInPKOrder(leftObject, primaryKey.getFieldNames());
        saveAssigningObject(handle, object, leftForeignKeyValues);
        
//        List<String> leftForeignKeyFieldNames = assocKeysForPre.getLeftForeignKeyFieldNames();
//        List<String> primaryKeyFieldNames = getPrimaryFieldNamesWithCheck(leftObject, leftForeignKeyFieldNames);
        
        Map<String, Object> pkNamesValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
        Map<Field, Object> fkFieldsValues = assocKeysForPre.getFkFieldsValues(pkNamesValues);
        
        //List<Object> objectKeys = object == null ? KeyUtil.populateNulls(primaryKey.size()) : FieldAccessUtil.getBasicValue(object, primaryKeyFieldNames);
        
        //FieldAccessUtil.setValueWithConversion(leftObject, assocKeysForPre.getLeftForeignKeyFields(), objectKeys);
        FieldAccessUtil.setValueWithConversion(leftObject, fkFieldsValues);
    }

    @Override
    public void deleteOrphan(Handle handle, Object leftObject) {
        if (this.orphanPrimKeysToDelete == null) {
            return;
        }
        
        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());

        SqlCommandUtil.deleteRecordsFromTable(handle, tablePrefix + tableName, mergeKeysValues(classColumnFields.columnNames, orphanPrimKeysToDelete));
        this.orphanPrimKeysToDelete = null;
    }  

    /**
     * Save primary key object
     * @param handle
     * @param object
     * @param leftForeignKeyValue 
     */
    protected void saveAssigningObject(Handle handle, Object object, List<Object> leftForeignKeyValues) {
        if (!isNull(leftForeignKeyValues)) {
            List<Object> objectPrimaryKey = object == null ? populateNulls(primaryKey.size()) : FieldAccessUtil.getObjectFieldKeyValues(object, primaryKey.getFieldNames());
            if (!keysSame(leftForeignKeyValues, objectPrimaryKey)) {
                if (deleteOrphan) {
                    if (orphanPrimKeysToDelete != null) {
                        LOGGER.error("There exists previous orphan record with id " + orphanPrimKeysToDelete + " in " + tablePrefix + tableName + " which wasn't deleted");
                    }
                    this.orphanPrimKeysToDelete = leftForeignKeyValues;
                }
            }
        }
        
        if (object != null) {
            super.save(handle, object);
        }
    }
    
    private List<String> getPrimaryFieldNamesWithCheck(Object leftObject, List<String> leftForeignKeyFieldNames) {
        List<String> result = new ArrayList<String>();
        
        for (int i = 0; i < primaryKey.getFieldNames().size(); i++) {
            String primaryKeyFieldName = primaryKey.getFieldNames().get(i);
            String leftForeignKeyFieldName = leftForeignKeyFieldNames.get(i);
            Class<?> typeByFieldName = FieldsUtil.typeByFieldName(leftObject.getClass(), leftForeignKeyFieldName);
            Class<?> primByFieldName = FieldsUtil.typeByFieldName(entityClass, primaryKeyFieldName);
            if (!typeByFieldName.equals(primByFieldName)) {
                LOGGER.warn(entityClass + " has primary key of " + primByFieldName + " but foreign key "
                        + leftObject.getClass().getName() + "." + leftForeignKeyFieldName + " has " + typeByFieldName);
            }
            result.add(primaryKeyFieldName);
        }
        return result;
    }
    
    @Override
    public FieldMapper toFieldMapper(Class<?> leftClass) {
        List<String> fieldNames = primaryKey.getFieldNames();
        Function<Map<String, Object>, Object> keyFunction = BuildKeyUtil.getKeyFunction(fieldNames, entityClass, alias);
        return new SingleFieldMapper(assocKeysForPre.getLeftAssocFieldName(), keyFunction, new SimpleRemapper(entityClass, "_" + alias), AbstractRelation.toFieldMappers(joins, entityClass));
    }
    
    @Override
    public void loadPrimByLeftFk(Handle handle, Object leftObject) {
        List<Object> leftForeignKeys = assocKeysForPre.fkValuesInPKOrder(leftObject, primaryKey.getFieldNames());
        //List<Object> leftForeignKeys = FieldAccessUtil.getValue(leftObject, assocKeysForPre.getLeftForeignKeyFields());
        if (KeyUtil.isNull(leftForeignKeys)) {
            return;
        }
        KeyUtil.checkFkToPrim(leftForeignKeys, entityClass, primaryKey.getFieldNames());

        Object object = loadAssigningObject(handle, leftForeignKeys);
        FieldAccessUtil.setValueWithConversion(leftObject, assocKeysForPre.getLeftAssocField(), object);
    }
    
    /**
     * Load primary key entity by leftForeignKeyFieldName
     * @param handle
     * @param leftForeignKey foreign key from other table which should be used here as a primary key
     */
    protected Object loadAssigningObject(Handle handle, List<Object> leftForeignKeys) {
        Selecter selecter = new Selecter(tablePrefix + tableName, primaryKey.getFieldNames()).withAlias(alias);
        String wherePart = FieldsUtil.generateWhereClause(entityClass, primaryKey.getFieldNames());

        Object object = selecter.queryOneByPlaceholders(handle, entityClass, wherePart, leftForeignKeys);
        
        doLoadJoins(handle, leftForeignKeys, object);
        
        return object;
    }

    private void doLoadJoins(Handle handle, List<Object> slaveForeignKeyValue, Object object) {
        for (Join join : joins) {
            if (join instanceof PreJoin) {
                PreJoin preJoin = (PreJoin) join;
                preJoin.loadPrimByLeftFk(handle, object);        
            } else if (join instanceof PostJoin) {
                PostJoin postJoin = (PostJoin) join;
                Map<String, Object> pkFieldsValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
                postJoin.loadByLeftPk(handle, object, pkFieldsValues);
            }
        }
    }

	@Override
	public void deleteByLeftFk(Handle handle, Object leftObject) {
		if (deleteOrphan) {
		    List<Object> objectKeys = assocKeysForPre.fkValuesInPKOrder(leftObject, primaryKey.getFieldNames());
	        KeyUtil.checkFkToPrim(objectKeys, entityClass, primaryKey.getFieldNames());
	        ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
			SqlCommandUtil.deleteRecordsFromTable(handle, tablePrefix + tableName, mergeKeysValues(classColumnFields.columnNames, objectKeys));
		}
	}

    @Override
    public boolean isDeleteOrphan() {
        return deleteOrphan;
    }
    
    /**
     * Set primary key
     */
    public ToOnePrim withPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }
    
    /**
     * Set joins, ToOne, or ToOnePrim or ToMany. The appropriate association will be also loaded.
     * @param joins
     * @return
     */
    public ToOnePrim withJoins(Join... joins) {
        this.joins = joins;
        return this;
    }
    
    /**
     * Set names of fields omitted in insert
     * @param nonUpdatableFieldNames
     * @return
     */
    public ToOnePrim withNonInsertableFields(String... nonInsertableFieldNames) {
        this.nonInsertableFieldNames = nonInsertableFieldNames;
        return this;
    }

    /**
     * Set names of fields omitted in update
     * @param nonUpdatableFieldNames
     * @return
     */
    public ToOnePrim withNonUpdatableFields(String... nonUpdatableFieldNames) {
        this.nonUpdatableFieldNames = nonUpdatableFieldNames;
        return this;
    }

    /**
     * Set human readable alias to replace default alias, like _1, _2,...
     * @param alias
     * @return
     */
    public ToOnePrim withAlias(String alias) {
        this.customAlias = alias;
        return this;
    }   
    
    /**
     * When deleting referencing record, delete also this
     * @return
     */
    public ToOnePrim withDeleteOrphan() {
        this.deleteOrphan = true;
        return this;
    }

    /**
     * Columns with null values will be omitted in insert.
     * @return
     */
    public ToOnePrim withNonInsertableNulls() {
        this.nonInsertableNulls = true;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in update.
     * @return
     */
    public ToOnePrim withNonUpdatableNulls() {
        this.nonUpdatableNulls = true;
        return this;
    }
    
    /**
     * A schema, catalog or database prefix before a table.
     * @param tablePrefix
     * @return
     */
    public ToOnePrim withTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
        if (!tablePrefix.endsWith(".")) {
            this.tablePrefix += ".";
        }
        return this;
    }
    
    @Override
    public void init(ColumnsGenerator columnsGenerator, Class<?> leftClass) {
        addToColumnsGenerator(columnsGenerator);
        
        initBasicByAnno();
        
        if (primaryKey == null) {
            throw new RsWrongDefException("Class " + entityClass.getCanonicalName() + " has no id field defined although it is used in ToOnePrim join.");
        }
        
        assocKeysForPre = new AssocKeysForPre(leftClass, entityClass, leftAssocFieldNameDef, foreignKeyFieldNameDef);
    }
}
