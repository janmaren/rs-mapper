package cz.jmare.rsmapper;

import java.util.HashMap;
import java.util.Map;

/**
 * See {@link Query}
 */
public class SimpleQuery implements Query  {
    private String queryString = "";
    
    private Map<String, Object> queryParameters = Map.of();

    /**
     * No filter, no ordering, no paramaters, it should return all records.
     * This is allowed although there exists better solution like Relation.loadAll(DsConf);
     */
    public SimpleQuery() {
    }

    /**
     * Useful to define for example order, like <pre>order by _1.name</pre>. In this case no parameter is present 
     */
    public SimpleQuery(String queryString) {
        this.queryString = queryString;
    }

    /**
     * Useful to define a where condition with parameters, like <pre>where _1.name=:name</pre> with <pre>Map.of("name", name)</pre>
     * @param queryString
     * @param queryParameters
     */
    public SimpleQuery(String queryString, Map<String, Object> queryParameters) {
        this.queryString = queryString;
        this.queryParameters = new HashMap<String, Object>(queryParameters);
    }
    
    public SimpleQuery(Query query) {
        this.queryString = query.getQueryString();
        this.queryParameters = new HashMap<String, Object>(query.getQueryParameters());
    }
    
    public SimpleQuery(Query query, String queryString, Map<String, Object> queryParameters) {
        this.queryString = query.getQueryString();
        this.queryParameters = new HashMap<String, Object>(query.getQueryParameters());
        this.queryString += " " + queryString;
        this.queryParameters.putAll(queryParameters);
    }
    
    @Override
    public String getQueryString() {
        return queryString;
    }

    /**
     * Column name -> value map
     */
    @Override
    public Map<String, Object> getQueryParameters() {
        return queryParameters;
    }
}
