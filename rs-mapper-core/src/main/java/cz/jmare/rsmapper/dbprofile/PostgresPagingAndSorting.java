package cz.jmare.rsmapper.dbprofile;

import java.util.HashMap;
import java.util.Map;

import cz.jmare.rsmapper.Query;
import cz.jmare.rsmapper.SimpleQuery;

public class PostgresPagingAndSorting implements Query {
    private String selectSuffixString;
    
    private Map<String, Object> selectSuffixParameters = Map.of();
    
    /**
     * Create paging data using order, page number and page size (but not any other condition)
     * @param orderColumns
     * @param pageNumber page number starting with 0
     * @param pageSize page size (should be &gt; 0)
     */
    public PostgresPagingAndSorting(String orderColumns) {
        super();
        selectSuffixString = "order by " + orderColumns;
    }
    
    /**
     * Create paging data using order and max limit of records number (but not any other condition)
     * @param orderColumns
     * @param maxRecords page size (should be &gt; 0)
     */
    public PostgresPagingAndSorting(String orderColumns, int maxRecords) {
        super();
        selectSuffixString = "order by " + orderColumns + " limit :limit";
        selectSuffixParameters = Map.of("limit", maxRecords);
    }
    
    /**
     * Create paging data using order, page number and page size (but not any other condition)
     * @param orderColumns
     * @param pageSize page size (should be &gt; 0)
     * @param pageNumber page number starting with 0
     */
    public PostgresPagingAndSorting(String orderColumns, int pageSize, int pageNumber) {
        super();
        selectSuffixString = "order by " + orderColumns + " limit :limit offset :offset";
        selectSuffixParameters = Map.of("limit", pageSize, "offset", pageNumber * pageSize);
    }

    /**
     * Create paging data using order, page number and page size and also the where condition can be defined
     * @param orderColumns
     * @param pageSize page size (should be &gt; 0)
     * @param pageNumber page number starting with 0
     */
    public PostgresPagingAndSorting(String orderColumns, int pageSize, int pageNumber, String wherePart, Map<String, Object> whereParameters) {
        this(orderColumns, pageSize, pageNumber, new SimpleQuery(wherePart, whereParameters));
    }

    /**
     * Create paging data using order, page number and page size and also the where condition can be defined
     * @param orderColumns
     * @param pageSize page size (should be &gt; 0)
     * @param pageNumber page number starting with 0
     */
    public PostgresPagingAndSorting(String orderColumns, int pageSize, int pageNumber, Query conditionAndParams) {
        super();
        selectSuffixString = conditionAndParams.getQueryString() + " order by " + orderColumns + " limit :limit offset :offset";
        selectSuffixParameters = new HashMap<String, Object>(conditionAndParams.getQueryParameters());
        selectSuffixParameters.put("limit", pageSize);
        selectSuffixParameters.put("offset", pageNumber * pageSize);
    }

    @Override
    public String getQueryString() {
        return selectSuffixString;
    }

    @Override
    public Map<String, Object> getQueryParameters() {
        return selectSuffixParameters;
    }
}
