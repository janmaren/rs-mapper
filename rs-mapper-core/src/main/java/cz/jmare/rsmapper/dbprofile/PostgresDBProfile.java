package cz.jmare.rsmapper.dbprofile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.pagingsorting.PagingAndSortingBuilder;
import cz.jmare.rsmapper.pagingsorting.PostgresPagingAndSortingBuilder;

public class PostgresDBProfile implements DBProfile {
    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDBProfile.class);
    
    @Override
    public String getSqlForSequence(String sequenceName) {
        return "SELECT nextval('" + sequenceName + "')";
    }

    @Override
    public Object getObjectFromResultSet(ResultSet rs, int columnIndex) throws SQLException {
        Object object = rs.getObject(columnIndex);
        if (object == null) {
            return null;
        }
        if (object.getClass().getName().startsWith("org.postgresql.")) {
            LOGGER.warn("Unknow type {} will not be able to process", object.getClass());
        }
        return object;
    }

    @Override
    public void setObjectToPreparedStatement(PreparedStatement preparedStatement, int parameterIndex, Object value)
            throws SQLException {
        if (value == null) {
            preparedStatement.setObject(parameterIndex, value);
            return;
        }
        if (value instanceof OffsetTime) {
            OffsetTime offsetTime = (OffsetTime) value;
            Time time = java.sql.Time.valueOf(offsetTime.toLocalTime());
            preparedStatement.setObject(parameterIndex, time);
            return;
        }
        if (value instanceof java.util.Date) {
            java.util.Date date = (java.util.Date) value;
            LocalDateTime ldt = Instant.ofEpochMilli( date.getTime() )
                    .atZone( ZoneId.systemDefault() )
                    .toLocalDateTime();
            preparedStatement.setObject(parameterIndex, ldt);
            return;
        }
        preparedStatement.setObject(parameterIndex, value);
    }

    @Override
    public PagingAndSortingBuilder createPagingAndSortingBuilder() {
        return new PostgresPagingAndSortingBuilder();
    }
}
