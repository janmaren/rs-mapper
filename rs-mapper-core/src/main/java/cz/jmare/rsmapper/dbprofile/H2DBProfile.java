package cz.jmare.rsmapper.dbprofile;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.pagingsorting.H2PagingAndSortingBuilder;
import cz.jmare.rsmapper.pagingsorting.PagingAndSortingBuilder;

public class H2DBProfile implements DBProfile {
    private static final Logger LOGGER = LoggerFactory.getLogger(H2DBProfile.class);
    
    @Override
    public String getSqlForSequence(String sequenceName) {
        return "SELECT nextval('" + sequenceName + "')";
    }

    @Override
    public Object getObjectFromResultSet(ResultSet rs, int columnIndex) throws SQLException {
        Object object = rs.getObject(columnIndex);
        if (object == null) {
            return null;
        }
        if (object.getClass().getName().equals("org.h2.jdbc.JdbcBlob")) {
            return rs.getObject(columnIndex, byte[].class);
        }
        if (object.getClass().getName().equals("org.h2.jdbc.JdbcClob")) {
            return rs.getObject(columnIndex, String.class);
        }
        if (object.getClass().getName().startsWith("org.h2.")) {
            LOGGER.warn("Unknow type {} will not be able to process", object.getClass());
        }
        return object;
    }

    @Override
    public PagingAndSortingBuilder createPagingAndSortingBuilder() {
        return new H2PagingAndSortingBuilder();
    }

    @Override
    public CountType getCountType() {
        return CountType.COUNT_WRAPPED_SELECT;
    }
}
