package cz.jmare.rsmapper.dbprofile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.pagingsorting.PagingAndSortingBuilder;
import cz.jmare.rsmapper.util.ConvertUtil;

public interface DBProfile {
    /**
     * The purpose is not to return a JDBC specific type despite of the target class is unknown.
     * Convert it manualy to a well known type here
     * @param rs
     * @param index index of column in resultset
     * @return value of well know type 
     * @throws SQLException
     */
    default Object getObjectFromResultSet(ResultSet rs, int index) throws SQLException {
        return rs.getObject(index);
    }
    
    /**
     * Convert a value to an sql type and set it to prepared statement.
     * Note: this mehod is a fallback when entity contains some strange field types
     * @param preparedStatement
     * @param index
     * @param value
     * @throws SQLException
     */
    default void setObjectToPreparedStatement(PreparedStatement preparedStatement, int index, Object value) throws SQLException {
        preparedStatement.setObject(index, value);
    }
    
    /**
     * Return sql to be launched to obtain new id
     * @param sequenceName
     * @return
     */
    default String getSqlForSequence(String sequenceName) {
        throw new RsWrongDefException("Unknown DB system - unable to determine the right sql string to get sequence. Use concrete DBProfile for your DBMS");
    }
    
    default PagingAndSortingBuilder createPagingAndSortingBuilder() {
        throw new RsWrongDefException("Not implemented paging and sorting builder or unable to detect DB profile. Check " + getClass());
    }
    
    default CountType getCountType() {
        return CountType.COUNT_DISTINCT;
    }
    
    static Object getObject(ResultSet rs, int index, Class<?> type, DBProfile dbProfile) {
        Object value;
        try {
            value = rs.getObject(index, type);
        } catch (Exception e) {
            try {
                value = dbProfile.getObjectFromResultSet(rs, index);
                value = ConvertUtil.convertValueToRequiredType(value, type);
            } catch (SQLException e1) {
                throw new RsWrongDefException("Unable to get value with required type " + type + " from database");
            }
        }
        return value;
    }
}
