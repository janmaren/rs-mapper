package cz.jmare.rsmapper.dbprofile;

public enum CountType {
    COUNT_DISTINCT,
    COUNT_WRAPPED_SELECT
}
