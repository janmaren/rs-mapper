package cz.jmare.rsmapper;

import static cz.jmare.rsmapper.util.SqlCommandUtil.deleteRecordsFromTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.active.basic.AbstractRelation;
import cz.jmare.rsmapper.active.basic.Selecter;
import cz.jmare.rsmapper.exception.NoRecordException;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;
import cz.jmare.rsmapper.key.PrimaryKey;
import cz.jmare.rsmapper.rs.ColumnsFields;
import cz.jmare.rsmapper.rs.FieldMapper;
import cz.jmare.rsmapper.rs.SimpleRemapper;
import cz.jmare.rsmapper.rs.SingleFieldMapper;
import cz.jmare.rsmapper.util.AssocFkValues;
import cz.jmare.rsmapper.util.AssocKeysForPost;
import cz.jmare.rsmapper.util.ColumnsGenerator;
import cz.jmare.rsmapper.util.FieldAccessUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.PlaceholdersQuery;
import cz.jmare.rsmapper.util.ProcessFieldsUtil;
import cz.jmare.rsmapper.util.SqlCommandUtil;
import cz.jmare.util.BuildKeyUtil;
import cz.jmare.util.KeyUtil;

/**
 * This table represents a related table which has a foreign key to join zero or one [0..1] record 
 */
public class ToOne extends AbstractRelation implements PostJoin {
    private static final Logger LOGGER = LoggerFactory.getLogger(ToOne.class);
    
    private String foreignKeyFieldNameDef;

    private String leftAssocFieldNameDef;
    
    private Query condition;

    private AssocKeysForPost assocKeysForPost;
    
    private boolean deleteOrphan;
   
    public ToOne(Class<?> entityClass) {
        super(entityClass);
    }
    
    public ToOne(Class<?> entityClass, String leftAssocFieldName) {
        super(entityClass);
        this.leftAssocFieldNameDef = leftAssocFieldName;
    }
    
    public ToOne(Class<?> entityClass, String leftAssocFieldName, String foreignKeyFieldName, PrimaryKey primaryKey) {
        super(entityClass, primaryKey);
        this.leftAssocFieldNameDef = leftAssocFieldName;
        this.foreignKeyFieldNameDef = foreignKeyFieldName;
    }
    
    public ToOne(Class<?> entityClass, String leftAssocFieldName, String foreignKeyFieldName) {
        super(entityClass);
        this.leftAssocFieldNameDef = leftAssocFieldName;
        this.foreignKeyFieldNameDef = foreignKeyFieldName;
    }
    
    @Override
    public void saveByLeftPk(Handle handle, Object leftObject, Map<String, Object> leftPrimKeyNameValueMap) {
        AssocFkValues assocFKValues = assocKeysForPost.getAssocFKValues(leftPrimKeyNameValueMap);
        KeyUtil.checkTypesSame(assocFKValues.foreignKeyFields.stream().map(f -> f.getType()).collect(Collectors.toList()), assocFKValues.leftPrimaryKeyValues.stream().map(o -> o.getClass()).collect(Collectors.toList()));
        
        Object object = FieldAccessUtil.getValue(leftObject, assocKeysForPost.getLeftAssocField());
        if (object instanceof Collection<?>) {
            throw new RsWrongDefException(assocKeysForPost.getLeftAssocFieldName() + " is a collection field but it should be a simple entity using ToOne. Or maybe the ToMany should be used");
        }
        saveAssignedObject(handle, assocFKValues, object);
    }

    /**
     * Save object with foreign key defined as masterKey
     * @param handle
     * @param leftPrimaryKey key from other table
     * @param object object to be persisted. Note it may be null - in this case a record should be deleted from database
     */
    protected void saveAssignedObject(Handle handle, AssocFkValues assocFKValues, Object object) {
        if (object == null) {
            // asssociation is null
            ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
            String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);
            LinkedHashMap<String, Object> keyOfPersistedRecord =  SqlCommandUtil.selectOneVector(handle, classColumnFields, tablePrefix + tableName, wherePart, assocFKValues.leftPrimaryKeyValues, condition);

            if (keyOfPersistedRecord != null) {
                // association in db
                if (deleteOrphan) {
                    for (Join join : joins) {
                        if (join instanceof PostJoin) {
                            PostJoin postJoin = (PostJoin) join;
                            postJoin.deleteByLeftPk(handle, entityClass, KeyUtil.mergeKeysValues(primaryKey.getFieldNames(), KeyUtil.values(keyOfPersistedRecord)));
                        }
                    }
                    deleteRecordsFromTable(handle, tablePrefix + tableName, keyOfPersistedRecord);
                } else {
                    ColumnsFields classColumnFieldsForeign = ProcessFieldsUtil.getClassColumnFields(entityClass, assocFKValues.foreignKeyFieldNames);
                    SqlCommandUtil.updateWithParams(handle, tablePrefix + tableName, classColumnFieldsForeign.columnNames, KeyUtil.populateNulls(classColumnFieldsForeign.columnNames.size()), classColumnFieldsForeign.columnNames, assocFKValues.leftPrimaryKeyValues);
                }
            }
            return;
        } else {
            // asssociation is not null
            List<Object> id = FieldAccessUtil.getBasicValue(object, primaryKey.getFieldNames());
            if (KeyUtil.isNull(id)) {
                // new object, if in db assigned previous object exists then delete it  
                ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
                String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);
                Collection<LinkedHashMap<String, Object>> deleteKeys = SqlCommandUtil.selectMultipleVectors(handle, classColumnFields, tablePrefix + tableName, wherePart, assocFKValues.leftPrimaryKeyValues, condition);
                if (deleteKeys.size() > 1) {
                    LOGGER.warn("Expected max one entity " + entityClass + " but found multiple in ToOne. Keys:" + deleteKeys);
                }
                for (LinkedHashMap<String, Object> deleteKey : deleteKeys) {
                    if (deleteOrphan) {
                        for (Join join : joins) {
                            if (join instanceof PostJoin) {
                                PostJoin postJoin = (PostJoin) join;
                                postJoin.deleteByLeftPk(handle, entityClass, KeyUtil.mergeKeysValues(primaryKey.getFieldNames(), KeyUtil.values(deleteKey)));
                            }
                        }
                        SqlCommandUtil.deleteRecordsFromTable(handle, tablePrefix + tableName, deleteKey);
                    } else {
                        ColumnsFields classColumnFieldsForeign = ProcessFieldsUtil.getClassColumnFields(entityClass, assocFKValues.foreignKeyFieldNames);
                        SqlCommandUtil.updateWithParams(handle, tablePrefix + tableName, classColumnFieldsForeign.columnNames, KeyUtil.populateNulls(classColumnFieldsForeign.columnNames.size()), classColumnFieldsForeign.columnNames, KeyUtil.populateNulls(classColumnFieldsForeign.columnNames.size()));
                    }
                }
            }
        }
        FieldAccessUtil.setValueWithConversion(object, assocFKValues.foreignKeyFields, assocFKValues.leftPrimaryKeyValues);
        super.save(handle, object);
    }

    /**
     * Condition to be added into where part to filter out records to be deleted or updated
     * @param condition without table alias, for example: employee_type=123
     * @return
     */    
    public ToOne withCondition(String condition) {
        this.condition = new SimpleQuery(condition);
        return this;
    }
    
    /**
     * Condition to be added into where part to filter out records to be deleted or updated
     * @param condition without table alias, for example: employee_type=123
     * @return
     */ 
    public ToOne withCondition(Query condition) {
        this.condition = condition;
        return this;
    }
    
    @Override
    public void deleteByLeftPk(Handle handle, Class<?> leftClass, Map<String, Object> leftPrimKeyNameValueMap) {
        AssocFkValues assocFKValues = assocKeysForPost.getAssocFKValues(leftPrimKeyNameValueMap);
        KeyUtil.checkPrimToFk(leftClass, assocFKValues.leftPrimaryKeyValues, entityClass, assocFKValues.foreignKeyFieldNames);

        if (deleteOrphan) {
        	ColumnsFields classColumnFields = ProcessFieldsUtil.getClassColumnFields(entityClass, primaryKey.getFieldNames());
            String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);
            Collection<LinkedHashMap<String, Object>> compositeIds = SqlCommandUtil.selectMultipleVectors(handle, classColumnFields, tablePrefix + tableName, wherePart, assocFKValues.leftPrimaryKeyValues, condition);
	        for (LinkedHashMap<String, Object> compositeId : compositeIds) {
	            for (Join saverRelated : joins) {
	                if (saverRelated instanceof PostJoin) {
	                    PostJoin postSaver = (PostJoin) saverRelated;
	                    postSaver.deleteByLeftPk(handle, entityClass, KeyUtil.mergeKeysValues(primaryKey.getFieldNames(), KeyUtil.values(compositeId)));
	                }
	            }
	            SqlCommandUtil.deleteRecordsFromTable(handle, tablePrefix + tableName, compositeId);
	        }
        } else {
            ColumnsFields classColumnFieldsForeign = ProcessFieldsUtil.getClassColumnFields(entityClass, assocFKValues.foreignKeyFieldNames);
            SqlCommandUtil.updateWithParams(handle, tablePrefix + tableName, classColumnFieldsForeign.columnNames, KeyUtil.populateNulls(classColumnFieldsForeign.columnNames.size()), classColumnFieldsForeign.columnNames, assocFKValues.leftPrimaryKeyValues);
        }
    }

    @Override
    public FieldMapper toFieldMapper(Class<?> leftClass) {
        List<String> fieldNames = primaryKey.getFieldNames();
        Function<Map<String, Object>, Object> keyFunction = BuildKeyUtil.getKeyFunction(fieldNames, entityClass, alias);
        return new SingleFieldMapper(assocKeysForPost.getLeftAssocFieldName(), keyFunction, new SimpleRemapper(entityClass, "_" + alias), AbstractRelation.toFieldMappers(joins, entityClass));
    }
    
    @Override
    public void loadByLeftPk(Handle handle, Object leftObject, Map<String, Object> leftPrimKeyNameValueMap) {
        AssocFkValues assocFKValues = assocKeysForPost.getAssocFKValues(leftPrimKeyNameValueMap);
        //List<Object> leftPrimaryKeys = assocKeysForPost.reorderToFKOrder(leftPrimaryKeyMap);
        KeyUtil.checkPrimToFk(leftObject.getClass(), assocFKValues.leftPrimaryKeyValues, entityClass, assocFKValues.foreignKeyFieldNames);

        Optional<Object> objectOpt = loadAssignedObject(handle, assocFKValues);
        if (objectOpt.isEmpty()) {
            return;
        }
        FieldAccessUtil.setValueWithConversion(leftObject, assocKeysForPost.getLeftAssocField(), objectOpt.get());
    }
    
    /**
     * Load assigned slave object which uses left primary key as foreign key value
     * @param handle
     * @param leftPrimaryKey key from other table
     * @return
     * @throws LoadException when multiple records found
     */
    protected Optional<Object> loadAssignedObject(Handle handle, AssocFkValues assocFKValues) {
        Selecter selecter = new Selecter(tablePrefix + tableName, primaryKey.getFieldNames());
        String wherePart = FieldsUtil.generateWhereClause(entityClass, assocFKValues.foreignKeyFieldNames);
        
        List<Object> placeholderValues = new ArrayList<Object>(assocFKValues.leftPrimaryKeyValues);
        if (condition != null) {
            PlaceholdersQuery query = SqlCommandUtil.toPlaceholdersQuery(condition);
            wherePart += " AND " + query.getQueryString();
            placeholderValues.addAll(query.getValues());
        }
        Object object;
        try {
            object = selecter.queryOneByPlaceholders(handle, entityClass, wherePart, placeholderValues);
        } catch (NoRecordException e) {
            return Optional.empty();
        }
        
        //LinkedHashMap<String, Object> key = FieldsUtil.extractColumnsValuesMap(object, primaryKey.getFieldNames());
        for (Join join : joins) {
            if (join instanceof PreJoin) {
                PreJoin preJoin = (PreJoin) join;
                preJoin.loadPrimByLeftFk(handle, object);        
            } else if (join instanceof PostJoin) {
                PostJoin postJoin = (PostJoin) join;
                Map<String, Object> pkFieldsValues = FieldAccessUtil.getBasicMapOrNulls(object, primaryKey.getFieldNames());
                postJoin.loadByLeftPk(handle, object, pkFieldsValues);
            }
        }
        
        return Optional.of(object);
    }
    
    /**
     * Set primary key
     */
    public ToOne withPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }
    
    /**
     * Set joins, ToOne, or ToOnePrim or ToMany
     * @param joins
     * @return
     */
    public ToOne withJoins(Join... joins) {
        this.joins = joins;
        return this;
    }
    
    /**
     * Set human readable alias to replace default alias, like _1, _2,...
     * @param alias
     * @return
     */
    public ToOne withAlias(String alias) {
        this.customAlias = alias;
        return this;
    }
    
    /**
     * Set names of fields omitted in insert
     * @param nonInsertableFieldNames
     * @return
     */
    public ToOne withNonInsertableFields(String... nonInsertableFieldNames) {
        this.nonInsertableFieldNames = nonInsertableFieldNames;
        return this;
    }

    /**
     * Set names of fields omitted in update
     * @param nonUpdatableFieldNames
     * @return
     */
    public ToOne withNonUpdatableFields(String... nonUpdatableFieldNames) {
        this.nonUpdatableFieldNames = nonUpdatableFieldNames;
        return this;
    }
    
    /**
     * When deleting referencing record, delete also this
     * @return
     */
    public ToOne withDeleteOrphan() {
        this.deleteOrphan = true;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in insert.
     * @return
     */
    public ToOne withNonInsertableNulls() {
        this.nonInsertableNulls = true;
        return this;
    }
    
    /**
     * Columns with null values will be omitted in update.
     * @return
     */
    public ToOne withNonUpdatableNulls() {
        this.nonUpdatableNulls = true;
        return this;
    }
    
    /**
     * A schema, catalog or database prefix before a table.
     * @param tablePrefix
     * @return
     */
    public ToOne withTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
        if (!tablePrefix.endsWith(".")) {
            this.tablePrefix += ".";
        }
        return this;
    }
    
    @Override
    public void init(ColumnsGenerator columnsGenerator, Class<?> leftClass) {
        addToColumnsGenerator(columnsGenerator);
        
        initBasicByAnno();
        
        if (primaryKey == null) {
            throw new RsWrongDefException("Class " + entityClass.getCanonicalName() + " has no id field defined although it is used in ToOne join.");
        }
        
        assocKeysForPost = new AssocKeysForPost(leftClass, entityClass,
                leftAssocFieldNameDef, foreignKeyFieldNameDef);
    }
}
