package cz.jmare.rsmapper;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({TYPE}) 
@Retention(RUNTIME)
public @interface Table {
    /**
     * (Optional) Table name. When not populated the name is taken from class name
     */
    String name() default "";
}
