package cz.jmare.rsmapper;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.key.IdentityPrimaryKey;
import cz.jmare.rsmapper.key.PassedPrimaryKey;
import cz.jmare.rsmapper.key.PrimaryKey;
import cz.jmare.rsmapper.key.SequencePrimaryKey;
import cz.jmare.rsmapper.util.CamelUtil;
import cz.jmare.rsmapper.util.FieldsUtil;
import cz.jmare.rsmapper.util.FieldsUtil.FieldAnno;

public class EntityAnnoFetcher {
    public static EntityData initMeta(Class<?> clazz, PrimaryKey primaryKey, String tableName) {
        EntityData entityData = new EntityData(primaryKey, tableName);
        if (entityData.primaryKey == null) {
            List<FieldAnno> idAnnos = FieldsUtil.findIdAnnos(clazz);
            if (idAnnos.size() > 0) {
                List<String> fieldNames = new ArrayList<>();
                IdType type = null;
                for (FieldAnno fieldAnno : idAnnos) {
                    fieldNames.add(fieldAnno.field.getName());
                    type = ((Id) fieldAnno.annotation).type();
                }
                
                if (fieldNames.size() > 1 || type == IdType.PASSED) {
                    entityData.primaryKey = new PassedPrimaryKey(fieldNames);
                } else if (type == IdType.IDENTITY) {
                    entityData.primaryKey = new IdentityPrimaryKey(fieldNames.get(0));
                } else if (type == IdType.SEQUENCE) {
                    FieldAnno fieldAnno = idAnnos.get(0);
                    Id id = (Id) fieldAnno;
                    if (id.sequenceName() == null || "".equals(id.sequenceName().trim())) {
                        throw new RsWrongDefException("Unknown sequence name for " + clazz.getCanonicalName() + "." + fieldAnno.field.getName());
                    }
                    entityData.primaryKey = new SequencePrimaryKey(fieldAnno.field.getName(), id.sequenceName());
                } else {
                    throw new RsWrongDefException("Unsupported type of primary key for " + tableName);
                }
            } // maybe will be primary passed by withPrimaryKey method
        }
        if (entityData.tableName == null) {
            entityData.tableName = CamelUtil.decamelString(clazz.getSimpleName());
            Table tableAnno = findTableAnno(clazz);
            if (tableAnno != null && !"".equals(tableAnno.name().trim())) {
                entityData.tableName = tableAnno.name();
            }
        }
        return entityData;
    }
    
    public static Table findTableAnno(Class<?> clazz) {
        return clazz.getAnnotation(Table.class);
    }
    
    public static class EntityData {
        public PrimaryKey primaryKey;
        
        public String tableName;

        public EntityData(PrimaryKey primaryKey, String tableName) {
            this.primaryKey = primaryKey;
            this.tableName = tableName;
        }
    }
}
