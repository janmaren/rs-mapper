package cz.jmare.rsmapper.handle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.dbprofile.DBProfile;
import cz.jmare.rsmapper.dbprofile.DefaultDBProfile;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.util.ClassUtil;

public class SimpleHandle implements Handle {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleHandle.class);
    
    private Connection connection;
    private DBProfile dbProfile;
    private DataSource dataSource;
    private String sequencePattern;
    
    private boolean supportSpringTransactions;

    private static Method releaseConnectionMethod; 
    
    static {
        try {
            Class<?> clazz = ClassUtil.getClassByName("org.springframework.jdbc.datasource.DataSourceUtils");
            releaseConnectionMethod = clazz.getMethod("doReleaseConnection", Connection.class, DataSource.class);
        } catch (NoSuchMethodException | SecurityException | RsWrongDefException e) {
            // not supported Spring
        }
    }
    public SimpleHandle(Connection connection, DBProfile dbProfile) {
        super();
        this.connection = connection;
        this.dbProfile = dbProfile;
    }

    public SimpleHandle(Connection connection) {
        super();
        this.connection = connection;
        this.dbProfile = new DefaultDBProfile();
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public DBProfile getDbProfile() {
        return dbProfile;
    }

    @Override
    public void close() {
        try {
            if (supportSpringTransactions) {
                try {
                    releaseConnectionMethod.invoke(null, connection, dataSource);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    LOGGER.error("Problems to call org.springframework.jdbc.datasource.DataSourceUtils.doReleaseConnection()", e);
                }
            } else {
                this.connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Unable to close connection", e);
        }
    }

    public void enableSupportSpringTransactions(DataSource dataSource) {
        supportSpringTransactions = true;
        this.dataSource = dataSource;
    }

    @Override
    public String getSequencePattern() {
        return sequencePattern;
    }

    public void setSequencePattern(String sequencePattern) {
        this.sequencePattern = sequencePattern;
    }
}
