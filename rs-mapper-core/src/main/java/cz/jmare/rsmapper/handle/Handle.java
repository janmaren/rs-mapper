package cz.jmare.rsmapper.handle;

import java.io.Closeable;
import java.sql.Connection;

import cz.jmare.rsmapper.dbprofile.DBProfile;

public interface Handle extends Closeable {
    /**
     * Return db connection. Note: it can return also null value - in this case 
     * the connection must be obtained in other way from a datasource
     * @return
     */
    Connection getConnection();
    
    /**
     * Return db profile
     * @return
     */
    DBProfile getDbProfile();

    /**
     * Pattern to by used to get name of sequence based on table name and column
     * Supported placeholders to be populated are: &lt;TABLE&gt; and &lt;IDCOLUMN&gt;<br>
     * Example: &lt;TABLE&gt;_&lt;IDCOLUMN&gt;_SEQ
     * @return
     */
    String getSequencePattern();
    
    void close();
}
