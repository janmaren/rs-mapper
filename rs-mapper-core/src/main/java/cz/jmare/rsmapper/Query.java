package cz.jmare.rsmapper;

import java.util.Map;

/**
 * Query represents part of a select command, where introducing part (format: select something from tableName alias) is pregenerated
 * and Query will be appended behind it.<br>
 * It can contain joins to tables, where condition (word 'where' must be entered) part, having part, order by part and placeholders starting with colon.
 * Example:
 * <pre>
 * join group g on _1.id=g.emp_id where _1.name=:name order by _1.age
 * </pre> 
 */
public interface Query {
    /**
     * Return part which is behind table name. The part 'select something from table' is generate automatically and this string should be appended behind.
     * It should usually start with 'where ...' unless next joins added. The named parameters can be used
     */
    String getQueryString();
    
    /**
     * Get pairs namedParameter-value to be set to prepared statement
     */
    Map<String, Object> getQueryParameters();
}
