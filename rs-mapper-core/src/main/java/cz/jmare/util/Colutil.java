package cz.jmare.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Colutil {
    public static <T> List<T> filter(Collection<T> collection, Predicate<T> predicate) {
        return (List<T>) filter(collection, predicate, new ArrayList<T>());
    }

    public static <T> Collection<T> filter(Collection<T> collection, Predicate<T> predicate, Collection<T> resultCollection) {
        for (T element : collection) {
            if (predicate.test(element)) {
                resultCollection.add(element);
            }
        }
        return resultCollection;
    }

    @SuppressWarnings("unchecked")
    public static <T, U extends T> List<U> filterTypes(Collection<T> collection, Class<U> clazz) {
        ArrayList<U> resultCollection = new ArrayList<U>();
        for (T element : collection) {
            Class<? extends Object> elClazz = element.getClass();
            if (clazz.isAssignableFrom(elClazz)) {
                resultCollection.add((U) element);
            }
        }
        return resultCollection;
    }

    public static <T> List<T> filterNegative(Collection<T> collection, Predicate<T> predicate) {
        return (List<T>) filterNegative(collection, predicate, new ArrayList<T>());
    }

    public static <T> Collection<T> filterNegative(Collection<T> collection, Predicate<T> predicate, Collection<T> resultCollection) {
        for (T element : collection) {
            if (!predicate.test(element)) {
                resultCollection.add(element);
            }
        }
        return resultCollection;
    }
    
    public static <T> T findOrCreate(Collection<T> collection, Predicate<T> predicate, Supplier<T> newInstanceSupplier) {
        for (T element : collection) {
            if (predicate.test(element)) {
                return element;
            }
        }
        T t = newInstanceSupplier.get();
        collection.add(t);
		return t;
    }
    
    public static <T> T findFirst(Collection<T> collection, Predicate<T> predicate) {
        for (T element : collection) {
            if (predicate.test(element)) {
                return element;
            }
        }
        return null;
    }

    public static <T> T findLast(Collection<T> collection, Predicate<T> predicate) {
        return findLast(collection, predicate, null);
    }

    public static <T> T findLast(Collection<T> collection, Predicate<T> predicate, T defaultValue) {
        T result = null;
        for (T element : collection) {
            if (predicate.test(element)) {
                result = element;
            }
        }
        return result != null ? result : defaultValue;
    }

    public static <T> List<T> subtract(final Collection<T> a, final Collection<T> b) {
        return (List<T>) subtract(a, b, new ArrayList<T>());
    }

    public static <T> Collection<T> subtract(final Collection<T> a, final Collection<T> b, Collection<T> resultCollection) {
        resultCollection.addAll(a);
        for (Iterator<T> it = b.iterator(); it.hasNext();) {
            resultCollection.remove(it.next());
        }
        return resultCollection;
    }

    public static <T> Set<List<T>> getGroups(final Collection<T> a, Comparator<T> comparator) {
        ArrayList<T> tmpList = new ArrayList<T>(a);
        HashSet<List<T>> groups = new HashSet<List<T>>();
        while (!tmpList.isEmpty()) {
            List<T> groupList = new ArrayList<T>();
            Iterator<T> iterator = tmpList.iterator();
            T first = iterator.next();
            groupList.add(first);
            iterator.remove();
            while (iterator.hasNext()) {
                T other = iterator.next();
                if (comparator.compare(first, other) == 0) {
                    groupList.add(other);
                    iterator.remove();
                }
            }
            groups.add(groupList);
        }

        return groups;
    }

    /**
     * Return groups, where each group is identified
     * @param a collection of entities to group
     * @param identifierProvider function to return identifier of entity
     * <pre>
     *    Map&lt;String, List&lt;String&gt;&gt; groups = getGroups(list, new Function&lt;String, String&gt;(){
     *       public &lt;T, R&gt; R apply(T t) {
     *           return (R) t;
     *       }
     *    });
     * </pre>
     * @return
     */
    public static <T, U> Map<U, List<T>> getGroups(final Collection<T> a, Function<T, U> identifierProvider) {
        LinkedHashMap<U, List<T>> map = new LinkedHashMap<U, List<T>>();
        for (T t: a) {
            U u = identifierProvider.apply(t);
            List<T> listInGroup = map.get(u);
            if (listInGroup == null) {
                listInGroup = new ArrayList<T>();
                map.put(u, listInGroup);
            }
            listInGroup.add(t);
        }

        return map;
    }

    /**
     * Find minimal or maximal value depending on direction
     * @param a
     * @param columnReturner must return value by which the comparision is done
     * @param comparator
     * @param direction 1 = as comparator returns, -1 means opposite comparator
     * @return
     */
    public static <T, U> T findEdge(final Collection<T> a, Function<T, U> columnReturner, Comparator<U> comparator, int direction) {
        Iterator<T> iterator = a.iterator();
        if (!iterator.hasNext()) {
            return null;
        }
        T minT = iterator.next();
        U minU = columnReturner.apply(minT);
        while (iterator.hasNext()) {
            T t = iterator.next();
            U u = columnReturner.apply(t);
            if (comparator.compare(u, minU) * direction < 0) {
                minT = t;
                minU = u;
            }
        }
        return minT;
    }

    /**
     * @param a
     * @param columnReturner
     * @param comparator (in comparator must be o1.compareTo(o2)) otherwise use {@link Colutil#findEdge(Collection, Function, Comparator, int)}
     * @return
     */
    public static <T, U> T findMinimal(final Collection<T> a, Function<T, U> columnReturner, Comparator<U> comparator) {
        return findEdge(a, columnReturner, comparator, 1);
    }

    /**
     * @param a
     * @param columnReturner
     * @param comparator (in comparator must be o1.compareTo(o2)) otherwise use {@link Colutil#findEdge(Collection, Function, Comparator, int)}
     * @return
     */
    public static <T, U> T findMaximal(final Collection<T> a, Function<T, U> columnReturner, Comparator<U> comparator) {
        return findEdge(a, columnReturner, comparator, -1);
    }

    /**
     * Return true when collection contains element defined by predicate
     * @param collection
     * @param predicate
     * @return
     */
    public static <T> boolean exists(Collection<T> collection, Predicate<T> predicate) {
        return findFirst(collection, predicate) != null;
    }

    /**
     * Return true when field of element of collection contains given value<br/>
     * Note: useful when elements don't contain the {@link Object#equals(Object)} method so {@link Collection#contains(Object)} method couldn't be used
     * @param collection
     * @param columnReturner function which returns value of field
     * @param searchColumnValue value of field that must match
     * @return
     */
    public static <T, R> boolean exists(Collection<T> collection, Function<T, R> columnReturner, R searchColumnValue) {
        for (T element : collection) {
            R elementFieldValue = columnReturner.apply(element);
            if (elementFieldValue.equals(searchColumnValue)) {
                return true;
            }
        }
        return false;
    }
}
