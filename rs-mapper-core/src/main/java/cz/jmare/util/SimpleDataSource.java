package cz.jmare.util;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import cz.jmare.rsmapper.exception.RsWrongDefException;

public class SimpleDataSource implements DataSource {
    private String url;
    
    private String username;
    
    private String password;
    
    private String schema;
    
    private String catalog;
    
    public SimpleDataSource(String url) {
        super();
        this.url = url;
    }

    public SimpleDataSource(String url, String driver) {
        super();
        this.url = url;
        setDriver(driver);
    }

    public SimpleDataSource(String url, String username, String password) {
        super();
        this.url = url;
        this.username = username;
        this.password = password;
    }
    
    public SimpleDataSource(String url, String username, String password, Driver driver) {
        super();
        this.url = url;
        this.username = username;
        this.password = password;
        setDriver(driver.getClass().getCanonicalName());
    }
    
    public SimpleDataSource(String url, String username, String password, String driver) {
        super();
        this.url = url;
        this.username = username;
        this.password = password;
        setDriver(driver);
    }
    
    public void setDriver(String driver) {
        ClassUtil.getClassByName(driver);
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        if (iface.isInstance(this)) {
            return (T) this;
        }
        throw new SQLException("Unable to unwrap " + iface.getCanonicalName());
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return iface.isInstance(this);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getConnection(null, null);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        if (url == null) {
            throw new RsWrongDefException("URL not defined");
        }
        String user = username != null ? username : this.username;
        Connection connection;
        if (user != null) {
            String pass = password != null ? password : this.password;
            connection = DriverManager.getConnection(url, user, pass);
        } else {
            connection = DriverManager.getConnection(url);
        }
        
        if (schema != null) {
            connection.setSchema(schema);
        }
        
        if (catalog != null) {
            connection.setCatalog(catalog);
        }
        
        return connection;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new UnsupportedOperationException("getLogWriter");
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        throw new UnsupportedOperationException("setLogWriter");
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        throw new UnsupportedOperationException("setLoginTimeout");
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }
}
