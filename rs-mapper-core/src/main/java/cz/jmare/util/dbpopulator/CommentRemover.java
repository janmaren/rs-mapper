package cz.jmare.util.dbpopulator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.rsmapper.exception.RsWrongDefException;

public class CommentRemover {
    private final static String PARSE_PATTERN = "(?<apos>\\')|(?<lineComment>--)|(?<startComment>/\\*)|(?<endComment>\\*/)|(?<terminator>\\r?\\n)";

    public static String remove(String str) {
        StringBuilder sb = new StringBuilder();
        int startPos = 0;
        boolean inLineComment = false;
        boolean inComment = false;
        boolean inApos = false;
        Pattern pattern = Pattern.compile(PARSE_PATTERN);
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            boolean lineComment = matcher.group("lineComment") != null;
            boolean startComment = matcher.group("startComment") != null;
            boolean endComment = matcher.group("endComment") != null;
            boolean terminator = matcher.group("terminator") != null;
            boolean apos = matcher.group("apos") != null;

            if (lineComment && !inApos) {
                sb.append(str.substring(startPos, matcher.start("lineComment")));
                inLineComment = true;
                continue;
            }

            if (startComment && !inApos && !inComment) {
                sb.append(str.substring(startPos, matcher.start("startComment")));
                inComment = true;
                continue;
            }

            if (endComment && !inApos) {
                startPos = matcher.end("endComment");
                inComment = false;
                continue;
            }

            if (apos) {
                inApos = !inApos;
                continue;
            }

            if (terminator) {
                if (inLineComment) {
                    startPos = matcher.end("terminator");
                    inLineComment = false;
                }
                continue;
            }
        }

        if (!inComment && !inLineComment && !inApos) {
            sb.append(str.substring(startPos));
        } else {
            if (inComment) {
                throw new RsWrongDefException("Missing closing comment");
            }
            if (inApos) {
                throw new RsWrongDefException("Missing closing quote");
            }
        }

        return sb.toString().trim();
    }

    public static void main(String[] args) {
        String remove = CommentRemover.remove("-- poznamka\ncreate table('// zach''ovat')/* poznamka */;");
        System.out.println(remove);
    }
}
