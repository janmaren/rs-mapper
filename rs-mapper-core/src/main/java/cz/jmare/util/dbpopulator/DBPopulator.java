package cz.jmare.util.dbpopulator;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.Connection;
import java.text.ParseException;
import java.util.List;

public class DBPopulator {
    public static void populate(Connection connection, String content) {
        try {
            StatementSeparator statementSeparator = new StatementSeparator();
            List<SimpleSqlExecutor> sqlExecutors = statementSeparator.getSqlExecutors(content);
            for (SimpleSqlExecutor simpleSqlExecutor : sqlExecutors) {
                simpleSqlExecutor.execute(connection);
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException("Unable to parse file", e);
        }
    }
    
    public static void populate(Connection connection, File filepath) {
        try {
            String content = Files.readString(filepath.toPath());
            populate(connection, content);
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to populate by file " + filepath, e);
        }
    }

    public static void populate(Connection connection, URL url) {
        File filepath;
        try {
            filepath = new File(url.toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("URL " + url + " is invalid", e);
        }
        populate(connection, filepath);
    }
}
