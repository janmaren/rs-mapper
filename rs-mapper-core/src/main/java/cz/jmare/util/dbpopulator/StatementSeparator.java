package cz.jmare.util.dbpopulator;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StatementSeparator {
    private String alternativeSeparator = "/";

    private final static String PARSE_PATTERN = "(?<lineComment>--)|(?<startComment>/\\*)|(?<endComment>\\*/)|(?<apos>\\')|(?<semicolon>;\\s*)|(?<terminator>\\r?\\n)|(?<begin1>^BEGIN\\s)|(?<begin2>\\sBEGIN\\s)|(?<altsep>%s\\s+|%s\\s+)";

    public List<SimpleSqlExecutor> getSqlExecutors(String sql) throws ParseException {
        List<SimpleSqlExecutor> sqlExecutors = new ArrayList<SimpleSqlExecutor>();

        int startPos = 0;
        boolean inLineComment = false;
        boolean inComment = false;
        boolean inPlSql = false;
        boolean inApos = false;

        Pattern pattern = Pattern.compile(String.format(PARSE_PATTERN, alternativeSeparator, alternativeSeparator), Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(sql);
        while (matcher.find()) {
            boolean semicolon = matcher.group("semicolon") != null;
            boolean altsep = matcher.group("altsep") != null;
            boolean lineComment = matcher.group("lineComment") != null;
            boolean startComment = matcher.group("startComment") != null;
            boolean endComment = matcher.group("endComment") != null;
            boolean terminator = matcher.group("terminator") != null;
            boolean apos = matcher.group("apos") != null;
            boolean begin1 = matcher.group("begin1") != null;
            boolean begin2 = matcher.group("begin2") != null;

            if (lineComment && !inApos) {
                inLineComment = true;
                continue;
            }

            if (startComment && !inApos) {
                inComment = true;
                continue;
            }

            if (endComment && !inApos) {
                inComment = false;
                continue;
            }

            if (apos) {
                inApos = !inApos;
                continue;
            }

            if ((begin1 || begin2) && !inApos && !inComment && !inLineComment) {
                inPlSql = true;
                continue;
            }

            if (terminator) {
                inLineComment = false;
                continue;
            }

            if (!inLineComment && !inComment && !inApos) {
                if (inPlSql) {
                    if (altsep) {
                        SimpleSqlExecutor sqlExecutor = new SimpleSqlExecutor(sql, startPos, matcher.start("altsep"), matcher.end("altsep"));
                        sqlExecutors.add(sqlExecutor);
                        startPos = matcher.end();
                        inPlSql = false;
                    }
                } else {
                    if (semicolon) {
                        sqlExecutors.add(new SimpleSqlExecutor(sql, startPos, matcher.start("semicolon"), matcher.end("semicolon")));
                        startPos = matcher.end("semicolon");
                    }
                }
            }
        }

        if (inComment) {
            throw new ParseException("Missing closing comment", startPos);
        }
        if (inApos) {
            throw new ParseException("Missing closing quote", startPos);
        }

        // ---- udelame zbytek, ke kteremu neni na konci ukoncovaci znak ----
        String remainedString = sql.substring(startPos, sql.length()).trim();
        if (!"".equals(remainedString)) {
            if (inPlSql) {
                SimpleSqlExecutor sqlExecutor = new SimpleSqlExecutor(sql, startPos, sql.length(), sql.length());
                sqlExecutors.add(sqlExecutor);
                inPlSql = false;
            } else {
                sqlExecutors.add(new SimpleSqlExecutor(sql, startPos, sql.length(), sql.length()));
            }
        }

        return sqlExecutors;
    }

    public void setAlternativeSeparator(String alternativeSeparator) {
        this.alternativeSeparator = alternativeSeparator;
    }
}
