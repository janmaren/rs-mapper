package cz.jmare.util.dbpopulator;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSqlExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleSqlExecutor.class);

    public SimpleSqlExecutor(String sql, int startOffset, int pureEndOffset, int endOffset) {
        super();
        this.sql = sql;
        this.startOffset = startOffset;
        this.pureEndOffset = pureEndOffset;
        this.endOffset = endOffset;
    }

    public void execute(Connection connection) {
        String executeSql = getExecuteSqlStatement();
        LOGGER.info("Executing statement:\n" + executeSql);
        try {
            Statement statement = connection.createStatement();
            boolean resultSet = statement.execute(executeSql);
            if (resultSet) {
                LOGGER.info("OK");
            } else {
                int updateCount = statement.getUpdateCount();
                if (updateCount >= 0) {
                    LOGGER.info("OK - Updated {} records", updateCount);
                } else {
                    LOGGER.info("OK");
                }
            }
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Unable to launch " + sql + ", problems on '" + sql.substring(startOffset, endOffset) + "'", e);
        }
    }

    @Override
    public String toString() {
        return "SimpleSqlExecutor [" + startOffset + "-" + endOffset + "]:" + getRawSqlStatement();
    }
    
    private final static Pattern DROP = Pattern.compile("^drop\\s", Pattern.CASE_INSENSITIVE);

    protected String sql;

    protected int startOffset;

    /**
     * End offset for statement without terminator
     */
    protected int pureEndOffset;

    /**
     * End offset for statement including terminator
     */
    protected int endOffset;

    protected String getExecuteSqlStatement() {
        return sql.substring(startOffset, pureEndOffset);//.trim();
    }

    protected String getRawSqlStatement() {
        return sql.substring(startOffset, endOffset);
    }

    public boolean isDrop() {
        String stripedSql = CommentRemover.remove(getExecuteSqlStatement());
        return DROP.matcher(stripedSql).find();
    }
}
