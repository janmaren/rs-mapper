package cz.jmare.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import cz.jmare.rsmapper.rs.CompoundKeyFunction;
import cz.jmare.rsmapper.util.FieldsUtil;

public class BuildKeyUtil {
    public static Function<Map<String, Object>, Object> getKeyFunction(List<String> fieldNames, Class<?> entityClass, String alias) {
        List<String> columnNames = new ArrayList<String>();
        for (String fieldName : fieldNames) {
            String columnName = FieldsUtil.columnNameByFieldName(entityClass, fieldName) + "_" + alias;
            columnNames.add(columnName);
        }
        return new CompoundKeyFunction(columnNames);
    }
}
