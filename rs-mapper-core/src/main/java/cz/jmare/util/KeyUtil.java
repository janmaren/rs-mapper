package cz.jmare.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jmare.rsmapper.exception.RsWrongDefException;

public class KeyUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyUtil.class);
    
    public static LinkedHashMap<String, Object> mergeKeysValues(List<String> keys, List<Object> values) throws RsWrongDefException {
        if (keys.size() != values.size()) {
            throw new RsWrongDefException("Keys and values have different number of items, " + keys + " vs " + values);
        }
        LinkedHashMap<String, Object> hashMap = new LinkedHashMap<>();
        for (int i = 0; i < values.size(); i++) {
            String key = keys.get(i);
            Object value = values.get(i);
            hashMap.put(key, value);
        }
        return hashMap;
    }
    
    public static List<Object> populateNulls(int number) {
        List<Object> result = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            result.add(null);
        }
        return result;
    }
    
    public static boolean isNull(List<Object> values) throws RsWrongDefException {
        boolean isNull = false;
        boolean isNonNull = false;
        for (Object object : values) {
            if (object == null) {
                isNull |= true;
            } else {
                isNonNull |= true;
            }
        }
        if (isNonNull == isNull) {
            throw new RsWrongDefException("Not all values are null or not all values are nonnull");
        }
        return isNull;
    }
    
    public static List<Object> values(LinkedHashMap<String, Object> map) {
        return new ArrayList<>(map.values());
    }

    public static List<Object> values(Map<String, Object> map, List<String> keyNames) throws RsWrongDefException {
        Map<String, Object> mapCopy = new HashMap<>(map);
        List<Object> values = new ArrayList<Object>();
        for (String keyName : keyNames) {
            Object value = map.get(keyName);
            if (value == null) {
                if (!map.containsKey(keyName)) {
                    throw new RsWrongDefException("Not found key " + keyName + " in " + map);
                }
            }
            values.add(value);
            mapCopy.remove(keyName);
        }
        if (mapCopy.size() > 0) {
            throw new RsWrongDefException("Map " + map + " contains parameter " + mapCopy + " which is doesn't make a key");
        }
        return values;
    }
    
    public static void checkFkToPrim(List<Object> fkKey, Class<?> entityClass, List<String> primKeyNames) throws RsWrongDefException, NullPointerException {
        if (primKeyNames.size() == 0) {
            throw new RsWrongDefException("Entity " + entityClass.getName() + " has no primary key to be joined with fk values " + fkKey);
        }
        if (fkKey == null) {
            throw new NullPointerException("Values which should make foreign key is null and cant' be connected with primary key " + primKeyNames);
        }
        if (fkKey.size() == 0) {
            throw new RsWrongDefException("Trying to join entity with no foreign key to entity " + entityClass.getName() + " which has primary keys " + primKeyNames);
        }
        if (fkKey.size() != primKeyNames.size()) {
            throw new RsWrongDefException("Trying to join entity with foreign key " + fkKey + " to entity " + entityClass.getName() + " which has primary keys " + primKeyNames);
        }
    }


    public static void checkPrimToFk(Class<?> leftClass, List<Object> leftPrimaryKey, Class<?> entityClass,
            List<String> foreignKeyFieldNames) throws RsWrongDefException {
        if (foreignKeyFieldNames.size() == 0) {
            throw new RsWrongDefException("Entity " + entityClass.getName() + " has no foreign key to be joined with primary entity " + leftClass.getName());
        }
        if (leftPrimaryKey == null) {
            throw new NullPointerException("Entity " + leftClass + " has null primary key values and cant' be connected with foreign key entity " + entityClass);
        }
        if (leftPrimaryKey.size() == 0) {
            throw new RsWrongDefException("Trying to join primary entity " + leftClass.getName() + " but it has not primary key to be joined with entity " + entityClass.getName());
        }
        if (leftPrimaryKey.size() != foreignKeyFieldNames.size()) {
            throw new RsWrongDefException("Trying to join entity " + leftClass.getName() + " with primary key " + leftPrimaryKey + " to entity " + entityClass.getName() + " which has foreign key " + foreignKeyFieldNames);
        }
    }

    public static boolean keysSame(List<Object> values1, List<Object> values2) throws RsWrongDefException {
        if (values1 == null || values2 == null) {
            throw new RsWrongDefException("Null value passed");
        }
        if (values1.size() != values2.size()) {
            throw new RsWrongDefException("List " + values1 + " and " + values2 + " have different values");
        }
        for (int i = 0; i < values1.size(); i++) {
            Object object1 = values1.get(i);
            Object object2 = values2.get(i);
            if (!KeyUtil.keysSame(object1, object2)) {
                return false;
            }
        }
        return true;
    }

    public static boolean keysSame(Object key1, Object key2) {
        if (key1 instanceof Long) {
            if (key2 instanceof Long) {
                return key1.equals(key2);
            }
            if (key2 instanceof Integer) {
                long key2Long = ((Integer) key2).longValue();
                return key1.equals(key2Long);
            }
            return key1.equals(key2);
        }
        if (key2 instanceof Long) {
            if (key1 instanceof Integer) {
                long key1Long = ((Integer) key1).longValue();
                return key2.equals(key1Long);
            }
        }
        return key1.equals(key2);
    }

    public static List<String> splitCommaValues(String values) {
        String[] strs = values.split(",");
        List<String> result = new ArrayList<String>();
        for (String str : strs) {
            result.add(str);
        }
        return result;
    }
    
    public static void checkTypesSame(List<Class<?>> classes1, List<Class<?>> classes2) {
        if (classes1.size() != classes2.size()) {
            throw new RuntimeException("Different numbers of classes, " + classes1 + " vs " + classes2);
        }
        for (int i = 0; i < classes1.size(); i++) {
            Class<?> class1 = classes1.get(i);
            Class<?> class2 = classes2.get(i);
            if (!class1.equals(class2)) {
                LOGGER.warn("Types of keys not fit, " + class1 + " vs " + class2);
            }
        }
    }
}
