package cz.jmare.util;

import cz.jmare.rsmapper.exception.RsWrongDefException;

public class ClassUtil {

    public static Class<?> getClassByName(String className) {
        ClassLoader clazz = null;
        try {
            clazz = Thread.currentThread().getContextClassLoader();
        } catch (Throwable ex) {
        }
        if (clazz == null) {
            clazz = SimpleDataSource.class.getClassLoader();
        }
    
        try {
            return Class.forName(className, true, clazz);
        } catch (ClassNotFoundException e) {
            throw new RsWrongDefException("Unable to find class " + className, e);
        }
    }

}
