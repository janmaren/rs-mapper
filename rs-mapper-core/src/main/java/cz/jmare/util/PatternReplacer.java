package cz.jmare.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.util.ReplaceCallback.Callback;

public class PatternReplacer {
    private String regex;
    
    private Pattern pattern;
    
    private String subject;
    
    private int limit = -1;
    
    private int count;
    
    private Callback callback;

    public PatternReplacer(Pattern pattern, String subject, Callback callback) {
        super();
        this.pattern = pattern;
        this.subject = subject;
        this.callback = callback;
    }

    public PatternReplacer(String regex, String subject, Callback callback) {
        super();
        this.regex = regex;
        this.subject = subject;
        this.callback = callback;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String replace() {
        StringBuffer sb = new StringBuffer();
        Pattern usePattern = pattern;
        if (usePattern == null) {
            usePattern = Pattern.compile(regex);
        }
        Matcher matcher = usePattern.matcher(subject);
        int i;
        for (i = 0; (limit < 0 || i < limit) && matcher.find(); i++) {
            String replacement = callback.matchFound(matcher);
            replacement = Matcher.quoteReplacement(replacement); // probably what you want...
            matcher.appendReplacement(sb, replacement);
        }
        matcher.appendTail(sb);

        count = i;
        return sb.toString();
    }

    public int getCount() {
        return count;
    } 
}
