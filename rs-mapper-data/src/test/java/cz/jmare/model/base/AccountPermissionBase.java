package cz.jmare.model.base;

import static cz.jmare.rsmapper.IdType.PASSED;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class AccountPermissionBase {
    @Column(name = "permission_id")
    @Id(type = PASSED)
    protected Integer permissionId;

    @Column(name = "account_id")
    @Id(type = PASSED)
    protected Integer accountId;

    public void setPermissionId(Integer value) {
        this.permissionId = value;
    }

    public Integer getPermissionId() {
        return this.permissionId;
    }

    public void setAccountId(Integer value) {
        this.accountId = value;
    }

    public Integer getAccountId() {
        return this.accountId;
    }

}
