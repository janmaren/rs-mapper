package cz.jmare.model.base;

import static cz.jmare.rsmapper.IdType.IDENTITY;
import cz.jmare.rsmapper.Column;
import cz.jmare.rsmapper.Id;

public class InterestBase {
    @Id(type = IDENTITY)
    protected Integer interestId;

    protected String interestName;

    @Column(name = "person_id")
    protected Integer personId;
    
    public void setPersonId(Integer value) {
        this.personId = value;
    }

    public Integer getPersonId() {
        return this.personId;
    }

    public Integer getInterestId() {
        return interestId;
    }

    public void setInterestId(Integer interestId) {
        this.interestId = interestId;
    }

    public String getInterestName() {
        return interestName;
    }

    public void setInterestName(String interestName) {
        this.interestName = interestName;
    }
}
