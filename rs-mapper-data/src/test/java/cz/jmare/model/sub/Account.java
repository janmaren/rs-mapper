package cz.jmare.model.sub;

import cz.jmare.model.base.AccountBase;
import cz.jmare.rsmapper.Assoc;

public class Account extends AccountBase {
    @Assoc(fkHere = "personId")
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
