package cz.jmare.model.sub;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.model.base.PersonBase;
import cz.jmare.rsmapper.Assoc;

public class Person extends PersonBase {
    @Assoc(fkThere = "personId")
    private List<Interest> interests = new ArrayList<Interest>();

    public List<Interest> getInterests() {
        return interests;
    }

    public void setInterests(List<Interest> interests) {
        this.interests = interests;
    }
}
