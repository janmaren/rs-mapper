package cz.jmare.rsmapper.pagingsorting;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;

import cz.jmare.model.sub.Account;
import cz.jmare.model.sub.Person;
import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.SimpleQuery;
import cz.jmare.rsmapper.ToOnePrim;
import cz.jmare.rsmapper.pagingsorting.PagerSorter.Builder;
import cz.jmare.util.dbpopulator.DBPopulator;

public class PagingTest {
    private static DSConf rsDataSource;

    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        rsDataSource = H2TestUtil.createRSDataSource();
        try (
            Connection connection = rsDataSource.getDataSource().getConnection()){
            DBPopulator.populate(connection,  PagingTest.class.getResource("/database.sql"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        Relation<Person> persRelation = new Relation<Person>(Person.class);
        Person person = new Person();
        person.setPersonName("John1");
        persRelation.save(rsDataSource, person);

        Relation<Account> relation = new Relation<Account>(Account.class).withJoins(new ToOnePrim(Person.class));
        for (int i = 0; i < 21; i++) {
            String index = String.valueOf(i);
            if (i < 10) {
                index = "0" + index;
            }
            Account account = new Account();
            account.setPassword("pass" + index);
            account.setEmail("ema" + index + "@s.cz");
            account.setUsername("user" + index);
            account.setPerson(person);
            relation.save(rsDataSource, account);
        }
    }

    @Test
    public void testSimpleSelectPaging() throws SQLException {
        Relation<Account> relation = new Relation<Account>(Account.class).withAlias("acc");

        Builder<Account> pagerSorterBuilder = new PagerSorter.Builder<Account>();
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(relation).withRemapSortName(Map.of("username", "USERNAME")).withSortingTableAlias("acc");
        
        PagerSorter<Account> pagerSorter = pagerSorterBuilder.build();
   
        Page<Account> selectMany = pagerSorter.selectPage(PageRequest.of(0, 10, Sort.by("username")));
        Assertions.assertEquals(10, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user00", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.selectPage(PageRequest.of(1, 10, Sort.by("username")));
        Assertions.assertEquals(10, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user10", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.selectPage(PageRequest.of(2, 10, Sort.by("username")));
        Assertions.assertEquals(1, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user20", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.selectPage(PageRequest.of(3, 10, Sort.by("username")));
        Assertions.assertEquals(0, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
    }
    
    @Test
    public void testAssocSelectPaging() throws SQLException {
        Relation<Account> relation = new Relation<Account>(Account.class).withAlias("acc").withJoins(new ToOnePrim(Person.class).withAlias("per"));

        Builder<Account> pagerSorterBuilder = PagerSorter.createBuilder(Account.class);
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(relation).withRemapSortName(Map.of("username", "USERNAME", "personName", "person_name")).withSortingTableAlias("acc", "per");
        
        PagerSorter<Account> pagerSorter = pagerSorterBuilder.build();
        SimpleQuery joinQuery = new SimpleQuery("join person per on acc.person_id=per.person_id");
        
        Page<Account> selectMany = pagerSorter.selectPage(PageRequest.of(0, 10, Sort.by("username")), joinQuery);
        Assertions.assertEquals(10, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user00", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.selectPage(PageRequest.of(1, 10, Sort.by("username")), joinQuery);
        Assertions.assertEquals(10, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user10", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.selectPage(PageRequest.of(2, 10, Sort.by("username")), joinQuery);
        Assertions.assertEquals(1, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user20", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.selectPage(PageRequest.of(3, 10, Sort.by("username")), joinQuery);
        Assertions.assertEquals(0, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
    }
    
    @Test
    public void testSimpleLoadPaging() throws SQLException {
        Relation<Account> relation = new Relation<Account>(Account.class).withAlias("acc");

        Builder<Account> pagerSorterBuilder = new PagerSorter.Builder<Account>();
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(relation).withRemapSortName(Map.of("username", "USERNAME")).withSortingTableAlias("acc");
        
        PagerSorter<Account> pagerSorter = pagerSorterBuilder.build();
   
        Page<Account> selectMany = pagerSorter.loadPage(PageRequest.of(0, 10, Sort.by("username")));
        Assertions.assertEquals(10, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user00", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadPage(PageRequest.of(1, 10, Sort.by("username")));
        Assertions.assertEquals(10, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user10", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadPage(PageRequest.of(2, 10, Sort.by("username")));
        Assertions.assertEquals(1, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
        Assertions.assertEquals("user20", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadPage(PageRequest.of(3, 10, Sort.by("username")));
        Assertions.assertEquals(0, selectMany.getNumberOfElements());
        Assertions.assertEquals(21, selectMany.getTotalElements());
    }
    
    @Test
    public void testAssocSelectSlice() throws SQLException {
        Relation<Account> relation = new Relation<Account>(Account.class).withAlias("acc").withJoins(new ToOnePrim(Person.class).withAlias("per"));

        Builder<Account> pagerSorterBuilder = PagerSorter.createBuilder(Account.class);
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(relation).withRemapSortName(Map.of("username", "USERNAME", "personName", "person_name")).withSortingTableAlias("acc", "per");
        
        PagerSorter<Account> pagerSorter = pagerSorterBuilder.build();
        SimpleQuery joinQuery = new SimpleQuery("join person per on acc.person_id=per.person_id");
        
        Slice<Account> selectMany = pagerSorter.selectSlice(PageRequest.of(0, 10, Sort.by("username")), joinQuery);
        Assertions.assertEquals(true, selectMany.hasNext());
        Assertions.assertEquals("user00", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(1, 10, Sort.by("username")));
        Assertions.assertEquals(true, selectMany.hasNext());
        Assertions.assertEquals("user10", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(2, 10, Sort.by("username")));
        Assertions.assertEquals(false, selectMany.hasNext());
        Assertions.assertEquals("user20", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(3, 10, Sort.by("username")));
        Assertions.assertEquals(false, selectMany.hasNext());
    }
    
    @Test
    public void testSimpleSelectSlice() throws SQLException {
        Relation<Account> relation = new Relation<Account>(Account.class).withAlias("acc");

        Builder<Account> pagerSorterBuilder = new PagerSorter.Builder<Account>();
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(relation).withRemapSortName(Map.of("username", "acc.USERNAME"));
        
        PagerSorter<Account> pagerSorter = pagerSorterBuilder.build();
   
        Slice<Account> selectMany = pagerSorter.selectSlice(PageRequest.of(0, 10, Sort.by("username")));
        Assertions.assertEquals(true, selectMany.hasNext());
        Assertions.assertEquals("user00", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(1, 10, Sort.by("username")));
        Assertions.assertEquals(true, selectMany.hasNext());
        Assertions.assertEquals("user10", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(2, 10, Sort.by("username")));
        Assertions.assertEquals(false, selectMany.hasNext());
        Assertions.assertEquals("user20", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(3, 10, Sort.by("username")));
        Assertions.assertEquals(false, selectMany.hasNext());
    }
    
    @Test
    public void testSimpleLoadSlice() throws SQLException {
        Relation<Account> relation = new Relation<Account>(Account.class).withAlias("acc");

        Builder<Account> pagerSorterBuilder = new PagerSorter.Builder<Account>();
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(relation).withRemapSortName(Map.of("username", "acc.USERNAME"));
        
        PagerSorter<Account> pagerSorter = pagerSorterBuilder.build();
   
        Slice<Account> selectMany = pagerSorter.loadSlice(PageRequest.of(0, 10, Sort.by("username")));
        Assertions.assertEquals(true, selectMany.hasNext());
        Assertions.assertEquals("user00", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(1, 10, Sort.by("username")));
        Assertions.assertEquals(true, selectMany.hasNext());
        Assertions.assertEquals("user10", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(2, 10, Sort.by("username")));
        Assertions.assertEquals(false, selectMany.hasNext());
        Assertions.assertEquals("user20", selectMany.getContent().get(0).getUsername());
        
        selectMany = pagerSorter.loadSlice(PageRequest.of(3, 10, Sort.by("username")));
        Assertions.assertEquals(false, selectMany.hasNext());
    }
}
