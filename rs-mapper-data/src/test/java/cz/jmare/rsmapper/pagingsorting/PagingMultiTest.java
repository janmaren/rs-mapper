package cz.jmare.rsmapper.pagingsorting;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cz.jmare.model.sub.Interest;
import cz.jmare.model.sub.Person;
import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.RsCommand;
import cz.jmare.rsmapper.ToMany;
import cz.jmare.rsmapper.pagingsorting.PagerSorter.Builder;
import cz.jmare.util.dbpopulator.DBPopulator;

public class PagingMultiTest {
    private static DSConf rsDataSource;

    @BeforeAll
    public static void prepare() throws ClassNotFoundException, SQLException {
        rsDataSource = H2TestUtil.createRSDataSource();
        try (
            Connection connection = rsDataSource.getDataSource().getConnection()){
            DBPopulator.populate(connection,  PagingMultiTest.class.getResource("/database.sql"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        Relation<Person> persRelation = new Relation<Person>(Person.class).withJoins(new ToMany(Interest.class));
        for (int i = 0; i < 11; i++) {
            Person person = new Person();
            person.setPersonName("John" + (i < 10 ? "0" : "") + i);

            Interest interest1 = new Interest();
            interest1.setInterestName("interest 1");
            
            Interest interest2 = new Interest();
            interest2.setInterestName("interest 2");
            
            person.getInterests().add(interest1);
            person.getInterests().add(interest2);
            persRelation.save(rsDataSource, person);
        }
    }

    @Test
    public void testAssocSelectPaging() throws SQLException {
        Relation<Person> persRelation = new Relation<Person>(Person.class).withAlias("p").withJoins(new ToMany(Interest.class).withAlias("i"));

        Builder<Person> pagerSorterBuilder = PagerSorter.createBuilder(Person.class);
        pagerSorterBuilder.withDsConf(rsDataSource).withRelation(persRelation).withRemapSortName(Map.of("personName", "p.person_name"));
        
        String sql = "select p.person_id person_id_p, p.person_name person_name_p,i.interest_id interest_id_i, i.interest_name interest_name_i, i.person_id person_id_i from (select * from person p order by p.person_name ASC limit :pagingLimit offset :pagingOffset) p join interest i on p.person_id=i.person_id";
        
        List<LinkedHashMap<String, Object>> selectMultipleVectors = RsCommand.selectMultipleVectors(rsDataSource, sql, Map.of("pagingLimit", 2, "pagingOffset", 3));
        System.out.println(selectMultipleVectors);
    }
}
