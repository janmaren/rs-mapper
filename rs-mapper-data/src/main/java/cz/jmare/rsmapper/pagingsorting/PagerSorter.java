package cz.jmare.rsmapper.pagingsorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.SliceImpl;
import org.springframework.data.domain.Sort.Order;

import cz.jmare.rsmapper.DSConf;
import cz.jmare.rsmapper.Query;
import cz.jmare.rsmapper.Relation;
import cz.jmare.rsmapper.SimpleQuery;
import cz.jmare.rsmapper.exception.RsWrongDefException;
import cz.jmare.rsmapper.handle.Handle;

/**
 * Main class for paging, sorting which can comprise also filtering. It also calculates number
 * of entities when needed which can be an additional select count
 * operation when unable to determine count from returned data.<br>
 * Supported 2 types of response: Slice and Page. The Slice is more efficient because
 * it doesn't return whole number of items but rather only information whether there
 * is next record behind returned page. It never selects count but rather it selects
 * one more record which will not be comprised in response.<br>
 * Returning a Page is including selecting count which can be sometimes omitted.
 * Note it uses a concrete sorting implementation for given DBMS. Concrete sorting implementation is
 * obtained by {@link cz.nn.nnmapper.dbprofile.DBProfile#createPagingAndSortingBuilder()} - this class
 * must have appropriate sorting implementation
 */
public class PagerSorter<T> {
    private DSConf dsConf;
    private Relation<T> relation;
    private Map<String, String> remapSortName;
    private List<String> sortingTableAlias;

    private PagerSorter() {
    }

    public static <T> Builder<T> createBuilder(@SuppressWarnings("unused") Class<T> clazz) {
        return new Builder<T>();
    }
    
    public static class Builder<T> {
        private DSConf dsConf;
        private Relation<T> relation;
        private Map<String, String> remapSortName;
        private List<String> sortingTableAlias;
        
        /**
         * DSConf. It may be omitted but the present Handle must be passed as argument during the select operation
         * @param dsConf
         * @return
         */
        public Builder<T> withDsConf(DSConf dsConf) {
            this.dsConf = dsConf;
            return this;
        }
        
        /**
         * Relation for which the select will be launched
         * @param relation
         * @return
         */
        public Builder<T> withRelation(Relation<T> relation) {
            this.relation = relation;
            return this;
        }

        /**
         * Some human readable sorting keys or constants to convert to column names.
         * Value may contain also a table alias. Format: &lt;COLUMN_NAME&gt; or &lt;TABLE_ALIAS&gt;.&lt;COLUMN_NAME&gt;
         * When table alias omitted the resolved ordering will be determined by withSortingTableAlias
         * @param remapSortName
         * @return
         */
        public Builder<T> withRemapSortName(Map<String, String> remapSortName) {
            this.remapSortName = remapSortName;
            return this;
        }
        
        /**
         * When column names already present in Pageable, limit supported columns and
         * don't allow to enter some malicious strings 
         * @param supportedColumns
         * @return
         */
        public Builder<T> withRemapSortName(List<String> supportedColumns) {
            this.remapSortName = new HashMap<String, String>();
            for (String supportedColumn : supportedColumns) {
                this.remapSortName.put(supportedColumn, supportedColumn);
            }
            return this;
        }
        
        /**
         * Name of table aliases which will be used in order by clause before sorting column.
         * When more sorting columns used than size of sortingTableAlias the last table
         * alias will be used for rest of all ordering columns. Default is '_1'
         * @param sortingTableAlias
         * @return
         */
        public Builder<T> withSortingTableAlias(List<String> sortingTableAlias) {
            this.sortingTableAlias = sortingTableAlias;
            return this;
        }

        /**
         * Name of table alias which will be used in order by clause before sorting column.
         * When more sorting columns used than 1 they all will share this alias. Default is '_1'
         * @param alias
         * @return
         */
        public Builder<T> withSortingTableAlias(String alias) {
            this.sortingTableAlias = new ArrayList<String>();
            this.sortingTableAlias.add(alias);
            return this;
        }
        
        /**
         * Name of table aliases which will be used in order by clause before sorting column.
         * When more sorting columns used than size of sortingTableAlias the last table
         * alias will be used for rest of all ordering columns. Default is '_1'
         * @param alias
         * @return
         */
        public Builder<T> withSortingTableAlias(String... alias) {
            this.sortingTableAlias = Arrays.asList(alias);
            return this;
        }
                
        public PagerSorter<T> build() {
            PagerSorter<T> pagerSorter = new PagerSorter<T>();
            pagerSorter.dsConf = this.dsConf;
            if (this.relation == null) {
                throw new IllegalStateException("Relation is a mandatory parameter and it must be passed to builder");
            }
            pagerSorter.relation = this.relation;
            pagerSorter.remapSortName = this.remapSortName;
            pagerSorter.sortingTableAlias = this.sortingTableAlias;
            return pagerSorter;
        }
    }

    public Page<T> selectPage(Pageable pageable) {
        return selectPage(pageable, new SimpleQuery());
    }
    
    public Page<T> selectPage(Pageable pageable, Query query) {
        if (dsConf == null) {
            throw new RsWrongDefException("DSConf must be populated for paging");
        }
        try (Handle handle = dsConf.createHandle()) {
            return selectPage(dsConf.createHandle(), pageable, query);
        }
    }
    
    public Page<T> selectPage(Handle handle, Pageable pageable) {
        return selectPage(handle, pageable, new SimpleQuery());
    }
    
    public Page<T> selectPage(Handle handle, Pageable pageable, Query query) {
        if (pageable == null) {
            throw new IllegalArgumentException("Pageable argument not populated");
        }
        Query pagingQuery = createWrappingQuery(pageable, false, query);
        List<T> entities = relation.selectMany(handle, pagingQuery);
        return calculateCountAndMakePage(pageable, query, entities);
    }

    public Page<T> selectPage(Handle handle, PageRequest pageable, Query joinQuery, Query limitQuery) {
        return selectPage(handle, pageable, joinQuery, limitQuery);
    }
    
    public Page<T> loadPage(Pageable pageable) {
        try (Handle handle = dsConf.createHandle()) {
            return loadPage(dsConf.createHandle(), pageable, new SimpleQuery());
        }
    }
    
    /**
     * Load a page with info about count.
     * @param handle
     * @param pageable
     * @return
     */
    public Page<T> loadPage(Handle handle, Pageable pageable) {
        return loadPage(handle, pageable, new SimpleQuery());
    }
    
    /**
     * Load a page with filtering query applied and return info about count.
     * @param pageable
     * @param query
     * @return
     */
    public Page<T> loadPage(Pageable pageable, Query query) {
        try (Handle handle = dsConf.createHandle()) {
            return loadPage(dsConf.createHandle(), pageable, query);
        }
    }
    
    /**
     * Load a page with filtering query applied and return info about count.
     * @param handle
     * @param pageable
     * @param query
     * @return
     */
    public Page<T> loadPage(Handle handle, Pageable pageable, Query query) {
        if (pageable == null) {
            throw new IllegalArgumentException("Pageable argument not populated");
        }
        Query pagingQuery = createWrappingQuery(pageable, false, query);
        List<T> entities = relation.loadMany(handle, pagingQuery);
        return calculateCountAndMakePage(pageable, query, entities);
    }

    /**
     * Load a slice with info whether next item exists.
     * @param pageable
     * @return
     */
    public Slice<T> loadSlice(Pageable pageable) {
        return loadSlice(pageable, new SimpleQuery());
    }
    
    /**
     * Load a slice using filtering query with info whether next item exists.
     * @param pageable
     * @param query
     * @return
     */
    public Slice<T> loadSlice(Pageable pageable, Query query) {
        if (dsConf == null) {
            throw new RsWrongDefException("DSConf must be populated for paging");
        }
        try (Handle handle = dsConf.createHandle()) {
            return loadSlice(dsConf.createHandle(), pageable, query);
        }
    }
    
    /**
     * Load a slice with info whether next item exists.
     * @param handle
     * @param pageable
     * @return
     */
    public Slice<T> loadSlice(Handle handle, Pageable pageable) {
        return loadSlice(handle, pageable, new SimpleQuery());
    }
    
    /**
     * Load a slice with info whether next item exists.
     * @param handle
     * @param pageable
     * @param query
     * @return
     */
    public Slice<T> loadSlice(Handle handle, Pageable pageable, Query query) {
        if (pageable == null) {
            throw new IllegalArgumentException("Pageable argument not populated");
        }
        Query pagingQuery = createWrappingQuery(pageable, true, query);
        List<T> entities = relation.loadMany(handle, pagingQuery);
        return calculateNextAndMakeSlice(pageable, entities);
    }
    
    /**
     * Select a slice with info whether next item exists.
     * @param pageable
     * @return
     */
    public Slice<T> selectSlice(Pageable pageable) {
        return selectSlice(pageable, new SimpleQuery());
    }
    
    /**
     * Select a slice with info whether next item exists.
     * @param handle
     * @param pageable
     * @return
     */
    public Slice<T> selectSlice(Handle handle, Pageable pageable) {
        return selectSlice(handle, pageable, new SimpleQuery());
    }
    
    /**
     * Select a slice using a query with info whether next item exists.
     * @param pageable
     * @param query
     * @return
     */
    public Slice<T> selectSlice(Pageable pageable, Query query) {
        if (dsConf == null) {
            throw new RsWrongDefException("DSConf must be populated for paging");
        }
        try (Handle handle = dsConf.createHandle()) {
            return selectSlice(dsConf.createHandle(), pageable, query);
        }
    }
    
    /**
     * Select a slice using a filtering query with info whether next item exists.
     * @param handle
     * @param pageable
     * @param query
     * @return
     */
    public Slice<T> selectSlice(Handle handle, Pageable pageable, Query query) {
        if (pageable == null) {
            throw new IllegalArgumentException("Pageable argument not populated");
        }
        Query pagingQuery = createWrappingQuery(pageable, true, query);
        List<T> entities = relation.selectMany(handle, pagingQuery);
        return calculateNextAndMakeSlice(pageable, entities);
    }
    
    private Query createWrappingQuery(Pageable pageable, boolean oneMoreEntity, Query query) {
        PagingAndSortingBuilder builder = dsConf.getDbProfile().createPagingAndSortingBuilder();
        if (pageable.getSort().isSorted()) {
            List<Order> order = pageable.getSort().toList();
            StringJoiner stringJoiner = new StringJoiner(", ");
            String alias = "_1";
            for (int i = 0; i < order.size(); i++) {
                if (sortingTableAlias != null && i < sortingTableAlias.size()) {
                    alias = sortingTableAlias.get(i);
                }
                Order o = order.get(i);
                stringJoiner.add(remapSortName(o.getProperty(), alias) + " " + o.getDirection());
            }
            builder.setOrderColumns(stringJoiner.toString());
        }
        if (pageable.isPaged()) {
            builder.setPageSize(pageable.getPageSize());
            builder.setPageNumber(pageable.getPageNumber());
            if (oneMoreEntity && pageable.getPageSize() != Integer.MAX_VALUE) {
                builder.setOneMoreEntity(true);
            }
        }
        if (query != null) {
            builder.setQuery(query);
        }
        Query pagingQuery = builder.build();
        return pagingQuery;
    }

    private Page<T> calculateCountAndMakePage(Pageable pageable, Query query, List<T> entities) {
        long total = 0;
        if (entities.size() < pageable.getPageSize()) {
            if (entities.size() > 0) {
                total = pageable.getPageNumber() * pageable.getPageSize() + entities.size();
            } else {
                if (pageable.getPageNumber() > 0) {
                    total = relation.selectCount(dsConf, query);
                }
            }
        } else {
            total = relation.selectCount(dsConf, query);
        }

        return new PageImpl<>(entities, pageable, total);
    }
    
    private Slice<T> calculateNextAndMakeSlice(Pageable pageable, List<T> entities) {
        if (entities.size() > pageable.getPageSize()) {
            entities.remove(entities.size() - 1);
            return new SliceImpl<>(entities, pageable, true);
        } else {
            return new SliceImpl<>(entities, pageable, false); 
        }
    }
    
    private String remapSortName(String name, String currentAlias) {
        if (remapSortName == null) {
            throw new IllegalArgumentException("remapSortName no populated - it must be populated to filter out malicious strings");
        }
        String newName = remapSortName.get(name);
        if (newName == null) {
            throw new IllegalArgumentException("Sorting column " + name + " not supported");
        }
        if (!newName.contains(".")) {
            newName = currentAlias + "." + newName;
        }
        return newName;
    }
}
